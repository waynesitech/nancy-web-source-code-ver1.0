<?php

return array(

    'appNameIOS'     => array(
//         'environment' => 'development',
        'environment' => 'production',
        // 'certificate' =>'/ck.pem',
        'certificate' => app_path() . '/../pro.pem',
        'passPhrase'  => '1234',
        'service'     => 'apns'
    ),
    'appNameAndroid' => array(
        'environment' => 'production',
        'apiKey'      => 'AIzaSyCyu9BcGjmx9lRi0uEHhk9WQ8KjJf8oJ5g',
        'service'     => 'gcm'
    )

);