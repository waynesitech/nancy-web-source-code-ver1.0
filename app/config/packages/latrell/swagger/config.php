<?php
return array(
    'enable' => true,

    'prefix' => 'api-docs',

    'paths' => 'app',
    'output' => 'docs',
    'exclude' => null,
    'default-base-path' => 'https://nancy.com.my/api',
    'default-api-version' => null,
    'default-swagger-version' => null,
    'api-doc-template' => null,
    'suffix' => '.{format}',

    'title' => 'Swagger UI'
);
