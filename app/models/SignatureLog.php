<?php

class SignatureLog extends BaseModel {

    protected $table = 'signature_log';

    public function order()
    {
		return $this->belongsTo('Order', 'order_id');
    }

    public function scopeGetLogByOrderId($query, $id = '')
    {
    	if (!empty($id)) {
    		$query
    		->where('order_id', $id)
    		;
    	}

		return $query;
	}

}