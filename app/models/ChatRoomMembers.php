<?php

class ChatRoomMembers extends BaseModel{

	protected $table = 'chat_room_members';

        public function member()
	{
		return $this->belongsTo('User', 'user_id');
	}

        public function room()
	{
		return $this->belongsTo('ChatRooms', 'chat_rooms_id');
	}
        
	public function user() 
	{
		return $this->belongsTo('User', 'created_by');
	}
}