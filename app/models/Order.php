<?php

class Order extends BaseModel {

	protected $table = 'orders';

	public function images(){
		return $this->hasMany('OrderImage','order_id');
	}

	public function statuses(){
		return $this->hasMany('OrderStatus','order_id')->where('type','order') ;
	}

	public function stampingStatuses(){
		return $this->hasMany('OrderStatus','order_id')->where('type','estamping') ;
	}

	public function logs(){
		return $this->hasMany('SignatureLog','order_id');
	}

	public function addOn(){
		return $this->belongsToMany('AddOn', 'order_addon', 'order_id', 'addon_id');
	}

	public function users(){
		return $this->belongsTo('User', 'created_by');
	}
        
        public function tenant(){
		return $this->hasMany('OrderUsers','order_id')->where('role','Tenant');
	}
        
        public function landlord(){
		return $this->hasMany('OrderUsers','order_id')->where('role','Landlord');
	}

	public function paymentRecord(){
		return $this->belongsTo('Payment', 'estamping_payment_record');
	}

	public function scopeGetActive($query){
		return $query
		->where('status', '=', 't')
		;
	}		

	public function scopeGetRowById($query, $id){
		return $query
		->where('id', '=', $id);
	}

	public function scopeGetList($query, $userID){
		return $query
		->where('orders.created_by', '=', $userID);
	}

	public function scopeGetAdminList($query){
		return $query
		->where('orders.tenancyAgreementStatus', '=', 'Received')
		->orderBy('orders.created_on', 'DESC');
	}

	public function scopeGetOrderByDay($query, $startdate, $enddate){
		return $query
		->select(DB::raw('count(orders.id) as order_count, orders.*, date(orders.created_on) as created_date'))
		->whereBetween(DB::raw('date(orders.created_on)'), array($startdate, $enddate))
		->groupBy('created_date');
	}

	public function scopeGetByKeyword($query, $keyword = '', $agency = ''){
		$query->select(DB::raw('orders.*, tenant.name AS tenant, landlord.name AS landlord'));

		if (!empty($keyword)){
			$query->where(function($query) use ($keyword){       		    
				$query
				->orWhere('tenant.name', 'LIKE', '%' . $keyword . '%')
				->orWhere('landlord.name', 'LIKE', '%' .$keyword .'%')
				->orWhere('order_num', 'LIKE', '%' .$keyword .'%');
			});
		}

		return $query
		->leftJoin(DB::raw("(SELECT order_id, GROUP_CONCAT(name SEPARATOR ', ') AS name FROM order_users WHERE role = 'Tenant' GROUP BY order_id) AS tenant") , 'orders.id', '=', 'tenant.order_id')
                ->leftJoin(DB::raw("(SELECT order_id, GROUP_CONCAT(name SEPARATOR ', ') AS name FROM order_users WHERE role = 'Landlord' GROUP BY order_id) AS landlord") , 'orders.id', '=', 'landlord.order_id');
	}
}