<?php

class Payment extends BaseModel{

	protected $table = 'payment';

    public function user() 
    {
        return $this->belongsTo('User', 'created_by');
    }

    public function batch() 
    {
        return $this->belongsTo('Batch');
    }

    public function package() 
    {
        return $this->belongsTo('Package');
    }

	public function scopeGetByRef($query, $ref)
	{
		return $query
		->where('ref', '=', $ref)
		;
	}    

	public function scopeGetByUserId($query, $user_id)
	{
		return $query
		->where('created_by', '=', $user_id)
		;
	}    
	
	public function scopeGetByToken($query, $token)
	{
		return $query
		->where('ref', '=', $token)
		;
	}  
        
        public function scopeGetByKeyword($query, $keyword = '', $agency = ''){
		$query->select(DB::raw('payment.*, orders.order_num AS order_num, user.name AS user_name'));

		if (!empty($keyword)){
			$query->where(function($query) use ($keyword){       		    
				$query
				->orWhere('payment.ref', 'LIKE', '%' . $keyword . '%')
				->orWhere('payment.user_email', 'LIKE', '%' .$keyword .'%')
				->orWhere('payment.user_contact', 'LIKE', '%' .$keyword .'%')
                                ->orWhere('payment.ipay88_trans_id', 'LIKE', '%' .$keyword .'%')
                                ->orWhere('orders.order_num', 'LIKE', '%' .$keyword .'%')
                                ->orWhere('user.name', 'LIKE', '%' .$keyword .'%');
			});
		}

		return $query
		->leftJoin('orders', 'orders.id', '=', 'payment.order_id')
                ->leftJoin('user', 'user.id', '=', 'payment.user_id');
	}
}
