<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;


class User extends BaseModel implements UserInterface {

    use UserTrait;

    protected $table = 'user';

    protected $hidden = array('password');

    public function agency() {
        return $this->belongsTo('Agency','agency_id');
    }

    public function userGender() {
        return $this->belongsTo('UserGender','gender_id');
    }
    public function role() {
        return $this->belongsTo('UserRole','role_id');
    }
    public function country() {
        return $this->belongsTo('Country', 'country_id');
    }
	
    public function scopeGetByKeyword($query, $keyword = '',$currentID=''){

        if (!empty($keyword)) {
                $query
                ->where('user.id', '<>', $currentID)
                ->where(function($query) use ($keyword){       		    
                        $query
                        ->orWhere('user.email', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('user.first_name', 'LIKE', '%' . $keyword .'%')
                        ->orWhere('user.last_name', 'LIKE', '%' .$keyword .'%')
                        ->orWhereRaw("CONCAT(user.first_name, ' ', user.last_name) LIKE '%$keyword%'");
                });
        }


        return $query
        ->select(array(
                'user.*', 'agency.name as agency'))
        ->leftjoin('agency', 'user.agency_id', '=', 'agency.id')
        ->where('user.status', '<>', -1);
    }

    public function scopeGetByKeywordAndRole($query, $keyword = '',$role, $currentID = ''){
            $query
            ->where('id', '<>', $currentID);

            if (!empty($keyword)) {
                    $query->where(function($query) use ($keyword){       		    
                            $query
                            ->orWhere('email', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('first_name', 'LIKE', '%' . $keyword .'%')
                            ->orWhere('last_name', 'LIKE', '%' .$keyword .'%')
                            ->orWhereRaw("CONCAT(first_name, ' ', last_name) LIKE '%$keyword%'");
                    });
            }
            if($role != 'all') {
                    $query->where('role_id', '=', $role);
            }

            return 
            $query->where('status', '<>', -1);
    }

    public function scopeGetById($query, $id = 0){

            return $query
            ->where('id', '=', $id)
            ->where('status', '!=', -1);

    }

    public function scopeGetByEmail($query, $email){
            return $query
            ->where('email', $email);
    }

    public function scopeGetByEmailAndID($query, $email,$id){
            return $query
            ->where('email', $email)
            ->where('id', $id)
            ->where('status', '=', 1);
    }

    public function scopeGetByEmailJoin($query, $email){
            return $query
            ->where('user.email', $email)
            ->where('user.status', '=', 1)
            ->leftJoin('user_role', 'user.role_id', '=', 'user_role.id');
    }

    public function scopeGetByHash($query, $hash){
            return $query
            ->where('hash', $hash)
            ->where('status', '=', 0);
    }    

    public function scopeGetByDeviceToken($query, $token = ''){
            return $query
            ->where('ios', $token)
            ->orWhere('android', $token);
    }    

    public function scopeGetByEmailWithExcludeId($query, $ue, $id = 0){
            return $query
            ->where('email', $ue)
            ->where('id', '<>', $id)
            ->where('status', '<>' , -1);
    }
    
    public function scopeGetByEmailExcludeStatus($query, $email){
        return $query
        ->where('email', $email);
    }

    public function getStatus(){
        if ($this->status == 0) return 'Inactive';
        if ($this->status == 1) return 'Active';
        return '';
    }

    public function scopeGetPostRptByKeyword($query, $keyword = '',$currentID=''){

        if (!empty($keyword)){
                $query
                ->where('user.id', '<>', $currentID)
                ->where(function($query) use ($keyword){       		    
                        $query
                        ->orWhere('user.email', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('user.first_name', 'LIKE', '%' . $keyword .'%')
                        ->orWhere('user.last_name', 'LIKE', '%' .$keyword .'%')
                                ->orWhereRaw("CONCAT(user.first_name, ' ', user.last_name) LIKE '%$keyword%'");
                        });
        }

        return $query
        ->select(array(
                'user.*',
                DB::raw('COUNT(*) as prop_count')
                ))
        ->leftJoin('property', 'property.created_by', '=', 'user.id')
        ->groupBy('user.id')
        ->where('user.status', '<>', -1)
        ->where('property.status', '=', 1);
    }

    public function scopeGetUserByDay($query, $startdate, $enddate){
            return $query
            ->select(DB::raw('count(user.id) as user_count, user.*, date(user.created_on) as created_date'))
            ->whereBetween(DB::raw('date(user.created_on)'), array($startdate, $enddate))
            ->groupBy(DB::raw('date(user.created_on)'));
    }
}
