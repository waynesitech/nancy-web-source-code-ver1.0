<?php

class Banner extends BaseModel{

	protected $table = 'banner';

	public function scopeGetRowById($query, $id){
		return $query
		->where('id', '=', $id)
		->where('status', '!=', -1)
		;
	}

	public function scopeGetByKeyword($query, $keyword = ''){

		$query
		->select(
			'banner.*'
			)
		->where('status', '!=', -1)
		;

		if ($keyword){

			$query
			->where(function($query) use ($keyword){
				$query
				->where('banner.name', 'LIKE', '%' . $keyword . '%')
				->orWhere('banner.type', 'LIKE', '%' . $keyword . '%')
				;
			})
			;
		}

		return $query;
	}

	public function scopeGetBannersByTypeId($query, $type_id){
		
		return $query
		->where('type', '=', $type_id)
		->where('status', '=', 1)
		;
	}

	public function scopeGetBannersByType($query, $type){
		
		return $query
			->where('type', '=', $type)
			->where('status', '=', 1)
		;
	}	


}
