<?php

class OrderSignature extends BaseModel {

    protected $table = 'order_signature';

    public function order()
    {
		return $this->belongsTo('Order', 'order_id');
    }

    public function scopeGetSignatureByOrderId($query, $id = '')
    {
    	if (!empty($id)) {
    		$query
    		->where('order_id', $id)
    		;
    	}

		return $query;
	}

}