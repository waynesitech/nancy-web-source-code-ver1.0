

<?php

class TimelineImages extends BaseModel {

    protected $table = 'timeline_images';

    public function timeline()
    {
		return $this->belongsTo('Timeline', 'id');
	}

    public function scopeGetImagesByTimelineId($query, $id = '')
    {
    	if (!empty($id)) {
    		$query
    		->where('id', $id)
    		;
    	}

		return $query;
	}

}