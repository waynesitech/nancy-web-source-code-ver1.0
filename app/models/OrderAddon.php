<?php

class OrderAddon extends BaseModel {

    protected $table = 'order_addon';

    public function order()
    {
		return $this->belongsTo('Order', 'order_id');
    }

    public function scopeGetAddOnByOrderId($query, $id = '')
    {
    	if (!empty($id)) {
    		$query
    		->where('order_id', $id)
    		;
    	}

		return $query;
	}

}