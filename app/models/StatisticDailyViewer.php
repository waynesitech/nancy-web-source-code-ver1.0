<?php

 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class StatisticDailyViewer extends Eloquent {
 
    
    protected $table = 'statistic_dailyViewer';
    
    
    public function user() 
	{
		return $this->belongsTo('User', 'user_id');
	}
        
        
    public function property() 
	{
		return $this->belongsTo('Property', 'property_id');
	}
        
    public function calendar() 
	{
		return $this->belongsTo('StatisticCalendar', 'statistic_calendar_id');
	}
        
        
            public function scopeGetDailyViewersStatistic($query,$property_id,$from,$to)
	{
                
                $result = DB::select( DB::raw("select statictic_calendar.date as title,COUNT(tbl1.id) as prop_count ".
                                "from   `statictic_calendar` ".
                                "left join (select * ".
                                "from   `statistic_dailyViewer` ".
                                "WHERE ". 
                                "(property_id=:propertyID OR property_id IS NULL)) ".
                                "As tbl1 ".
                                "ON tbl1.statistic_calendar_id= statictic_calendar.id ".
                                "WHERE ". 
                                "date BETWEEN  :from ".
                                "AND  :to ".
                                "group by statictic_calendar.id ".
                                "ORDER BY `statictic_calendar`.`date`  ASC"),
                        array(
                           'propertyID' => $property_id,
                           'from'=>$from,
                           'to'=>$to
                         ));
           
                return $result;
	}
	
	public function scopeGetTotalVisitorStatistic($query, $uid = 0, $from,$to)
    {
		return $query
		->select(array('statistic_dailyViewer.*', DB::raw('count(DATE(statistic_dailyViewer.created_at)) as count'), DB::raw('DATE(statistic_dailyViewer.created_at) created_date')))
		->leftJoin('property', 'statistic_dailyViewer.property_id', '=', 'property.id')
		->leftJoin('user', 'property.created_by', '=', 'user.id')
		->where('user.id', '=', $uid)
		->where('statistic_dailyViewer.action', '=', 'view')
		->whereBetween(DB::raw('DATE(statistic_dailyViewer.created_at)'), array($from, $to))
		->groupBy('created_date')
		;
    }
}