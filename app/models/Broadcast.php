<?php

class Broadcast extends BaseModel {

	protected $table = 'broadcast';

	public function scopeGetRowById($query, $id)
	{
		return $query
            ->where('id', '=', $id)
            ->where('status', '=', 1)
		;
	}
	
	public function scopeGetByKeyword($query, $keyword = ''){

		$query
		->select(
			'broadcast.*'
			)
		->where('status', '=', 1)
		;

		if ($keyword){

			$query
			->where(function($query) use ($keyword){
				$query
				->where('broadcast.title', 'LIKE', '%' . $keyword . '%')
				->orWhere('broadcast.content', 'LIKE', '%' . $keyword . '%')
				;
			})
			;
		}

		return $query;
	}
}
