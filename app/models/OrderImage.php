<?php

class OrderImage extends BaseModel {

    protected $table = 'order_image';

    public function property()
    {
		return $this->belongsTo('Order', 'order_id');
    }

    public function scopeGetImagesByOrderId($query, $id = '')
    {
    	if (!empty($id)) {
    		$query
    		->where('order_id', $id)
    		;
    	}

		return $query;
	}

}