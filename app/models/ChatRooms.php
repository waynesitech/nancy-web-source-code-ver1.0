<?php

class ChatRooms extends BaseModel{

	protected $table = 'chat_rooms';


        public function members()
	{
		return $this->hasMany('ChatRoomMembers');
	}
        
        public function allMembers()
	{
		return $this->hasMany('ChatRoomMembers');
	}
        
	public function user() 
	{
		return $this->belongsTo('User', 'created_by');
	}
        
        
        public function scopeApiGetRoomByUserIDs($query,$user_id1,$user_id2) {

			
                return $query
                        ->select("chat_rooms.*",DB::raw(' GROUP_CONCAT( DISTINCT chat_room_members.user_id ) AS members'))
                        ->join('chat_room_members', 'chat_rooms.id', '=', 'chat_room_members.chat_rooms_id')
                        ->where('chat_rooms.type','personal')
                        ->groupBy('chat_rooms.id')
                        ->havingRaw("FIND_IN_SET('$user_id1', members ) >0")
                        ->havingRaw("FIND_IN_SET('$user_id2', members ) >0");
	}
}

