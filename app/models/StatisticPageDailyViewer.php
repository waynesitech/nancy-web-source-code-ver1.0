<?php

 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class StatisticPageDailyViewer extends Eloquent {
 
    
    protected $table = 'statistic_page_dailyViewer';
    
    
    public function user() 
	{
		return $this->belongsTo('User', 'user_id');
	}
        
        
    public function advertiser() 
	{
		return $this->belongsTo('User', 'advertiser');
	}
        
    public function calendar() 
	{
		return $this->belongsTo('StatisticCalendar', 'statistic_calendar_id');
	}
        
        
            public function scopeGetDailyViewersStatistic($query,$advertiser_id,$from,$to)
	{
                
                $result = DB::select( DB::raw("select statictic_calendar.date as title,COUNT(tbl1.id) as prop_count ".
                                "from   `statictic_calendar` ".
                                "left join (select * ".
                                "from   `statistic_page_dailyViewer` ".
                                "WHERE ". 
                                "(advertiser=:advertiser OR advertiser IS NULL)) ".
                                "As tbl1 ".
                                "ON tbl1.statistic_calendar_id= statictic_calendar.id ".
                                "WHERE ". 
                                "date BETWEEN  :from ".
                                "AND  :to ".
                                "group by statictic_calendar.id ".
                                "ORDER BY `statictic_calendar`.`date`  ASC"),
                        array(
                           'advertiser' => $advertiser_id,
                           'from'=>$from,
                           'to'=>$to
                         ));
           
                return $result;
	}
}