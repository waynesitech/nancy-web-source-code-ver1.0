<?php

class Country extends BaseModel {

	protected $table = 'country';

	public function country(){
		return $this->hasMany('State');
	}

	public function parent(){
		return $this->belongsTo('Country', 'country_id');
	}

	public function scopeGetActive($query)
	{
		return $query
		->where('status', '=', 't')
		;
	}		
	
	public function scopeGetRowById($query, $id){

		return $query
		->where('id', '=', $id)
		;
	}

	public function scopeGetList($query, $keyword = ''){
		
		if($keyword){
			$query
			->where('country.country_name', 'LIKE', '%' . $keyword . '%')
			;
		}

		return $query
		->where('status', '=', 't')
		->orderBy('country.country_name', 'ASC')
		;
	}
}