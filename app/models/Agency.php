<?php

class Agency extends BaseModel {

	protected $table = 'agency';

    public function scopeGetById($query, $id)
    {
        $query
            ->where('id', '=', $id)
            ->where('status', '=', 1)
        ;

        return $query;
    }    

    public function scopeGetActive($query)
    {
        return $query
                ->where('status', '=', 1)
        ;
    }
    
    public function scopeGetAll($query)
    {
        return $query
        ;
    }
    
    public function scopeGetAllById($query, $id)
    {
        $query
            ->where('id', '=', $id)
        ;

        return $query;
    }
    
    public function scopeGetByKeyword($query, $keyword = '')
    {
        if (!empty($keyword)){
            $query
                ->where(function($query) use ($keyword){                
                    $query
                        ->orWhere('agency.name', 'LIKE', '%' . $keyword . '%')
                    ;
                })
            ;
        }

        return $query
        ;
    }
    
    public function scopeGetByKeyword2($query, $keyword = '')
    {
        $query
            ->select(array(
                    'agency.name'
                    , 'agency.status'
                    , 'agency.created_on'
        ));
        if (!empty($keyword)){
            $query
                ->where(function($query) use ($keyword){                
                    $query
                        ->orWhere('agency.name', 'LIKE', '%' . $keyword . '%')
                    ;
                })
            ;
        }

        return $query
        ;
    }
    
    public function getStatus()
    {
        if ($this->status == 0) return 'Inactive';
        if ($this->status == 1) return 'Active';
        return '';
    }
}
