<?php

class Package extends BaseModel {

	protected $table = 'package';

	public function payment() 
    {
        return $this->hasMany('Payment');
    }

	public function scopeGetById($query, $id)
	{
		return $query
            ->where('id', '=', $id)
            ->where('status', '=', 1)
		;
	}

}
