<?php

class State extends BaseModel {

	protected $table = 'state';

	public function state(){
		return $this->hasMany('Area');
	}

	public function parent(){
		return $this->belongsTo('State', 'state_id');
	}

        
           public function property()
	{
		return $this->hasMany('Property', 'area_id')->where('status', '<>', -1);
	}
        
	public function scopeGetActive($query)
	{
		return $query
			// ->where('status', '=', 1)
		;
	}		
	
	public function scopeGetRowById($query, $id){

		return $query
		->where('id', '=', $id)
		;
	}

	public function scopeGetList($query, $keyword = ''){
		
		if($keyword){
			$query
			->where('state.state', 'LIKE', '%' . $keyword . '%')
			;
		}

		return $query
		;
	}
	
	public function scopeGetStatesByCountry($query, $country_id){

		$query
		->select(
			'state.*'
			)
		->leftjoin('country', 'country.id', '=', 'state.country_id')
		;
		
		return $query
		->where('state.country_id', '=', $country_id)
		->orWhere('state.country_id', '=', '0')
		->orderBy('state.state', 'ASC')
		;

	}
}