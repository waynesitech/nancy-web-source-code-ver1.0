<?php

class Review extends BaseModel{
	
	protected $table = 'review';

	public function user(){
		return $this->belongsTo('User');
	}

	public function member(){
		return $this->belongsTo('User', 'member_id');
	}	

	public function scopeGetByAgentIdMemberId($query, $agent_id, $member_id)
	{
		return $query
			->where('agent_id', '=', $agent_id)
			->where('member_id', '=', $member_id)
		;
	}	

	public function scopeGetByAgentId($query, $id)
	{
		return $query
			->where('agent_id', '=', $id)
		;
	}
}