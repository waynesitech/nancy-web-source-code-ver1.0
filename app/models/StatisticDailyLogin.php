<?php

 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class StatisticDailyLogin extends Eloquent {
 
    
    protected $table = 'statistic_daily_login';
    
    
    public function user() 
	{
		return $this->belongsTo('User', 'user_id');
	}
        
        
        
    public function calendar() 
	{
		return $this->belongsTo('StatisticCalendar', 'statistic_calendar_id');
	}
	
	public function scopeGetDailylogin($query,$dateFrom, $dateTo)
    {           
        return $query
        ->select(array('statistic_daily_login.*', DB::raw('COUNT(*) as login_count'), DB::raw('DATE(statistic_daily_login.lastlogin_at) created_date')))
        ->whereBetween(DB::raw('DATE(statistic_daily_login.lastlogin_at)'), array($dateFrom, $dateTo))
        ->groupBy('created_date')
        ;
        
    }
        
        
//             
//    public function scopeGetDailyLoginStatistic($query,$user_id,$from,$to)
//	{
//		return $query
//		->select(array('statictic_calendar.date as title', DB::raw('SUM(view_count) as prop_count')))
//                ->rightJoin('statictic_calendar','statistic_countryViewer.statistic_calendar_id','=','statictic_calendar.id') 
//                ->where('statistic_countryViewer.user_id', '=', $user_id)
//                ->orwhereNull('statistic_countryViewer.user_id')
//                ->whereBetween('date', array($from, $to))
//                ->groupBy('statictic_calendar.id')
//                ->orderBy('statictic_calendar.date','asc') ;
//	}
//        
//     public function scopeGetTotalViewersStatistic($query,$user_id,$from,$to)
//	{
//		return $query
//		->select( DB::raw('SUM(view_count) as prop_count'))
//                ->rightJoin('statictic_calendar','statistic_countryViewer.statistic_calendar_id','=','statictic_calendar.id') 
//                ->where('statistic_countryViewer.user_id', '=', $user_id)
//                ->whereBetween('date', array($from, $to))
//                ->groupBy('statistic_countryViewer.user_id')
//                ->orderBy('statictic_calendar.date','asc') ;
//	}
//        
//    public function scopeGetCountryViewersStatistic($query,$user_id,$from,$to)
//	{
//		return $query
//		->select(array('country_name as title', DB::raw('SUM(view_count) as prop_count')))
//                ->Join('statictic_calendar','statistic_countryViewer.statistic_calendar_id','=','statictic_calendar.id') 
//                ->Join('country','country.id','=','statistic_countryViewer.country_id')
//                ->where('statistic_countryViewer.user_id', '=', $user_id)
//                ->orwhereNull('statistic_countryViewer.user_id')
//                ->whereBetween('date', array($from, $to))
//                ->groupBy('statistic_countryViewer.country_id')
//                ->orderBy('prop_count','desc') ;
//	}        
}