<?php

class ChatHistory extends BaseModel{

	protected $table = 'chat_history';

        public function sender()
	{
		return $this->belongsTo('User', 'senderID');
	}

        public function room()
	{
		return $this->belongsTo('ChatRooms', 'chat_rooms_id');
	}
        
	public function user() 
	{
		return $this->belongsTo('User', 'created_by');
	}
        
        
            public function scopeApiGetUnreadByUserID($query,$user_id,$room_id) {

			
                return $query
                        ->select("chat_history.*")
                        ->join('chat_unread_users', 'chat_unread_users.chat_history_id', '=', 'chat_history.id')
                        ->where('chat_unread_users.user_id', '=', $user_id)
                        ->where('chat_unread_users.isRead', '=',  0)
                        ->where('chat_history.chat_rooms_id', '=',  $room_id)
                        ->orderBy('chat_history.created_on', 'DESC');
	}
        
        
         public function scopeApiUpdateToReadByUserID($query,$user_id,$room_id) {

			
                return $query
                        ->join('chat_unread_users', 'chat_unread_users.chat_history_id', '=', 'chat_history.id')
                        ->where('chat_unread_users.user_id', '=', $user_id)
                        ->where('chat_unread_users.isRead', '=',  0)
                        ->where('chat_history.chat_rooms_id', '=',  $room_id)
                        ->update(array('chat_unread_users.isRead' => 1));
                       
	}
}