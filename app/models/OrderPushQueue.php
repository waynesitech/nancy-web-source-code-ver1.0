<?php

class OrderPushQueue extends BaseModel {

    protected $table = 'order_push_queue';

    public function order()
    {
		return $this->belongsTo('Order', 'order_id');
    }

    
    public function scopeGetPendingRecords($query)
    {
    	$query->where('status', 'pending');
    }

}