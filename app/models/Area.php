<?php

class Area extends BaseModel {

	protected $table = 'area';


	public function state(){
		return $this->belongsTo('State', 'state_id');
	}

        public function property()
	{
		return $this->hasMany('Property', 'area_id')->where('status', '<>', -1);
	}
        
	public function scopeGetRowById($query){

		return $query
		->where('id', '=', $id)
		;
	}

	public function scopeGetActive($query)
	{
		return $query
			// ->where('status', '=', 1)
		;
	}	

	public function scopeGetList($query, $keyword = ''){
		
		$query
		->select(
			'area.*'
			)
		;

		if($keyword){
			$query
			->where('area.name', 'LIKE', '%' . $keyword . '%')
			;
		}

		return $query
		;
	}

	public function scopeGetAreasByState($query, $state_id){

		$query
		->select(
			'area.*','state.state'
			)
		->leftjoin('state', 'state.id', '=', 'area.state_id')
		;

		return $query
		->where('area.state_id', '=', $state_id)
                ->orWhere('area.state_id', '=', '0')
                ->orderBy('area.name', 'ASC')
		;

	}
            public function scopeGetAreasByLngLat($query, $lat,$lng){

		$query
                        ->select(array(
                          'area.*','state.state'
                          ,DB::raw('( 6371 * ACOS( COS( RADIANS( '.$lat.' ) ) * COS( RADIANS( area.latitude ) ) * COS( RADIANS( area.longitude ) - RADIANS( '.$lng.' ) ) + SIN( RADIANS( '.$lat.' ) ) * SIN( RADIANS( area.latitude ) ) ) ) AS distance')

                        ))
		->leftjoin('state', 'state.id', '=', 'area.state_id');

		return $query
                 ->orderBy('distance', 'ASC')
                 ->where('area.latitude', '!=', 0)
		;

}
}