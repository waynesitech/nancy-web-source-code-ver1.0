<?php

class OrderUsers extends BaseModel {

    protected $table = 'order_users';

    public function order()
    {
		return $this->belongsTo('Order', 'order_id');
    }

    public function scopeGetTenantByOrderId($query, $id = '') {
		return $query
			->where('order_id', $id)
			->where('role', 'Tenant');
	}

	public function scopeGetLandlordByOrderId($query, $id = '') {
		return $query
			->where('order_id', $id)
			->where('role', 'Landlord');
	}

	public function scopeGetRowByOrderId($query, $id = '') {
		return $query
			->where('order_id', $id);
	}
}