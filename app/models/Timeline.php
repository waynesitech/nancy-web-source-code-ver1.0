
<?php

class Timeline extends BaseModel{

	protected $table = 'timeline';

	public function images()
	{
		return $this->hasMany('TimelineImages');
	}
	public function comments()
	{
		return $this->hasMany('TimelineComment')
                            ->orderBy('created_on', 'desc');
	}
        
	public function timelinelikes()
	{
			return $this->hasMany('TimelineLike');
	}

	public function user() 
	{
		return $this->belongsTo('User', 'created_by');
	}
	public function scopeGetalltimeline($query)
	{
		return $query;
	}
	
	public function getTimelineDif()
	{
		$now = date_create(date("Y-m-d H:i:s"));
		$created = date_create($this->created_on);
		$date_dif = date_diff($now, $created);
		$day_dif = floor(((time() - strtotime($this->created_on))/(60*60*24)));
		$dif = '';
		
		if($day_dif > 0)
		{
			if($day_dif == 1)
			{
				$dif = 'Yesterday';
			}
			else if ($day_dif > 30)
			{
				if($date_dif->m > 3)
				{
					$dif = date('M d, Y', strtotime($this->created_on));
				}
				else
				{
					$dif = $date_dif->m;

					if($date_dif->m > 1)
					{
						$dif .= ' months ago';
					}
					else
					{ 
						$dif .= ' month ago';
					}
				}
			}
			else
			{
				$dif = $date_dif->d;
			
				if($date_dif->d > 1)
				{
					$dif .= ' days ago';
				}
				else
				{ 
					$dif .= ' day ago';
				}
			}
		}
		
		else if($date_dif->h > 0)
		{
			$dif = $date_dif->h;
			
			if($date_dif->h > 1)
			{
				$dif .= ' hours ago';
			}
			else
			{ 
				$dif .= ' hour ago';
			}
		}
		else if($date_dif->i > 0)
		{
			$dif = $date_dif->i;
			
			if($date_dif->i > 1)
			{
				$dif .= ' minutes ago';
			}
			else
			{ 
				$dif .= ' minute ago';
			}
		}
		else
		{
			$dif = $date_dif->s;
			
			if($date_dif->s > 1)
			{
				$dif .= ' seconds ago';
			}
			else
			{ 
				$dif .= ' second ago';
			}
		}
		
		return $dif;
	}
}