<?php

class PushQueue extends BaseModel {

	protected $table = 'push_queue';

	public function scopeApiGetByRecipient($query, $id,$user)
	{
		return $query
			->where('recipient', '=', $id)
//                        
			->where('status', '=', 1)
			/*
                        ->orWhere(function($query)use ($user)
                                     {
                                         $query->Where('recipient', '=', 0)
                                               ->Where('message_type','adminBroadcast')
                                               ->Where('created_on','>=',$user->created_on);
                                     })
			*/
		;
	}  
	
	public function scopeApiGetByRecipientWithAdminBroadcast($query, $id,$user,$platform)
	{
            
                    if ($platform == 'ios') {
            
                            $query->where(function($query) {
                                                $query->orwhere('type', 'apns')
                                                      ->orwhere('type', 'both');
                            });

                    } else{

                        $query->where(function($query) {
                                            $query->orwhere('type', 'gcm')
                                                  ->orwhere('type', 'both');
                        });
                    }
                        
		return $query
			->where('recipient', '=', $id)
//                        
			->where('status', '=', 1)
                        ->orWhere(function($query)use ($user)
                                     {
                                         $query->Where('recipient', '=', 0)
										 		->Where('content_id', '>', 0)
                                               ->Where('message_type','adminBroadcast')
                                               ->Where('created_on','>=',$user->created_on);
                                     })
			
		;
	}  

}