<?php

class OrderStatus extends BaseModel {

    protected $table = 'order_status';

    public function order()
    {
		return $this->belongsTo('Order', 'order_id');
    }

    public function scopeGetStatusByOrderId($query, $id = '')
    {
    	if (!empty($id)) {
    		$query
    		->where('order_id', $id)
    		;
    	}

		return $query;
	}

}