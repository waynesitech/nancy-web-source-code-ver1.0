<?php

class ChatUnreadUsers extends BaseModel{

	protected $table = 'chat_unread_users';

        public function member()
	{
		return $this->belongsTo('User', 'user_id');
	}

        public function chat()
	{
		return $this->belongsTo('ChatHistory', 'chat_history_id');
	}
        
	public function user() 
	{
		return $this->belongsTo('User', 'created_by');
	}
}