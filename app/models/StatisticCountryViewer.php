<?php

 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class StatisticCountryViewer extends Eloquent {
 
    
    protected $table = 'statistic_countryViewer';
    
    
    public function user() 
	{
		return $this->belongsTo('User', 'user_id');
	}
        
        
    public function country() 
	{
		return $this->belongsTo('Country', 'country_id');
	}
        
    public function calendar() 
	{
		return $this->belongsTo('StatisticCalendar', 'statistic_calendar_id');
	}
        
        
             
    public function scopeGetDailyViewersStatistic($query,$user_id,$from,$to)
	{
        
            $result = DB::select( DB::raw("select statictic_calendar.date as title,SUM(tbl1.view_count) as prop_count ".
                                "from   `statictic_calendar` ".
                                "left join (select * ".
                                "from   `statistic_countryViewer` ".
                                "WHERE ". 
                                "(user_id=:user_id OR user_id IS NULL)) ".
                                "As tbl1 ".
                                "ON tbl1.statistic_calendar_id= statictic_calendar.id ".
                                "WHERE ". 
                                "date BETWEEN  :from ".
                                "AND  :to ".
                                "group by statictic_calendar.id ".
                                "ORDER BY `statictic_calendar`.`date`  ASC"),
                        array(
                           'user_id' => $user_id,
                           'from'=>$from,
                           'to'=>$to
                         ));
           
                return $result;
                
//		return $query
//		->select(array('statictic_calendar.date as title', DB::raw('SUM(view_count) as prop_count')))
//                ->rightJoin('statictic_calendar','statistic_countryViewer.statistic_calendar_id','=','statictic_calendar.id') 
//                ->where('statistic_countryViewer.user_id', '=', $user_id)
//                ->orwhereNull('statistic_countryViewer.user_id')
//                ->whereBetween('date', array($from, $to))
//                ->groupBy('statictic_calendar.id')
//                ->orderBy('statictic_calendar.date','asc') ;
	}
        
     public function scopeGetTotalViewersStatistic($query,$user_id,$from,$to)
	{
		return $query
		->select( DB::raw('SUM(view_count) as prop_count'))
                ->rightJoin('statictic_calendar','statistic_countryViewer.statistic_calendar_id','=','statictic_calendar.id') 
                ->where('statistic_countryViewer.user_id', '=', $user_id)
                ->whereBetween('date', array($from, $to))
                ->groupBy('statistic_countryViewer.user_id')
                ->orderBy('statictic_calendar.date','asc') ;
	}
        
    public function scopeGetCountryViewersStatistic($query,$user_id,$from,$to)
	{
        
            $result = DB::select( DB::raw("select country_name as title,SUM(tbl1.view_count) as prop_count ".
                                "from `statictic_calendar` ".
                                "left join (select * ".
                                "from   `statistic_countryViewer` ".
                                "WHERE ". 
                                "(user_id=:user_id OR user_id IS NULL)) ".
                                "As tbl1 ".
                                "ON tbl1.statistic_calendar_id= statictic_calendar.id ".
                                "JOIN country ON country.id=tbl1.country_id WHERE ". 
                                "date BETWEEN  :from ".
                                "AND  :to ".
                                "group by tbl1.country_id ".
                                "ORDER BY prop_count  desc"),
                        array(
                           'user_id' => $user_id,
                           'from'=>$from,
                           'to'=>$to
                         ));
           
                return $result;
                
        
//		return $query
//		->select(array('country_name as title', DB::raw('SUM(view_count) as prop_count')))
//                ->Join('statictic_calendar','statistic_countryViewer.statistic_calendar_id','=','statictic_calendar.id') 
//                ->Join('country','country.id','=','statistic_countryViewer.country_id')
//                ->where('statistic_countryViewer.user_id', '=', $user_id)
//                ->orwhereNull('statistic_countryViewer.user_id')
//                ->whereBetween('date', array($from, $to))
//                ->groupBy('statistic_countryViewer.country_id')
//                ->orderBy('prop_count','desc') ;
	}     
	public function scopeGetTotalCountryViewersStatistic($query,$user_id)
    {
		return $query
		->select( DB::raw('country.country_name title'), DB::raw('SUM(statistic_countryViewer.view_count) prop_count'))
		->leftJoin('country','statistic_countryViewer.country_id','=','country.id') 
		->where('statistic_countryViewer.user_id', '=', $user_id)
		->groupBy('statistic_countryViewer.country_id')
		->orderBy('prop_count','desc')
		;
    }     
}