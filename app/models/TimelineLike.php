<?php

class TimelineLike extends BaseModel{

	protected $table = 'timeline_like';

	public function timeline()
	{
		return $this->belongsTo('Timeline', 'timeline_id');
	}

	public function user() 
	{
		return $this->belongsTo('User', 'created_by');
	}
}