<?php
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;

 
class SystemUseController extends BaseController{

//    protected $layout = 'layout.admin';

 
    public function apiUpdateAndroidVersion()
    {
        $jsonResponse = new JsonResponse();
        $response = '';
        
              $ANDROID_VERSION = Input::get('ANDROID_VERSION');
              $ANDROID_VERSION_CODE= Input::get('ANDROID_VERSION_CODE');
              $ANDROID_DB_VERSION = Input::get('ANDROID_DB_VERSION');
              
        
        $appVersion=  AppVersion::first();

        
        if(!$appVersion){
            $appVersion=new AppVersion();
        }
        
        if(!empty($ANDROID_VERSION)){
            $appVersion->ANDROID_VERSION=$ANDROID_VERSION;
        }
        
        if(!empty($ANDROID_VERSION_CODE)){
            $appVersion->ANDROID_VERSION_CODE=$ANDROID_VERSION_CODE;
        }
          
        if(!empty($ANDROID_DB_VERSION)){
            $appVersion->ANDROID_DB_VERSION=$ANDROID_DB_VERSION;
        }
	
        
        if($appVersion->save()){
            $appVersionHistory=new AppVersionHistory();
            $appVersionHistory->ANDROID_VERSION=$appVersion->ANDROID_VERSION;
            $appVersionHistory->ANDROID_VERSION_CODE=$appVersion->ANDROID_VERSION_CODE;
            $appVersionHistory->ANDROID_DB_VERSION=$appVersion->ANDROID_DB_VERSION;
            $appVersionHistory->platform='android';
            $appVersionHistory->save();
//            $appVersionHistory->IOS_APP_VERSION=
//            $appVersionHistory->IOS_BUILD_VERSION
        }
        
            $response=array(
            'ANDROID_VERSION'=>$appVersion->ANDROID_VERSION,
            'ANDROID_VERSION_CODE'=>(int)$appVersion->ANDROID_VERSION_CODE,
            'ANDROID_DB_VERSION'=>(int)$appVersion->ANDROID_DB_VERSION,
            'platform'=>$appVersionHistory->platform
            );
        
        $jsonResponse->setResponse($response);

    	return $jsonResponse->get();
        

    }

    public function apiUpdateIosVersion()
    {
        $jsonResponse = new JsonResponse();
        $response = '';
        
              $IOS_APP_VERSION = Input::get('IOS_APP_VERSION');
              $IOS_BUILD_VERSION= Input::get('IOS_BUILD_VERSION');
              
        
        $appVersion=  AppVersion::first();

        
        if(!$appVersion){
            $appVersion=new AppVersion();
        }
        
        if(!empty($IOS_APP_VERSION)){
            $appVersion->IOS_APP_VERSION=$IOS_APP_VERSION;
        }
        
        if(!empty($IOS_BUILD_VERSION)){
            $appVersion->IOS_BUILD_VERSION=$IOS_BUILD_VERSION;
        }
          
	
        
        if($appVersion->save()){
            $appVersionHistory=new AppVersionHistory();
            $appVersionHistory->IOS_APP_VERSION= $appVersion->IOS_APP_VERSION;
            $appVersionHistory->IOS_BUILD_VERSION=$appVersion->IOS_BUILD_VERSION;
            $appVersionHistory->platform='ios';
            $appVersionHistory->save();

        }
        
            $response=array(
            'IOS_APP_VERSION'=>$appVersion->IOS_APP_VERSION,
            'IOS_BUILD_VERSION'=>(int)$appVersion->IOS_BUILD_VERSION,
            'platform'=>$appVersionHistory->platform
            );
        
        $jsonResponse->setResponse($response);

    	return $jsonResponse->get();
        

    }

    public function apiGetAppVersion()
    {
        $jsonResponse = new JsonResponse();
        
            
        
        $appVersion=  AppVersion::first();

        
        if(!$appVersion){
            $jsonResponse->setCode(400);
            $jsonResponse->setBody('No App version found');
            return $jsonResponse->get();
        }
        
 
            $response=array(
                'IOS_APP_VERSION'=>$appVersion->IOS_APP_VERSION,
                'IOS_BUILD_VERSION'=>(int)$appVersion->IOS_BUILD_VERSION,
                'ANDROID_VERSION'=>$appVersion->ANDROID_VERSION,
                'ANDROID_VERSION_CODE'=>(int)$appVersion->ANDROID_VERSION_CODE,
                'ANDROID_DB_VERSION'=>(int)$appVersion->ANDROID_DB_VERSION
           
            );
        
        $jsonResponse->setResponse($response);

    	return $jsonResponse->get();
        

    }

    public function apiPushNotification()
    {
        $jsonResponse = new JsonResponse();
        
              $platform = Input::get('platform');
              $userID= Input::get('userID');
              $msg = Input::get('msg');
              
        $user=User::find($userID);
    
        switch($platform){
            case 'ios':
                $id = str_replace(array('<', '>', ' '), '', trim($user->ios));   
                
			
                if(!empty($id)){
                    $message = PushNotification::Message($msg, array(
						'badge' => 1,
						'custom' => array(
                                                        'apns-priority'=>10,
							'type' => TYPE_ORDER_RECEIVED,
							'id' => 1
						),
					));
	
					PushNotification::app('appNameIOS')
						->to($id)
						->send($message); 
                }else{
                    $jsonResponse->setBody("Notification failed to send due to empty token");
                }
					
					
            break;
         case 'android':
             
              if(!empty($user->android)){
                  PushNotification::app('appNameAndroid')
                    ->to($user->android)
                    ->send($msg);
              }else{
                    $jsonResponse->setBody("Notification failed to send due to empty token");
              }
               
             
          break;                             
        }
        
        $response=array(
                'id' => (int) $user->id,
                'ios'=>$user->ios,
                'android'=>$user->android,
                'facebook_id' => $user->facebook_id,
                'google_id' => $user->google_id,
                'name' => ($user->first_name . " " . $user->last_name),
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
        );
        
        $jsonResponse->setResponse($response);

    	return $jsonResponse->get();
        
    }
    

    public function apiPushOrderNotification()
    {
        $jsonResponse = new JsonResponse();
        
              $orderID = Input::get('orderID');
              $userID= Input::get('userID');
              $status = Input::get('status');
              
        $user=User::find($userID);
    
      if(!$user){
               $jsonResponse->setCode(400);
               $jsonResponse->setBody('User not found');
               return $jsonResponse->get();
      }
      
      $order=Order::find($orderID);
    
      if(!$order){
               $jsonResponse->setCode(400);
               $jsonResponse->setBody('Order not found');
               return $jsonResponse->get();
      }
      
      $msg=App::make('OrderController')->pushOrderNotification($user,$order,$status);
      
        $response=array(
                'id' => (int) $user->id,
                'ios'=>$user->ios,
                'android'=>$user->android,
                'email' => $user->email,
                'name' => ($user->first_name . " " . $user->last_name),
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'pushed_msg'=>$msg
        );
        
        $jsonResponse->setResponse($response);

    	return $jsonResponse->get();
        
    }
    

    public function apiPushAdminNotification()
    {
        $jsonResponse = new JsonResponse();
       
        $status=TYPE_ADMIN_ORDER_RECEIVED;
                    
                    $users=User::where('role_id',1)
                                 ->orWhere('role_id',7)
                                 ->get();
                     
                    $device_array=array();
                    $ios_device_array=array();
                    
                    
                     $pendingRecord=OrderPushQueue::where('status','pending')->first();
                     
                     if(!$pendingRecord){
                         $pendingRecord=OrderPushQueue::orderBy('id','desc')->first();
                     }
                      $order=$pendingRecord->order;
                      
                     foreach($users as $user){
                        
                         $msg=App::make('OrderController')->pushOrderNotification($user,$order,$status);
                     }
                     
                     $pendingRecord->status='sent';
                     $pendingRecord->save();
                     
                     
//                            $pendingRecords=OrderPushQueue::getPendingRecords()->get();
//                            
//                            foreach($pendingRecords as $pendingRecord){
//                                
//                                $order=$pendingRecord->order;
//                                
//                                $msg='System has just received an order from '.$order->users->first_name.' '.$order->users->last_name.'.';
//                                if(sizeof($device_array)>0){
//             
//                                    $devices = PushNotification::DeviceCollection($device_array);
//
//                                     $message = PushNotification::Message($msg, array(
//                                                                    'id' => $order->id,
//                                                                    'type' => $status,
//                                                                    'title'=>$msg,
//                                                                    'priority' => 'high',
//                                                                    'collapse_key'=>$status
//                                                            ));
//
//                                    PushNotification::app('appNameAndroid')
//                                           ->to($devices)
//                                           ->send($message);
//
//                                }
//              
//                                if(sizeof($ios_device_array)>0){
//
//                                    $ios_devices = PushNotification::DeviceCollection($ios_device_array);
//
//                                      $message = PushNotification::Message($msg, array(
//                                            'apns-priority'=>10,
//                                            'badge' => 1,
//                                            'custom' => array(
//                                                    'type' => $status,
//                                                    'id' => $order->id
//                                            ),
//                                        ));
//
//                                     PushNotification::app('appNameIOS')
//                                         ->to($ios_devices)
//                                         ->send($message); 
//                                }
//                                
//                                $pendingRecord->status='sent';
//                                $pendingRecord->save();
//                                
//                            }
      
        $response=array(
                'ios'=>sizeOf($ios_device_array),
                'android'=>sizeOf($device_array),
 
        );
        
        $jsonResponse->setResponse($response);

    	return $jsonResponse->get();
        
    }
    

    public function apiSendSms(){
        
        $jsonResponse = new JsonResponse();
        $method=Input::get('method');
        $mobile=Input::get('mobile');
        $content=Input::get('content');
        
        
        $validationResult=$this->checkPhoneValidation($mobile);
        
        if($validationResult['status'] == 0){
            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody($validationResult['msg']);

            return $jsonResponse->get();
        }
        $mobile=$validationResult['phoneObject']['countryCode'].$validationResult['phoneObject']['nationalNumber'];
        
        
        if($method=='http'){
              $result=$this->sendHttpSms();
        }else{
            $result=$this->sendSMS($mobile,$content);
        }

        $response=array(
          'result'=>$result,
          'validation'=>$validationResult
        );
        $jsonResponse->setResponse($response);
            			        
        return $jsonResponse->get();
        
  
    } 
    

    public function apiResendVerificationCode()
    {

        $jsonResponse = new JsonResponse();

        $user=User::find(Input::get('userID'));
        
        if(!$user){
            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody('User not found.');
            return $jsonResponse->get();   
        }
        $isPhoneVerify=$user->isPhoneVerify;
        $verificationCode=$user->phoneVerificationCode;
        
        $mobile= Input::get('mobile');

        if($isPhoneVerify == 'false'){
            $isPhoneVerify=false;
        }else{
            $isPhoneVerify=true;
        }
        
        
         if($isPhoneVerify){
            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody('Your mobile number is verified');
            return $jsonResponse->get();  
         }
            
         
          $existMobile=User::where('mobile',$mobile)   
                   ->where('id','!=',$user->id)
                   ->first();
                
            if($existMobile){
                
                $jsonResponse->setCode(400);
                $jsonResponse->setSubject('Error');
                $jsonResponse->setBody('Your mobile number is already used. Please try another mobile number.');
                return $jsonResponse->get();  
                
            }
            
         if($verificationCode == ""){
             
             $codeLength=6;
             $verificationCode=App::make('UserController')->gen_uuid_integer($codeLength,Auth::user()->id);
             
             $user=User::where('phoneVerificationCode',$verificationCode)->first();
             
                while($user) {
                   
                    $codeLength++;
                    $verificationCode=App::make('UserController')->gen_uuid_integer($codeLength,Auth::user()->id);
                    $user=User::where('phoneVerificationCode',$verificationCode)->first();
                } 
             
                 
            
             Auth::user()->phoneVerificationCode=$verificationCode;
             Auth::user()->tempMobile=$mobile;
             Auth::user()->save();
         }
         
         
          $content='Thank you for signing up for WonderList. Your mobile verification code is "'.$verificationCode.'". Visit http://wonderlist.property/index.php for more info.';
          $result=$this->sendSMS($mobile,$content);
        
          if($result == 0){
                 $jsonResponse->setBody('Your mobile verification code has been sent.');
          }else{
              
              Log::info('sms ERROR - statusCode : '.$result.' with phone Number'.$mobile.', userID is'.Auth::user()->id);

               $jsonResponse->setCode(400);
               $jsonResponse->setSubject('Error');
               $jsonResponse->setBody('Your mobile verification code failed to send. Please try again later.');
          }
          
        $response=array(
          'mobile'=>Auth::user()->mobile,
          'isPhoneVerify'=>$isPhoneVerify,  
          'mobile_code'=>(Auth::user()->country)?Auth::user()->country->mobile_code :"",
          'verificationCode'=>$verificationCode,
          'smsStatus'=>$result
        );
        
        $jsonResponse->setResponse($response);
        
        return $jsonResponse
        ->get()
        ;

    }

    public function apiResetSignatureStatus(){
   
        $jsonResponse = new JsonResponse();
        $passcode=Input::get('passcode');
        
        
        $orderSignature=OrderSignature::where('passcode',$passcode)->first();

             
            if(!$orderSignature){
                
                $jsonResponse->setCode(400);
                $jsonResponse->setSubject('Error');
                $jsonResponse->setBody('Passcode not found.');
                return $jsonResponse->get();  
                
            }
            
            $orderSignature->status='Pending';
            $orderSignature->save();
            
        $response=array(
          'status'=>$orderSignature->status,
        );
        $jsonResponse->setResponse($response);
            			        
        return $jsonResponse->get();
        
  
    } 

    
   
         function createDateRangeArray($strDateFrom,$strDateTo)
        {
            // takes two dates formatted as YYYY-MM-DD and creates an
            // inclusive array of the dates between the from and to dates.

            // could test validity of dates here but I'm already doing
            // that in the main script

            $aryRange=array();

            $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
            $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

            if ($iDateTo>=$iDateFrom)
            {
                array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
                while ($iDateFrom<$iDateTo)
                {
                    $iDateFrom+=86400; // add 24 hours
                    array_push($aryRange,date('Y-m-d',$iDateFrom));
                }
            }
            return $aryRange;
        }
        
        
           public function checkSmsCredits()
            {
               
               $endpoint="http://203.223.130.115/ExtMTPush/CheckSMSUserCredit?custid=wonderlist";
               $options = [
                   'http' => [
                       'method'  => 'GET',
                   ],
               ];
               // see: http://php.net/manual/en/function.stream-context-create.php
               $context = stream_context_create($options);
               // see: http://php.net/manual/en/function.file-get-contents.php
               $result = file_get_contents($endpoint, FALSE, $context);
               if ($result === FALSE)
               {
                   throw new ServerErrorException('Error validating transaction.', 560);
               }
               // Decode json object (TRUE variable decodes as an associative array)
//               return json_decode($result, TRUE);
               
               return $result;
            }
            
            
     public function sendHttpSms()
            {
               
               $endpoint="http://203.223.130.115/ExtMTPush/extmtpush?shortcode=39398&custid=wonderlist&rmsisdn=60163917794&smsisdn=62003&mtid=a002&mtprice=000&productCode=&productType=16&keyword=&dataEncoding=0&dataStr=Anythingtexthere&dataUrl=&dnRep=0&groupTag=10";
               $options = [
                   'http' => [
                       'method'  => 'GET',
                   ],
               ];
               // see: http://php.net/manual/en/function.stream-context-create.php
               $context = stream_context_create($options);
               // see: http://php.net/manual/en/function.file-get-contents.php
               $result = file_get_contents($endpoint, FALSE, $context);
               if ($result === FALSE)
               {
                   throw new ServerErrorException('Error validating transaction.', 560);
               }
               // Decode json object (TRUE variable decodes as an associative array)
//               return json_decode($result, TRUE);
               
               return $result;
            }
            
           
           
    public function apiRebuildAgency(){
        
        $jsonResponse = new JsonResponse();
        
        $agencies=AgencyBk::all();
        
        $exits=0;
        $inserted=0;
        foreach($agencies as $agency){
            $agentRecord=Agency::where('name',$agency->agency)->first();
            
            if(!$agentRecord){
                $agencyBk=new Agency();
                $agencyBk->name=$agency->agency;
                $agencyBk->status=1;
                $agencyBk->save();
                
                $inserted++;
            }else
            {
                $agentRecord->status=1;
                $agentRecord->save();
                $exits++;
            }
        }
        
        

        $response=array(
          'inserted'=>$inserted,
          'exits'=>$exits
        );
        $jsonResponse->setResponse($response);
            			        
        return $jsonResponse->get();
        
  
    } 

}

