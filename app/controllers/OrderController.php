<?php

use Swagger\Annotations as SWG;
use Barryvdh\DomPDF\Facade as PDF;

/**
 * @SWG\Resource(
 *  apiVersion="1.0",
 *  resourcePath="/order",
 *  description="Order operations",
 *  produces="['application/json']"
 * )
 */
class OrderController extends BaseController {

    protected $layout = 'layout.admin';

    //guest
    public function getUserOrder() {
        $currentUser = Auth::user();
        
        if(isset($_GET['type'])){
            $type = $_GET['type'];
        }
        $orders = Order::getList($currentUser->id);
        if(isset($_GET['type'])){
            if($_GET['type'] == 'esign'){
                 $orders = $orders->where('tenancyAgreementStatus', 'Done');
            }else if($_GET['type'] == 'estamp'){
                $orders = $orders->where('eSignStatus', 'ALL_SIGNED');
            }
        }
        $orders = $orders->orderBy('orders.created_on', 'DESC')
                ->get();

        $this->layout = View::make('layout.guest');
        $this->layout->body = View::make('order.list')
                ->with('orders', $orders);
    }

    public function getOrderForm($id = 0) {
        if (empty($id)) {
            $order = NULL;
        } else {
            $order = Order::find($id);
        }
        $states = State::where('country_id', 62)
                ->orderBy('state', 'ASC')
                ->lists('state', 'state')
        ;
        $month = array('0' => '0');
        for ($j = 1; $j < 12; $j++) {
            $month[$j] = $j;
        }

        $this->layout = View::make('layout.guest');
        $this->layout->body = View::make('order.order')
                ->with('order', $order)
                ->with('states', array('' => 'Please select state') + $states)
                ->with('month', $month);
    }

    public function postOrderForm($id = 0) {
        $isRenew = (Input::get('hidRoute') == 'order.renew') ? true : false;
        $type = '';
        $rules = array(
            'propertyStreet' => 'required',
            'propertyTown' => 'required',
            'propertyPostcode' => 'required',
            'propertyState' => 'required',
            'commencementDate' => 'required',
            'termsOfTenancyMonths' => 'required|numeric',
            'termsOfTenancyYears' => 'required|numeric|max:3',
            'optionToRenewMonths' => 'numeric',
            'optionToRenewYears' => 'numeric|max:3',
            'rental' => 'required|numeric',
            'rentalFreePeriodMonths' => 'numeric',
            'rentalFreePeriodYears' => 'numeric',
        );
        
        if (Input::get('hidtype') != '') {
            $type = Input::get('hidtype');
            if (!Input::hasFile('taPDF')) {
                $rules['taPDF'] = 'required|mimes:pdf';
            }
        }
        
        if(!$isRenew){
            //$rules['propertyPicture'] = 'required';
        }
        $messages = array();
        if(Input::get('termsOfTenancyYears') >=3 && Input::get('termsOfTenancyMonths') > 0){
            $rules['termsOfTenancyMonths'] = 'required|numeric|max:0';
            $messages = array(
                'termsOfTenancyMonths.max' => 'Term of Tenancy cannot be more than 3 years.',
            );
        }
        
        if(Input::get('optionToRenewYears') >=3 && Input::get('optionToRenewMonths') > 0){
            $rules['optionToRenewMonths'] = 'numeric|max:0';
            $messages = array(
                'optionToRenewMonths.max' => 'Option to Renew cannot be more than 3 years.',
            );
        }

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {            
            if (Input::get('hidtype') != '') {
                return Redirect::route('order.add', array('type' => Input::get('hidtype')))
                                ->withErrors($validator)
                                ->withInput(Input::except(array('carpark', 'specialRequest', 'fixturesFitting','taPDF')));
            } else {            
                if($isRenew){
                    return Redirect::route('order.renew', array('id' => $id))
                                ->withErrors($validator)
                                ->withInput(Input::except(array('carpark', 'specialRequest', 'fixturesFitting')));
                }else{
                    return Redirect::route('order.add')
                                ->withErrors($validator)
                                ->withInput(Input::except(array('carpark', 'specialRequest', 'fixturesFitting')));
                }
                
            }
        }

        $currentUser = Auth::user();

        $propertyStreet = Input::get('propertyStreet');
        $propertyTown = Input::get('propertyTown');
        $propertyPostcode = Input::get('propertyPostcode');
        $propertyState = Input::get('propertyState');
        $isResidential = Input::get('isResidential');
        $isLandlord = Input::get('isLandlord');
        $commencementDate = date("Y-m-d", strtotime(str_replace('/', '-', Input::get('commencementDate'))));
        $termsOfTenancyMonths = Input::get('termsOfTenancyMonths');
        $termsOfTenancyYears = Input::get('termsOfTenancyYears');
        $isOptionToRenew = Input::get('isOptionToRenew', 0);
        $optionToRenewMonths = Input::get('optionToRenewMonths');
        $optionToRenewYears = Input::get('optionToRenewYears');
        $optionToRenewCondition = Input::get('optionToRenewCondition');
        $otherTermsofRenewal = Input::get('otherTermsofRenewal');
        $rental = Input::get('rental');
        $advanceRental = Input::get('advanceRental');
        $securityDepositRent = Input::get('securityDepositRent');
        $securityDepositUtilities = Input::get('securityDepositUtilities');
        $rentalFreePeriodWeeks = Input::get('rentalFreePeriodWeeks');
        $rentalFreePeriodMonths = Input::get('rentalFreePeriodMonths');
        $rentalFreePeriodYears = Input::get('rentalFreePeriodYears');
        if(Input::get('rentalFreePeriodStart') != ''){
            $rentalFreePeriodStart = date("Y-m-d", strtotime(str_replace('/', '-', Input::get('rentalFreePeriodStart'))));
        }else{
            $rentalFreePeriodStart = NULL;
        }
        if(Input::get('rentalFreePeriodEnd') != ''){
            $rentalFreePeriodEnd = date("Y-m-d", strtotime(str_replace('/', '-', Input::get('rentalFreePeriodEnd'))));
        }else{
            $rentalFreePeriodEnd = NULL;
        }
        $carParkLots = json_encode(Input::get('carpark', []));
        $paymentMethod = Input::get('paymentMethod');
        $paymentAccountName = Input::get('paymentAccountName');
        $bank = Input::get('bank');
        $bankAccountNum = Input::get('bankAccountNum');
        $notificationMethod = Input::get('notificationMethod');
        $postDatedMethod = Input::get('postDatedMethod');
        
        $sp_arr = array();
        $sp_count = count(Input::get('specialRequest'));
        for($i = 0; $i < $sp_count; $i++){
            $sp_arr[] = array('original' => Input::get('specialRequest')[$i], 'edited' => '');
        }                
        $specialRequest = json_encode($sp_arr);
        $fixturesFitting = json_encode(Input::get('fixturesFitting', []));

        $tenantsCount = Input::get('hidTenantCount');
        $landlordsCount = Input::get('hidLandlordCount');

        $order = Order::find($id);
        if(!empty($order->id)){
            $existingImg = $order->propertyPicture;
            $existingImgPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $existingImg;
        }

        $order = (empty($order->id) || $isRenew) ? new Order : $order;
        
        if ($type != '') {
            if ($type == 'esign') {
                $order->uploadType = 'E_SIGN';
            } elseif ($type == 'estamp') {
                $order->uploadType = 'E_STAMP';
            } elseif ($type == 'autoreminder') {
                $order->uploadType = 'AUTO_REMINDER';
            }
        }
        $order->propertyStreet = $propertyStreet;
        $order->propertyTown = $propertyTown;
        $order->propertyPostcode = $propertyPostcode;
        $order->propertyState = $propertyState;
        if ($isResidential == "true" || $isResidential == true) {
            $order->isResidential = true;
        } else {
            $order->isResidential = false;
        }
        if ($isLandlord == "true" || $isLandlord == true) {
            $order->isLandlord = '1';
        } else {
            $order->isLandlord = '0';
        }
        $order->commencementDate = $commencementDate;
        $order->termsOfTenancyMonths = $termsOfTenancyMonths;
        $order->termsOfTenancyYears = $termsOfTenancyYears;
        if ($isOptionToRenew == "true" || $isOptionToRenew == true) {
            $order->isOptionToRenew = true;
        } else {
            $order->isOptionToRenew = false;
        }
        $order->optionToRenewMonths = $optionToRenewMonths;
        $order->optionToRenewYears = $optionToRenewYears;
        $order->optionToRenewCondition = $optionToRenewCondition;
        $order->otherTermsofRenewal = $otherTermsofRenewal;
        $order->rental = $rental;
        $order->advanceRental = $advanceRental;
        $order->securityDepositRent = $securityDepositRent;
        $order->securityDepositUtilities = $securityDepositUtilities;
        $order->rentalFreePeriodWeeks = $rentalFreePeriodWeeks;
        $order->rentalFreePeriodMonths = $rentalFreePeriodMonths;
        $order->rentalFreePeriodYears = $rentalFreePeriodYears;
        $order->rentalFreePeriodStart = $rentalFreePeriodStart;
        $order->rentalFreePeriodEnd = $rentalFreePeriodEnd;
        $order->carParkLots = $carParkLots;
        $order->paymentMethod = $paymentMethod;
        if ($paymentMethod == 'isDirectBankIn') {
            $order->paymentAccountName = $paymentAccountName;
            $order->bank = $bank;
            $order->bankAccountNum = $bankAccountNum;
            $order->notificationMethod = $notificationMethod;
        } else if ($paymentMethod == 'isPostDatedCheque') {
            $order->postDatedMethod = $postDatedMethod;
        }
        $order->specialRequest = $specialRequest;
        $order->fixturesFitting = $fixturesFitting;
        if($isRenew){
            $order->renewFromID = $id;
        }
        
        if ($order->save()) {
            $order->order_num = date('Ymd') . '-' . sprintf("%05s", $order->id);
            if (Input::hasFile('propertyPicture')) {
                $file = Input::file('propertyPicture');
                if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id))
                    @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id);

                $validator = Validator::make(array('file' => $file), array('file' => 'image'));

                if ($validator->passes()) {
                    $propertyImage = uniqid(time()) . '.' . $file->getClientOriginalExtension();

                    $img = Image::make($file)
                            ->orientate()
                            ->save(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $propertyImage);
                    $order->propertyPicture = $propertyImage;

                    if ($type != '') {
                        $file = Input::file('taPDF');
                        if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'))
                            @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf');
                        if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part1'))
                            @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part1');
                        if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part2'))
                            @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part2');
                        $uploadedDocument = uniqid(time()) . '.' . $file->getClientOriginalExtension();
                        $uploadedDocumentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR;
                        $file->move($uploadedDocumentPath, $uploadedDocument);
                        $order->actualDocument = $uploadedDocument;
                        $order->uploadedDocument = $uploadedDocument;
                        if ($type == 'esign') {
                            $order->actualDocumentWithoutSign = $uploadedDocument;
                        }
                    }else {
//                        $previewDocument = uniqid(time()) . '_preview.pdf';
//                        $actualDocument = uniqid(time()) . '.pdf';
//                        $order->previewDocument = $previewDocument;
//                        $order->actualDocument = $actualDocument;
                    }
                }
            }else{
                if($isRenew){
                    if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id))
                    @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id);
                    
                    $newImgPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $existingImg;
                    copy($existingImgPath,$newImgPath);
                    
                    $order->propertyPicture = $existingImg;
                }
            }
            $order->save();

            for ($i = 1; $i <= $tenantsCount; $i++) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                $order_users->isCompany = Input::get('isTCompany' . $i);
                $order_users->companyName = Input::get('T_companyName' . $i);
                $order_users->companyRegNum = Input::get('T_companyRegNum' . $i);
                $order_users->name = Input::get('T_name' . $i);
                $order_users->icNum = Input::get('T_icNum' . $i);
                $order_users->address = Input::get('T_address' . $i);
                $order_users->contactNum = Input::get('T_contactNum' . $i);
                $order_users->email = Input::get('T_email' . $i);
                $order_users->role = "Tenant";
                $order_users->save();
            }

            for ($j = 1; $j <= $landlordsCount; $j++) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                $order_users->isCompany = Input::get('isLCompany' . $j);
                $order_users->companyName = Input::get('L_companyName' . $j);
                $order_users->companyRegNum = Input::get('L_companyRegNum' . $j);
                $order_users->name = Input::get('L_name' . $j);
                $order_users->icNum = Input::get('L_icNum' . $j);
                $order_users->address = Input::get('L_address' . $j);
                $order_users->contactNum = Input::get('L_contactNum' . $j);
                $order_users->email = Input::get('L_email' . $j);
                $order_users->role = "Landlord";
                $order_users->save();
            }
//            if ($type == '') {
//                $this->generatePreview($order->id, $previewDocument, $actualDocument);
//            }

            return Redirect::route('order.lists')
                            ->with('alert.success', 'Update done.');
        } else {
            if($isRenew){
                return Redirect::route('order.renew', array('id' => $id))
                            ->with('alert.danger', 'Failed to submit.');
            }else{
                return Redirect::route('order.add')
                            ->with('alert.danger', 'Failed to submit.');
            }
            
        }
            if($isRenew){
                return Redirect::route('order.renew', array('id' => $id))
                            ->with('alert.danger', 'Failed to submit.');
            }else{
                return Redirect::route('order.add')
                            ->with('alert.danger', 'Failed to submit.');
            }
    }

    public function getOrderDetail($id = 0) {
        if (empty($id)) {
            $order = NULL;
        } else {
            $order = Order::find($id);
            $estamp = $this->cal_estamping($order);
        }

        $this->layout = View::make('layout.guest');
        $this->layout->body = View::make('order.detail')
                ->with('order', $order)
                ->with('estamp', $estamp);
    }

    public function getOrderPDF($id = 0) {
        if (empty($id)) {
            $order = NULL;
        } else {
            $order = Order::find($id);
        }

        $this->layout = View::make('layout.guest');
        $this->layout->body = View::make('order.viewpdf')
                ->with('order', $order);
    }
    
    public function getOrderExplanation() {
        $audios = Explanation::get();
        $lang = Input::get('lang');
        
        $this->layout = View::make('layout.guest');
        $this->layout->body = View::make('order.explanation')
                ->with('lang', $lang)
                ->with('audios', $audios);
    }
    
    public function getOrderScheduleB($orderID = 0) {
        $order = Order::find($orderID);
        $documentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->actualDocumentWithoutSign;
        
        $this->generateOrderScheduleBPDF($order->id, $documentPath);
        
//        $this->layout = View::make('layout.empty');
//        $this->layout->body = View::make('order.pdf.scheduleB')
//                ->with('order', $order);
    }
    
    public function postOrderManualSign($id) {
        $order = Order::find($id);

        if (!$order) {
            return Redirect::route('order.detail', array('id'=>$id))
                            ->with('alert.danger', 'Order Not Found.');
        }

        if (Input::hasFile('manualSign')) {
            $file = Input::file('manualSign');

            $validator = Validator::make(array('file' => $file), array('file' => 'mimes:pdf'));

            if ($validator->passes()) {
                if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'))
                    @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf');
                $uploadedDocument = 'sign_' . uniqid(time()) . '.' . $file->getClientOriginalExtension();
                $uploadedDocumentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR;
                $file->move($uploadedDocumentPath, $uploadedDocument);

                $order->manualSignDocument = $uploadedDocument;
                $order->eSignStatus = 'MANUALLY_SIGNED';
                if($order->save()){
                    $documentPath = $uploadedDocumentPath . $order->actualDocumentWithoutSign;
                    //merge sign page with actual template without sign
                    $this->generateOrderSignPDF($order->id, $documentPath);
                }

                return Redirect::route('order.detail', array('id'=>$id))
                            ->with('alert.success', 'Manual sign submitted');
            } else {
                return Redirect::route('order.detail', array('id'=>$id))
                            ->with('alert.danger', 'Failed to submit.');
            }
        } else {
            return Redirect::route('order.detail', array('id'=>$id))
                            ->with('alert.danger', 'Failed to submit.');
        }

    }    

    public function docxToPdf($orderID = 0) {
        //---------------------get tenancy template without sign page - part 1---------------------------
        $order = Order::find($orderID);
        $documentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->actualDocument;
        $noSignDocumentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderID . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->actualDocumentWithoutSign;
        $filepath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderID . DIRECTORY_SEPARATOR . 'pdf';
        $file = $_SERVER['DOCUMENT_ROOT'] . "/tenancy/ResidentialWithoutSign_1.docx";
        $document = new Gears\Pdf($file);
        $document->converter = function() {
            return new Gears\Pdf\Docx\Converter\LibreOffice();
        };

        $tenancyTemplateKeyword = new TenancyTemplateKeyword;
        $tenancyTemplateKeyword = $tenancyTemplateKeyword->get();

        foreach ($tenancyTemplateKeyword as $tenancyTemplateKeywords) {
            if (!empty($tenancyTemplateKeywords->refer_column)) {
                $temp_col = $tenancyTemplateKeywords->refer_column;

                if ($tenancyTemplateKeywords->refer_table == "orders") {
                    $obj = Order::where('id', '=', $orderID)->first();
                    
                    if ($tenancyTemplateKeywords->refer_column == "todayDate" || $tenancyTemplateKeywords->refer_column == "titleDate") {
                        $temp = date("Y-m-d");
                    } elseif ($tenancyTemplateKeywords->refer_column == "CompletionDate") {
                        $commencementDate = $obj->commencementDate;
                        $year = $obj->termsOfTenancyYears;
                        $month = $obj->termsOfTenancyMonths;
                        $total_month = ((int) $year * 12 ) + (int) $month;
                        $commencementDate_1 = date("Y-m-d", strtotime("+" . $total_month . " months", strtotime($commencementDate)));
                        $temp = date("Y-m-d", strtotime("-1 days", strtotime($commencementDate_1)));
                    } elseif ($tenancyTemplateKeywords->refer_column == "property") {
                        //get property address
                        $temp = $obj->propertyStreet . PHP_EOL . $obj->propertyTown . PHP_EOL . $obj->propertyPostcode . ' ' . $obj->propertyState;
                    } elseif ($tenancyTemplateKeywords->refer_column == "optionToRenewCondition") {
                        //get renew condition
                        switch ($obj->$temp_col) {
                            case "isMarketPrevaillingRate":
                                $temp = "market prevailing rate";
                                break;
                            case "isIncreaseOf10percent":
                                $temp = "increase of 10% from the last rental rate";
                                break;
                            case "isIncreaseOf15percent":
                                $temp = "increase of 15% from the last rental rate";
                                break;
                            case "isMutuallyAgreedRate":
                                $temp = "at a mutually agreed rate";
                                break;
                            default:
                                $temp = "------";
                        }
                    } elseif ($tenancyTemplateKeywords->refer_column == "rentalFreePeriodWeeksMonthYear") {
                        $week = $obj->rentalFreePeriodWeeks;
                        $month = $obj->rentalFreePeriodMonths;
                        $year = $obj->rentalFreePeriodYears;
                        
                        if($week != 0 || $month != 0 || $year != 0){
                            $temp = $week . " Weeks " . $month . " Months " . $year . " Years";
                        }else{
                            $temp = "";
                        }
                    }else {
                        if (isset($obj)) {
                            $temp = $obj->$temp_col;
                        }
                    }
                } elseif ($tenancyTemplateKeywords->refer_table == "order_users") {
                    $obj = OrderUsers::where('order_id', '=', $orderID);
                    $user_type = $tenancyTemplateKeywords->name;
                    $user_type_arr = explode(" ", $user_type);
                    if ($user_type_arr[0] == 'Tenant' || $user_type_arr[0] == 'Landlord') {
                        $obj = $obj->where('role', $user_type_arr[0]);
                    }
                    $obj = $obj->get();
                    $temp = '';
                    foreach ($obj as $item) {
                        if ($tenancyTemplateKeywords->refer_column == "name") {
                            //get all landlord/tenant name
                            if ($item->isCompany) {
                                $temp .= $item->companyName;
                            } else {
                                $temp .= $item->$temp_col;
                            }
                            $temp .= PHP_EOL;
                        }elseif($tenancyTemplateKeywords->refer_column == "landlordDetails" || $tenancyTemplateKeywords->refer_column == "tenantDetails"){
                            $orderuserName = '';
                            $orderuserReg_ic = '';
                            if ($item->isCompany) {
                                $orderuserName = $item->companyName;
                                $orderuserReg_ic = $item->companyRegNum;
                            } else {
                                $orderuserName = $item->name;
                                $orderuserReg_ic = $item->icNum;
                            }
                            if($temp != '') $temp .= PHP_EOL;
                            $temp .= 'Name: ' .  $orderuserName . PHP_EOL;
                            $temp .= '[NRIC No./Registration No.]: ' .  $orderuserReg_ic . PHP_EOL;
                            $temp .= 'Address: ' .  $item->address . PHP_EOL;
                            $temp .= 'Tel: ' .  $item->contactNum . PHP_EOL;
                            $temp .= 'Email: ' .  $item->email . PHP_EOL;
                        }
                    }
                }

                if (empty($temp)) 
                    $temp = "";
                    //$temp = "------";


                if (DateTime::createFromFormat('Y-m-d', $temp) || DateTime::createFromFormat('Y-m-d G:i:s', $temp)) {
                    if ($tenancyTemplateKeywords->refer_column == "titleDate") {
                        $temp = date("d") . ' OF ' . date("F Y");
                    } else {
                        $temp = date("d-m-Y", strtotime(str_replace('/', '-', $temp)));
                    }
                } else if (preg_match('/^\d+\.\d+$/', $temp)) {
                    $temp = number_format($temp, 2);
                }elseif ($tenancyTemplateKeywords->refer_column == "carParkLots")  {
                    $arr = json_decode($temp);
                    if(is_array($arr)){
                        $temp = implode(",", $arr);
                    }
                }
                
                

                $document->setValue($tenancyTemplateKeywords->keyword, str_replace("<br />", " ", nl2br($temp)));
            } else {
                $document->setValue($tenancyTemplateKeywords->keyword, " [xxxxxx] ");
            }
        }

        $document->save($_SERVER['DOCUMENT_ROOT'] . "/temp/test.pdf");
//        $document1_filename = 'part 1_'.time().'.pdf';
//        $document->save($filepath . '/'. $document1_filename);
        //---------------------end - part 1---------------------------
        //
        //---------------------get tenancy template without sign page - part 2---------------------------
        $file_2 = $_SERVER['DOCUMENT_ROOT'] . "/tenancy/ResidentialWithoutSign_2.docx";
        $document_2 = new Gears\Pdf($file_2);
        $document_2->converter = function() {
            return new Gears\Pdf\Docx\Converter\LibreOffice();
        };

        $tenancyTemplateKeyword = new TenancyTemplateKeyword;
        $tenancyTemplateKeyword = $tenancyTemplateKeyword->get();

        foreach ($tenancyTemplateKeyword as $tenancyTemplateKeywords) {
            if (!empty($tenancyTemplateKeywords->refer_column)) {
                $temp_col = $tenancyTemplateKeywords->refer_column;

                if ($tenancyTemplateKeywords->refer_table == "orders") {
                    $obj = Order::where('id', '=', $orderID)->first();
                    
                    if ($tenancyTemplateKeywords->refer_column == "fixturesFitting") {
                        //get Fixtures Fitting
                        $temp = "";
                        $val = $obj->$temp_col;
                        $arr = json_decode($val, true);
                        if (is_array($arr)) {
                            foreach($arr as $key=>$value){
                                $temp .= ($key+1) . '. ' . $value;
                                $temp .= PHP_EOL;
                            }
                        }
                    } else {
                        if (isset($obj)) {
                            $temp = $obj->$temp_col;
                        }
                    }
                } 

                if (empty($temp))
                    $temp = "";

                $document_2->setValue($tenancyTemplateKeywords->keyword, str_replace("<br />", " ", nl2br($temp)));
            } else {
                $document_2->setValue($tenancyTemplateKeywords->keyword, " [xxxxxx] ");
            }
        }

        $document_2->save($_SERVER['DOCUMENT_ROOT'] . "/temp/test2.pdf");
//        $document2_filename = 'part 2_'.time().'.pdf';
//        $document_2->save($filepath . '/'. $document2_filename);
        //---------------------end - part 2---------------------------
        
        //merge schedule B
        $this->generateOrderScheduleBPDF($orderID, $filepath . '/temp/test.pdf', $filepath . '/temp/test2.pdf', $documentPath);        
        
        copy($documentPath,$noSignDocumentPath);
        //merge actual doc with empty sign page
        $this->generateOrderSignPDF($orderID, $documentPath);
        
        $file = $documentPath; // path: file name        
        //$file = $_SERVER['DOCUMENT_ROOT'] . "/temp/test.pdf"; // path: file name
        $pdf = new PDFWatermark();

        if (file_exists($file)) {
            $pagecount = $pdf->setSourceFile($file);
        } else {
            return FALSE;
        }

        /* loop for multipage pdf */
        for ($i = 1; $i <= $pagecount; $i++) {
            $tpl = $pdf->importPage($i);
            $pdf->addPage();
            $pdf->useTemplate($tpl, 1, 1, 0, 0, TRUE);
        }
        
        $pdf->Output(); //specify path filename to save or keep as it is to view in browser
        //@unlink($_SERVER['DOCUMENT_ROOT'] . "/temp/test.pdf");
        //@unlink($_SERVER['DOCUMENT_ROOT'] . "/temp/test2.pdf");

        $this->layout = View::make('layout.home');
        $this->layout->body = View::make('order.order');
    }

    public function getUserEsign() {
        $this->layout = View::make('layout.auth');
        $this->layout->body = View::make('account.user.esign');
    }

    public function postUserEsign() {
        $rules = array(
            'otp1' => 'required',
            'otp2' => 'required',
            'otp3' => 'required',
            'otp4' => 'required',
            'ic1' => 'required',
            'ic2' => 'required',
            'ic3' => 'required',
            'ic4' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::route('user.esign')
                            ->withErrors($validator);
        } else {

            $otp1 = Input::get('otp1');
            $otp2 = Input::get('otp2');
            $otp3 = Input::get('otp3');
            $otp4 = Input::get('otp4');
            $ic1 = Input::get('ic1');
            $ic2 = Input::get('ic2');
            $ic3 = Input::get('ic3');
            $ic4 = Input::get('ic4');

            $passcode = $otp1 . $otp2 . $otp3 . $otp4;
            $ic_num = $ic1 . $ic2 . $ic3 . $ic4;

            $orderUser = OrderUsers::where('passcode', $passcode)->first();

            if (!$orderUser) {
                return Redirect::route('user.esign')
                                ->with('alert.danger', 'Passcode not found. Please try again.')
                ;
            }

            if ($orderUser->signed_on != NULL) {
                $orderUser->passcode = null;
                $orderUser->save();

                return Redirect::route('user.esign')
                                ->with('alert.danger', 'Your passcode has been cleared.')
                ;
            }

            if (substr(trim($orderUser->icNum), -4) != $ic_num) {
                return Redirect::route('user.esign')
                                ->with('alert.danger', 'Passcode and ic number mismatch. Please try again.')
                ;
            }

            return Redirect::route('user.esign_view')
                            ->with('orderUserID', $orderUser->id);
        }
    }

    public function getEsignOrder() {
        $esign_userid = Session::get('orderUserID');
        if (Session::has('orderUserID')) {
            $orderUser = OrderUsers::where('id', $esign_userid)->first();
            $order = Order::find($orderUser->order_id);
            
            $this->layout = View::make('layout.plain');
            $this->layout->body = View::make('order.view')
                    ->with('order', $order)
                    ->with('orderUserID', Session::get('orderUserID'));
        } else {
            return Redirect::route('user.esign');
        }
    }

    public function postEsignOrder() {
        $jsonResponse = new JsonResponse();
        $orderUserID = Input::get('orderUserID');
        $data = Input::get('sign');

        $orderUser = OrderUsers::where('id', $orderUserID)->first();

        if (!$orderUser) {
            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody('Order User Not Found.');

            return $jsonResponse->get();
        }
        
        if (!is_null($orderUser->document_url)) {
            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody('ESign only can submit once.');

            return $jsonResponse->get();
        }

        $filepath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderUser->order_id . '/sign';
        if (!File::exists($filepath))
            File::makeDirectory($filepath, 0755, true);

        $signatureName = 'sign_' . uniqid(time()) . '.png';

        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents($filepath . '/' . $signatureName, $data);

        $orderUser->document_url = $signatureName;
        $orderUser->signed_on = date('Y-m-d H:i:s');
        $orderUser->passcode = null;
        if ($orderUser->save()) {
            $code = 200;
            $subject = 'Success';
            $msg = 'Congratulation. Your tenure agreement has been signed.';
            
            //update order esign status
            $isDoneSign = OrderUsers::where('order_id', $orderUser->order_id)
                    ->whereNull('document_url')
                    ->get();
            
            if(count($isDoneSign) == 0){
                $order = Order::find($orderUser->order_id);
                $order-> eSignStatus = 'ALL_SIGNED';
                $order->save();
                
                $documentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->actualDocumentWithoutSign;
                //merge sign page with actual template
                $this->generateOrderSignPDF($order->id, $documentPath);
            }
        } else {
            $code = 400;
            $subject = 'Error';
            $msg = 'Please try again.';
        }
        
        $jsonResponse->setCode($code);
        $jsonResponse->setSubject($subject);
        $jsonResponse->setBody($msg);

        return $jsonResponse->get();
    }

    //admin
    public function getAdminOrder() {
        $col = Input::get('col', 'created_on');
        $sort = Input::get('sort', 'desc');
        $keyword = Input::get('keyword', '');
        $daterange = Input::get('daterange', date('01/01/Y') . ' - ' . date('d/m/Y'));
        $start = date('01/01/Y');
        $end = date('d/m/Y');
        if (!empty($daterange)) {
            list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }
        
        $type=Route::currentRouteName();
        

        $orders = Order::getByKeyword($keyword, '')
                ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))));
        if($type == 'e_sign.list'){
            $orders = $orders->where('uploadType', 'E_SIGN');
        }else if($type == 'e_stamping.list'){
            $orders = $orders->where('uploadType', 'E_STAMP');
        }
        $orders = $orders->orderBy($col, $sort)
                ->paginate(20)
        ;

        $this->layout->body = View::make('order.admin.index')
                ->with('orders', $orders)
                ->with('col', $col)
                ->with('sort', $sort)
                ->with('keyword', $keyword)
                ->with('daterange', $daterange);
    }

    public function getAdminOrderForm($id = 0) {

        if (empty($id)) {
            return Redirect::route('order.list')
                            ->with('alert.danger', 'Empty Order');
        }

        $month = array('0' => '0');
        for ($j = 1; $j < 12; $j++) {
            $month[$j] = $j;
        }

        $order = Order::find($id);
        $states = State::where('country_id', 62)
                ->orderBy('state', 'ASC')
                ->lists('state', 'state')
        ;

        if (count($order) > 0) {
            $this->layout->body = View::make('order.admin.form')
                    ->with('order', $order)
                    ->with('states', $states)
                    ->with('month', $month)
            ;
        } else {
            return Redirect::route('order.list')
                            ->with('alert.danger', 'Order not found');
        }
    }

    public function postAdminOrderForm($id = 0) {
        if (empty($id)) {
            return Redirect::route('order.list')
                            ->with('alert.danger', 'Empty Order');
        }

        $status = Input::get('orderStatus', '');
        $order = Order::find($id);

        if (!empty($status)) {
            if (empty($order)) {
                return Redirect::route('order.edit', ['id' => $id])
                                ->with('alert.danger', 'Order not found.');
            } else {
                $original_status = $order->tenancyAgreementStatus;

                if ($status == 'Rejected') {
                    $order->tenancyAgreementStatus = 'Rejected';
                } elseif ($status == 'Ready') {
                    $order->tenancyAgreementStatus = 'Ready';
                    
                    $previewDocument = uniqid(time()) . '_preview.pdf';
                    $actualDocument = uniqid(time()) . '.pdf';
                    $actualDocumentWithoutSign = uniqid(time()) . '_nosign.pdf';
                    $order->previewDocument = $previewDocument;
                    $order->actualDocument = $actualDocument;
                    $order->actualDocumentWithoutSign = $actualDocumentWithoutSign;
                    $this->generatePreview($order->id, $previewDocument, $actualDocument, $actualDocumentWithoutSign);
                } elseif ($status != 'Update') {
                    return Redirect::route('order.edit', ['id' => $id])
                                    ->with('alert.danger', 'Invalid Status');
                }

                $order->propertyStreet = Input::get('propertyStreet', '');
                $order->propertyTown = Input::get('propertyTown', '');
                $order->propertyPostcode = Input::get('propertyPostcode', '');
                $order->propertyState = Input::get('propertyState', '');
                $order->commencementDate = date("Y-m-d", strtotime(str_replace('/', '-', Input::get('commencementDate'))));
                $order->termsOfTenancyMonths = Input::get('termsOfTenancyMonths', '');
                $order->termsOfTenancyYears = Input::get('termsOfTenancyYears', '');
                $order->optionToRenewCondition = Input::get('optionToRenewCondition', '');
                $order->otherTermsofRenewal = Input::get('otherTermsofRenewal', '');
                $order->rental = Input::get('rental', '');
                $order->advanceRental = Input::get('advanceRental', '');
                $order->securityDepositRent = Input::get('securityDepositRent', '');
                $order->securityDepositUtilities = Input::get('securityDepositUtilities', '');
                $order->rentalFreePeriodWeeks = Input::get('rentalFreePeriodWeeks');
                $order->rentalFreePeriodMonths = Input::get('rentalFreePeriodMonths', '');
                $order->rentalFreePeriodYears = Input::get('rentalFreePeriodYears', '');
                $order->rentalFreePeriodStart = date("Y-m-d", strtotime(str_replace('/', '-', Input::get('rentalFreePeriodStart'))));
                $order->rentalFreePeriodEnd = date("Y-m-d", strtotime(str_replace('/', '-', Input::get('rentalFreePeriodEnd'))));
                $order->paymentMethod = Input::get('paymentMethod', '');
                $order->paymentAccountName = Input::get('paymentAccountName', '');
                $order->bank = Input::get('bank', '');
                $order->bankAccountNum = Input::get('bankAccountNum', '');
                $order->notificationMethod = Input::get('notificationMethod', '');
                $order->postDatedMethod = Input::get('postDatedMethod', '');
                
                $sp_original_count = count(Input::get('hidspecialRequest'));
                $sp_edited_count = count(Input::get('specialRequest'));
                $sp_arr = array();
                if($sp_original_count === $sp_edited_count){
                    for($i = 0; $i < $sp_original_count; $i++){
                        $sp_arr[] = array('original' => Input::get('hidspecialRequest')[$i], 'edited' => Input::get('specialRequest')[$i]);
                    }
                }
                $order->specialRequest = json_encode($sp_arr);

                $order->save();

                if ($status == 'Ready') {
                    $user = User::find($order->created_by);
                    $this->pushOrderNotification($user, $order, TYPE_ORDER_READY);
                }

                return Redirect::route('order.edit', ['id' => $id])
                                ->with('alert.success', 'Order Updated.');
            }
        } else {
            return Redirect::route('order.edit', ['id' => $id])
                            ->with('alert.danger', 'Update failed. Please try again later.');
        }
    }

    public function getCSV() {
        $keyword = Input::get('skeyword', '');
        $status = Input::get('sstatus', '');
        $daterange = Input::get('sdate', '01/01/2015 - ' . date('d/m/Y'));
        $start = '01/01/2015';
        $end = date('d/m/Y');
        $modified = '01/01/' . date('Y');
        if (!empty($daterange)) {
            list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="orders.csv"',
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $output = array('order', 'tenant', 'tenant contact', 'land lord', 'land lord contact', 'created by', 'email', 'contact', 'agency', 'created on', 'modified_on', 'modified_by', 'status');

        $temp = Order::getByKeyword($keyword)->select('orders.id')
                ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                ->where('orders.modified_on', '>=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $modified))));
        ;

        if (!empty($status)) {
            $temp = $temp->where('orders.status', $status);
        }

        $temp = $temp->get();

        $order_count = count($temp);
        $limitrate = 1000;

        return Response::stream(function() use ($order_count, $limitrate, $output, $keyword, $start, $end, $status, $modified) {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $output);

                    $counter = 0;
                    $remainder = $order_count;
                    if ($order_count > $limitrate) {
                        while ($counter < $order_count) {
                            $orders = Order::getByKeyword($keyword)
                                    ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                                    ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                                    ->where('orders.modified_on', '>=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $modified))));
                            ;

                            if (!empty($status)) {
                                $orders = $orders->where('orders.status', $status);
                            }

                            $orders = $orders->skip($counter)
                                    ->take(min($remainder, $limitrate))
                                    ->get()
                            ;

                            foreach ($orders as $row) {
                                fputcsv($file, [$row->order_num, $row->tenant_name, '="' . $row->tenant_contact . '"', $row->land_lord_name, '="' . $row->land_lord_contact . '"', $row->user_name, $row->email, $row->mobile, $row->agency, $row->created_on, $row->modified_on, $row->modified_by, $row->status]);
                            }

                            $counter = $counter + $limitrate;
                            $remainder = $remainder - $limitrate;
                        }
                    } else {
                        $orders = Order::getByKeyword($keyword)
                                ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                                ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                                ->where('orders.modified_on', '>=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $modified))));
                        ;
                        if (!empty($status)) {
                            $orders = $orders->where('orders.status', $status);
                        }
                        $orders = $orders->take($order_count)
                                ->get()
                        ;

                        foreach ($orders as $row) {
                            fputcsv($file, [$row->order_num, $row->tenant_name, '="' . $row->tenant_contact . '"', $row->land_lord_name, '="' . $row->land_lord_contact . '"', $row->user_name, $row->email, $row->mobile, $row->agency,
                                date('d/m/Y H:m:i', strtotime($row->created_on)), date('d/m/Y H:m:i', strtotime($row->modified_on)), $row->modified_name, $row->status]);
                        }
                    }

                    fclose($file);
                }, 200, $headers);
    }

    public function sendReceivedEmail($order) {

        //The recipient for email
        $receiver = User::where('id', $order->created_by)->first();

        $status = TYPE_ORDER_RECEIVED;

        $this->pushOrderNotification($receiver, $order, $status);

        $subject = 'Your Tenancy agreement is Received.';

        Mail::send('emails.orderreceive', array('name' => $receiver->first_name . ' ' . $receiver->last_name), function($message) use ($receiver, $subject) {
            $message->to($receiver->email, $receiver->first_name)->subject($subject);
        });

        //what to return
        return 0;
    }

    public function getEStamping() {

        $col = Input::get('col', 'created_on');
        $sort = Input::get('sort', 'desc');
        $keyword = Input::get('keyword', '');
        $daterange = Input::get('daterange', '01/01/' . date('Y') . ' - ' . date('d/m/Y'));
        $start = '01/01/' . date('Y');
        $end = date('d/m/Y');
        $modified = '01/01/' . date('Y');
        $status = Input::get('status', '');
        if (!empty($daterange)) {
            list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }

        $orders = Order::getByKeyword($keyword)
                ->where('orders.estamping_payment', 1)
                ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                ->where('orders.modified_on', '>=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $modified))));
        if (!empty($status)) {
            $orders = $orders->where('orders.estamping_status', $status);
        }
        $orders = $orders->orderBy($col, $sort)
                ->paginate(20)
        ;

        $this->layout->body = View::make('estamping.admin.index')
                ->with('orders', $orders)
                ->with('col', $col)
                ->with('sort', $sort)
                ->with('keyword', $keyword)
                ->with('daterange', $daterange)
        ;
    }

    public function getEStampingForm($id = 0) {

        if (empty($id)) {
            return Redirect::route('estamping.list')
                            ->with('alert.danger', 'Record not found');
        }

        $order = Order::find($id);
        $order_save = true;


        if (count($order) > 0) {
            if ($order->estamping_status == 'Received') {
                $order->estamping_status = 'Processing';
                $order_save = $order->save();

                $orderStatus = new OrderStatus();
                $orderStatus->order_id = $order->id;
                $orderStatus->status = $order->estamping_status;
                $orderStatus->type = 'estamping';
                $orderStatus->save();

                if ($order_save) {

                    $user = $order->users;
                    $status = TYPE_ORDER_ESTAMPING_PROCESS;

                    $this->pushOrderNotification($user, $order, $status);
                }
            }

            if ($order_save) {
                $val = trim(ini_get("post_max_size"));
                $last = strtolower($val[strlen($val) - 1]);

                switch ($last) {
                    // The 'G' modifier is available since PHP 5.1.0
                    case 'g':
                        $val *= 1024;
                    case 'm':
                        $val *= 1024;
                }

                $tenure_year = array('' => 'Select year');
                for ($i = 1; $i <= 3; $i++) {
                    $tenure_year[$i] = $i;
                }

                $tenure_month = array('' => 'Select month');
                for ($j = 1; $j <= 12; $j++) {
                    $tenure_month[$j] = $j;
                }

                $e_fee = $this->cal_estamping($order);

                $this->appendStyle('/assets/css/owl.carousel.css?ver=0.1', 'head');
                $this->appendStyle('/assets/css/fileinput.min.css?ver=0.1', 'head');
                $this->layout->body = View::make('estamping.admin.form')
                        ->with('order', $order)
                        ->with('val', $val)
                        ->with('tenure_year', $tenure_year)
                        ->with('tenure_month', $tenure_month)
                        ->with('e_fee', $e_fee)
                ;
            } else {
                return Redirect::route('estamping.list')
                                ->with('alert.danger', 'Error occured when access to the page.');
            }
        } else {
            return Redirect::route('estamping.list')
                            ->with('alert.danger', 'Order not found');
        }
    }

    public function postEStampingForm($id = 0) {
        if (empty($id)) {
            return Redirect::route('estamping.list')
                            ->with('alert.danger', 'Record not found');
        }

        $status = Input::get('orderStatus', '');

        if (!empty($status)) {
            $exdate = Input::get('expiry_date', '');

            $order = Order::find($id);

            if ($status == 'Update' || $status == 'Ready') {
                $rules = [
                    'estamping_doc' => 'mimes:pdf',
                ];

                if (!empty($order) && $order->estamping_doc == '') {
                    $rules['estamping_doc'] = 'required';
                }

                $messages = [
                    'estamping_doc.required' => 'The Stamping File is required and must be pdf format.',
                ];
                $validator = Validator::make(Input::all(), $rules, $messages);

                if ($validator->fails()) {
                    return Redirect::route('estamping.edit', ['id' => $id])
                                    ->withErrors($validator)
                                    ->withInput();
                }
            }


            if (empty($order)) {
                return Redirect::route('estamping.edit', ['id' => $id])
                                ->with('alert.danger', 'Order not found.');
            } else {
                $order->estamping_duplicate = Input::get('estamping_duplicate', 0);

                $original_status = $order->estamping_status;

                if ($status == 'Rejected') {
                    $order->estamping_status = 'Rejected';
                } elseif ($status == 'Ready') {
                    $order->estamping_status = 'Ready';
                } elseif ($status != 'Update') {
                    return Redirect::route('estamping.edit', ['id' => $id])
                                    ->with('alert.danger', 'Invalid Status');
                } else {
                    //E-stamping
                    if (Input::hasFile('estamping_doc')) {
                        $estamping_doc = Input::file('estamping_doc', '');

                        if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $id)) {
                            @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $id);
                        }

                        $ext_edoc = $estamping_doc->getClientOriginalExtension();
                        if (strtolower($ext_edoc) != 'pdf') {
                            return Redirect::route('estamping.edit', ['id' => $id])
                                            ->with('alert.danger', 'E-Stamping document must be .pdf format.')
                            ;
                        }

                        if (!empty($estamping_doc)) {
                            if (File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $order->estamping_doc)) {
                                File::delete(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $order->estamping_doc);
                            }

                            $fname = uniqid(time()) . '.' . $estamping_doc->getClientOriginalExtension();
                            $estamping_doc->move(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $id, $fname);

                            $order->estamping_doc = $fname;
                            $order->estamping_status = 'Ready';
                            //send email & push sms
                            $this->pushOrderNotification($order->users, $order, TYPE_ORDER_ESTAMPING);
                        } else {
                            return Redirect::route('estamping.edit', ['id' => $id])
                                            ->with('alert.danger', 'File not found')
                            ;
                        }
                    }
                }
                $order->save();

                if ($original_status != $order->status) {
                    $orderStatus = new OrderStatus();
                    $orderStatus->order_id = $order->id;
                    $orderStatus->status = $order->estamping_status;
                    $orderStatus->type = 'estamping';
                    $orderStatus->save();
                }

                return Redirect::route('estamping.edit', ['id' => $id])
                                ->with('alert.success', 'E-Stamping Updated.');
            }
        } else {
            return Redirect::route('estamping.edit', ['id' => $id])
                            ->with('alert.danger', 'Update failed. Please try again later.');
        }
    }

    public function getEStampingCSV() {
        $keyword = Input::get('skeyword', '');
        $status = Input::get('sstatus', '');
        $daterange = Input::get('sdate', '01/01/2015 - ' . date('d/m/Y'));
        $start = '01/01/2015';
        $end = date('d/m/Y');
        $modified = '01/01/' . date('Y');
        if (!empty($daterange)) {
            list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="orders.csv"',
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $output = array('order', 'tenant', 'tenant contact', 'land lord', 'land lord contact', 'created by', 'email', 'contact', 'agency', 'created on', 'modified_on', 'modified_by', 'duplicate', 'status');

        $temp = Order::getByKeyword($keyword)->select('orders.id')
                ->where('orders.estamping_payment', 1)
                ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                ->where('orders.modified_on', '>=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $modified))));

        if (!empty($status)) {
            $temp = $temp->where('orders.estamping_status', $status);
        }

        $temp = $temp->get();

        $order_count = count($temp);
        $limitrate = 1000;

        return Response::stream(function() use ($order_count, $limitrate, $output, $keyword, $start, $end, $status, $modified) {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, $output);

                    $counter = 0;
                    $remainder = $order_count;
                    if ($order_count > $limitrate) {
                        while ($counter < $order_count) {
                            $orders = Order::getByKeyword($keyword)
                                    ->where('orders.estamping_payment', 1)
                                    ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                                    ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                                    ->where('orders.modified_on', '>=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $modified))));
                            ;

                            if (!empty($status)) {
                                $orders = $orders->where('orders.estamping_status', $status);
                            }

                            $orders = $orders->skip($counter)
                                    ->take(min($remainder, $limitrate))
                                    ->get()
                            ;

                            foreach ($orders as $row) {
                                if ($row->estamping_duplicate == 0) {
                                    $duplicate = 'No';
                                } else {
                                    $duplicate = 'Yes';
                                }

                                fputcsv($file, [$row->order_num, $row->tenant_name, '="' . $row->tenant_contact . '"', $row->land_lord_name, '="' . $row->land_lord_contact . '"', $row->user_name, $row->email, $row->mobile, $row->agency, $row->created_on, $row->modified_on, $row->modified_by, $duplicate, $row->estamping_status]);
                            }

                            $counter = $counter + $limitrate;
                            $remainder = $remainder - $limitrate;
                        }
                    } else {
                        $orders = Order::getByKeyword($keyword)
                                ->where('orders.estamping_payment', 1)
                                ->where('orders.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                                ->where('orders.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                                ->where('orders.modified_on', '>=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $modified))));
                        ;
                        if (!empty($status)) {
                            $orders = $orders->where('orders.estamping_status', $status);
                        }
                        $orders = $orders->take($order_count)
                                ->get()
                        ;

                        foreach ($orders as $row) {
                            if ($row->estamping_duplicate == 0) {
                                $duplicate = 'No';
                            } else {
                                $duplicate = 'Yes';
                            }

                            fputcsv($file, [$row->order_num, $row->tenant_name, '="' . $row->tenant_contact . '"', $row->land_lord_name, '="' . $row->land_lord_contact . '"', $row->user_name, $row->email, $row->mobile, $row->agency,
                                date('d/m/Y H:m:i', strtotime($row->created_on)), date('d/m/Y H:m:i', strtotime($row->modified_on)), $row->modified_name, $duplicate, $row->estamping_status]);
                        }
                    }

                    fclose($file);
                }, 200, $headers);
    }
    
    public function getExplanationList() {
        $col = Input::get('col', 'created_on');
        $sort = Input::get('sort', 'asc');
        
        $explanation = Explanation::orderBy($col, $sort)->get();
        ;

        $this->layout->body = View::make('explanation.admin.index')
                ->with('explanation', $explanation)
                ->with('col', $col)
                ->with('sort', $sort);
    }
    
    public function getExplanationForm($id = 0) {

        $explanation = Explanation::find($id);
        if (empty($id)) {
            $explanation = NULL;
        } else {
            $explanation = Explanation::find($id);
            if (!isset($explanation->id)) {
                return Redirect::route('explanation.list')
                ->with('alert.danger', 'Explanation not found');
            }
        }     
        
        $this->layout->body = View::make('explanation.admin.form')
                ->with('explanation', $explanation)
        ;
    }
    
    public function postExplanationForm($id = 0) {
        $explanation = Explanation::find($id);
        $explanation = empty($explanation->id) ? new Explanation : $explanation;
        
        $explanation->explanationNumber = Input::get('explanationNumber');
        $explanation->explanationText = Input::get('explanationText');
        $explanation->explanationText_cn = Input::get('explanationText_cn');       

        if ($explanation->save()) {
            //English
            if (Input::hasFile('explanationURL')) {
                $file = Input::file('explanationURL');
                if (!File::exists(EXPLANATION_AUDIO_PATH . DIRECTORY_SEPARATOR . 'en'))
                    @mkdir(EXPLANATION_AUDIO_PATH . DIRECTORY_SEPARATOR . 'en');

                $filename = $file->getClientOriginalName();
                $filePath = EXPLANATION_AUDIO_PATH . DIRECTORY_SEPARATOR . 'en' . DIRECTORY_SEPARATOR;
                $file->move($filePath, $filename);
                $explanation->explanationURL = $filename;
            }
            
            //Chinese
            if (Input::hasFile('explanationURL_cn')) {
                $file = Input::file('explanationURL_cn');
                if (!File::exists(EXPLANATION_AUDIO_PATH . DIRECTORY_SEPARATOR . 'cn'))
                    @mkdir(EXPLANATION_AUDIO_PATH . DIRECTORY_SEPARATOR . 'cn');

                $filename = $file->getClientOriginalName();
                $filePath = EXPLANATION_AUDIO_PATH . DIRECTORY_SEPARATOR . 'cn' . DIRECTORY_SEPARATOR;
                $file->move($filePath, $filename);
                $explanation->explanationURL_cn = $filename;
            }
            $explanation->save();

            return Redirect::route('explanation.list')
                            ->with('alert.success', 'Update done.');
        } else {
            return Redirect::route('explanation.add')
                            ->with('alert.danger', 'Failed to submit.');
        }
    }
    
    public function deleteExplanation($id = 0) {
        $explanation = Explanation::find($id);

        if (empty($explanation->id)) {
            return Redirect::route('explanation.list')
            ->with('alert.danger', 'Explanation does not exist.');
        }

        $explanation->delete();

        return Redirect::route('explanation.list')
        ->with('alert.success', 'Explanation has been deleted.');
    }
    
    public function getOrderPayment() {
        $col = Input::get('col', 'created_on');
        $sort = Input::get('sort', 'desc');
        $keyword = Input::get('keyword', '');
        $daterange = Input::get('daterange', date('01/01/Y') . ' - ' . date('d/m/Y'));
        $start = date('01/01/Y');
        $end = date('d/m/Y');
        if (!empty($daterange)) {
            list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }
        
        $payments = Payment::getByKeyword($keyword, '')
                ->where('payment.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                ->where('payment.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                ->orderBy($col, $sort)
                ->paginate(20)
        ;

        $this->layout->body = View::make('order.admin.transaction')
                ->with('payments', $payments)
                ->with('col', $col)
                ->with('sort', $sort)
                ->with('keyword', $keyword)
                ->with('daterange', $daterange);
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/new",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Create new order",
     *       notes="Returns a json",
     *       nickname="New",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="propertyStreet",
     *           description="Property street",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyTown",
     *           description="Property town",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyPostcode",
     *           description="Property postcode",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyState",
     *           description="Property state",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="isResidential",
     *           description="Property type, true = residential / false = commercial",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyPicture",
     *           description="Property picture",
     *           paramType="form",
     *           required=true,
     *           type="file"
     *          ),
     *         @SWG\Parameter(
     *           name="isLandlord",
     *           description="Onwer isLandlord type, true = isLandlord / false = isTenant",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="tenants",
     *           description="An Array of Tenants, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="landlords",
     *           description="An Array of Landlord, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="commencementDate",
     *           description="Commencement Date",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="termsOfTenancyMonths",
     *           description="Terms of Tenancy (Months)",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="termsOfTenancyYears",
     *           description="Terms of Tenancy (Years)",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="isOptionToRenew",
     *           description="Option to Renew",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewMonths",
     *           description="Option to Renew (Months)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewYears",
     *           description="Option to Renew (Years)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewCondition",
     *           description="Condition for the option to renew",
     *           paramType="form",
     *           required=false,
     *           enum={"isMarketPrevaillingRate", "isIncreaseOf10percent", "isIncreaseOf15percent", "isMutuallyAgreedRate", "isOtherPercent"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="otherTermsofRenewal",
     *           description="Other terms of renewal (Option to Renew)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="rental",
     *           description="Rental per month",
     *           paramType="form",
     *           required=true,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="advanceRental",
     *           description="Advance Rental per month",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="securityDepositRent",
     *           description="Security Deposit (Rent)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="securityDepositUtilities",
     *           description="Security Deposit (Utilities)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="otherSecurityDeposit",
     *           description="Other Security Deposit, Array(name, amount)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodWeeks",
     *           description="Rental Free Period (Weeks)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodMonths",
     *           description="Rental Free Period (Months)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodYears",
     *           description="Rental Free Period (Years)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodStart",
     *           description="Rental Free Period (Start Date)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodEnd",
     *           description="Rental Free Period (End Date)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="carParkLots",
     *           description="An array of Car Park Lots, Stringify JSON",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="paymentMethod",
     *           description="Payment method",
     *           paramType="form",
     *           required=true,
     *           enum={"isDirectBankIn", "isPostDatedCheque", "isCollectionByLandlord"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="paymentAccountName",
     *           description="Payment Account Name (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="bank",
     *           description="Payment Bank Name (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="bankAccountNum",
     *           description="Payment Bank Account Number (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="notificationMethod",
     *           description="Notification method (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           enum={"isEmail", "isSMS", "isWhatsapp"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="postDatedMethod",
     *           description="Post dated method (isPostDatedCheque)",
     *           paramType="form",
     *           required=false,
     *           enum={"is6Months", "is12Months", "is24Months"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="specialRequest",
     *           description="An array of special request, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="fixturesFitting",
     *           description="An array of fixtures and fittings, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *       ),
     *          @SWG\ResponseMessage(
     *            code=400,
     *            message="Image not found"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="User not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiNewOrder() {


        $jsonResponse = new JsonResponse();

        $rules = array(
            'propertyStreet' => 'required',
            'propertyTown' => 'required',
            'propertyPostcode' => 'required',
            'propertyState' => 'required',
            'isResidential' => 'required',
            'propertyPicture' => 'required',
            'isLandlord' => 'required',
            'tenants' => 'required',
            'landlords' => 'required',
            'commencementDate' => 'required',
            'termsOfTenancyMonths' => 'required|numeric',
            'termsOfTenancyYears' => 'required|numeric',
            'isOptionToRenew' => 'required',
            'optionToRenewMonths' => 'numeric',
            'optionToRenewYears' => 'numeric',
            'rental' => 'required|numeric',
            'rentalFreePeriodWeeks' => 'numeric',
            'rentalFreePeriodMonths' => 'numeric',
            'rentalFreePeriodYears' => 'numeric',
            'paymentMethod' => 'required',
            'specialRequest' => 'required',
            'fixturesFitting' => 'required'
        );


        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody($validator->messages()->first());

            return $jsonResponse->get();
        }


        $currentUser = Auth::user();

        $propertyStreet = Input::get('propertyStreet');
        $propertyTown = Input::get('propertyTown');
        $propertyPostcode = Input::get('propertyPostcode');
        $propertyState = Input::get('propertyState');
        $isResidential = Input::get('isResidential');
        $isLandlord = Input::get('isLandlord');
        $tenants = Input::get('tenants');
        $landlords = Input::get('landlords');
        $commencementDate = Input::get('commencementDate');
        $termsOfTenancyMonths = Input::get('termsOfTenancyMonths');
        $termsOfTenancyYears = Input::get('termsOfTenancyYears');
        $isOptionToRenew = Input::get('isOptionToRenew');
        $optionToRenewMonths = Input::get('optionToRenewMonths');
        $optionToRenewYears = Input::get('optionToRenewYears');
        $optionToRenewCondition = Input::get('optionToRenewCondition');
        $otherTermsofRenewal = Input::get('otherTermsofRenewal');
        $rental = Input::get('rental');
        $advanceRental = Input::get('advanceRental');
        $securityDepositRent = Input::get('securityDepositRent');
        $securityDepositUtilities = Input::get('securityDepositUtilities');
        $otherSecurityDeposit = Input::get('otherSecurityDeposit');
        $rentalFreePeriodWeeks = Input::get('rentalFreePeriodWeeks');
        $rentalFreePeriodMonths = Input::get('rentalFreePeriodMonths');
        $rentalFreePeriodYears = Input::get('rentalFreePeriodYears');
        $rentalFreePeriodStart = Input::get('rentalFreePeriodStart');
        $rentalFreePeriodEnd = Input::get('rentalFreePeriodEnd');
        $carParkLots = Input::get('carParkLots');
        $paymentMethod = Input::get('paymentMethod');
        $paymentAccountName = Input::get('paymentAccountName');
        $bank = Input::get('bank');
        $bankAccountNum = Input::get('bankAccountNum');
        $notificationMethod = Input::get('notificationMethod');
        $postDatedMethod = Input::get('postDatedMethod');
        $specialRequest = Input::get('specialRequest');
        $fixturesFitting = Input::get('fixturesFitting');

        $order = new Order();

        $order->propertyStreet = $propertyStreet;
        $order->propertyTown = $propertyTown;
        $order->propertyPostcode = $propertyPostcode;
        $order->propertyState = $propertyState;
        if ($isResidential === "true" || $isResidential === true) {
            $order->isResidential = true;
        } else {
            $order->isResidential = false;
        }
        if ($isLandlord === "true" || $isLandlord === true) {
            $order->isLandlord = '1';
        } else {
            $order->isLandlord = '0';
        }
        $order->commencementDate = $commencementDate;
        $order->termsOfTenancyMonths = $termsOfTenancyMonths;
        $order->termsOfTenancyYears = $termsOfTenancyYears;
        if ($isOptionToRenew === "true" || $isOptionToRenew === true) {
            $order->isOptionToRenew = true;
        } else {
            $order->isOptionToRenew = false;
        }
        $order->optionToRenewMonths = $optionToRenewMonths;
        $order->optionToRenewYears = $optionToRenewYears;
        $order->optionToRenewCondition = $optionToRenewCondition;
        $order->otherTermsofRenewal = $otherTermsofRenewal;
        $order->rental = $rental;
        $order->advanceRental = $advanceRental;
        $order->securityDepositRent = $securityDepositRent;
        $order->securityDepositUtilities = $securityDepositUtilities;
        $order->otherSecurityDeposit = $otherSecurityDeposit;
        $order->rentalFreePeriodWeeks = $rentalFreePeriodWeeks;
        $order->rentalFreePeriodMonths = $rentalFreePeriodMonths;
        $order->rentalFreePeriodYears = $rentalFreePeriodYears;
        $order->rentalFreePeriodStart = $rentalFreePeriodStart;
        $order->rentalFreePeriodEnd = $rentalFreePeriodEnd;
        $order->carParkLots = $carParkLots;
        $order->paymentMethod = $paymentMethod;
        $order->paymentAccountName = $paymentAccountName;
        $order->bank = $bank;
        $order->bankAccountNum = $bankAccountNum;
        $order->notificationMethod = $notificationMethod;
        $order->postDatedMethod = $postDatedMethod;
        $order->specialRequest = $specialRequest;
        $order->fixturesFitting = $fixturesFitting;

        if ($order->save()) {
            $order->order_num = date('Ymd') . '-' . sprintf("%05s", $order->id);
            if (Input::hasFile('propertyPicture')) {
                $file = Input::file('propertyPicture');
                if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id))
                    @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id);

                $validator = Validator::make(array('file' => $file), array('file' => 'image'));

                if ($validator->passes()) {
                    $propertyImage = uniqid(time()) . '.' . $file->getClientOriginalExtension();

                    $img = Image::make($file)
                            ->orientate()
                            ->save(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $propertyImage);
                    $order->propertyPicture = $propertyImage;
                }
            }
            $order->save();

            $tenants_decoded = json_decode($tenants, true);
            foreach ($tenants_decoded as $tenant) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                if ($tenant['isCompany'] === "true" || $tenant['isCompany'] === true) {
                    $order_users->isCompany = true;
                } else {
                    $order_users->isCompany = false;
                }
                $order_users->companyName = $tenant['companyName'];
                $order_users->companyRegNum = $tenant['companyRegNum'];
                $order_users->name = $tenant['name'];
                $order_users->icNum = $tenant['icNum'];
                $order_users->address = $tenant['address'];
                $order_users->contactNum = $tenant['contactNum'];
                $order_users->email = $tenant['email'];
                $order_users->role = "Tenant";
                $order_users->save();
            }

            $landlords_decoded = json_decode($landlords, true);
            foreach ($landlords_decoded as $landlord) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                if ($landlord['isCompany'] === "true" || $landlord['isCompany'] === true) {
                    $order_users->isCompany = true;
                } else {
                    $order_users->isCompany = false;
                }
                $order_users->companyName = $landlord['companyName'];
                $order_users->companyRegNum = $landlord['companyRegNum'];
                $order_users->name = $landlord['name'];
                $order_users->icNum = $landlord['icNum'];
                $order_users->address = $landlord['address'];
                $order_users->contactNum = $landlord['contactNum'];
                $order_users->email = $landlord['email'];
                $order_users->role = "Landlord";
                $order_users->save();
            }
        }
        $response = array('order' => $this->orderToJsonWithDetails($order));

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }


    /**
     *
     * @SWG\Api(
     *   path="/order/edit",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Edit order",
     *       notes="Returns a json",
     *       nickname="New",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="Order ID",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyStreet",
     *           description="Property street",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyTown",
     *           description="Property town",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyPostcode",
     *           description="Property postcode",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyState",
     *           description="Property state",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="isResidential",
     *           description="Property type, true = residential / false = commercial",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyPicture",
     *           description="Property picture",
     *           paramType="form",
     *           required=false,
     *           type="file"
     *          ),
     *         @SWG\Parameter(
     *           name="isLandlord",
     *           description="Onwer isLandlord type, true = isLandlord / false = isTenant",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="tenants",
     *           description="An Array of Tenants, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="landlords",
     *           description="An Array of Landlord, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="commencementDate",
     *           description="Commencement Date",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="termsOfTenancyMonths",
     *           description="Terms of Tenancy (Months)",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="termsOfTenancyYears",
     *           description="Terms of Tenancy (Years)",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="isOptionToRenew",
     *           description="Option to Renew",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewMonths",
     *           description="Option to Renew (Months)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewYears",
     *           description="Option to Renew (Years)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewCondition",
     *           description="Condition for the option to renew",
     *           paramType="form",
     *           required=false,
     *           enum={"isMarketPrevaillingRate", "isIncreaseOf10percent", "isIncreaseOf15percent", "isMutuallyAgreedRate", "isOtherPercent"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="otherTermsofRenewal",
     *           description="Other terms of renewal (Option to Renew)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="rental",
     *           description="Rental per month",
     *           paramType="form",
     *           required=true,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="advanceRental",
     *           description="Advance Rental per month",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="securityDepositRent",
     *           description="Security Deposit (Rent)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="securityDepositUtilities",
     *           description="Security Deposit (Utilities)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="otherSecurityDeposit",
     *           description="Other Security Deposit, Array(name, amount)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodWeeks",
     *           description="Rental Free Period (Weeks)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodMonths",
     *           description="Rental Free Period (Months)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodYears",
     *           description="Rental Free Period (Years)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodStart",
     *           description="Rental Free Period (Start Date)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodEnd",
     *           description="Rental Free Period (End Date)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="carParkLots",
     *           description="An array of Car Park Lots, Stringify JSON",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="paymentMethod",
     *           description="Payment method",
     *           paramType="form",
     *           required=true,
     *           enum={"isDirectBankIn", "isPostDatedCheque", "isCollectionByLandlord"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="paymentAccountName",
     *           description="Payment Account Name (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="bank",
     *           description="Payment Bank Name (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="bankAccountNum",
     *           description="Payment Bank Account Number (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="notificationMethod",
     *           description="Notification method (isDirectBankIn)",
     *           paramType="form",
     *           required=false,
     *           enum={"isEmail", "isSMS", "isWhatsapp"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="postDatedMethod",
     *           description="Post dated method (isPostDatedCheque)",
     *           paramType="form",
     *           required=false,
     *           enum={"is6Months", "is12Months", "is24Months"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="specialRequest",
     *           description="An array of special request, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="fixturesFitting",
     *           description="An array of fixtures and fittings, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *       ),
     *          @SWG\ResponseMessage(
     *            code=400,
     *            message="Image not found"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="User not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiEditOrder() {


        $jsonResponse = new JsonResponse();

        $rules = array(
        	'orderID' => 'required',
            'propertyStreet' => 'required',
            'propertyTown' => 'required',
            'propertyPostcode' => 'required',
            'propertyState' => 'required',
            'isResidential' => 'required',
            'isLandlord' => 'required',
            'tenants' => 'required',
            'landlords' => 'required',
            'commencementDate' => 'required',
            'termsOfTenancyMonths' => 'required|numeric',
            'termsOfTenancyYears' => 'required|numeric',
            'isOptionToRenew' => 'required',
            'optionToRenewMonths' => 'numeric',
            'optionToRenewYears' => 'numeric',
            'rental' => 'required|numeric',
            'rentalFreePeriodWeeks' => 'numeric',
            'rentalFreePeriodMonths' => 'numeric',
            'rentalFreePeriodYears' => 'numeric',
            'paymentMethod' => 'required',
            'specialRequest' => 'required',
            'fixturesFitting' => 'required'
        );


        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody($validator->messages()->first());

            return $jsonResponse->get();
        }


        $currentUser = Auth::user();

        $orderID = Input::get('orderID');
        $propertyStreet = Input::get('propertyStreet');
        $propertyTown = Input::get('propertyTown');
        $propertyPostcode = Input::get('propertyPostcode');
        $propertyState = Input::get('propertyState');
        $isResidential = Input::get('isResidential');
        $isLandlord = Input::get('isLandlord');
        $tenants = Input::get('tenants');
        $landlords = Input::get('landlords');
        $commencementDate = Input::get('commencementDate');
        $termsOfTenancyMonths = Input::get('termsOfTenancyMonths');
        $termsOfTenancyYears = Input::get('termsOfTenancyYears');
        $isOptionToRenew = Input::get('isOptionToRenew');
        $optionToRenewMonths = Input::get('optionToRenewMonths');
        $optionToRenewYears = Input::get('optionToRenewYears');
        $optionToRenewCondition = Input::get('optionToRenewCondition');
        $otherTermsofRenewal = Input::get('otherTermsofRenewal');
        $rental = Input::get('rental');
        $advanceRental = Input::get('advanceRental');
        $securityDepositRent = Input::get('securityDepositRent');
        $securityDepositUtilities = Input::get('securityDepositUtilities');
        $otherSecurityDeposit = Input::get('otherSecurityDeposit');
        $rentalFreePeriodWeeks = Input::get('rentalFreePeriodWeeks');
        $rentalFreePeriodMonths = Input::get('rentalFreePeriodMonths');
        $rentalFreePeriodYears = Input::get('rentalFreePeriodYears');
        $rentalFreePeriodStart = Input::get('rentalFreePeriodStart');
        $rentalFreePeriodEnd = Input::get('rentalFreePeriodEnd');
        $carParkLots = Input::get('carParkLots');
        $paymentMethod = Input::get('paymentMethod');
        $paymentAccountName = Input::get('paymentAccountName');
        $bank = Input::get('bank');
        $bankAccountNum = Input::get('bankAccountNum');
        $notificationMethod = Input::get('notificationMethod');
        $postDatedMethod = Input::get('postDatedMethod');
        $specialRequest = Input::get('specialRequest');
        $fixturesFitting = Input::get('fixturesFitting');

        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }

        $order->propertyStreet = $propertyStreet;
        $order->propertyTown = $propertyTown;
        $order->propertyPostcode = $propertyPostcode;
        $order->propertyState = $propertyState;
        if ($isResidential === "true" || $isResidential === true) {
            $order->isResidential = true;
        } else {
            $order->isResidential = false;
        }
        if ($isLandlord === "true" || $isLandlord === true) {
            $order->isLandlord = '1';
        } else {
            $order->isLandlord = '0';
        }
        $order->commencementDate = $commencementDate;
        $order->termsOfTenancyMonths = $termsOfTenancyMonths;
        $order->termsOfTenancyYears = $termsOfTenancyYears;
        if ($isOptionToRenew === "true" || $isOptionToRenew === true) {
            $order->isOptionToRenew = true;
        } else {
            $order->isOptionToRenew = false;
        }
        $order->optionToRenewMonths = $optionToRenewMonths;
        $order->optionToRenewYears = $optionToRenewYears;
        $order->optionToRenewCondition = $optionToRenewCondition;
        $order->otherTermsofRenewal = $otherTermsofRenewal;
        $order->rental = $rental;
        $order->advanceRental = $advanceRental;
        $order->securityDepositRent = $securityDepositRent;
        $order->securityDepositUtilities = $securityDepositUtilities;
        $order->otherSecurityDeposit = $otherSecurityDeposit;
        $order->rentalFreePeriodWeeks = $rentalFreePeriodWeeks;
        $order->rentalFreePeriodMonths = $rentalFreePeriodMonths;
        $order->rentalFreePeriodYears = $rentalFreePeriodYears;
        $order->rentalFreePeriodStart = $rentalFreePeriodStart;
        $order->rentalFreePeriodEnd = $rentalFreePeriodEnd;
        $order->carParkLots = $carParkLots;
        $order->paymentMethod = $paymentMethod;
        $order->paymentAccountName = $paymentAccountName;
        $order->bank = $bank;
        $order->bankAccountNum = $bankAccountNum;
        $order->notificationMethod = $notificationMethod;
        $order->postDatedMethod = $postDatedMethod;
        $order->specialRequest = $specialRequest;
        $order->fixturesFitting = $fixturesFitting;

        if ($order->save()) {
            if (Input::hasFile('propertyPicture')) {
                $file = Input::file('propertyPicture');
                if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id))
                    @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id);

                $validator = Validator::make(array('file' => $file), array('file' => 'image'));

                if ($validator->passes()) {
                    $propertyImage = uniqid(time()) . '.' . $file->getClientOriginalExtension();

                    $img = Image::make($file)
                            ->orientate()
                            ->save(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $propertyImage);
                    $order->propertyPicture = $propertyImage;
                }
            }
            $order->save();


            $tmp = OrderUsers::where('order_id', '=', $orderID)->delete();

            $tenants_decoded = json_decode($tenants, true);
            foreach ($tenants_decoded as $tenant) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                if ($tenant['isCompany'] === "true" || $tenant['isCompany'] === true) {
                    $order_users->isCompany = true;
                } else {
                    $order_users->isCompany = false;
                }
                $order_users->companyName = $tenant['companyName'];
                $order_users->companyRegNum = $tenant['companyRegNum'];
                $order_users->name = $tenant['name'];
                $order_users->icNum = $tenant['icNum'];
                $order_users->address = $tenant['address'];
                $order_users->contactNum = $tenant['contactNum'];
                $order_users->email = $tenant['email'];
                $order_users->role = "Tenant";
                $order_users->save();
            }

            $landlords_decoded = json_decode($landlords, true);
            foreach ($landlords_decoded as $landlord) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                if ($landlord['isCompany'] === "true" || $landlord['isCompany'] === true) {
                    $order_users->isCompany = true;
                } else {
                    $order_users->isCompany = false;
                }
                $order_users->companyName = $landlord['companyName'];
                $order_users->companyRegNum = $landlord['companyRegNum'];
                $order_users->name = $landlord['name'];
                $order_users->icNum = $landlord['icNum'];
                $order_users->address = $landlord['address'];
                $order_users->contactNum = $landlord['contactNum'];
                $order_users->email = $landlord['email'];
                $order_users->role = "Landlord";
                $order_users->save();
            }
        }
        $response = array('order' => $this->orderToJsonWithDetails($order));

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/newWithPDF",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Create new order with existing Tenancy Agreement",
     *       notes="Returns a json",
     *       nickname="New",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="taPDF",
     *           description="Tenancy Agreement in PDF format",
     *           paramType="form",
     *           required=true,
     *           type="file"
     *          ),
     *         @SWG\Parameter(
     *           name="uploadType",
     *           description="Condition for the option to renew",
     *           paramType="form",
     *           required=false,
     *           enum={"E_SIGN", "E_STAMP", "AUTO_REMINDER"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyStreet",
     *           description="Property street",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyTown",
     *           description="Property town",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyPostcode",
     *           description="Property postcode",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyState",
     *           description="Property state",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="isResidential",
     *           description="Property type, true = residential / false = commercial",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="propertyPicture",
     *           description="Property picture",
     *           paramType="form",
     *           required=true,
     *           type="file"
     *          ),
     *         @SWG\Parameter(
     *           name="isLandlord",
     *           description="Onwer isLandlord type, true = isLandlord / false = isTenant",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="tenants",
     *           description="An Array of Tenants, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="landlords",
     *           description="An Array of Landlord, Stringify JSON",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="commencementDate",
     *           description="Commencement Date",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="termsOfTenancyMonths",
     *           description="Terms of Tenancy (Months)",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="termsOfTenancyYears",
     *           description="Terms of Tenancy (Years)",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="isOptionToRenew",
     *           description="Option to Renew",
     *           paramType="form",
     *           required=true,
     *           type="boolean"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewMonths",
     *           description="Option to Renew (Months)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewYears",
     *           description="Option to Renew (Years)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="optionToRenewCondition",
     *           description="Condition for the option to renew",
     *           paramType="form",
     *           required=false,
     *           enum={"isMarketPrevaillingRate", "isIncreaseOf10percent", "isIncreaseOf15percent", "isMutuallyAgreedRate", "isOtherPercent"},
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="otherTermsofRenewal",
     *           description="Other terms of renewal (Option to Renew)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="rental",
     *           description="Rental per month",
     *           paramType="form",
     *           required=true,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="advanceRental",
     *           description="Advance Rental per month",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="securityDepositRent",
     *           description="Security Deposit (Rent)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="securityDepositUtilities",
     *           description="Security Deposit (Utilities)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="otherSecurityDeposit",
     *           description="Other Security Deposit, Array(name, amount)",
     *           paramType="form",
     *           required=false,
     *           type="double"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodWeeks",
     *           description="Rental Free Period (Weeks)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodMonths",
     *           description="Rental Free Period (Months)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodYears",
     *           description="Rental Free Period (Years)",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodStart",
     *           description="Rental Free Period (Start Date)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="rentalFreePeriodEnd",
     *           description="Rental Free Period (End Date)",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="carParkLots",
     *           description="An array of Car Park Lots, Stringify JSON",
     *           paramType="form",
     *           required=false,
     *           type="string"
     *          ),
     *       ),
     *          @SWG\ResponseMessage(
     *            code=400,
     *            message="Image not found / PDF not found"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="User not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiNewOrderWithPDF() {


        $jsonResponse = new JsonResponse();

        $rules = array(
            'taPDF' => 'required|mimes:pdf',
            'uploadType' => 'required',
            'propertyStreet' => 'required',
            'propertyTown' => 'required',
            'propertyPostcode' => 'required',
            'propertyState' => 'required',
            'isResidential' => 'required',
            'isLandlord' => 'required',
            'propertyPicture' => 'required',
            'tenants' => 'required',
            'landlords' => 'required',
            'commencementDate' => 'required',
            'termsOfTenancyMonths' => 'required|numeric',
            'termsOfTenancyYears' => 'required|numeric',
            'isOptionToRenew' => 'required',
            'optionToRenewMonths' => 'numeric',
            'optionToRenewYears' => 'numeric',
            'rental' => 'required|numeric',
            'rentalFreePeriodWeeks' => 'numeric',
            'rentalFreePeriodMonths' => 'numeric',
            'rentalFreePeriodYears' => 'numeric',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody($validator->messages()->first());

            return $jsonResponse->get();
        }


        $currentUser = Auth::user();

        $uploadType = Input::get('uploadType');
        $propertyStreet = Input::get('propertyStreet');
        $propertyTown = Input::get('propertyTown');
        $propertyPostcode = Input::get('propertyPostcode');
        $propertyState = Input::get('propertyState');
        $isResidential = Input::get('isResidential');
        $isLandlord = Input::get('isLandlord');
        $tenants = Input::get('tenants');
        $landlords = Input::get('landlords');
        $commencementDate = Input::get('commencementDate');
        $termsOfTenancyMonths = Input::get('termsOfTenancyMonths');
        $termsOfTenancyYears = Input::get('termsOfTenancyYears');
        $isOptionToRenew = Input::get('isOptionToRenew');
        $optionToRenewMonths = Input::get('optionToRenewMonths');
        $optionToRenewYears = Input::get('optionToRenewYears');
        $optionToRenewCondition = Input::get('optionToRenewCondition');
        $otherTermsofRenewal = Input::get('otherTermsofRenewal');
        $rental = Input::get('rental');
        $advanceRental = Input::get('advanceRental');
        $securityDepositRent = Input::get('securityDepositRent');
        $securityDepositUtilities = Input::get('securityDepositUtilities');
        $otherSecurityDeposit = Input::get('otherSecurityDeposit');
        $rentalFreePeriodWeeks = Input::get('rentalFreePeriodWeeks');
        $rentalFreePeriodMonths = Input::get('rentalFreePeriodMonths');
        $rentalFreePeriodYears = Input::get('rentalFreePeriodYears');
        $rentalFreePeriodStart = Input::get('rentalFreePeriodStart');
        $rentalFreePeriodEnd = Input::get('rentalFreePeriodEnd');
        $carParkLots = Input::get('carParkLots');

        $order = new Order();

        $order->uploadType = $uploadType;
        $order->propertyStreet = $propertyStreet;
        $order->propertyTown = $propertyTown;
        $order->propertyPostcode = $propertyPostcode;
        $order->propertyState = $propertyState;
        if ($isResidential === "true" || $isResidential === true) {
            $order->isResidential = true;
        } else {
            $order->isResidential = false;
        }
        if ($isLandlord === "true" || $isLandlord === true) {
            $order->isLandlord = '1';
        } else {
            $order->isLandlord = '0';
        }
        $order->commencementDate = $commencementDate;
        $order->termsOfTenancyMonths = $termsOfTenancyMonths;
        $order->termsOfTenancyYears = $termsOfTenancyYears;
        if ($isOptionToRenew === "true" || $isOptionToRenew === true) {
            $order->isOptionToRenew = true;
        } else {
            $order->isOptionToRenew = false;
        }
        $order->optionToRenewMonths = $optionToRenewMonths;
        $order->optionToRenewYears = $optionToRenewYears;
        $order->optionToRenewCondition = $optionToRenewCondition;
        $order->otherTermsofRenewal = $otherTermsofRenewal;
        $order->rental = $rental;
        $order->advanceRental = $advanceRental;
        $order->securityDepositRent = $securityDepositRent;
        $order->securityDepositUtilities = $securityDepositUtilities;
        $order->otherSecurityDeposit = $otherSecurityDeposit;
        $order->rentalFreePeriodWeeks = $rentalFreePeriodWeeks;
        $order->rentalFreePeriodMonths = $rentalFreePeriodMonths;
        $order->rentalFreePeriodYears = $rentalFreePeriodYears;
        $order->rentalFreePeriodStart = $rentalFreePeriodStart;
        $order->rentalFreePeriodEnd = $rentalFreePeriodEnd;
        $order->carParkLots = $carParkLots;


        if ($order->save()) {
            $order->order_num = date('Ymd') . '-' . sprintf("%05s", $order->id);
            if (Input::hasFile('propertyPicture')) {
                $file = Input::file('propertyPicture');
                if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id))
                    @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id);

                $validator = Validator::make(array('file' => $file), array('file' => 'image'));

                if ($validator->passes()) {
                    $propertyImage = uniqid(time()) . '.' . $file->getClientOriginalExtension();

                    $img = Image::make($file)
                            ->orientate()
                            ->save(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $propertyImage);
                    $order->propertyPicture = $propertyImage;

                    $file = Input::file('taPDF');
                    if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'))
                        @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf');
                    if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part1'))
                            @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part1');
                        if (!File::exists(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part2'))
                            @mkdir(ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'.DIRECTORY_SEPARATOR.'part2');
                    $uploadedDocument = uniqid(time()) . '.' . $file->getClientOriginalExtension();
                    $uploadedDocumentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR;
                    $file->move($uploadedDocumentPath, $uploadedDocument);
                    $order->actualDocument = $uploadedDocument;
                    $order->uploadedDocument = $uploadedDocument;
                    if ($order->uploadType == 'E_SIGN') {
                        $order->actualDocumentWithoutSign = $uploadedDocument;
                    }
                }
            }

            $order->save();

            $tenants_decoded = json_decode($tenants, true);
            foreach ($tenants_decoded as $tenant) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                if ($tenant['isCompany'] === "true" || $tenant['isCompany'] === true) {
                    $order_users->isCompany = true;
                } else {
                    $order_users->isCompany = false;
                }
                $order_users->companyName = $tenant['companyName'];
                $order_users->companyRegNum = $tenant['companyRegNum'];
                $order_users->name = $tenant['name'];
                $order_users->icNum = $tenant['icNum'];
                $order_users->address = $tenant['address'];
                $order_users->contactNum = $tenant['contactNum'];
                $order_users->email = $tenant['email'];
                $order_users->role = "Tenant";
                $order_users->save();
            }

            $landlords_decoded = json_decode($landlords, true);
            foreach ($landlords_decoded as $landlord) {
                $order_users = new OrderUsers;
                $order_users->order_id = $order->id;
                if ($landlord['isCompany'] === "true" || $landlord['isCompany'] === true) {
                    $order_users->isCompany = true;
                } else {
                    $order_users->isCompany = false;
                }
                $order_users->companyName = $landlord['companyName'];
                $order_users->companyRegNum = $landlord['companyRegNum'];
                $order_users->name = $landlord['name'];
                $order_users->icNum = $landlord['icNum'];
                $order_users->address = $landlord['address'];
                $order_users->contactNum = $landlord['contactNum'];
                $order_users->email = $landlord['email'];
                $order_users->role = "Landlord";
                $order_users->save();
            }
        }
        $response = array('order' => $this->orderToJsonWithDetails($order));

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/list",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="List orders",
     *       notes="Returns a json",
     *       nickname="ListOrders",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="pageLength",
     *           description="page's length",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="page",
     *           description="page number",
     *           paramType="form",
     *           required=false,
     *           type="integer"
     *          )
     *       )
     *       )
     *     )
     *   )
     * )
     */
    public function apiListOrder() {

        $jsonResponse = new JsonResponse();
        $currentUser = Auth::user();
        $pageLength = Input::get('pageLength', 25);
        $orders = array();
        $response_array = array();

        if ($currentUser != null) {
            if ($currentUser->role_id == 2) {
                $orders = Order::getAdminList()->paginate($pageLength);
            } else {
                $orders = Order::getList($currentUser->id)
                        ->orderBy('orders.created_on', 'DESC')
                        ->paginate($pageLength);
            }

            foreach ($orders as $order) {
                $response_array[] = $this->orderToJson($order);
            }
        }

        $response = array('orders' => $response_array);

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/detail",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Get order's detail",
     *       notes="Returns a json",
     *       nickname="OrderDetail",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiOrderDetail() {

        $jsonResponse = new JsonResponse();
        $orderID = Input::get('orderID');

        if (Auth::user() != null)
            $currentUserID = Auth::user()->id;
        else
            $currentUserID = 0;

        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }

        $response = array('order' => $this->orderToJsonWithDetails($order));

        $response['order']['ipayBackendURL'] = route('payment.ipay88_sdk_backend');
        $response['order']['ipayRemark'] = App::make('PaymentController')->_encryptString(sprintf('%d-%d', $order->id, $currentUserID));

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/fullDetail",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Get order's full detail",
     *       notes="Returns a json",
     *       nickname="OrderDetail",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiOrderFullDetail() {

        $jsonResponse = new JsonResponse();
        $orderID = Input::get('orderID');

        if (Auth::user() != null)
            $currentUserID = Auth::user()->id;
        else
            $currentUserID = 0;

        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }

        $response = array('order' => $this->orderToFullJson($order));

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/resendAgreement",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Send Tenancy Agreement to requester",
     *       notes="Returns a json",
     *       nickname="OrderDetail",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          ),
     *          @SWG\ResponseMessage(
     *            code=400,
     *            message="Fail to send email."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiResendAgreement() {

        $jsonResponse = new JsonResponse();
        $orderID = Input::get('orderID');
        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }

        if (Auth::user()->id != $order->created_by) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }

        $receiver = User::find($order->created_by);

        $subject = 'Your Tenancy agreement, ' . $order->order_num . ', is ready.';

        $file = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderID . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->actualDocument;

        Mail::send('emails.order', array('name' => $receiver->first_name . ' ' . $receiver->last_name), function($message) use ($receiver, $subject, $file) {
            $message->from('admin@nancy.com.my', 'Nancy');
            $message->to($receiver->email, $receiver->name)->subject($subject);
            $message->attach($file);
        });

        if (count(Mail::failures()) > 0) {
            $jsonResponse->setCode(400);
            $jsonResponse->setBody('Fail to send email. Please try again later');

            return $jsonResponse->get();
        }

        $jsonResponse->setSubject('Success');
        $jsonResponse->setBody('Email has been sent, Please check your email inbox.');
        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/sendSignInvitation",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Send OTP to landlord and tenants",
     *       notes="Returns a json",
     *       nickname="OrderDetail",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiSendSignInvitation() {

        $jsonResponse = new JsonResponse();
        $orderID = Input::get('orderID');
        $smsStatus = array();

        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }
        
        $orderUsers = OrderUsers::getRowByOrderId($order->id)->get();
        foreach ($orderUsers as $orderUser) {
            $smsStatus[] = $this->sendDigitalSigningSms($orderUser, $order->order_num);
        }
        $order->eSignStatus = "PARTIAL_SIGNED";
        $order->save();

        $response = array('smsStatus' => $smsStatus);

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/loginViaPasscode",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Login with passcode",
     *       notes="Returns a json",
     *       nickname="login",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="passcode",
     *           description="The One Time Password",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="ic_num",
     *           description="The user's IC Number or Passport",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=401,
     *            message="Passcode and IC Number does not match"
     *          ),
     *            @SWG\ResponseMessage(
     *            code=403,
     *            message="Signed."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiLoginViaPasscode() {
        $jsonResponse = new JsonResponse();
        $passcode = Input::get('passcode');
        $ic_num = Input::get('ic_num');

        $orderUser = OrderUsers::where('passcode', $passcode)->first();

        if (!$orderUser) {
            $jsonResponse->setCode(401);
            $jsonResponse->setBody('Passcode and ic number mismatch. Please try again.');

            return $jsonResponse->get();
        }

        if ($orderUser->signed_on != NULL) {
            $orderUser->passcode = null;
            $orderUser->save();

            $jsonResponse->setCode(403);
            $jsonResponse->setBody('Your passcode has been cleared.');

            return $jsonResponse->get();
        }

        if (substr(trim($orderUser->icNum), -4) != $ic_num) {
            $jsonResponse->setCode(401);
            $jsonResponse->setBody('Passcode and ic number mismatch. Please try again.');

            return $jsonResponse->get();
        }

        $order = Order::find($orderUser->order_id);

        $response = array(
            'order' => $this->orderToJsonWithDetails($order),
            'orderUserID' => $orderUser->id,
            'orderUserName' => $orderUser->name
        );

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/apiSubmitSignature",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Submit E-Signature",
     *       notes="Returns a json",
     *       nickname="login",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderUserID",
     *           description="Order User ID",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="signatureImage",
     *           description="Signature Image",
     *           paramType="form",
     *           required=true,
     *           type="file"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=401,
     *            message="Passcode and IC Number does not match"
     *          ),
     *            @SWG\ResponseMessage(
     *            code=403,
     *            message="Signed."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiSubmitSignature() {
        $jsonResponse = new JsonResponse();
        $orderUserID = Input::get('orderUserID');

        $orderUser = OrderUsers::where('id', $orderUserID)->first();

        if (!$orderUser) {
            $jsonResponse->setCode(401);
            $jsonResponse->setBody('Order User Not Found.');

            return $jsonResponse->get();
        }

        if (Input::hasFile('signatureImage')) {
            $file = Input::file('signatureImage');

            $validator = Validator::make(array('file' => $file), array('file' => 'image'));

            if ($validator->passes()) {
                $filepath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderUser->order_id . '/sign';

                if (!File::exists($filepath))
                    File::makeDirectory($filepath, 0755, true);

                $signatureName = 'sign_' . uniqid(time()) . '.' . $file->getClientOriginalExtension();
                $img = Image::make($file)
                        ->orientate()
                        ->save($filepath . '/' . $signatureName);

                $orderUser->document_url = $signatureName;
                $orderUser->signed_on = date('Y-m-d H:i:s');
                $orderUser->passcode = null;
                $orderUser->save();

                $response = array(
                    'success' => 1
                );
            } else {
                $response = array(
                    'success' => 0
                );
            }
        } else {
            $response = array(
                'success' => 0
            );
        }

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/apiSubmitManualSignature",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Submit Manual Sign PDF",
     *       notes="Returns a json",
     *       nickname="login",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="Order ID",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          ),
     *         @SWG\Parameter(
     *           name="signaturePDF",
     *           description="Signature PDF",
     *           paramType="form",
     *           required=true,
     *           type="file"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=400,
     *            message="Validation Error"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order Not Found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiSubmitManualSignature() {


        $jsonResponse = new JsonResponse();

        $rules = array(
            'signaturePDF' => 'required|mimes:pdf',
            'orderID' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            $jsonResponse->setCode(400);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody($validator->messages()->first());

            return $jsonResponse->get();
        }

        $orderID = Input::get('orderID');

        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order Not Found.');

            return $jsonResponse->get();
        }

        if (Input::hasFile('signaturePDF')) {
            $file = Input::file('signaturePDF');

            $filepath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf'. DIRECTORY_SEPARATOR;

            if (!File::exists($filepath))
                File::makeDirectory($filepath, 0755, true);

            $signatureName = 'sign_' . uniqid(time()) . '.pdf';
            $file->move($filepath, $signatureName);

            $order->manualSignDocument = $signatureName;
            $order->eSignStatus = 'MANUALLY_SIGNED';
            if($order->save()){
                    $documentPath = $filepath . $order->actualDocumentWithoutSign;
                    //merge sign page with actual template without sign
                    $this->generateOrderSignPDF($order->id, $documentPath);
                }

            $response = array(
                'success' => 1
            );
        } else {
            $response = array(
                'success' => 0
            );
        }

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/getEstampingPrice",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Get Payment Information for Estamping",
     *       notes="Returns a json",
     *       nickname="EstampingPrice",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiGetEstampingPrice() {

        $jsonResponse = new JsonResponse();
        $orderID = Input::get('orderID');

        if (Auth::user() != null)
            $currentUserID = Auth::user()->id;
        else
            $currentUserID = 0;

        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }

        $response = array(
            'ipayEstampingBackendURL' => route('payment.ipay88_sdk_backend_estamping', array('quantity' => '')),
            'ipayRemark' => App::make('PaymentController')->_encryptString(sprintf('%d-%d', $order->id, $currentUserID)),
            'pricing' => $this->cal_estamping($order)
        );

        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/getExplanationAudio",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Get Explanation Audio",
     *       notes="Returns a json",
     *       nickname="EstampingPrice",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *         @SWG\Parameter(
     *           name="language",
     *           description="Explanation Language (en / cn)",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiGetExplanationAudio() {

        $jsonResponse = new JsonResponse();
        $orderID = Input::get('orderID');
        $language = Input::get('language');

        if (Auth::user() != null)
            $currentUserID = Auth::user()->id;
        else
            $currentUserID = 0;

        $order = Order::find($orderID);

        if (!$order) {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Order not found.');
            return $jsonResponse->get();
        }


        $response_array = [];
        if ($language === 'cn' || $language === 'en') {
            $explanations = Explanation::select('explanationNumber', 'explanationText', 'explanationURL')->get();
            foreach ($explanations as $explanation) {
                $response_array[] = $this->explanationToJson($explanation, $language);
            }
        }

        $jsonResponse->setResponse($response_array);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/approve",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Approve Order (Admin)",
     *       notes="Returns a json",
     *       nickname="OrderDetail",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=401,
     *            message="Not Authroized"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiApproveOrder() {

        $jsonResponse = new JsonResponse();
        $currentUser = Auth::user();
        $orderID = Input::get('orderID');

        if ($currentUser->role_id == 2) {
            $order = Order::find($orderID);
            if (!$order) {
                $jsonResponse->setCode(404);
                $jsonResponse->setBody('Order not found.');
                return $jsonResponse->get();
            }
            $previewDocument = uniqid(time()) . '_preview.pdf';
            $actualDocument = uniqid(time()) . '.pdf';
            $actualDocumentWithoutSign = uniqid(time()) . '_nosign.pdf';
            $order->previewDocument = $previewDocument;
            $order->actualDocument = $actualDocument;
            $order->actualDocumentWithoutSign = $actualDocumentWithoutSign;
            $order->tenancyAgreementStatus = 'Ready';
            $this->generatePreview($order->id, $previewDocument, $actualDocument, $actualDocumentWithoutSign);
            $order->save();
        } else {
            $jsonResponse->setCode(401);
            $jsonResponse->setBody('Not Authroized');
            return $jsonResponse->get();
        }

        $response = array('status' => 1);
        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/reject",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Reject Order (Admin)",
     *       notes="Returns a json",
     *       nickname="OrderDetail",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=401,
     *            message="Not Authroized"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiRejectOrder() {

        $jsonResponse = new JsonResponse();
        $currentUser = Auth::user();
        $orderID = Input::get('orderID');

        if ($currentUser->role_id == 2) {
            $order = Order::find($orderID);
            if (!$order) {
                $jsonResponse->setCode(404);
                $jsonResponse->setBody('Order not found.');
                return $jsonResponse->get();
            }
            $order->tenancyAgreementStatus = 'Rejected';
            $order->save();
        } else {
            $jsonResponse->setCode(401);
            $jsonResponse->setBody('Not Authroized');
            return $jsonResponse->get();
        }

        $response = array('status' => 1);
        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    /**
     *
     * @SWG\Api(
     *   path="/order/updateSpecialRequest",
     *   @SWG\Operations(
     *     @SWG\Operation(
     *       method="POST",
     *       summary="Update Order's Special Request (Admin)",
     *       notes="Returns a json",
     *       nickname="OrderDetail",
     *       @SWG\Parameters(
     *         @SWG\Parameter(
     *           name="orderID",
     *           description="order ID",
     *           paramType="form",
     *           required=true,
     *           type="integer"
     *          ),
     *          @SWG\Parameter(
     *           name="specialRequest",
     *           description="An array of special request, Stringify JSON [{original: "", edited: ""}]",
     *           paramType="form",
     *           required=true,
     *           type="string"
     *          )
     *       ),
     *          @SWG\ResponseMessage(
     *            code=401,
     *            message="Not Authroized"
     *          ),
     *          @SWG\ResponseMessage(
     *            code=404,
     *            message="Order not found."
     *          )
     *       )
     *     )
     *   )
     * )
     */
    public function apiUpdateOrderSpecialRequest() {

        $jsonResponse = new JsonResponse();
        $currentUser = Auth::user();
        $orderID = Input::get('orderID');
        $specialRequest = Input::get('specialRequest');

        if ($currentUser->role_id == 2) {
            $order = Order::find($orderID);
            if (!$order) {
                $jsonResponse->setCode(404);
                $jsonResponse->setBody('Order not found.');
                return $jsonResponse->get();
            }
            $order->specialRequest = $specialRequest;
            $order->save();
        } else {
            $jsonResponse->setCode(401);
            $jsonResponse->setBody('Not Authroized');
            return $jsonResponse->get();
        }

        $response = array('status' => 1);
        $jsonResponse->setResponse($response);

        return $jsonResponse->get();
    }

    //
    //
    // Old Functions
    //
    //

    public function pushOrderNotification($user, $order, $status) {
        switch ($status) {
            case TYPE_ORDER_READY:

                $subject = 'Your Tenancy Agreement, ' . $order->order_num . ', is ready.';

                $file = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $order->document;

                Mail::send('emails.order', array('name' => $user->name), function($message) use ($user, $subject) {
                    $message
                            ->to($user->email, $user->name)
                            ->subject($subject)
                    ;
                });

                $msg = 'DEAR ' . $user->name . ', ' . 'Your tenancy agreement is ready for sign.';
                $mobile = $user->mobile;
                $this->sendSMS($mobile, $msg);

                break;
            case TYPE_ORDER_PROCESS:
                $msg = 'Your Order is Received by Lesys Tenancy Team, it is now Processing';
                $this->sendSMS($user->mobile, $msg);

                //email
                $subject = 'Your tenancy agreement is processing. (Order No.: ' . $order->order_num . ')';

                $config = Config::get('mail.from');
                Mail::send('emails.orderprocessing', array('name' => $user->first_name . ' ' . $user->last_name), function($message) use ($config, $user, $subject) {
                    //$message->from($sender->email, $sender->first_name);
                    $message->from($config['address'], $config['name']);
                    $message->to($user->email, $user->first_name)->subject($subject);
                });

                break;
            case TYPE_ADMIN_ORDER_RECEIVED:
                $msg = 'System has just received an order from ' . $order->users->first_name . ' ' . $order->users->last_name . '.';
                break;
            case TYPE_ORDER_SIGNED:
                $msg = 'DEAR ' . $user->first_name . ' ' . $user->last_name . ', ' . 'YOUR TENANCY AGREEMENT has been signed by both parties.';
                $this->sendSMS($user->mobile, $msg);


                $subject = 'Your E-Sign Tenancy Agreement, ' . $order->order_num . ', is ready.';

                $file = ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'sign' . DIRECTORY_SEPARATOR . $order->digital_merge_agreement;

                Mail::send('emails.bothsigned', array('name' => $user->first_name . ' ' . $user->last_name, 'order_num' => $order->order_num), function($message) use ($user, $subject, $file) {
                    $message->from('admin@leprosystemberhad.com', 'Lesys Tenancy');

                    $message->to($user->email, $user->first_name)->subject($subject);

                    $message->attach($file);
                });

                break;
            case TYPE_ORDER_SIGNED_CONFIRMATION:
                $msg = 'DEAR ' . $user->first_name . ' ' . $user->last_name . ', ' . 'YOUR TENANCY AGREEMENT ready for sign.';

                break;
            case TYPE_ORDER_ESTAMPING:
                $msg = 'DEAR ' . $user->first_name . ' ' . $user->last_name . ', ' . 'Your Stamping File is ready.';

                //email
                $receiver = $order->users;
                $subject = 'Your Stamping File, ' . $order->order_num . ', is ready.';

                $file = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $order->estamping_doc;

                Mail::send('emails.estamping', array('name' => $receiver->first_name . ' ' . $receiver->last_name), function($message) use ($receiver, $subject, $file) {
                    $message->to($receiver->email, $receiver->first_name)->subject($subject);

                    $message->attach($file, array(
                        'as' => 'Stamping File.pdf',
                        'mime' => 'application/pdf')
                    );
                });
                /* $fail = Mail::failures();
                  if(!empty($fail)) {
                  return Redirect::route('estamping.edit', ['id' => $id])
                  ->with('alert.danger', 'Email send unsuccessfully. Please try again.')
                  ;
                  } */

                break;
            case TYPE_ADMIN_STAMPING_REQUESTED:
                $msg = 'System has just received an E-stamping request from ' . $order->users->first_name . ' ' . $order->users->last_name . '.';
                break;
            case TYPE_ORDER_ESTAMPING_PROCESS:

                $msg = 'Your tenancy agreement no ' . $order->order_num . ' has been received by Lesys Tenancy for E-stamping.';
                $subject = 'Your E-Stamping is processing. (Order No.: ' . $order->order_num . ')';

                $config = Config::get('mail.from');
                Mail::send('emails.estampingprocessing', array('name' => $user->first_name . ' ' . $user->last_name), function($message) use ($config, $user, $subject) {
                    $message->from($config['address'], $config['name']);

                    $message->to($user->email, $user->first_name)->subject($subject);
                });

                break;
            default:
                $msg = 'Your order [' . $order->order_num . '] has a new update';
                break;
        }



        if (!empty($user->ios)) {



            $id = str_replace(array('<', '>', ' '), '', trim($user->ios));


            $message = PushNotification::Message($msg, array(
                        'apns-priority' => 10,
                        'badge' => 1,
                        'custom' => array(
                            'type' => $status,
                            'id' => $order->id
                        ),
            ));

            PushNotification::app('appNameIOS')
                    ->to($id)
                    ->send($message);
        }

        if (!empty($user->android)) {

            $orderObject = array('type' => $status,
                'msg' => $this->orderToJson($order));

            $message = PushNotification::Message($orderObject, array(
                        'id' => $order->id,
                        'type' => $status,
                        'title' => $msg,
                        'priority' => 'high',
            ));

            PushNotification::app('appNameAndroid')
                    ->to($user->android)
                    ->send($message);
        }

        return $msg;
    }

    function changeStatus($status) {

        if ($status == 'Pending') {
            return 'Processing';
        } else {
            return $status;
        }
    }

    function sendExpiryNotification() {
        $today = date('Y-m-d');
        $end = date('Y-m-d', strtotime("+4 months"));


        $orders = Order::where('expiry_date', '>=', date('Y-m-d', strtotime($today)))
                ->where('expiry_date', '<=', date('Y-m-d', strtotime($end)))
                ->where('expiry_email', '!=', 'Sent')
                ->get()
        ;

        if (!empty($orders)) {
            foreach ($orders as $order) {
                $receiver = User::where('id', $order->created_by)->first();
                $subject = 'Your tenancy agreement will expire soon';
                $status = TYPE_ORDER_EXPIRE;

                Mail::send('emails.orderexpire', array('order' => $order), function($message) use ($receiver, $subject) {
                    $message->to($receiver->email, $receiver->first_name)->subject($subject);
                });

                if (count(Mail::failures()) > 0) {//mail send failed
                } else {
                    $order->expiry_email = 'Sent';
                    $order->save();
                }

                $msg = 'DEAR ' . $receiver->first_name . ' ' . $receiver->last_name . ', ' . 'YOUR TENANCY AGREEMENT WILL EXPIRE SOON ! We have e mailed to you via ' . $receiver->email . '.';
                $mobile = $receiver->mobile;
                $this->sendSMS($mobile, $msg);
            }
        }
    }

    public function generateOrderSignPDF($orderID, $document) {
        $order = Order::find($orderID);
        $orderSignature_landlord = OrderUsers::getLandlordByOrderId($order->id)->get();

        $orderSignature_tenure = OrderUsers::getTenantByOrderId($order->id)->get();
        
        $data = [
            'order' => $order,
            'orderSignature_landlords' => $orderSignature_landlord,
            'orderSignature_tenures' => $orderSignature_tenure
        ];
        
        try {            
            $filepath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf';

            if (!File::exists($filepath))
                File::makeDirectory($filepath, 0755, true);

            $fileName = 'sign_' . time() . '.pdf';
            $file = $filepath . '/' . $fileName;

            $pdf = PDF::loadView('order.pdf.sign', array('data'=>$data));
            $pdf->save($file);
            
            //$document = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf/Residential_beforeSign.pdf';
            $pdf_merge = new \Clegginabox\PDFMerger\PDFMerger;


            $pdf_merge->addPDF($document, 'all');
            $pdf_merge->addPDF($file, 'all');

//            $mergeFileName='agreement_'.time().'.pdf';
            $mergeFile = $filepath . '/'.$order->actualDocument;
            $pdf_merge->merge('file', $document);
        } catch (Exception $ex) {
            return array('status' => 0,
                'msg' => $ex->getMessage());
        }
    }
    
    public function generateOrderScheduleBPDF($orderID, $document, $document_part2, $documentPath) {
        $order = Order::find($orderID);
        
        try {            
            $filepath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf';

            if (!File::exists($filepath))
                File::makeDirectory($filepath, 0755, true);

            $fileName = 'scheduleB_' . time() . '.pdf';
            $file = $filepath . '/' . $fileName;

            $pdf = PDF::loadView('order.pdf.scheduleB', array('data'=>$order));
            $pdf->save($file);
            
            $pdf_merge = new \Clegginabox\PDFMerger\PDFMerger;

            $pdf_merge->addPDF($document, 'all');
            $pdf_merge->addPDF($file, 'all');
            $pdf_merge->addPDF($document_part2, 'all');

//            $mergeFileName='file_'.time().'.pdf';
//            $mergeFile = $filepath . '/'.$mergeFileName;
            $pdf_merge->merge('file', $documentPath);
        } catch (Exception $ex) {
            Log::info('Merge file===========' . $ex->getMessage());
            return array('status' => 0,
                'msg' => $ex->getMessage());
        }
    }

    //
    //
    // New Functions
    //
    //

    public function orderToJson($order) {

        $response = array(
            'id' => (int) $order->id,
            'order_num' => $order->order_num,
            'propertyStreet' => $order->propertyStreet,
            'propertyTown' => $order->propertyTown,
            'propertyPostcode' => $order->propertyPostcode,
            'propertyState' => $order->propertyState,
            'tenancyAgreementStatus' => $order->tenancyAgreementStatus,
            'eSignStatus' => $order->eSignStatus,
            'eStampStatus' => $order->eStampStatus,
            'propertyPicture' => ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $order->propertyPicture
        );

        return $response;
    }

    public function orderToJsonWithDetails($order) {
        $tenants = array();
        $landlords = array();

        $tenants_queried = OrderUsers::getTenantByOrderId($order->id)->get();
        foreach ($tenants_queried as $tenant_queried) {
            $tenants[] = $this->orderUserToJson($tenant_queried);
        }

        $landlords_queried = OrderUsers::getLandlordByOrderId($order->id)->get();
        foreach ($landlords_queried as $landlord_queried) {
            $landlords[] = $this->orderUserToJson($landlord_queried);
        }

        $response = array(
            'id' => (int) $order->id,
            'order_num' => $order->order_num,
            'propertyStreet' => $order->propertyStreet,
            'propertyTown' => $order->propertyTown,
            'propertyPostcode' => $order->propertyPostcode,
            'propertyState' => $order->propertyState,
            'tenancyAgreementStatus' => $order->tenancyAgreementStatus,
            'eSignStatus' => $order->eSignStatus,
            'eStampStatus' => $order->eStampStatus,
            'specialRequest' => $order->specialRequest,
            'propertyPicture' => ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $order->propertyPicture,
            'previewDocumentPath' => ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->previewDocument,
            'tenants' => $tenants,
            'landlords' => $landlords
        );

        if ($order->tenancyAgreementStatus != "Ready") {
            $response['actualDocumentPath'] = ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->actualDocument;
        }

        return $response;
    }

    public function orderToFullJson($order) {
        $tenants = array();
        $landlords = array();

        $tenants_queried = OrderUsers::getTenantByOrderId($order->id)->get();
        foreach ($tenants_queried as $tenant_queried) {
            $tenants[] = $this->orderUserToFullJson($tenant_queried);
        }

        $landlords_queried = OrderUsers::getLandlordByOrderId($order->id)->get();
        foreach ($landlords_queried as $landlord_queried) {
            $landlords[] = $this->orderUserToFullJson($landlord_queried);
        }

        $response = array(
            'id' => (int) $order->id,
            'order_num' => $order->order_num,
            'propertyStreet' => $order->propertyStreet,
            'propertyTown' => $order->propertyTown,
            'propertyPostcode' => $order->propertyPostcode,
            'propertyState' => $order->propertyState,
            'isResidential' => $this->booleanConverter($order->isResidential),
            'isLandlord' => $this->booleanConverter($order->isLandlord),
	        'commencementDate' => $order->commencementDate,
	        'termsOfTenancyMonths' => $order->termsOfTenancyMonths,
	        'termsOfTenancyYears' => $order->termsOfTenancyYears,
	        'isOptionToRenew' => $this->booleanConverter($order->isOptionToRenew),
	        'optionToRenewMonths' => $order->optionToRenewMonths,
	        'optionToRenewYears' => $order->optionToRenewYears,
	        'optionToRenewCondition' => $order->optionToRenewCondition,
	        'otherTermsofRenewal' => $order->otherTermsofRenewal,
	        'rental' => $order->rental,
	        'advanceRental' => $order->advanceRental,
	        'securityDepositRent' => $order->securityDepositRent,
	        'securityDepositUtilities' => $order->securityDepositUtilities,
	        'otherSecurityDeposit' => $order->otherSecurityDeposit,
	        'rentalFreePeriodWeeks' => $order->rentalFreePeriodWeeks,
	        'rentalFreePeriodMonths' => $order->rentalFreePeriodMonths,
	        'rentalFreePeriodYears' => $order->rentalFreePeriodYears,
	        'rentalFreePeriodStart' => $order->rentalFreePeriodStart,
	        'rentalFreePeriodEnd' => $order->rentalFreePeriodEnd,
	        'carParkLots' => $order->carParkLots,
            'specialRequest' => $order->specialRequest,
            'fixturesFitting' => $order->fixturesFitting,
            'propertyPicture' => ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . $order->propertyPicture,
            'previewDocumentPath' => ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $order->id . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $order->previewDocument,
            'paymentMethod' => $order->paymentMethod,
	        'paymentAccountName' => $order->paymentAccountName,
	        'bank' => $order->bank,
	        'bankAccountNum' => $order->bankAccountNum,
	        'notificationMethod' => $order->notificationMethod,
	        'postDatedMethod' => $order->postDatedMethod,
            'tenants' => $tenants,
            'landlords' => $landlords
        );

        return $response;
    }

    public function orderUserToFullJson($orderUser) {
        if ($this->booleanConverter($orderUser->isCompany)) {
            $response = array(
            	'isCompany' => $this->booleanConverter($orderUser->isCompany),
                'companyName' => $orderUser->companyName,
                'companyRegNum' => $orderUser->companyRegNum,
                'name' => $orderUser->name,
                'icNum' => $orderUser->icNum,
                'address' => $orderUser->address,
                'contactNum' => $orderUser->contactNum,
                'email' => $orderUser->email
            );
        } else {
            $response = array(
            	'isCompany' => $this->booleanConverter($orderUser->isCompany),
                'name' => $orderUser->name,
                'icNum' => $orderUser->icNum,
                'address' => $orderUser->address,
                'contactNum' => $orderUser->contactNum,
                'email' => $orderUser->email
            );
        }

        return $response;
    }

    public function orderUserToJson($orderUser) {
        $status = 'Done';
        if ($orderUser->signed_on == NULL) {
            $status = 'Pending';
        }
        if ($orderUser->isCompany) {
            $response = array(
                'companyName' => $orderUser->companyName,
                'name' => $orderUser->name,
                'status' => $status
            );
        } else {
            $response = array(
                'name' => $orderUser->name,
                'status' => $status
            );
        }

        return $response;
    }

    public function explanationToJson($explanation, $language) {

        $response = array(
            'explanationNumber' => $explanation->explanationNumber,
            'explanationText' => $explanation->explanationText,
            'explanationURL' => EXPLANATION_AUDIO_URL . DIRECTORY_SEPARATOR . $language . DIRECTORY_SEPARATOR . $explanation->explanationURL
        );

        return $response;
    }

    function sendDigitalSigningSms($OrderUser, $ReferenceNumber) {
        $mobile = $OrderUser->contactNum;
        $name = $OrderUser->name;
        $passcode = $OrderUser->passcode;

        $validationResult = $this->checkPhoneValidation($mobile);

        if ($validationResult['status'] == 0) {
            return array(
                'status' => 0,
                'msg' => $validationResult['msg'] . ' With phone number: ' . $mobile
            );
        }


        if ($passcode == NULL) {
            $passcode = $this->generateSignaturePassword();
        }

        if ($OrderUser->document_url != NULL) {
            return array(
                'status' => 2,
                'msg' => 'signed'
            );
        }

        $mobile = "+" . $validationResult['phoneObject']['countryCode'] . $validationResult['phoneObject']['nationalNumber'];
        $msg_esign = "Dear $name, Your tenancy agreement $ReferenceNumber is ready for E-Signing. Your passcode for E-Signing is $passcode.Download from the below links :" . URL::route('app.redirect');
        $smsResult = $this->sendSMS($mobile, $msg_esign);
        //$smsResult = 'Mock Sending of SMS';
        $OrderUser->passcode = $passcode;
        $OrderUser->last_sent_date = date('Y-m-d H:i:s');
        $OrderUser->save();

        return array(
            'status' => 1,
            'smsResult' => $smsResult,
            'smsMobile' => $mobile,
            'userRole' => $OrderUser->role,
            'userName' => $OrderUser->name
        );
    }

    function generateSignaturePassword() {

        $passcode = strtoupper(substr(md5(uniqid(rand(), true)), -4));
        $digitalSignature = OrderUsers::where('passcode', $passcode)->first();

        while ($digitalSignature) {
            $passcode = strtoupper(substr(md5(uniqid(rand(), true)), -4));
            $digitalSignature = OrderUsers::where('passcode', $passcode)->first();
        }

        return $passcode;
    }

    function booleanConverter($rawValue = 0) {
        if ($rawValue === '1' || $rawValue === 1) {
            return true;
        }
        return false;
    }

    public function generatePreview($orderID = 0, $previewDocument, $actualDocument, $actualDocumentWithoutSign) {

        $previewDocumentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderID . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $previewDocument;
        $documentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderID . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $actualDocument;
        $noSignDocumentPath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderID . DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR . $actualDocumentWithoutSign;
        $filepath = ORDER_IMAGE_PATH . DIRECTORY_SEPARATOR . $orderID . DIRECTORY_SEPARATOR . 'pdf';

        //---------------------get tenancy template without sign page - part 1---------------------------
        $file = $_SERVER['DOCUMENT_ROOT'] . "/tenancy/ResidentialWithoutSign_1.docx";
        $document = new Gears\Pdf($file);
        $document->converter = function() {
            return new Gears\Pdf\Docx\Converter\LibreOffice();
        };

        $tenancyTemplateKeyword = new TenancyTemplateKeyword;
        $tenancyTemplateKeyword = $tenancyTemplateKeyword->get();

        foreach ($tenancyTemplateKeyword as $tenancyTemplateKeywords) {
            if (!empty($tenancyTemplateKeywords->refer_column)) {
                $temp_col = $tenancyTemplateKeywords->refer_column;

                if ($tenancyTemplateKeywords->refer_table == "orders") {
                    $obj = Order::where('id', '=', $orderID)->first();
                    
                    if ($tenancyTemplateKeywords->refer_column == "todayDate" || $tenancyTemplateKeywords->refer_column == "titleDate") {
                        $temp = date("Y-m-d");
                    } elseif ($tenancyTemplateKeywords->refer_column == "CompletionDate") {
                        $commencementDate = $obj->commencementDate;
                        $year = $obj->termsOfTenancyYears;
                        $month = $obj->termsOfTenancyMonths;
                        $total_month = ((int) $year * 12 ) + (int) $month;
                        $commencementDate_1 = date("Y-m-d", strtotime("+" . $total_month . " months", strtotime($commencementDate)));
                        $temp = date("Y-m-d", strtotime("-1 days", strtotime($commencementDate_1)));
                    } elseif ($tenancyTemplateKeywords->refer_column == "property") {
                        //get property address
                        $temp = $obj->propertyStreet . PHP_EOL . $obj->propertyTown . PHP_EOL . $obj->propertyPostcode . ' ' . $obj->propertyState;
                    } elseif ($tenancyTemplateKeywords->refer_column == "optionToRenewCondition") {
                        //get renew condition
                        switch ($obj->$temp_col) {
                            case "isMarketPrevaillingRate":
                                $temp = "market prevailing rate";
                                break;
                            case "isIncreaseOf10percent":
                                $temp = "increase of 10% from the last rental rate";
                                break;
                            case "isIncreaseOf15percent":
                                $temp = "increase of 15% from the last rental rate";
                                break;
                            case "isMutuallyAgreedRate":
                                $temp = "at a mutually agreed rate";
                                break;
                            default:
                                $temp = "------";
                        }
                    } elseif ($tenancyTemplateKeywords->refer_column == "rentalFreePeriodWeeksMonthYear") {
                        $week = $obj->rentalFreePeriodWeeks;
                        $month = $obj->rentalFreePeriodMonths;
                        $year = $obj->rentalFreePeriodYears;
                        
                        if($week != 0 || $month != 0 || $year != 0){
                            $temp = $week . " Weeks " . $month . " Months " . $year . " Years";
                        }else{
                            $temp = "";
                        }
                    } else {
                        if (isset($obj)) {
                            $temp = $obj->$temp_col;
                        }
                    }
                } elseif ($tenancyTemplateKeywords->refer_table == "order_users") {
                    $obj = OrderUsers::where('order_id', '=', $orderID);
                    $user_type = $tenancyTemplateKeywords->name;
                    $user_type_arr = explode(" ", $user_type);
                    if ($user_type_arr[0] == 'Tenant' || $user_type_arr[0] == 'Landlord') {
                        $obj = $obj->where('role', $user_type_arr[0]);
                    }
                    $obj = $obj->get();
                    $temp = '';
                    foreach ($obj as $item) {
                        if ($tenancyTemplateKeywords->refer_column == "name") {
                            //get all landlord/tenant name
                            if ($item->isCompany) {
                                $temp .= $item->companyName;
                            } else {
                                $temp .= $item->$temp_col;
                            }
                            $temp .= PHP_EOL;
                        }elseif($tenancyTemplateKeywords->refer_column == "landlordDetails" || $tenancyTemplateKeywords->refer_column == "tenantDetails"){
                            $orderuserName = '';
                            $orderuserReg_ic = '';
                            if ($item->isCompany) {
                                $orderuserName = $item->companyName;
                                $orderuserReg_ic = $item->companyRegNum;
                            } else {
                                $orderuserName = $item->name;
                                $orderuserReg_ic = $item->icNum;
                            }
                            if($temp != '') $temp .= PHP_EOL;
                            $temp .= 'Name: ' .  $orderuserName . PHP_EOL;
                            $temp .= '[NRIC No./Registration No.]: ' .  $orderuserReg_ic . PHP_EOL;
                            $temp .= 'Address: ' .  $item->address . PHP_EOL;
                            $temp .= 'Tel: ' .  $item->contactNum . PHP_EOL;
                            $temp .= 'Email: ' .  $item->email . PHP_EOL;
                        }
                    }
                }

                if (empty($temp))
                    $temp = "";


                if (DateTime::createFromFormat('Y-m-d', $temp) || DateTime::createFromFormat('Y-m-d G:i:s', $temp)) {
                    if ($tenancyTemplateKeywords->refer_column == "titleDate") {
                        $temp = date("d") . ' OF ' . date("F Y");
                    } else {
                        $temp = date("d-m-Y", strtotime(str_replace('/', '-', $temp)));
                    }
                } else if (preg_match('/^\d+\.\d+$/', $temp)) {
                    $temp = number_format($temp, 2);
                }elseif ($tenancyTemplateKeywords->refer_column == "carParkLots")  {
                    $arr = json_decode($temp);
                    if(is_array($arr)){
                        $temp = implode(",", $arr);
                    }
                }
                
                $document->setValue($tenancyTemplateKeywords->keyword, str_replace("<br />", " ", nl2br($temp)));
            } else {
                $document->setValue($tenancyTemplateKeywords->keyword, " [xxxxxx] ");
            }
        }

        $document1_filename = 'part1_'.time().'.pdf';
        $document1_filepath = $filepath . '/part1/'. $document1_filename;
        $document->save($document1_filepath);
        
        //---------------------end - part 1---------------------------
        //
        //---------------------get tenancy template without sign page - part 2---------------------------
        $file_2 = $_SERVER['DOCUMENT_ROOT'] . "/tenancy/ResidentialWithoutSign_2.docx";
        $document_2 = new Gears\Pdf($file_2);
        $document_2->converter = function() {
            return new Gears\Pdf\Docx\Converter\LibreOffice();
        };

        $tenancyTemplateKeyword = new TenancyTemplateKeyword;
        $tenancyTemplateKeyword = $tenancyTemplateKeyword->get();

        foreach ($tenancyTemplateKeyword as $tenancyTemplateKeywords) {
            if (!empty($tenancyTemplateKeywords->refer_column)) {
                $temp_col = $tenancyTemplateKeywords->refer_column;

                if ($tenancyTemplateKeywords->refer_table == "orders") {
                    $obj = Order::where('id', '=', $orderID)->first();
                    
                    if ($tenancyTemplateKeywords->refer_column == "fixturesFitting") {
                        //get Fixtures Fitting
                        $temp = "";
                        $val = $obj->$temp_col;
                        $arr = json_decode($val, true);
                        if (is_array($arr)) {
                            foreach($arr as $key=>$value){
                                $temp .= ($key+1) . '. ' . $value;
                                $temp .= PHP_EOL;
                            }
                        }
                    } else {
                        if (isset($obj)) {
                            $temp = $obj->$temp_col;
                        }
                    }
                } 

                if (empty($temp))
                    $temp = "";

                $document_2->setValue($tenancyTemplateKeywords->keyword, str_replace("<br />", " ", nl2br($temp)));
            } else {
                $document_2->setValue($tenancyTemplateKeywords->keyword, " [xxxxxx] ");
            }
        }

        $document2_filename = 'part2_'.time().'.pdf';
        $document2_filepath = $filepath . '/part2/'. $document2_filename;
        $document_2->save($document2_filepath);
        //---------------------end - part 2---------------------------
        
        //merge schedule B
        $this->generateOrderScheduleBPDF($orderID, $document1_filepath, $document2_filepath, $documentPath);        
        
        copy($documentPath,$noSignDocumentPath);
        //merge actual doc with empty sign page
        $this->generateOrderSignPDF($orderID, $documentPath);
        
        $file = $documentPath; // path: file name  
        $pdf = new PDFWatermark();

        if (file_exists($file)) {
            $pagecount = $pdf->setSourceFile($file);
        } else {
            return FALSE;
        }

        /* loop for multipage pdf */
        for ($i = 1; $i <= $pagecount; $i++) {
            $tpl = $pdf->importPage($i);
            $pdf->addPage();
            $pdf->useTemplate($tpl, 1, 1, 0, 0, TRUE);
        }
        $pdf->Output($previewDocumentPath, 'F'); //specify path filename to save or keep as it is to view in browser
        // @unlink($documentPath); // Delete the file
    }
}
