<?php


 
if (!defined('SESS_PAYMENT_KEY')) define('SESS_PAYMENT_KEY', 'NancyPaymentSessionKey');

class PaymentController extends BaseController {

    protected $layout = 'layout.guest';

    public function getRedirect()
    {
        $config = Config::get('nancy.ipay88');

		$order_id = Input::get('order', 0);
		$order = Order::find($order_id);
		
		if(!empty($order)){
			$user = User::find($order->created_by);
			if(!empty($user)){
				$name = $user->name;
				$mobile = $user->mobile;
				$email = $user->email;
				$user_id = $user->id;
			}
		}
        
        $ref_no = $this->_generateRefNo();
        $signature = $this->_iPay88_signature(array(
            $ref_no,
            $this->_formatMoneyWithNoCommaAndDot(1),
            $config['currency'],
        ));
		
        $data = array(
            'RefNo' => $ref_no,
            'Amount' => $this->_formatMoney(1),
            'Signature' => $signature,
            'ProdDesc' => sprintf('%s @ RM%.2f', 'Tenancy Agreement', 1),
            'UserName' => trim(sprintf('%s', $name)),
            'UserEmail' => $email,
            'UserContact' => empty($mobile) ? '0' : $mobile,
            'MerchantCode' => $config['code'],
            'Currency' => $config['currency'],
            'Remark' => $this->_encryptString(sprintf('%d-%d', $order_id, $user_id)), // as a meta data for backend url to work correctly
            'ResponseURL' => route('payment.ipay88_response'),
            'BackendURL' => route('payment.ipay88_backend'),
        );

		
        $session_info = array(
            'UserName' => $data['UserName'],
            'UserEmail' => $data['UserEmail'],
            'UserContact' => $data['UserContact'],
            'ProdDesc' => $data['ProdDesc'],

            'order_id' => $order_id,
			'user_id' => $user_id,
        );

//        if (Input::get('success_url')) {
//            $session_info['success_url'] = Input::get('success_url');
//        }
//        if (Input::get('failure_url')) {
//            $session_info['failure_url'] = Input::get('failure_url');
//        }

        Session::put(SESS_PAYMENT_KEY, $session_info);

        return View::make('payment.redirect')
            ->with('data', $data)
            ->with('ipay88_url', $config['url'])
        ;
    }

    public function postIPay88Backend()
    {
        // file_put_contents(storage_path().'/logs/ipay88_backend.log', json_encode(Input::all(), JSON_PRETTY_PRINT));
        $ref = Input::get('RefNo', 0);

        $payment = Payment::getByRef($ref)->first();

        $is_new = false;
        if (!isset($payment->id)) { // if not yet exists

            $signature = $this->_iPay88_signature(array(
                Input::get('PaymentId', 0),
                $ref,
                $this->_formatMoneyWithNoCommaAndDot(Input::get('Amount', 0)),
                Config::get('nancy.ipay88.currency'),
                Input::get('Status', '0'),
            ));
            if ($signature != Input::get('Signature', 0)) exit;

            $is_new = true;

            $meta = $this->_decryptString(Input::get('Remark', ''));
            if (empty($meta)) exit;

            $meta_chunks = explode('-', $meta);
            $order_id = $meta_chunks[0];
            $user_id = $meta_chunks[1];

            $payment = new Payment();
            $payment->type = 'ipay';
            $payment->user_email = '';
            $payment->user_contact = '';
            $payment->order_id = $order_id;
            $payment->description = '';
            $payment->ref = $ref;
            $payment->created_on = date('Y-m-d H:i:s');
            $payment->created_by = $user_id;
        }

        $payment->payment_option = Input::get('PaymentId', 0);
        $payment->amount = Input::get('Amount', 0);
        $payment->error_desc = Input::get('ErrDesc', null);
        $payment->ipay88_trans_id = Input::get('TransId', null);
        $payment->status = Input::get('Status', 0);

        if ($payment->save()) {

            if ($is_new) {
                //$this->_sendSuccessMail($payment->id);

//                $package = Package::getById($payment->package_id)->first();
//
//                if (empty($payment->batch_id)) {
//                    $batch = new Batch();
//                    $batch->user_id = $user_id;
//                    $batch->created_on = date('Y-m-d H:i:s');
//                    $batch->created_by = $user_id;
//                    $batch->expired_on = date('Y-m-d', strtotime(sprintf('+%d days', $package->days)));
//                    $batch->status = 1;
//                    $batch->package_id=$package_id;
//
//                } else {
//                    $batch->package_id=$package_id;
//                    $batch = Batch::getById($payment->batch_id)->first();
//                    $batch->expired_on = date('Y-m-d', strtotime(sprintf('%s +%d days', $batch->expired_on, $package->days)));
//                }
//
//                $batch->save();
//
//                $payment->batch_id = $batch->id;
//                $payment->save();
            }
            echo 'RECEIVEOK';
        }
        exit;
    }

    public function postIPay88Response()
    {
        $ref = Input::get('RefNo', 0);

        $payment = Payment::getByRef($ref)->first();

        $is_new = false;

        if (!isset($payment->id)) { // if not yet exists
            $payment_info = Session::get(SESS_PAYMENT_KEY);
            if (empty($payment_info)) {
                return $this->_getFailureRoute()
                    ->with('alert.danger', 'Session expired.')
                ;
            }
            
            $signature = $this->_iPay88_signature(array(
                Input::get('PaymentId', 0),
                $ref,
                $this->_formatMoneyWithNoCommaAndDot(Input::get('Amount', 0)),
                Config::get('nancy.ipay88.currency'),
                Input::get('Status', '0'),
            ));
			
			echo 'Signature key from [_iPay88_signature]: ' . $signature;
			echo '<br/>';
			echo 'Signature key from [postIPay88Response]: ' . Input::get('Signature');
			echo '<br/>';
			echo 'Both signature key are not same. Payment failed. Error code: ' . Input::get('ErrDesc') ;
			die();
			
			if ($signature != Input::get('Signature', 0)) {
                return $this->_getFailureRoute()
                    ->with('alert.danger', 'Invalid signature.')
                ;
            }

            $is_new = true;

            $payment = new Payment();
            $payment->type = 'ipay';
            $payment->user_email = $payment_info['UserEmail'];
            $payment->user_contact = $payment_info['UserContact'];
            $payment->order_id = $payment_info['order_id'];
            $payment->description = $payment_info['ProdDesc'];
            $payment->ref = $ref;
            $payment->created_on = date('Y-m-d H:i:s');
            $payment->created_by = $payment_info['user_id'];//Auth::user()->id;
        }

        $payment->payment_option = Input::get('PaymentId', 0);
        $payment->amount = Input::get('Amount', 0);
        $payment->error_desc = Input::get('ErrDesc', null);
        $payment->ipay88_trans_id = Input::get('TransId', null);
        $payment->status = Input::get('Status', 0);

        if ($payment->save()) {
            if ($payment->status != 1) {
                return $this->_getFailureRoute()
                    ->with('alert.danger', $payment->error_desc);
                ;
            }

            //if ($is_new) $this->_sendSuccessMail($payment->id);

//            $package = Package::getById($payment->package_id)->first();
//
//            if (empty($payment->batch_id)) {
//                $batch = new Batch();
//                $batch->user_id = Auth::user()->id;
//                $batch->created_on = date('Y-m-d H:i:s');
//                $batch->created_by = Auth::user()->id;
//                $batch->expired_on = date('Y-m-d', strtotime(sprintf('+%d days', $package->days)));
//                $batch->status = 1;
//                $batch->package_id=$payment->package_id;
//            } else {
//                $batch = Batch::getById($payment->batch_id)->first();
//                $batch->expired_on = date('Y-m-d', strtotime(sprintf('%s +%d days', $batch->expired_on, $package->days)));
//                $batch->package_id=$payment->package_id;
//            }
//
//            $batch->save();
//
//            $payment->batch_id = $batch->id;
//            $payment->save();

            // success
            if (isset($payment_info['success_url'])) {
                return Redirect::to($payment_info['success_url'])
                    ->with('alert.success', 'Thank you for payment.')
                ;
            }

            return Redirect::route('payment.success', array('id' => $payment->id))
                ->with('alert.success', 'Thank you for payment.')
            ;
        }

        return $this->_getFailureRoute()
            ->with('alert.danger', 'Payment failure.')
        ;
    }

 
    public function postSdkIPay88Backend() {
        $config = Config::get('nancy.ipay88');
        $merchantkey=$config['key'];

        $ref = Input::get('RefNo', 0);
        $merchantcode= Input::get('MerchantCode');
        $paymentid= Input::get('PaymentId');
        $amount= Input::get('Amount');
        $ecurrency= Input::get('Currency');
        $remark= Input::get('Remark');
        $transid= Input::get('TransId'); 
        $authcode= Input::get('AuthCode');
        $status = Input::get('Status');
        $errdesc= Input::get('ErrDesc');
        $signature= Input::get('Signature');

        $file=storage_path().'/logs/ipay88_backend.log';
        $current = file_get_contents($file);
        $current .="\n ============================\n";
        $current .= json_encode(Input::all(), JSON_PRETTY_PRINT)."\n";
        file_put_contents($file, $current);

        Log::useDailyFiles(storage_path().'/logs/ipay.log');
        Log::info('\n ref:'.$ref.'\n=======================\n'.'paymentid:'.$paymentid.'\n'.'signature:'.$signature.'\n'.'status:'.$status.'\n'.'errdesc:'.$errdesc.'\n');

        $payment = Payment::getByRef($ref)->first();
        $order_id=null;
        $is_new = false;
        if (!isset($payment->id)) { // if not yet exists

            $signature = $this->_iPay88_signature(array(
                Input::get('PaymentId', 0),
                $ref,
                $this->_formatMoneyWithNoCommaAndDot(Input::get('Amount', 0)),
                Config::get('nancy.ipay88.currency'),
                Input::get('Status', '0'),
                ));
            if ($signature != Input::get('Signature', 0)) exit;

            $is_new = true;

            $meta = $this->_decryptString(Input::get('Remark', ''));
            if (empty($meta)) exit;

            $meta_chunks = explode('-', $meta);
            $order_id = $meta_chunks[0];
            $user_id = $meta_chunks[1];
            $user=User::find($user_id);
            if($user) {
                $userName=$user->first_name.$user->last_name;
                $userEmail=$user->email;
                $userContact=$user->mobile;
            } else {
                $userName='';
                $userEmail='';
                $userContact='';
            }
            $payment = new Payment();
            $payment->type = 'ipay';
            $payment->user_email = '';
            $payment->user_contact = '';
            $payment->order_id = $order_id;
            $payment->description = '';
            $payment->ref = $ref;
            $payment->order_id = $order_id;
            $payment->created_on = date('Y-m-d H:i:s');
            $payment->user_id = $user_id;
            $payment->user_email =$userEmail;
            $payment->user_contact = $userContact;
        }

        $payment->payment_option = $paymentid;
        $payment->amount = $amount;
        $payment->error_desc = $errdesc;
        $payment->ipay88_trans_id = $transid;
        $payment->status = $status;

        if ($payment->save()) {

            $order = Order::find($order_id);
            if($order){
                $order->tenancyAgreementStatus='Done';
                $order->save();

                $order_queue = new OrderPushQueue;
                $order_queue->order_id = $order->id;
                $order_queue->type=TYPE_ADMIN_ORDER_RECEIVED;
                $order_queue->save();
            }

            $current = file_get_contents($file);

            $current .= "'RECEIVEOK' \n";
            file_put_contents($file, $current);

            echo 'RECEIVEOK';
        }
        exit;
    }
    

    public function postSdkIPay88Estamping() {

        $config = Config::get('nancy.ipay88');
        $merchantkey=$config['key'];

        $ref = Input::get('RefNo', 0);
//        $quantity= Input::get('quantity','1');
        $quantity=substr($ref, strrpos($ref, '-') + 1);
        $merchantcode= Input::get('MerchantCode');
        $paymentid= Input::get('PaymentId');
        $amount= Input::get('Amount');
        $ecurrency= Input::get('Currency');
        $remark= Input::get('Remark');
        $transid= Input::get('TransId'); 
        $authcode= Input::get('AuthCode');
        $status = Input::get('Status');
        $errdesc= Input::get('ErrDesc');
        $signature= Input::get('Signature');

        $file=storage_path().'/logs/ipay88_backend.log';
        $current = file_get_contents($file);
        $current .="\n ============================\n";
        $current .= json_encode(Input::all(), JSON_PRETTY_PRINT)."\n";
        file_put_contents($file, $current);

        Log::useDailyFiles(storage_path().'/logs/ipay.log');
        Log::info('\n ref:'.$ref.'\n=======================\n'.'paymentid:'.$paymentid.'\n'.'signature:'.$signature.'\n'.'status:'.$status.'\n'.'errdesc:'.$errdesc.'\n');

        $payment = Payment::getByRef($ref)->first();
        $order_id=null;
        $is_new = false;
        $meta = $this->_decryptString(Input::get('Remark', ''));
        if (empty($meta)) exit;


        $meta_chunks = explode('-', $meta);
        $order_id = $meta_chunks[0];
        $user_id = $meta_chunks[1];



        if (!isset($payment->id)) { // if not yet exists

            $signature = $this->_iPay88_signature(array(
                Input::get('PaymentId', 0),
                $ref,
                $this->_formatMoneyWithNoCommaAndDot(Input::get('Amount', 0)),
                Config::get('nancy.ipay88.currency'),
                Input::get('Status', '0'),
                ));
            if ($signature != Input::get('Signature', 0)) exit;

            $is_new = true;


            $user=User::find($user_id);
            if($user){
                $userName=$user->first_name.$user->last_name;
                $userEmail=$user->email;
                $userContact=$user->mobile;
            }else{
                $userName='';
                $userEmail='';
                $userContact='';
            }
            $payment = new Payment();
            $payment->type = 'ipay';
            $payment->user_email = '';
            $payment->user_contact = '';
            $payment->order_id = $order_id;
            $payment->description = '';
            $payment->ref = $ref;
            $payment->order_id = $order_id;
            $payment->created_on = date('Y-m-d H:i:s');
            $payment->user_id = $user_id;
            $payment->user_email =$userEmail;
            $payment->user_contact = $userContact;
        }

        $payment->payment_option = $paymentid;
        $payment->amount = $amount;
        $payment->error_desc = $errdesc;
        $payment->ipay88_trans_id = $transid;
        $payment->status = $status;

        if ($payment->save()) {

            $order = Order::find($order_id);
            if($order){

                $order->estamping_payment=1;
                $order->estamping_payment_record=$payment->id;
                $order->estamping_duplicate= $quantity;
                $order->estamping_status='Received';
                $order->eStampStatus='Received';
                $order->save();

                $orderStatus = new OrderStatus();
                $orderStatus->order_id = $order->id;
                $orderStatus->status = $order->estamping_status;
                $orderStatus->type='estamping';
                $orderStatus->save();

                $order_queue = new OrderPushQueue;
                $order_queue->order_id = $order->id;
                $order_queue->type=TYPE_ADMIN_STAMPING_REQUESTED;
                $order_queue->save();   
            }

            $current = file_get_contents($file);

            $current .= "'RECEIVEOK' \n";
            file_put_contents($file, $current);

            echo 'RECEIVEOK';
        }
        exit;
    }
    
    public function getSuccess($id)
    {
        $language = Session::get('language', 'en');
        $this->appendScript('/assets/js/payment.js', 'inline');

        $payment = Payment::find($id);

        if (empty($payment)) {
            if($language == 'zh')
            {
                $err_msg = '未寻获此付费记录。';
            }
            else{
                $err_msg = 'Payment not found.';
            }
            return Redirect::route('payment.failed')
                ->withErrors($err_msg)
            ;
        }else{
			$user = User::find($payment->created_by);
		}

        $this->layout->body = View::make('payment.success')
            //->with('user', Auth::user())
			->with('user', $user)
            ->with('payment', $payment)
        ;
    }
	
    public function getFailed()
    {
        $this->appendScript('/assets/js/payment.js', 'inline');

        //$this->layout->body = View::make('payment.failed');
		return View::make('payment.failed')
        ;
    }

    // helpers
    private function _getSuccessRoute()
    {
        $payment_info = Session::get(SESS_PAYMENT_KEY);

        if (!empty($payment_info) && isset($payment_info['success_url'])) {
            return Redirect::to($payment_info['success_url']);
        }

        return Redirect::route('payment.success', array('id' => $payment->id));
    }

    private function _getFailureRoute()
    {
        $payment_info = Session::get(SESS_PAYMENT_KEY);

        //if (!empty($payment_info) && isset($payment_info['failure_url'])) {
        //    return Redirect::to($payment_info['failure_url']);
        //}

        return Redirect::route('payment.failed');
    }

    private function _sendSuccessMail($id = 0) // payment ID
    {
        try {
            $payment = Payment::find($id);

            $price_b4_gst = $payment->amount / 1.06;

            $data = [
                'payment' => $payment,
                'price_b4_gst' => $price_b4_gst,
            ];

            $filepath = storage_path() . '/files/receipt/';
            if (!File::exists($filepath)) File::makeDirectory($filepath, 0755, true);

            $file = $filepath . '/receipt-'.$payment->ref.'.pdf';

            $pdf = PDF::loadView('subscription.pdf.receipt', $data);
            $pdf->save($file);

            Mail::send('payment.email.success', array(
                'payment' => $payment,
            ), function($message) use ($payment, $file) {
                $message
                    ->to($payment->user->email, $payment->user->first_name . ' ' . $payment->user->last_name)
                    ->subject($payment->description . ' - Lesys Tenancy')
                ;
                $message->attach($file);
            });

        } catch (Exception $e) {
        }
    }

    private function _generateRefNo()
    {
        return sprintf('WDL%d%d', time(), rand(10*45, 100*98));
    }

    private function _formatMoney($amount)
    {
        return number_format($amount, 2, '.', ',');
    }

    private function _formatMoneyWithNoCommaAndDot($amount)
    {
        $amt = $this->_formatMoney($amount);
        return str_replace(array(',', '.'), '', $amt . '');
    }

    public function _iPay88_signature($items)
    {
        $config = Config::get('nancy.ipay88');
        $source = sprintf('%s%s%s', $config['key'], $config['code'], implode('', $items));
        return base64_encode($this->_hex2bin(sha1($source)));
    }

    private function _hex2bin($hexSource)
    {
        $bin = '';
        for ($i=0;$i<strlen($hexSource);$i=$i+2) {
            $bin .= chr(hexdec(substr($hexSource,$i,2)));
        }
        return $bin;
    }

    // RSA
    public function _encryptString($plain_text)
    {
        // read the public key
        $public_key = openssl_pkey_get_public(file_get_contents(app_path().'/rsa/public_key.pem'));
        $public_key_details = openssl_pkey_get_details($public_key);
        // there are 11 bytes overhead for PKCS1 padding
        $encrypt_chunk_size = ceil($public_key_details['bits'] / 8) - 11;
        $output = '';
        // loop through the long plain text, and divide by chunks
        while ($plain_text) {
            $chunk = substr($plain_text, 0, $encrypt_chunk_size);
            $plain_text = substr($plain_text, $encrypt_chunk_size);
            $encrypted = '';
            if (!openssl_public_encrypt($chunk, $encrypted, $public_key))
                return false;
            $output .= $encrypted;
        }
        openssl_free_key($public_key);
        return base64_encode($output);
    }

    private function _decryptString($cipher_text)
    {
        // decode the text to bytes
        $encrypted = base64_decode($cipher_text);

        // read the private key
        $private_key = openssl_pkey_get_private(file_get_contents((app_path().'/rsa/private_key.pem')));
        $private_key_details = openssl_pkey_get_details($private_key);

        // there is no need to minus the overhead
        $decrypt_chunk_size = ceil($private_key_details['bits'] / 8);
        $output = '';

        // decrypt it back chunk-by-chunk
        while ($encrypted) {
            $chunk = substr($encrypted, 0, $decrypt_chunk_size);
            $encrypted = substr($encrypted, $decrypt_chunk_size);
            $decrypted = '';
            if (!openssl_private_decrypt($chunk, $decrypted, $private_key))
                return false;
            $output .= $decrypted;
        }
        openssl_free_key($private_key);
        return $output;
    }
	
	public function PromoCode()
    {
		$this->layout->body = View::make('promocode.form')
		->with('user', Auth::user())
		;
}
	
	public function getPromoCode(){
		$isPhoneVerify=Auth::user()->isPhoneVerify;
		if($isPhoneVerify == 'false'){
			if(!empty(Auth::user()->tempMobile)){
				$sendMobile = Auth::user()->tempMobile;
			}else{
				$sendMobile = Auth::user()->mobile;
			}
			if(substr($sendMobile, 0, 1) == "0"){
				if(Auth::user()->country_id != ''){
					$sendMobile = Auth::user()->country->mobile_code . substr($sendMobile, 1);
				}
			}
			$isPhoneVerify = false;
            return Response::json(array('result'=>true, 'mobileVerify'=>$isPhoneVerify, 'mobile'=>$sendMobile));
        }else{
			$isPhoneVerify = true;
			$promoCode = Input::get('code');
			$promo = PromoCodes::where('promo_code', $promoCode)->first();
            if(!$promo){
                return Response::json(array('result'=>false, 'msg'=>'PromoID Not Found'));
            }else{
				return Response::json(array('result'=>true, 'mobileVerify'=>$isPhoneVerify, 'code'=>$promoCode, 'package'=>$promo->package->name));
			}
            
           
		}
	}
//    public function getPromoCode()
//    {
//        $promoCode = Input::get('code');
// 
// 
//        $rules = array(
//            'code' => 'required',
//        );
//        
//        $validator = Validator::make(Input::all(), $rules);
//        
//        if ($validator->fails()) {
//            return Redirect::route('promocode.me')
//                ->withErrors($validator)
//                ;
//        }else{
//            $promo = PromoCodes::where('promo_code', $promoCode)->first();
//            if(!$promo){
//                return Redirect::route('promocode.me')
//                ->withErrors('PromoID Not Found')
//                ;
//            }
//            
//            $this->layout->body = View::make('promocode.form')
//            ->with('user', Auth::user())
//            ->with('promo', $promo)
//            ;
//        }
//    }
    
    public function redeemPromoItem()
    {
        $promoCode=Input::get('hiddenCode');
        $currentUser=Auth::user();
                
        $promo = PromoCodes::where('promo_code', $promoCode)->first();
        if(!$promo){
            return Redirect::route('promocode.me')
            ->withErrors('PromoID Not Found')
            ;
        }
        
        $isRedeemed = PromoCodeRedeem::ApiGetExisting($promo->id,$currentUser->id)->first();
        if($isRedeemed){
            return Redirect::route('promocode.me')
            ->withErrors('Promotion code has been redeemed')
            ;
        }
        
        $package=$promo->package;
        $batch = new Batch();
        $batch->user_id = $currentUser->id;
        $batch->created_on = date('Y-m-d H:i:s');
        $batch->created_by = $currentUser->id;
        $batch->expired_on = date('Y-m-d', strtotime(sprintf('+%d days', $package->firstExpiredDays)));
        $batch->status = 1;
        $batch->package_id=$package->id;
        $batch->paymentType=$package->billingType;
        
        if($batch->save()){
            $payment= new Payment();
            $payment->type = 'PROMO_REDEEM';
            $payment->user_email = $currentUser->email;
            $payment->package_id = $package->id;
            $payment->batch_id = $batch->id;
            $payment->ref = $promo->promo_code;
            $payment->created_by = $currentUser->id;
            $payment->amount = $package->price;
            $payment->status = 1;
            $payment->save();
        }
        
        $redeem_now=new PromoCodeRedeem();
        $redeem_now->user_id=$currentUser->id;
        $redeem_now->promo_codes_id=$promo->id;
        $redeem_now->batch_id=$batch->id;
        $redeem_now->payment_id=$payment->id;
        $redeem_now->save();
        
        return Redirect::route('promocode.me')
        ->with('alert.success', 'Promotion code has been successfully redeem.')
        ;
    }
	
	public function getPromos()
    {    
        $col = Input::get('col', 'created_on');
         $sort = Input::get('sort', 'asc');
         $keyword = Input::get('keyword', '');
         
        $daterange = Input::get('daterange', '01/01/2015 - '.date('d/m/Y'));
        $start = '01/01/2015';
        $end = date('d/m/Y');
        if (!empty($daterange)) {
           list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }
        
          $promo = PromoCodes::getByKeyword($keyword)
            ->where('promo_codes.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
            ->where('promo_codes.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))     
        ->orderBy($col, $sort)
        ->paginate(20)
        ;
        
         $this->layout = View::make('layout.admin');
         
         $this->appendScript('/assets/js/moment.js', 'inline');
        $this->appendScript('/assets/js/bootstrap-daterangepicker/daterangepicker.js', 'inline');
        $this->appendStyle('/assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css', 'inline');
        $this->appendScript('/assets/js/user.js', 'inline');
        
         $this->layout->body = View::make('promocode.admin.index')
            ->with('keyword', $keyword)
            ->with('sort', $sort)
            ->with('col', $col)
            ->with('promo', $promo)
            ->with('daterange', $daterange)
         ;
            
    }
    
    public function getPromo($id = 0){
		if (empty($id)) {
            $promo = NULL;
        } else {
            $promo = PromoCodes::getById($id)
            ->first()
            ;
            if (!isset($promo->id)) {
                return Redirect::route('promocode.list')
                ->with('alert.danger', 'Promo code not found.')
                ;

            }
        }
		
        $resultset = User::select('user.id', 'user.first_name', 'user.last_name')
			->where('status', '=', '1')
			->orderBy('first_name', 'asc')
            ->get()
            ;
        $agent = array('' => 'Please select');
        foreach ($resultset as $rowset) {
            $agent[$rowset->id] = $rowset->first_name . ' ' . $rowset->last_name;
        }
		
		$result_package = Package::where('isPromo', '=', 'true')
		->where('category', 'property')
		->get()
		;
        $package = array('' => 'Select package');
        foreach ($result_package as $rowset) {
            $package[$rowset->id] = $rowset->name;
        }
        
        $this->layout = View::make('layout.admin');
        $this->layout->body = View::make('promocode.admin.form')
		->with('promo', $promo)
		->with('agent', $agent)
		->with('package', $package)
		;
    }
    
    public function addPromo($id = 0)
    {  
        $rules = array(
             'promo_code' => 'required',
             'agent' => 'required',
			 'package' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
           	if (empty($id)){

				return Redirect::route('promocode.add')
						->withErrors($validator)
				;
			}
			else{
				return Redirect::route('promocode.edit', array('id' => $id))
						->withErrors($validator)
				;
			}
        }
        
        $promo = PromoCodes::getById($id)
        ->first();
        $promo = empty($promo->id) ? new PromoCodes : $promo;
        $promo -> promo_code = Input::get('promo_code');
        $promo -> remark = Input::get('remark', '');
        $promo -> user_id = Input::get('agent');
		$promo -> package_id = Input::get('package');
        $promo -> status = Input::get('status');

        if($promo->save()) {
           //success
		   if (empty($id)){
				return Redirect::route('promocode.add')
				->with('alert.success', 'Promo code has been created');
		   }else{
				return Redirect::route('promocode.edit', array('id' => $id))
				->with('alert.success', 'Promo code has been updated');
				;
		   }

        }else{
			if (empty($id)){
				return Redirect::route('promocode.add')
				->with('alert.danger', 'Create failed.');
			}else{
				return Redirect::route('promocode.edit', array('id' => $id))
				->with('alert.danger', 'Update failed.');
				;
			}
        }
      
      
    }
    
    public function getMemberList()
    {   
        $id = Input::get('promoid');
        if(empty($id))
        {
            return Redirect::route('promocode.list')
                ->with('alert.danger', 'No such id found.');
        }
        else
        {
            $col = Input::get('col', 'created_on');
            $sort = Input::get('sort', 'asc');
            $keyword = Input::get('keyword', '');

            $daterange = Input::get('daterange', '01/01/2015 - '.date('d/m/Y'));
            $start = '01/01/2015';
            $end = date('d/m/Y');
            if (!empty($daterange)) {
               list($start, $end) = array_map('trim', explode('-', $daterange));
            } else {
                $daterange = $start . ' - ' . $end;
            }
            
            $promo_name = PromoCodes::where('id', '=', $id)->first()->promo_code;
            $promo = PromoCodeRedeem::GetMemberByKeyword($id, $keyword)
                ->where('promo_user_redeem.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                ->where('promo_user_redeem.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))     
            ->orderBy($col, $sort)
            ->paginate(20)
            ;

             $this->layout = View::make('layout.admin');
             $this->appendScript('/assets/js/moment.js', 'inline');
            $this->appendScript('/assets/js/bootstrap-daterangepicker/daterangepicker.js', 'inline');
            $this->appendStyle('/assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css', 'inline');
            $this->appendScript('/assets/js/user.js', 'inline');
             $this->layout->body = View::make('promocode.admin.memberlist')
                ->with('keyword', $keyword)
                ->with('sort', $sort)
                ->with('col', $col)
                ->with('promo', $promo)
                ->with('promo_name', $promo_name)
                ->with('daterange', $daterange)
             ;
        }
            
    }
    

   public function apiUpdateAndroidPurchase()
    {
       
       
       $platform=Input::get('platform');

       $purchaseToken=Input::get('purchaseToken');
       $packageID    =Input::get('packageID');
       $purchaseEmail=Input::get('purchaseEmail');
       $orderID      =Input::get('orderID');
       $description  =Input::get('description',"");
       $remark  =Input::get('remark');
       $batchID =Input::get('batchID',0);
       $prospectBatchID =Input::get('prospectID',0);
       
          if($platform == 'android')
                $paymentType = 'ANDROID_INAPP';
            else if($platform == 'ios')
                $paymentType = 'IOS_INAPP';
       
       $jsonResponse = new JsonResponse();
        
       $package = Package::getById($packageID)->first();
	   
	   $duplicate_token = Payment::getByToken($purchaseToken)->first();
	   
	   if($duplicate_token){
	   
	   		$jsonResponse->setCode(404);
       //           $jsonResponse->setSubject('Duplicate Error');
       //           $jsonResponse->setBody('Duplicate Purchase Token Record Found.');
                  
                    return $jsonResponse->get();
			
	   }
	   
        $payment = new Payment();

        if(!$package){
                     
                  $jsonResponse->setCode(404);
                  $jsonResponse->setSubject('Not Found');
                  $jsonResponse->setBody('Package not found.');
                  
                    return $jsonResponse->get();
            }
        
            
           
            if($package->category=='prospect'){
                
                if(!isset($prospectBatchID) || empty($prospectBatchID)){
                        $batch = new ProspectBatch();
                        $batch->user_id = Auth::user()->id;
                        $batch->created_on = date('Y-m-d H:i:s');
                        $batch->created_by = Auth::user()->id;
                        $batch->expired_on = date('Y-m-d', strtotime(sprintf('+%d days', $package->firstExpiredDays)));
                        $batch->status = 1;
                        $batch->package_id=$packageID;

                }else{

                        $batch = ProspectBatch::getById($prospectBatchID)->first();
                        $batch->expired_on = date('Y-m-d', strtotime(sprintf('%s +%d days', $batch->expired_on, $package->days)));
                        $batch->package_id=$packageID;
                        $batch->status = 1;
                        $batch->paymentType=$package->billingType;


                        $oldPayment = Payment::
                                where('payment.type', $paymentType)
                                ->where('payment.status',1)
                                ->where('payment.prospect_batch_id',$prospectBatchID)
                                ->orderBy('created_on','desc')
                                ->first();

                        $oldPayment->status=3; //upgraded
                        $oldPayment->save();

                        $payment->renew_paymentid = $oldPayment->id;


                    }
                    
                    $batch->save();
                    $prospectBatchID=$batch->id;
            }else{
                                
                 if(!isset($batchID) || empty($batchID)){
                        $batch = new Batch();
                        $batch->user_id = Auth::user()->id;
                        $batch->created_on = date('Y-m-d H:i:s');
                        $batch->created_by = Auth::user()->id;
                        $batch->expired_on = date('Y-m-d', strtotime(sprintf('+%d days', $package->firstExpiredDays)));
                        $batch->status = 1;
                        $batch->package_id=$packageID;
                        $batch->paymentType=$package->billingType;

                }else{

                        $batch = Batch::getById($batchID)->first();
                        $batch->expired_on = date('Y-m-d', strtotime(sprintf('%s +%d days', $batch->expired_on, $package->days)));
                        $batch->package_id=$packageID;
                        $batch->status = 1;
                        $batch->paymentType=$package->billingType;


                        $oldPayment = Payment::
                                where('payment.type', $paymentType)
                                ->where('payment.status',1)
                                ->where('payment.batch_id',$batchID)
                                ->orderBy('created_on','desc')
                                ->first();

                        $oldPayment->status=3; //upgraded
                        $oldPayment->save();

                        $payment->renew_paymentid = $oldPayment->id;


                    }
            
                $batch->save();
                $batchID=$batch->id;
            }
            
           
           
            
            if($platform == 'android')
                $payment->type = 'ANDROID_INAPP';
            else if($platform == 'ios')
                $payment->type = 'IOS_INAPP';

            $payment->user_email = $purchaseEmail;
            $payment->package_id = $packageID;
            
            $payment->batch_id = $batchID;
            $payment->prospect_batch_id = $prospectBatchID;
			
            if($platform == 'android')
                $payment->android_order_id=$orderID;
            else if($platform == 'ios')
                $payment->ios_order_id=$orderID;
			
            $payment->ref = $purchaseToken;
            $payment->description = $description;
            $payment->remark = $remark;
            $payment->created_on = date('Y-m-d H:i:s');
            $payment->created_by = Auth::user()->id;
            $payment->amount = $package->price;
            $payment->status = 1;

        $payment->save();
            
        
                $jsonResponse->setCode(200);
                $jsonResponse->setSubject('Success');
                $jsonResponse->setBody('Purchase is updated.'); 
  

        return $jsonResponse
            ->get()
        ;
    }

    
    
     public function apiCancelAndroidPurchase()
    {

       $purchaseToken=Input::get('purchaseToken');
       
       $jsonResponse = new JsonResponse();
        
       $payment = Payment::getByRef($purchaseToken)->first();
      
       
        if(!$payment){
                     
                  $jsonResponse->setCode(404);
                  $jsonResponse->setSubject('Not Found');
                  $jsonResponse->setBody('Payment not found.');
                  
                    return $jsonResponse->get();
            }
        
        $payment->status=2; //Canceled    
        $payment->save();
            
        $batch = Batch::getById($payment->batch_id)->first();
        $batch->status=2; //canceled

        
                $jsonResponse->setCode(200);
                $jsonResponse->setSubject('Success');
                $jsonResponse->setBody('Purchase status is updated.'); 
  

        return $jsonResponse
            ->get()
        ;
    }
    

       public function apiCheckAndroidPurchase($platform)
    {
       $jsonResponse = new JsonResponse();
       
        if($platform == 'android')
                $paymentType = 'ANDROID_INAPP';
            else if($platform == 'ios')
                $paymentType = 'IOS_INAPP';
           else{
                  $jsonResponse->setCode(404);
                  $jsonResponse->setSubject('Not Found');
                  $jsonResponse->setBody('Platform not found.');
                  
                    return $jsonResponse->get();
           }
               
           $payments = Payment::
			where('payment.type', $paymentType)
                        ->join('package', 'package.id', '=', 'payment.package_id')
                        ->where('payment.created_by',Auth::user()->id)
                        ->where('payment.status',1)
                        ->select('package.*','payment.*','payment.id As paymentID')

			->get();
           
        $response = array();
        
          foreach($payments as $payment) {

			if($platform == 'android'){
				$order_id = $payment->android_order_id;
			}elseif($platform == 'ios'){
				$order_id = $payment->ios_order_id;
			}else{
				$order_id = $payment->android_order_id;
			}
            
            $response[] = array(
                'paymentID' => $payment->id,
                'packageID' => $payment->package_id,
                'purchaseToken' => $payment->ref,
                'packageName' => $payment->name,
                'slots' => $payment->slots,
                'skuID' => $payment->skuID,
                'batchID'=>$payment->batch_id,
                'orderID' => $order_id,
                'purchaseOn' => $payment->created_on
            );

        }
        $jsonResponse->setResponse($response);

        return $jsonResponse
            ->get()
        ;
    }

    
   public function apiCheckPurchaseDetails()
    {
       $jsonResponse = new JsonResponse();
       
 
        $platform=Input::get('platform');
        $batchID=Input::get('batchID');
        
        $timeStamp=time();
        
        if($platform == 'android')
                $paymentType = 'ANDROID_INAPP';
            else if($platform == 'ios')
                $paymentType = 'IOS_INAPP';
           else{
                  $jsonResponse->setCode(404);
                  $jsonResponse->setSubject('Not Found');
                  $jsonResponse->setBody('Platform not found.');
                  
                    return $jsonResponse->get();
           }
               
           $payment = Payment::
			where('payment.type', $paymentType)
                        ->join('package', 'package.id', '=', 'payment.package_id')
                        ->join('batch', 'batch.id', '=', 'payment.batch_id')
                        ->where('payment.created_by',Auth::user()->id)
                        ->where('payment.batch_id',$batchID)
                        ->select('package.*','payment.*','payment.id As paymentID','batch.expired_on','batch.id as batchID')
                        ->first();
           
 
       if(!$payment){
            $jsonResponse->setCode(404);
                  $jsonResponse->setSubject('Not Found');
                  $jsonResponse->setBody('Payment not found.');
                  
                    return $jsonResponse->get();
       }
       
       $expiredDate=$payment->expired_on;
       $expired_on=strtotime($expiredDate);
       
       
       if($timeStamp >= $expired_on){
           
            if($platform == 'android'){

                 $subscription=$this->getAndroidBilling($payment->skuID, $payment->ref);


                          $batch=Batch::where('id',$batchID);

                          if(isset($subscription->expiryTimeMillis)){

                              $seconds = $subscription->expiryTimeMillis / 1000;
                              $expiredDate= date("Y-m-d", $seconds);

                              $batch->update(array('expired_on' => $expiredDate));
                          }

                           if($subscription->autoRenewing == false){
                                  $payment->status=2; //Canceled 
                                  $payment->save();

                                  $batch->update(array('status' => 2)); //Canceled

                              }

           }
       }
       


       

          $response=array(
            
                 'android'=> isset($subscription)?array(
                    'autoRenewing'=>$subscription->autoRenewing,
                    'expiryTimeMillis'=>$subscription->expiryTimeMillis,
                    'kind'=>$subscription->kind,
                    'startTimeMillis'=>$subscription->startTimeMillis
                  ):'',
                  'batchID'=>$payment->batchID,
                  'paymentID' => $payment->id,
                  'packageID' => $payment->package_id,
                  'purchaseToken' => $payment->ref,
                  'packageName' => $payment->name,
                  'slots' => $payment->slots,
                  'skuID' => $payment->skuID,
                  'amount' => $payment->amount,
                  'paymentType' => $payment->type,
                  'expiredDate' => $expiredDate,
                  'today_milis'=>$timeStamp,
                  'expired_milis'=>$expired_on,
                );

            $jsonResponse->setResponse($response);
            
        return $jsonResponse->get();
        
    } 
    

    
   public function apiSubmitPromoCode()
    {
       $jsonResponse = new JsonResponse();
       
 
        $promoCode=Input::get('promoCode');
        $currentUser=Auth::user();
                
        if(empty($promoCode)){
            
                  $jsonResponse->setCode(400);
                  $jsonResponse->setSubject('PromoCode is Empty');
                  $jsonResponse->setBody('PromoCode is Empty');
                  
                    return $jsonResponse->get();
           }
       
       $promo=  PromoCodes::ApiGetByPromoCode($promoCode)->first();
       
       if(!$promo){
            
                  $jsonResponse->setCode(400);
                  $jsonResponse->setSubject('PromoCode Not Found');
                  $jsonResponse->setBody('PromoCode Not Found');
                  
                    return $jsonResponse->get();
           }
           
         $package=$promo->package;
            
           $isExiting=PromoCodeRedeem::ApiGetExistingByCategory($promo->id,$currentUser->id,$package->category)->first();

           $isRedeemed=false;
         if($isExiting){
            
                  $isRedeemed=true;
           }    
           
        $package=$promo->package;

          $response= array(
                            'promoId'=>(int)$promo->id,
                            'promoCode'=>$promo->promo_code,
                            'remark'=>$promo->remark,
                            'isRedeemed'=>$isRedeemed,
                            'user'=>$this->userToJson($promo->user,""),
                            'me'=>$this->userToJson($currentUser,""),
                            'package'=>array(
                                'id' => $package->id,
				'name' => $package->name,
				'price' => $package->price,
				'usd_price' => $package->usd_price,
				'slots' => $package->slots,
				'days' => $package->firstExpiredDays,
                                'skuID' => $package->skuID,
				'iosProductId' => $package->iosProductId,
                                'packageType' => $package->type,
                                'billingType' => $package->billingType
                              )		
                );

            $jsonResponse->setResponse($response);
            
        return $jsonResponse->get();
        
    }
    

   public function apiRedeemPromoItem()
    {
       $jsonResponse = new JsonResponse();
       
 
        $promoId=Input::get('promoId');
        $currentUser=Auth::user();
                
        if(empty($promoId)){
            
                  $jsonResponse->setCode(400);
                  $jsonResponse->setSubject('PromoID is Empty');
                  $jsonResponse->setBody('PromoID is Empty');
                  
                    return $jsonResponse->get();
           }
       
       $promo=  PromoCodes::find($promoId);
       $package=$promo->package;
       $packageID=$package->id;
       $prospectBatchID=null;
       $batchID=null;
       
       if(!$promo){
            
                  $jsonResponse->setCode(400);
                  $jsonResponse->setSubject('PromoID Not Found');
                  $jsonResponse->setBody('PromoID Not Found');
                  
                    return $jsonResponse->get();
           }
           
           $isExiting=PromoCodeRedeem::ApiGetExistingByCategory($promo->id,$currentUser->id,$package->category)->first();

         if($isExiting){
            // commit ad
                  $jsonResponse->setCode(400);
                  $jsonResponse->setSubject('Opss');
                  $jsonResponse->setBody('You can only redeem promotion code one time.');
                  
                  $jsonResponse->setResponse($isExiting);
                    return $jsonResponse->get();
           }    
           
        
            if($package->category=='prospect'){
                
                 $batch = new ProspectBatch();
                        $batch->user_id = Auth::user()->id;
                        $batch->created_on = date('Y-m-d H:i:s');
                        $batch->created_by = Auth::user()->id;
                        $batch->expired_on = date('Y-m-d', strtotime(sprintf('+%d days', $package->firstExpiredDays)));
                        $batch->status = 1;
                        $batch->package_id=$packageID;
                        
                        $batch->save();
                        $prospectBatchID=$batch->id;
            }else{
                                
                

                        $batch = new Batch();
                        $batch->user_id = Auth::user()->id;
                        $batch->created_on = date('Y-m-d H:i:s');
                        $batch->created_by = Auth::user()->id;
                        $batch->expired_on = date('Y-m-d', strtotime(sprintf('+%d days', $package->firstExpiredDays)));
                        $batch->status = 1;
                        $batch->package_id=$packageID;
                        $batch->paymentType=$package->billingType;
                        
            
                        $batch->save();
                        $batchID=$batch->id;
            }
        
                    $payment= new Payment();
                    $payment->type = 'PROMO_REDEEM';
                    $payment->user_email = $currentUser->email;
                    $payment->package_id = $package->id;
                    $payment->batch_id = $batch->id;
                    $payment->batch_id = $batchID;
                    $payment->prospect_batch_id = $prospectBatchID;
                    $payment->ref = $promo->promo_code;
                    $payment->created_by = $currentUser->id;
                    $payment->amount = $package->price;
                    $payment->status = 1;
                    $payment->save();
                    
                
              $redeem_now=new PromoCodeRedeem();
              $redeem_now->user_id=$currentUser->id;
              $redeem_now->promo_codes_id=$promo->id;
              $redeem_now->batch_id = $batchID;
              $redeem_now->prospect_batch_id = $prospectBatchID;
              $redeem_now->payment_id=$payment->id;
              $redeem_now->save();
                
        
          $response= array(
                            'promoId'=>(int)$promo->id,
                            'promoCode'=>$promo->promo_code,
                            'remark'=>$promo->remark,
                            'user'=>$this->userToJson($promo->user,""),
                            'package'=>array(
                                'id' => $package->id,
				'name' => $package->name,
				'price' => $package->price,
				'usd_price' => $package->usd_price,
				'slots' => $package->slots,
				'days' => $package->firstExpiredDays,
                                'skuID' => $package->skuID,
				'iosProductId' => $package->iosProductId,
                                'packageType' => $package->type,
                                'billingType' => $package->billingType
                              ),
                            'batch'=>array(
                                'id' => $batch->id,
                                'total' => ($batchID!=null)?$batch->property()->count():$batch->matchCriterias()->count(),
                                'paymentType' => $batch->paymentType,
                                'max' => $batch->package->slots,
                                'expired_on' => $batch->expired_on,
                                'created_on' => $batch->created_on,
                                'status' => $batch->status
                            )    
                );

            $jsonResponse->setResponse($response);
            
        return $jsonResponse->get();
        
    }  
    
    
   public function apiRemoveRedeemedItems()
    {
       $jsonResponse = new JsonResponse();
       
 
        $promoId=Input::get('promoId');
        $currentUser=Auth::user();
                
        if(empty($promoId)){
            
                  $jsonResponse->setCode(400);
                  $jsonResponse->setSubject('PromoID is Empty');
                  $jsonResponse->setBody('PromoID is Empty');
                  
                    return $jsonResponse->get();
           }
       
       $promo=  PromoCodes::find($promoId);
       
       if(!$promo){
            
                  $jsonResponse->setCode(400);
                  $jsonResponse->setSubject('PromoID Not Found');
                  $jsonResponse->setBody('PromoID Not Found');
                  
                    return $jsonResponse->get();
           }
           
           $isExiting=PromoCodeRedeem::ApiGetExisting($promo->id,$currentUser->id)->first();

         if($isExiting){
            
                  $isExiting->delete();
                  $jsonResponse->setSubject('Redeemed history deleted');
                  $jsonResponse->setBody('Redeemed history deleted');
                  
                  
           }else{
               $jsonResponse->setSubject('redeemed history not found.');
               $jsonResponse->setBody('Redeemed history not found.');
           }    
           
      
              
        return $jsonResponse->get();
        
    } 
    
    function getAndroidBilling($skuID,$ref){
        try {
         // create new Google Client
                 $client = new Google_Client();
                 // set Application Name to the name of the mobile app
                 $client->setApplicationName("Nancy");
                 // get p12 key file
                 $key = file_get_contents(app_path().'/../Nancy-c837ec42b46c.p12');
                 // create assertion credentials class and pass in:
                 // - service account email address
                 // - query scope as an array (which APIs the call will access)
                 // - the contents of the key file
                 $cred = new Google_Auth_AssertionCredentials(
                     '577941075664-4627tubc0d3ldnde7lb2jpfnknqotn4t@developer.gserviceaccount.com',
                     ['https://www.googleapis.com/auth/androidpublisher'],
                     $key
                 );
                 // add the credentials to the client
                 $client->setAssertionCredentials($cred);

                 // create a new Android Publisher service class
                 $service = new Google_Service_AndroidPublisher($client);

                 // set the package name and subscription id
                 $packageName = "com.wonderlist.property";
                 $subscriptionId =$skuID;

                 $purchaseToken=$ref;
     //            $purchaseToken="fphcdbgcbpghelchfliinfcg.AO-J1Ozg9yQALcQa9v-20I__OklrUbHv3J3Jd33r4jIPFlrbdEGHDJdEcPaRoUfImp2C4rnJEWSRLHTJ_i9MlZ9YR0pvxAWM8YFBZc7EYp6Tj7nzu6DF4wZb2DMFHOqoQst5dUP5EyboW1qhFGPowa3oNuLgnwBHEw";
                 // use the purchase token to make a call to Google to get the subscription info
                 $subscription = $service->purchases_subscriptions->get($packageName, $subscriptionId, $purchaseToken);
                 
                 
               if (is_null($subscription)) {
                         // query returned no data
//                         throw new ServerErrorException('Error validating transaction.', 500);


                     } elseif (isset($subscription['error']['code'])) {
                         // query returned an error
     //                    throw new ServerErrorException('Error validating transaction.', 500);


                     } elseif (!isset($subscription['expiryTimeMillis'])) {
                         // query did not return a subscription expiration time
//                         throw new ServerErrorException('Error validating transaction.', 500);
                     }
                     
                 return $subscription;


             } catch (Google_Auth_Exception $e) {
                 // if the call to Google fails, throw an exception
                 throw new Exception('Error validating transaction', 500);
             }

         }
}
