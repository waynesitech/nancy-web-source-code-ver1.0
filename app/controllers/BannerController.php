<?php

class BannerController extends BaseController
{

	protected $layout = 'layout.admin';

	//List all of the items
	public function getBanners()
	{

		$col = Input::get('col', 'name');
		$sort = Input::get('sort', 'asc');
		$keyword = Input::get('keyword', '');

		$banners = Banner::getByKeyword($keyword)
			->orderBy($col, $sort)
			->paginate(10)
		;

		$this->layout->body = View::make('banner.index')
			->with('keyword', $keyword)
			->with('banners', $banners)
			->with('col', $col)
			->with('sort', $sort)
		;
	}

    public function getCSV()
    {
        $banners = Banner::
            select(
                'banner.id',
                'banner.name',
                'banner.type',
                'banner.url',
                'banner.image',
                'banner.created_on',
                'user.email'
            )
            ->join('user', 'user.id', '=', 'banner.created_by')
            ->orderBy('banner.created_on', 'DESC')
            ->get()
        ;

        $output = "name,type,url,image,date,email\n";
        foreach ($banners as $row) {
            $output .=  '"' . $row->name . 
                        '","' . $row->type . 
                        '","' . $row->url . 
                        '","' . asset('files/banners' . $row->id . '/' . $row->image) . 
                        '","' . $row->created_on . 
                        '","' . $row->email . 
                        "\"\n";
        }
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="banners.csv"',
        );
     
        return Response::make($output, 200, $headers);



    }	

	//Display a single Banner
	public function getBanner($id = 0)
	{

		$this->appendScript('/assets/js/banner.js', 'inline');

		$options = array('' => 'Select one');

		$types = array(
			'highlight' => 'Highlight',
			'featured' => 'Featured'
			);

		if (empty($id)){
			$banner = NULL;
		}
		else{
			$banner = Banner::getRowById($id)
			->first()
			;
		}

		if(!empty($banner->image)){
			$banner->image = BANNER_IMAGE_URL . DIRECTORY_SEPARATOR . $banner->id . DIRECTORY_SEPARATOR . $banner->image;
		}

		//Pass info to the Form View
		$this->layout->body = View::make('banner.form')
		->with('banner', $banner)
		->with('types', $options + $types)
		;

	}

	public function postBanner($id = 0)
	{

		$validator = Validator::make(Input::all(), array(
			'name' => 'required',
			'image' => 'image',
			'type' => 'required',
			'url' => 'required'
		));

		if ($validator->fails()){

			if (empty($id)){

				return Redirect::route('banner.add')
					->withErrors($validator)
					->withInput(Input::except('image'))
				;
			}
			else{
				return Redirect::route('banner.edit', array('id' => $id))
					->withErrors($validator)
					->withInput(Input::except('image'))
				;
			}
		}
		else{
			//New Banner
			if(empty($id)){

				$banner = new Banner();

				$banner->name = Input::get('name', '');
				$banner->type = Input::get('type');

				if (Input::hasFile('image')){
					$banner->image = uniqid(time()) . '.' . Input::file('image')->getClientOriginalExtension();
				}
				$banner->url = Input::get('url');
				$banner->created_on = date('Y-m-d H:i:s');
				$banner->created_by = Auth::user()->id;
				$banner->status = Input::get('status');

				if($banner->save()){
					if (Input::hasFile('image')) {
		                if(!File::exists(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id)) @mkdir(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id);             
		                $img = Image::make(Input::file('image'))
		                    ->fit(640, 480)
		                    ->save(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id . DIRECTORY_SEPARATOR . $banner->image)
		                ;  

						// Input::file('image')->move(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id, $banner->image);
					}

					return Redirect::route('banner.list', array('id' => $banner->id))
						->with('alert.success', 'Banner has been created')
					;
				}
				else{

					return Redirect::route('banner.add')
						->withInput(Input::except('image'))
						->with('alert.danger', 'Unable to create Banner')
					;
				}

			}
			else{

				$banner = Banner::getRowById($id)
				->first()
				;

				//dd($banner->name);

				if (!isset($banner->id)){

					return Redirect::route('banner.list')
						->with('alert.danger', 'Banner not Found')
					;
				}

				$banner->name = Input::get('name', '');
				$banner->type = Input::get('type');
				$banner->url = Input::get('url');

				if (Input::hasFile('image')){
					$old_image = $banner->image;
					$banner->image = uniqid(time()) . '.' . Input::file('image')->getClientOriginalExtension();
				}

				$banner->modified_on = date('Y-m-d H:i:s');
				$banner->modified_by = Auth::user()->id;
				$banner->status = Input::get('status');

				if($banner->save()){
					if(Input::hasFile('image')) {
						if(!empty($old_image)){
							File::delete(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $old_image);
						}

		                if(!File::exists(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id)) @mkdir(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id);             
		                $img = Image::make(Input::file('image'))
		                    ->fit(640, 480)
		                    ->save(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id . DIRECTORY_SEPARATOR . $banner->image)
		                ;  						
						// Input::file('image')->move(BANNER_IMAGE_PATH . DIRECTORY_SEPARATOR . $banner->id, $banner->image);
					}

					return Redirect::route('banner.list', array('id' => $banner->id))
						->with('alert.success', 'Banner has been Updated')
					;
				}
				else{
					return Redirect::route('banner.edit', array('id' => $banner->id))
						->withInput(Input::except('image'))
						->with('alert.danger', 'Unable to update Banner')
					;
				}
			}
		}
	}

	public function deleteBanner($id = 0)
	{

		if(empty($id)){

			return Redirect::route('banner.list')
				->with('alert.danger', 'Banner not found')
			;
		}
		else{
			$banner = Banner::getRowById($id)
				->first()
			;

			if(isset($banner->id)){

				$banner->status = 0;

				if($banner->save()){

					return Redirect::route('banner.list')
						->with('alert.success', 'Banner has been deleted')
					;
				}
				else{

					return Redirect::route('banner.edit', array('id' => $banner->id ))
						->withInput(Input::except('image'))
						->with('alert.danger', 'Unable to delete Banner')
					;
				}
			}
			else{

				return Redirect::route('banner.list')
					->with('alert.danger', 'Banner not found')
				;
			}
		}
	}

	public function apiGetBannerByType($id)
	{

		$type = array(
			'1' => 'highlight',
			'2' => 'featured',
		);

		$jsonResponse = new JsonResponse();

		$response = array();

		if(!empty($id)){

			$banners = Banner::getBannersByType($type[$id])
				->get()
			;

			foreach($banners as $banner){
				if(isset($banner->id)){
					$response[] = array(
						'id' => (int)$banner->id,
						'name' => $banner->name,
						'url' => $banner->url,
						'image' => BANNER_IMAGE_URL . DIRECTORY_SEPARATOR . $banner->id . DIRECTORY_SEPARATOR . $banner->image,
						'created_on' => $banner->created_on
					);
				}
			}
			
			$jsonResponse->setResponse($response);
			return $jsonResponse->get();

		}
		else{
			$jsonResponse->setSubject("Null Error");
			$jsonResponse->setBody("Type must have id");

			return $jsonResponse->get();
		}
	}

}
