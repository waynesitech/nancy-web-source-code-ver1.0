<?php

class HomeController extends BaseController {

	protected $layout = 'layout.guest';
	
	public function getIndex()
	{
//		return View::make('index');
            $this->layout = View::make('layout.home');
            $this->layout->body = View::make('home.index');
	}
        
        public function postContactUs()
	{
            //validation
            $rules = array(
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            );
        
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::route('home')
                    ->withErrors($validator)
                    ->withInput();
            }
		
            Mail::send('account.user.email.contactus', array(
			'name' => Input::get('name', ''),
                        'email' => Input::get('email', ''),
                        'message' => Input::get('message', '')
		), function($message) {
		    $message
		    	->to('janice_wsm@hotmail.com', 'tester')
		    	->subject('Welcome to Nancy')
		    ;
		});
            if(count(Mail::failures()) == 0){
                return Redirect::route('home');
            }
        }

//	public function getIndex()
//	{
//
//
//		$this->layout = View::make('layout.home')
//			->with('identity', Auth::user())
//		;	
//
//		$this->appendStyle('/assets/js/select2/select2.css');
//		$this->appendScript('/assets/js/select2/select2.min.js', 'inline');
//		$this->appendScript('/assets/js/home.js', 'inline');
//
//		$states = State::getActive()
//			->orderBy('state', 'ASC')
//			->lists('state', 'id')
//		;
//
//		$areas = Area::getActive()
//			->orderBy('name')
//			->lists('name', 'id')
//		;
//
//		$highlight = Banner::getBannersByType('highlight')
//			->limit(5)
//			->get()
//		;	
//
//		$featured = Banner::getBannersByType('featured')
//			->limit(5)
//			->get()
//		;
//
//		$this->layout->body = View::make('home.index')
//			->with('states', array('' => 'All States') + $states)
//			->with('areas', array('' => 'All Areas') + $areas)
//			->with('highlight', $highlight)
//			->with('featured', $featured)
//		;
//	}

	public function getLanding()
	{
		$this->layout = null;
		return View::make('home.test');
	}
	
	public function getContactEmail()
	{
		Mail::raw('Text to e-mail', function($message) 
		{
			$message->from('janice_wsm@hotmail.com');
	
			$message->to('janice_wsm@hotmail.com');
		});
	}

	public function getMail()
	{

		Mail::send('account.user.email.welcome', array(
			'link' => 'sfadsfs',
		), function($message) {
		    $message
		    	->to('janice_wsm@hotmail.com', 'tester')
		    	->subject('Welcome to Nancy')
		    ;
		});
		echo 'Sent';

		exit;

	}

}
