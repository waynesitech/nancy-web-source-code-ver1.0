<?php

class NotificationController extends BaseController
{
    protected $layout = 'layout.admin';

    public function apiPostOn()
    {

        $jsonResponse = new JsonResponse();

        $response = array();

        if(Auth::user()!=null){
            
             $user = User::getById(Auth::user()->id)
            ->first()
            ;

            $token = trim(Input::get('token'));
            $old_users = User::getByDeviceToken($token)->get();

            if (trim(Input::get('platform', 'ios')) == 'ios') {

                foreach ($old_users as $old_user) {
                    if($old_user->id != $user->id) {
                        $old_user->ios = '';
                        $old_user->save();
                    }
                }

                $user->ios = $token;  
                // $user->android = NULL;  

            } else {

                foreach ($old_users as $old_user) {
                    if($old_user->id != $user->id) {
                        $old_user->android = '';
                        $old_user->save();
                    }
                }
 
                $user->android = $token;
                // $user->ios = NULL;  
            }
            
            $user->save();
        }
           

        $jsonResponse->setResponse($response);

        return $jsonResponse
            ->get()
        ;
    }   

    public function apiPostApple()
    {
        $jsonResponse = new JsonResponse();

        $token = Input::get('token', '');
        $msg = Input::get('msg', 'hi ios');

        // $this->apns($token, $msg);

        $push = PushNotification::app('appNameIOS')
            // ->to(str_replace(array('<', '>', ' '), '', trim('<76cdd4bf 42aeb37b 215213fa aa7db96e c44697c7 ffd7d8fe 43167008 582acc6e>')))
            ->to(str_replace(array('<', '>', ' '), '', trim('<d331c891 a0d02483 d43f9372 a032648f 97fc01a7 032595f3 2b650979 8cd51278>')))
            // ->to(str_replace(array('<', '>', ' '), '', trim('<0f46e42d 3de5b542 9e169c71 2e7fbddd 31840c24 c80cecd4 d9e7ae04 f09dede3>')))
            // ->to(str_replace(array('<', '>', ' '), '', trim('<58f899fb d127c026 b280272b bb5e41a0 cb818e6b 4ecd7fa9 6abc28ff 2836b1a2>')))
            // ->to(str_replace(array('<', '>', ' '), '', trim('<5adf6992 0b04e2f2 659b329b ec5aa6f4 4227b462 23a4cff8 4dad977b 57f9ad72>')))
            // ->send('Hello World, i\'m a push message')
            ->send(PushNotification::Message('Message Text',array('badge' => 1)))
        ;       

        dd($push); 


            $url = 'ssl://gateway.sandbox.push.apple.com:2195';
            $cert = app_path() . '/../ck.pem';
            $passphrase = 'pushchat';

            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $cert);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);    

            if ($fp) {

                    $id = str_replace(array('<', '>', ' '), '', trim('<ff65a58e 258a8415 3c16e882 c4f204aa 49155c87 4c8c3166 1024b6e8 6e5d10d1>'));                    

                    $body['aps'] = array(
                        'alert' => 'hello do you read me?! ' . time(),
                        'badge' => 0,
                        'sound' => 'default',
                    );
                    $payload = json_encode($body);

                    // var_dump($payload);

                    $msg = chr(0) . pack('n', 32) . pack('H*', $id) . pack('n', strlen($payload)) . $payload;
                    $result = fwrite($fp, $msg, strlen($msg));         
                    var_dump($result);
                    if ($result === FALSE) {
                        fclose($fp);
                        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                    } else {
                        
                    }

                fclose($fp); 

            }            

        return $jsonResponse
            ->get()
        ;            

    }   

    public function apiPostGoogle()
    {
        $jsonResponse = new JsonResponse();

        $token = Input::get('token', '');
        $msg = Input::get('msg', 'hello android');

        $this->gcm($token, $msg);

        // $this->gcm('APA91bEJ5dH2SFitPMtAwr5akNI9EAQzvla-o3o1gLrRR730QPWBvmVZRJILuy-wQlCEb25t8S5uhqe-do3TfkaO18TUXrFNz-G4az-r5YUcfJQLvKlzfuW-ihsAzMIwdp_nEqoiqwZP5Okx1eExUlvVmpq0ggxxtiHMVKs3d1JjmLP3ZsclMOI', $msg);

        return $jsonResponse
            ->get()
        ;            

    }           

    public function apiAnySend()
    {
        $jsonResponse = new JsonResponse();

        // prevent duplicated email sent
        $mailed_queue_ids = [];

            $pushqueue = PushQueue::where('type', 'apns')
                ->whereNull('time_sent')
                ->orderBy('token')
                ->get()
            ;

            $badge = 0;
            $last_token = '';

            foreach($pushqueue as $queue) {

                $id = str_replace(array('<', '>', ' '), '', trim($queue->token));      

                if ($last_token != $id) {
                    $last_token = $id;
                    $badge = 0;
                }
                $badge++;

                $type_id = 0;
                if($queue->message_type == TYPE_BROADCAST)
                    $type_id = $queue->property_id;
                elseif($queue->message_type == TYPE_FOLLOW)
                    $type_id = $queue->follower_id;
                elseif(in_array($queue->message_type, [TYPE_NEGO, TYPE_TRANS]))
                    $type_id = $queue->nego_id;

                $message = PushNotification::Message($queue->message, array(
                    'badge' => $badge,
                    'custom' => array(
                        'type' => $queue->message_type,
                        'id' => $type_id
                    ),
                ));

                PushNotification::app('appNameIOS')
                    ->to($id)
                    ->send($message)
                ;     

                $queue->status = 1;
                $queue->time_sent = date('Y-m-d H:i:s');
                $queue->save();                     

                $uniq_queue_id = sprintf('%d%d%d%s', $queue->recipient, $queue->sender, $queue->property_id, $queue->message);
				try {
                    $user = User::find($queue->recipient);
                    if (isset($user->id) && $queue->property_id > 0 && !in_array($uniq_queue_id, $mailed_queue_ids)) {
                        $property = Property::getById($queue->property_id)->first();
                        if (isset($property->id)) {
                            Mail::send('emails.broadcast', array(
                                'property' => $property,
                                'msg' => $queue->message,
                            ), function($mail_msg) use($user, $property) {
                                $mail_msg
                                    ->to($user->email, $user->first_name . ' ' . $user->last_name)
                                    ->subject($property->name . ' for ' . (strtolower($property->category) == 'rent' ? 'Rent' : 'Sale') . ' - Wonderlist')
                                ;
                            });
                        }
                    }
					
	            } catch (Exception $e) {

	            }					

                $mailed_queue_ids[] = $uniq_queue_id;
            }

            /*
            $url = 'ssl://gateway.push.apple.com:2195';
            $cert = app_path() . '/../pro.pem';
            // $url = 'ssl://gateway.sandbox.push.apple.com:2195';
            // $cert = app_path() . '/../dev.pem';
            $passphrase = 'pushchat';

            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $cert);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);    

            $badge = 0;
            $last_token = '';
            if ($fp) {

                foreach($pushqueue as $queue) {

                    $id = str_replace(array('<', '>', ' '), '', trim($queue->token));                    

                    if ($last_token != $id) {
                        $last_token = $id;
                        $badge = 0;
                    }
                    $badge++;                    

                    $body['aps'] = array(
                        'alert' => $queue->message,
                        // 'alert' => $queue->message . ' = ' . md5(mt_rand()),
                        'badge' => $badge,
                        'sound' => 'default',
                        // 'content-available' => '1'
                    );
                    $payload = json_encode($body);

                    // var_dump($payload);

                    $msg = chr(0) . pack('n', 32) . pack('H*', $id) . pack('n', strlen($payload)) . $payload;
                    $result = fwrite($fp, $msg, strlen($msg));         
                    // var_dump($result);
                    if ($result === FALSE) {
                        $queue->status = 0;
                        fclose($fp);
                        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                    } else {
                        $queue->status = 1;
                    }

                    $queue->time_sent = date('Y-m-d H:i:s');
                    $queue->save();                               

                }

                fclose($fp); 

            }    
            */
            // exit;

        
        $pushqueue = PushQueue::where('type', 'gcm')
            ->whereNull('time_sent')
            ->orderBy('token')
            ->get()
        ;            

            $url = 'https://android.googleapis.com/gcm/send';
            
            $headers = array(
                'Authorization: key=AIzaSyCGhzQLXui1xNRx4x3OLQ5xMoG59BxK2P4', 
                'Content-Type: application/json'
            );

            // $ids = array();
                // $ids = array('APA91bEJ5dH2SFitPMtAwr5akNI9EAQzvla-o3o1gLrRR730QPWBvmVZRJILuy-wQlCEb25t8S5uhqe-do3TfkaO18TUXrFNz-G4az-r5YUcfJQLvKlzfuW-ihsAzMIwdp_nEqoiqwZP5Okx1eExUlvVmpq0ggxxtiHMVKs3d1JjmLP3ZsclMOI');            

            foreach($pushqueue as $queue) {
                // $ids[] = $queue->token;
                // $ids = array('APA91bEJ5dH2SFitPMtAwr5akNI9EAQzvla-o3o1gLrRR730QPWBvmVZRJILuy-wQlCEb25t8S5uhqe-do3TfkaO18TUXrFNz-G4az-r5YUcfJQLvKlzfuW-ihsAzMIwdp_nEqoiqwZP5Okx1eExUlvVmpq0ggxxtiHMVKs3d1JjmLP3ZsclMOI');
                
                $type_id = 0;
                if($queue->message_type == TYPE_BROADCAST)
                    $type_id = $queue->property_id;
                elseif($queue->message_type == TYPE_FOLLOW)
                    $type_id = $queue->follower_id;
                elseif(in_array($queue->message_type, [TYPE_NEGO, TYPE_TRANS]))
                    $type_id = $queue->nego_id;

                $fields = array(
                    'registration_ids' => array(trim($queue->token)),
                    'data' => array(
                        'message' => $queue->message,
                        'type' => $queue->message_type,
                        'id' => $type_id

                    ),
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    // die('Curl failed: ' . curl_error($ch));
                    $queue->status = 0;
                } else {
                    $queue->status = 1;
                }
                curl_close($ch);

                $queue->time_sent = date('Y-m-d H:i:s');
                $queue->save();                    

                $uniq_queue_id = sprintf('%d%d%d%s', $queue->recipient, $queue->sender, $queue->property_id, $queue->message);
				try {
                    $user = User::find($queue->recipient);
                    if (isset($user->id) && $queue->property_id > 0 && !in_array($uniq_queue_id, $mailed_queue_ids)) {
                        $property = Property::getById($queue->property_id)->first();
                        if (isset($property->id)) {
                            Mail::send('emails.broadcast', array(
                                'property' => $property,
                                'msg' => $queue->message,
                            ), function($mail_msg) use($user, $property) {
                                $mail_msg
                                    ->to($user->email, $user->first_name . ' ' . $user->last_name)
                                    ->subject($property->name . ' for ' . (strtolower($property->category) == 'rent' ? 'Rent' : 'Sale') . ' - Wonderlist')
                                ;
                            });
                        }
                    }
					
	            } catch (Exception $e) {
	            }					

                $mailed_queue_ids[] = $uniq_queue_id;
            }
        
            // var_dump($result);
            // exit;

        return $jsonResponse
            ->get()
        ;            

    }    

    public function apiGetMyNotifications()
    {

        $jsonResponse = new JsonResponse();

        $response = array();
        $currentUser=Auth::user();

        
        $page = Input::get('page', 1);
        $pageLength = Input::get('pageLength', 20);

       
        if($currentUser->role_id==1 || $currentUser->role_id==7){
            
        $notifications = OrderPushQueue::orderBy('created_on','desc')->paginate($pageLength);



        foreach($notifications as $notification) {

            //$time_sent = Carbon::createFromTimeStamp(strtotime($notification->time_sent));

            $response[] = array(
                'id' => $notification->id,
                'order'=> App::make('OrderController')->orderToJson($notification->order),
                        'type'=>$notification->type,
                'created_ago'=>$this->_timeago(strtotime($notification->created_on)),
                'created_on'=>date("g:i a, j F Y ",strtotime($notification->created_on)),
                );

        }
        }

        $jsonResponse->setResponse($response);

        return $jsonResponse
        ->get()
        ;

    }
    
}
