<?php

class BroadcastController extends BaseController {

	protected $layout = 'layout.plain';

	public function getBroadcastPage($id=0){
		
		if (empty($id)){
			$broadcast = NULL;
		}
		else{
			$broadcast = Broadcast::getRowById($id)
			->first()
			;
		}

        if (isset($broadcast->id)) {
			
			$broadcast->pageView_count += 1;
			$broadcast->save();
			
			$this->layout->body = View::make('broadcast.view')
			->with('broadcast', $broadcast)
			;
			
		}else{
			return Redirect::route('broadcast.list')
					->with('alert.danger', 'Broadcast not found')
				;
		}
	}

}
