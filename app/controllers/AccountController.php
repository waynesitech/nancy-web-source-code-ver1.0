<?php

class AccountController extends BaseController {
	
	protected $layout = 'layout.guest';

	public function getAdminLogin()
	{
            $this->layout = View::make('layout.auth_admin');
            $this->layout->body = View::make('account.admin.login');
	}

        public function postAdminLogin()
        {
                $validator = Validator::make(Input::all(), array(
                        'email' => 'required'
                ));

                if ($validator->fails()) {
                        return Redirect::route('admin.login')
                                ->withErrors($validator)
                                ->withInput(Input::except('password'))
                        ;	
                } else {
                        $user = User::getByEmailJoin(Input::get('email'))
                                ->where('user_role.name_en', 'Administrator')
                                ->orWhere('user_role.name_en', 'Lesys Team')
                                ->first()
                        ;

                        if (isset($user->id)) {
                                if (Auth::attempt(Input::all())) {
                                        if($user->role->name_en == 'Administrator'){
                                            return Redirect::route('dashboard.list');
                                        }
                                        elseif($user->role->name_en == 'Lesys Team'){
                                            return Redirect::route('order.list');
                                        }

                                } else {

                                        return Redirect::route('admin.login')
                                        ->with('alert.danger', 'Your email/password combination was incorrect.')
                                        ->withInput(Input::except('password'))
                                        ;
                                }
                        } else {
                                return Redirect::route('admin.login')
                                ->with('alert.danger', 'Email address not found.')
                                ->withInput(Input::except('password'))
                                ;
                        }
                }
        }

	public function getAdminLogout()
	{
		Auth::logout();
		
		return Redirect::route('admin.login');
	}

	public function getAdminSettings()
	{
		$user = User::find(Auth::user()->id);
                $this->layout = View::make('layout.admin');
		$this->layout->body = View::make('account.admin.setting')
			->with('user', $user)
		;
	}

	public function postAdminSettings()
	{
		$rules = array(
		//	'first_name' => 'required',
		//	'last_name' => 'required',
			//'username' => 'required|unique:user,username,'.Auth::user()->id,
			'email' => 'required|unique:user,email,'.Auth::user()->id
			);

		// Add additional rules if user want to change password.
		$password = \Input::get('password', '');
		if (strlen($password) > 0) {
			$rules['password'] = 'min:6';
			$rules['new_password'] = 'required|min:6';
			$rules['confirm_password'] = 'required_with:new_password|same:new_password';
		}

/*		$check_username = User::getByUsernameWithExcludeId(Input::get('username'), Auth::user()->id);
		if(!empty($check_username->id)) {
			$rules['username'] = 'required|unique:users,username';
		}
		$check_email = User::getByEmailWithExcludeId(Input::get('email'), Auth::user()->id);
		if(!empty($check_email->id)) {
			$rules['email'] = 'required|unique:users,email';
		}*/

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::route('admin.settings')
			->withErrors($validator)
			->withInput(Input::except(array('password', 'new_password','confirm_password')));

		}

		$user = User::find(Auth::user()->id);
	//	$user->first_name = Input::get('first_name');
	//	$user->last_name = Input::get('last_name');
	//	$user->username = Input::get('username');
		$user->email = Input::get('email');
	//	$user->status = Input::get('status');
		

		if (strlen($password) > 0) {
			if (!(\Hash::check($password, $user->password))) {
				return \Redirect::route('admin.settings')
					->with('alert.danger', 'Invalid current password, please try again.')
					;
			} else {
				$user->password = Hash::make(Input::get('new_password'));
			}
		}

		if($user->save()) {
			return Redirect::route('admin.settings')
				->with('alert.success', 'User has been updated.')
			;
		}

		return Redirect::route('admin.settings')
			->with('alert.danger', 'Update Failed.')
		;

	}

	// Guest 
	public function getUserLogin()
	{
            $this->layout = View::make('layout.auth');
            $this->layout->body = View::make('account.user.login');
	}

	public function postUserLogin()
	{

		$validator = Validator::make(Input::all(), array(
			'email' => 'required',
			'password' => 'required'
		));

		if ($validator->fails()) {
			return Redirect::route('user.login')
				->withErrors($validator)
				->withInput(Input::except('password'))
			;	

		} else {
                        $user = User::getByEmailJoin(Input::get('email'))
                            ->where('user_role.name_en', 'Member')
                            ->first()
                        ;
                        
                        if (isset($user->id)) {
                            if ($user->isPhoneVerify == 'false') {
                                $this->sendAuthSMS($user->mobile);
                                
                                return Redirect::route('user.verifyCode')
                                ->with('id', $user->id)
                                ->with('mobile', $user->mobile);
                            }
                            
                            if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
                                //return Redirect::route('user.me');
                                return Redirect::route('order.lists');
                            } else {
                                return Redirect::route('user.login')
                                        ->with('alert.danger', 'Invalid email or password.')
                                        ->withInput(Input::except('password'))
                                ;
                            }
			} else {
                            return Redirect::route('user.login')
                                    ->with('alert.danger', 'Email address not found.')
                                    ->withInput(Input::except('password'))
                            ;
			}

		}

	}

	public function getUserLogout()
	{
            Auth::logout();
            return Redirect::route('user.login');
	}

	public function getUserRegister()
	{
            $this->layout = View::make('layout.auth');
            $this->layout->body = View::make('account.user.register');
        }

	public function postUserRegister()
        {
            $rules = array(
                'name' => 'required',
                'email' => 'required|email|unique:user,email',
                'password' => 'required',
                'confirm_password' => 'required:password|same:password',
                'mobile' => 'required|numeric|unique:user,mobile',
                'agree' => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);

                if ($validator->fails()) {
                        return Redirect::route('user.register')
                                ->withErrors($validator)
                                ->withInput(Input::except(array('password', 'confirm_password')));
                } else {
                    $user = new User;
                    $user->name = Input::get('name');
                    $user->email = Input::get('email');
                    $user->password = Hash::make(Input::get('password'));
                    $user->mobile = Input::get('mobile');
                    $user->isPhoneVerify = 'false';
                    $user->role_id = 3;
                    $user->status = 0;

                    if($user->save()) {

                        try {
                            $this->sendAuthSMS(Input::get('mobile'));

                            Mail::send('emails.user_register', array(), function($message) use($user) {
                                $message
                                    ->to($user->email, $user->name)
                                    ->subject('Thank you for registering with NanCy')
                                ;
                            });

                        } catch (Exception $e) {

                        }					

                        return Redirect::route('user.verifyCode')
                                ->with('id', $user->id)
                                ->with('mobile', Input::get('mobile'));

                    } else {

                        return Redirect::route('user.register')
                            ->with('alert.danger', 'Register failed.')
                        ;

                    }
                }			

        }
        
        public function getUserVerification()
	{
            $this->layout = View::make('layout.auth');
            $this->layout->body = View::make('account.user.verify')
                                    ->with('user_id', Session::get('id'))
                                    ->with('mobile', Session::get('mobile'));
        }
        
        public function postUserVerification()
	{
            $verificationCode= Input::get('verificationCode');
            $userID=Input::get('id');
            $currentUser=User::find($userID);

            if (!$currentUser) {
                return Redirect::route('user.register')
                    ->with('alert.danger', 'User not found.')
                ;
            }

            $isPhoneVerify=$currentUser->isPhoneVerify;

            if ($isPhoneVerify == 'true') {            
                $isPhoneVerify=true;
            } else {
                $isPhoneVerify=false;
            }
            
            if ($isPhoneVerify) {
                //Your account is verified
                return Redirect::route('user.verified');
            }

            $mobile=$currentUser->mobile;
            $validationResult=$this->checkPhoneValidation($mobile);

            $phoneNumber = $validationResult['phoneObject']['nationalNumber'];
            $countryCode = $validationResult['phoneObject']['countryCode'];
            $url = 'https://api.authy.com/protected/json/phones/verification/check?api_key=wsLOBZxPXePqoUrmnCkCwisoPoDwByee&phone_number=' . $phoneNumber . '&country_code=' . $countryCode . '&verification_code=' . $verificationCode;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            $result = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($result, true);

            if ($response['success']) {
                $currentUser->isPhoneVerify='true';
                $currentUser->status=1;
                $currentUser->save();

                $isPhoneVerify=true;
                $attempt = Auth::loginUsingId($currentUser->id);
                
                return Redirect::route('user.verified');
            } else {
                return Redirect::route('user.verifyCode')
                    ->with('alert.danger', 'Invalid verification code. Please try again later.')
                ;
            }
        }
        
        public function getUserVerified()
	{
            $this->layout = View::make('layout.auth');
            $this->layout->body = View::make('account.user.verified');
        }
        
        public function getResetPW()
	{
            $this->layout = View::make('layout.auth');
            $this->layout->body = View::make('account.user.forgetpassword');
	}

        public function postResetPW() {
            $rules = array(
                'email' => 'required',
                );

            $validator = Validator::make(Input::all(), $rules);

            if($validator->fails()) {
                return Redirect::route('user.forget')
                    ->withErrors($validator)
                    ->withInput()
                ;
            } else {
                $user = User::getByEmail(Input::get('email'))
                ->first();
                if (isset($user->id)) {
                    $password = $this->_generatePassword(8);
                    $user->password = Hash::make($password);
                    $user->modified_on = date('Y-m-d H:i:s');
                    $user->modified_by = $user->id;
                    $user->save();

                    try {
                        Mail::send('emails.auth.reset', array('password' => $password), function($message) use ($user) {
                            $message
                                ->to($user->email, $user->name)->subject('Reset password')
                                ->subject('New password has been sent to your mailbox.');
                        });
                    } catch (Exception $e) {
                        return Redirect::route('user.forget')
                            ->with('alert.danger', $e->getMessage())
                        ;
                    }   
                    
                    return Redirect::route('user.forget')
                        ->with('alert.success', 'New password had send to your email. Remember reset it after login successfully.')
                    ;
                } else {
                    return Redirect::route('user.forget')
                            ->with('alert.danger', 'Email address not found')
                        ;
                }
            }
        }

	public function getUserActivate($hash = '')
	{

		if (empty($hash)) {

			return Redirect::route('user.register')
				->with('alert.danger', 'User not found.')
			;

		} else {

			$user = User::getByHash($hash)
				->first()
			;

			if (isset($user->id)) {

				$user->status = 1;

				if  ($user->save()) {
					return Redirect::route('user.login')
						->with('alert.success', 'Thank you for activating your account.')
					;

				} else {

					return Redirect::route('user.register')
						->with('alert.danger', 'Cannot activate account.')
					;

				}

			} else {

				return Redirect::route('user.register')
					->with('alert.danger', 'User not found.')
				;

			}

		}

	}

	public function getMe()
	{
		$this->appendStyle('/assets/js/raty/jquery.raty.css');

		$this->appendScript('/assets/js/moment.js', 'inline');
		$this->appendScript('/assets/js/combodate.js', 'inline');
		$this->appendScript('/assets/js/raty/jquery.raty.js', 'inline');
		$this->appendScript('/assets/js/user.js', 'inline');

		$user = User::getById(Auth::user()->id)
			->first()
		;

		if (isset($user->id)) {

			if (!empty($user->image)) {
				$user->image = USER_IMAGE_URL . DIRECTORY_SEPARATOR . $user->id . DIRECTORY_SEPARATOR . $user->image;
			}

			$this->layout->body = View::make('account.user.follow')
				->with('user', $user)
			;

		} else {

			App::abort(404);

		}

	}

	public function getUserProfile()
	{
		$this->appendStyle('/assets/js/raty/jquery.raty.css');

		$this->appendScript('/assets/js/moment.js', 'inline');
		$this->appendScript('/assets/js/combodate.js', 'inline');
		$this->appendScript('/assets/js/raty/jquery.raty.js', 'inline');

		$this->appendScript('/assets/js/account.js', 'inline');
		$this->appendScript('/assets/js/user.js', 'inline');

		$user = User::getById(Auth::user()->id)
			->first()
		;

		if (isset($user->id)) {
			$this->layout->body = View::make('account.user.form')
				->with('user', $user)
			;

		} else {

			App::abort(404);

		}

	}

	public function postUserProfile()
	{
		$rules = array(
			 'name' => 'required',
            'email' => 'required|email|unique:user,email,'. Auth::user()->id,
            'mobile' => 'required|numeric|unique:user,mobile,'. Auth::user()->id

		);

		$user = User::getById(Auth::user()->id)
			->first()
		;

                                     $password = Input::get('new_password', '');

                                     if (strlen($password) > 0) {
                                    $rules['new_password'] = 'required';
                                    $rules['new_password'] = 'required|same:retype_new_password';
                                    }

		// $check_email = User::getByEmailWithExcludeId(Input::get('email'), Auth::user()->id)
		// 	->first()
		// ;

		// if (!empty($check_email->id)) {
		// 	$rules['email'] = 'required|unique:user,email';
		// }

		// if (Auth::user()->role == 'agent') {
			// $rules['agency'] = 'required';
		// }

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			
			return Redirect::route('user.me')
				->withErrors($validator)
				->withInput(Input::except(array('new_password', 'retype_new_password')))
			;

		} else {

			$user->email = Input::get('email');
			$user->name = Input::get('name');
			$user->mobile = Input::get('mobile');
                        $user->password = Hash::make(Input::get('new_password'));
						

			// if (strlen($password) > 0) 
			// 	$user->password = Hash::make($password);

			if ($user->save()) {

				return Redirect::route('user.me')
					->with('alert.success', 'Your profile has been updated')
				;

			} else {

				return Redirect::route('user.me')
					->with('alert.danger', 'Profile update failed.')
				;

			}

		}

	}

	public function postUserPassword()
	{

		$rules = array(
			'new_password' => 'required',
			'new_password' => 'required|same:retype_new_password',
		);

		$user = User::getById(Auth::user()->id)
			->first()
		;	

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) {
			
			return Redirect::route('user.me')
				->withErrors($validator)
				->withInput(Input::except(array('old_password', 'new_password', 'retype_new_password')))
			;

		} else {

			if (!(\Hash::check(Input::get('old_password'), $user->password))) {
				return \Redirect::route('user.me')
					->with('alert.danger', 'Invalid old password, please try again.')
				;
			} else {

				$user->password = Hash::make(Input::get('new_password'));

				if ($user->save()) {			

					return Redirect::route('user.me')
						->with('alert.success', 'Your password has been updated.')
					;

				} else {

					return Redirect::route('user.me')
						->with('alert.danger', 'Password update failed.')
					;

				}				
			}				

		}

	}
}
