<?php

class StateController extends BaseController {

	public function getArea(){

		$jsonResponse = new JsonResponse();
		$response = '';

		$stateName = State::getAreaByState()
		->get()
		;

		

		for($i = 0; $i < count($stateName); $i++){

			$areaArray = explode('|', $stateName[$i]->area);




			foreach ($areaArray as $areaName) {

				if($areaName != ''){
					$area_database = new Area();
					DB::table('area')->insert(
						array('state_id' => $i+1, 'name' => $areaName)
						);
				}
			}
		}
		
		$jsonResponse->setResponse($areaArray);

		return $jsonResponse
		->get();

	}

}