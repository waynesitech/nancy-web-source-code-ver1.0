<?php
class DashboardController extends BaseController
{
	protected $layout = 'layout.admin';	
	
	public function getPropAnalytic(){
            
            //total active user
            $total_user = User::where('status', 1)
              ->where('role_id', 3)
            ->count();
			
            //total current month new user
            $total_new_user = User::where('status', 1)
              ->where('role_id', 3)
			  ->whereRaw('MONTH(created_on) = ' . date("m") . ' and YEAR(created_on) = ' . date("Y"))
			->count();

            //total active agent
            $total_agt = User::where('status', 1)
              ->where('role_id', 2)
            ->count();
			
			//total current month new agent
            $total_new_agt = User::where('status', 1)
              ->where('role_id', 2)
			  ->whereRaw('MONTH(created_on) = ' . date("m") . ' and YEAR(created_on) = ' . date("Y"))
            ->count();
            
            $total_order = Order::all()->count();
            
            $total_pending_order = Order::where('tenancyAgreementStatus', 'Received')->count();
            
            $start = date('01/m/Y');
            $end = date('d/m/Y');
            $daterange = Input::get('daterange');
            
            if (!empty($daterange)) {
               list($start, $end) = array_map('trim', explode('-', $daterange));
            } else {
                $daterange = $start . ' - ' . $end;
            }
            
            //Order Analytics
            $start_date = date('Y-m-d', strtotime(str_replace('/', '-', $start)));
            $end_date = date('Y-m-d', strtotime(str_replace('/', '-', $end)));
            //get date diff
            $start_prop = new DateTime($start_date);
            $end_prop = new DateTime($end_date);
            $numdate = $start_prop->diff($end_prop)->format('%a');
            
            $data = array();
            $log_data = Order::getOrderByDay($start_date, $end_date)
                                    ->get();
            $date = $start_date;
            for ($i=1; $i<=$numdate + 1; $i++)
            {
                $total = 0;
                foreach($log_data as $log_detail)
                {
                    if(strtotime($date) == strtotime($log_detail['created_date']))
                    {
                            $total = $log_detail['order_count'];
                            break;
                    }
                }
                $data[] = array('day' => $date, 'total' => (int)$total);
                $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
            
            
            //User Analytics
            $start_user = date('01/m/Y');
            $end_user = date('d/m/Y');
            $daterange_user = Input::get('daterange_user');
            
            if (!empty($daterange_user)) {
               list($start_user, $end_user) = array_map('trim', explode('-', $daterange_user));
            } else {
                $daterange_user = $start_user . ' - ' . $end_user;
            }
            
            $start_date_user = date('Y-m-d', strtotime(str_replace('/', '-', $start_user)));
            $end_date_user = date('Y-m-d', strtotime(str_replace('/', '-', $end_user)));
            //get date diff
            $start_temp_user = new DateTime($start_date_user);
            $end_temp_user = new DateTime($end_date_user);
            $numdate_user = $start_temp_user->diff($end_temp_user)->format('%a');
            
            $data_user = array();
            
            $log_data_user = User::getUserByDay($start_date_user, $end_date_user)
                                    ->get();
            
            $date_user = $start_date_user;
            
            for ($j=1; $j<=$numdate_user + 1; $j++)
            {
                $total = 0;
                foreach($log_data_user as $log_detail)
                {
                    if(strtotime($date_user) == strtotime($log_detail['created_date']))
                    {
                            $total = $log_detail['user_count'];
                            break;
                    }
                }
                $data_user[] = array('day' => $date_user, 'total' => (int)$total);
                $date_user = date("Y-m-d", strtotime("+1 day", strtotime($date_user)));
            }
            
            $this->appendScript('/assets/js/moment.js', 'inline');
            $this->appendScript('/assets/js/bootstrap-daterangepicker/daterangepicker.js', 'inline');
            $this->appendStyle('/assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css', 'inline');
            $this->appendScript('/assets/js/user.js', 'inline');

            $this->layout->body = View::make('dashboard.index')
                    ->with('total_user', $total_user)
                    ->with('total_new_user', $total_new_user)
                    ->with('total_agt', $total_agt)
                    ->with('total_new_agt', $total_new_agt)
                    ->with('total_order', $total_order)
                    ->with('total_pending_order', $total_pending_order)
                    ->with('data', $data)
                    ->with('daterange', $daterange)
                    ->with('data_user', $data_user)
                    ->with('daterange_user', $daterange_user)
    ;
    }
}

?>