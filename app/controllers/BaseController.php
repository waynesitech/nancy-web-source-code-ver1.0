<?php
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

define('USER_IMAGE_PATH', public_path().'/files/users');
define('USER_IMAGE_URL', URL::to('/files/users'));

define('CHAT_IMAGE_URL', URL::to('/files/chats'));
define('CHAT_IMAGE_PATH', public_path().'/files/chats');

define('BANNER_IMAGE_PATH', public_path().'/files/banners');
define('BANNER_IMAGE_URL', URL::to('files/banners'));

define('PROPERTY_IMAGE_PATH', public_path().'/files/properties');
define('PROPERTY_IMAGE_URL', URL::to('files/properties'));

define('ORDER_IMAGE_PATH', public_path().'/files/orders');
define('ORDER_IMAGE_URL', URL::to('files/orders'));

define('EXPLANATION_AUDIO_PATH', public_path().'/files/explanations');
define('EXPLANATION_AUDIO_URL', URL::to('files/explanations'));

define('TYPE_BROADCAST', 'broadcast');
define('TYPE_FOLLOW', 'follow');
define('TYPE_NEGO', 'nego');
define('TYPE_TRANS', 'trans');
define('TYPE_ORDER_PROCESS', 'orderProcessing');
define('TYPE_ORDER_READY', 'orderReady');
define('TYPE_ORDER_RECEIVED', 'orderReceived');
define('TYPE_ORDER_EXPIRE', 'orderExpire');
define('TYPE_ADMIN_ORDER_RECEIVED', 'orderAdminReceived');
define('TYPE_ADMIN_STAMPING_REQUESTED', 'stampingAgentRequested');

define('TYPE_ORDER_SIGNED', 'orderSigned');
define('TYPE_ORDER_SIGNED_CONFIRMATION', 'orderSignConfirmation');
define('TYPE_ORDER_ESTAMPING_PROCESS', 'orderEStampingProcessing');
define('TYPE_ORDER_ESTAMPING', 'orderEStamping');

define('SIGN_CONFIRMATION', 'AGENT_SIGN_CONFIRMATION');
define('SIGN_ACCEPT', 'AGENT_ACCEPTED');
define('SIGN_REJECT', 'AGENT_REJECTED');
define('SIGN_ALL_DONE', 'ALL_SIGNED');


class BaseController extends Controller {

	protected function setupLayout()
	{
		if (!is_null($this->layout)) {
			
			if(Auth::check())
				$identity = Auth::user();
			else 
            	$identity = new stdClass();

			if (!empty($identity->image)) {
				// $identity->image = USER_IMAGE_URL . DIRECTORY_SEPARATOR . $identity->id . DIRECTORY_SEPARATOR . $identity->image;
			}

			$this->layout = View::make($this->layout)
				->with('route', Route::currentRouteName())
				->with('identity', $identity);
		}
	}

	protected function appendScript($src, $section = 'inline')
    {	
    	// TODO: Overide or merge same scripts.
    	// TODO: Sort or order.
    	$this->layout->scripts[$section][] = $src;
    	
    }	

    protected function appendStyle($src, $section = 'inline')
    {	
    	// TODO: Overide or merge same scripts.
    	// TODO: Sort or order.
    	$this->layout->styles[$section][] = $src;
    }

	protected function getRoles()
	{
		return array(
            ROLE_ADMIN => 'Administrator',
			ROLE_MOD => 'Moderator',
			ROLE_AGENT => 'Agent',
			ROLE_MEMBER => 'Member'
		    );
	}

	protected function getPostTypes()
	{
		return array(
			    POST_TYPE_TEXT => 'Text',
			    POST_TYPE_IMAGE => 'Image',
			    POST_TYPE_VIDEO => 'Video'

			);

	}
        
        public function apiGetAppVersion()
	{
            
            
             $appVersion=  AppVersion::first();

        
                if(!$appVersion){
                    $jsonResponse->setCode(400);
                    $jsonResponse->setBody('No App version found');
                    return $jsonResponse->get();
                }
        
 
            $response=array(
                'IOS_APP_VERSION'=>$appVersion->IOS_APP_VERSION,
                'IOS_BUILD_VERSION'=>(int)$appVersion->IOS_BUILD_VERSION,
                'ANDROID_VERSION'=>$appVersion->ANDROID_VERSION,
                'ANDROID_VERSION_CODE'=>(int)$appVersion->ANDROID_VERSION_CODE,
                'ANDROID_DB_VERSION'=>(int)$appVersion->ANDROID_DB_VERSION,
                'order_submission_price'=>'1.00'
           
            );
            return $response;
            
//		return array(
//			    "IOS_APP_VERSION" => '1.3.4',
//                            "IOS_BUILD_VERSION" => 154,
//			    "ANDROID_VERSION" => '1.3.6',
//                            "ANDROID_VERSION_CODE" => 2015072401
//			);
                
		// IMPORTANT :: Please write down the app latest version number with date publish
		// 1. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.1 = 2015/06/24
		// 2. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.2 = 2015/07/20 (2015072102)
		// 3. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.3 = 2015/07/20 (2015072103)
                // 4. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.4 = 2015/07/21 (2015072104)
                // 5. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.5 = 2015/07/23 (2015072301)
                // 6. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.6 = 2015/07/24 (2015072401)
                // 7. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.7 = 2015/07/30 (2015073002)
                // 8. IOS 1.3.1 = 2015/06/24 | ANDROID 1.3.8 = 2015/09/09 (2015090902)
                // 10. IOS 1.3.1 = 2015/06/24| ANDROID 1.4.0 = 2015/10/09 (2015100901)
                // 11. IOS 1.3.1 = 2015/06/24| ANDROID 1.4.0 = 2015/10/13 (2015101301)

	}

    protected function gcm($token, $msg = 'hello', $id = 0, $type = '')
    {
        if (empty($token)) return;

        $recipient = User::getByDeviceToken($token)->first();

        $pushqueue = new PushQueue;
        $pushqueue->recipient = isset($recipient->id) ? $recipient->id : 0;
        $pushqueue->sender = Auth::user()->id;
        $pushqueue->type = 'gcm';
        $pushqueue->token = $token;
        $pushqueue->message = $msg;
        $pushqueue->time_queued = date('Y-m-d H:i:s');
        $pushqueue->time_sent = NULL;
        $pushqueue->message_type = $type;

        if($type == TYPE_BROADCAST)
            $pushqueue->property_id = $id;
        elseif($type == TYPE_FOLLOW)
            $pushqueue->follower_id = $id;
        elseif(in_array($type, [TYPE_NEGO, TYPE_TRANS]))
            $pushqueue->nego_id = $id;

        $pushqueue->save();

    }   

    protected function apns($token, $msg = 'hello', $id = 0, $type = '')
    {
        // $user = User::find(Auth::user()->id);
        if (empty($token)) return;

        $recipient = User::getByDeviceToken($token)->first();

        $pushqueue = new PushQueue;
        $pushqueue->recipient = isset($recipient->id) ? $recipient->id : 0;
        $pushqueue->sender = Auth::user()->id;
        $pushqueue->type = 'apns';
        $pushqueue->token = $token;
        $pushqueue->message = $msg;
        $pushqueue->time_queued = date('Y-m-d H:i:s');
        $pushqueue->time_sent = NULL;
        $pushqueue->message_type = $type;

        if($type == TYPE_BROADCAST)
            $pushqueue->property_id = $id;
        elseif($type == TYPE_FOLLOW)
            $pushqueue->follower_id = $id;
        elseif(in_array($type, [TYPE_NEGO, TYPE_TRANS]))
            $pushqueue->nego_id = $id;

        $pushqueue->save();

    }    

 public function userToJson($user,$follow){
     
        $user_array=array(
            'id' => (int) $user->id,
            'name' => $user->name,
            // 'ic_number' => $user->ic_number,    
            'email' => $user->email,
            'mobile' => $user->mobile,
            'role'=>array(
                'id'=>(int)($user->role->id==4)?7:$user->role->id,
                'name'=>($user->lang=='zh')?$user->role->name_zh:$user->role->name_en,
            ),
            'isPhoneVerify'=>$user->isPhoneVerify,
            'status'=>(int)$user->status,
        );
        return $user_array;
    }	

    public function cal_estamping($order){
    
        $rentalFee=$order->rental;
        $tenureYear=$order->termsOfTenancyYears;
		$tenureMonth=$order->termsOfTenancyMonths;
		$commencementDate=$order->commencementDate;

		$overallTenure=$tenureYear + ($tenureMonth / 12);
        
        $stampingFee=ceil((( $rentalFee * 12 ) - 2400 ) / 250);
        if ($stampingFee <= 0) {
        	$stampingFee=0;
        }

        if ($overallTenure > 4) {
        	$stampingFee=$stampingFee * 3;
        } else if ($overallTenure > 1) {
        	$stampingFee=$stampingFee * 2;
        }

		$today = new DateTime('NOW');
	    $commencement = new DateTime($commencementDate);
	    
	    $interval = date_diff($today, $commencement);
	    $monthDifference = $interval->format('%m');

	    if ($monthDifference > 6) {
	    	if ($stampingFee * 0.2 > 100) {
	    		$penaltyFee=$stampingFee * 0.2;
	    	} else {
	    		$penaltyFee=100;
	    	}
	    } else if ($monthDifference > 3) {
	    	if ($stampingFee * 0.1 > 50) {
	    		$penaltyFee=$stampingFee * 0.1;
	    	} else {
	    		$penaltyFee=50;
	    	}
	    } else if ($monthDifference > 0) {
	    	if ($stampingFee * 0.05 > 25) {
	    		$penaltyFee=$stampingFee * 0.05;
	    	} else {
	    		$penaltyFee=25;
	    	}
	    } else {
	    	$penaltyFee=0;
	    }

        $stampingDuplicateFee=10;

        if ($stampingFee > 800) {
        	$serviceFee=$stampingFee * 0.05;
        }
        $serviceFee=40;
        
        return array(
        	'stampingFee' => $stampingFee,
        	'penaltyFee' => $penaltyFee,
        	'stampingDuplicateFee' => $stampingDuplicateFee,
        	'serviceFee' => $serviceFee
        );
    }
    
    public function _timeago($ptime)
	{
	    $estimate_time = time() - $ptime;

	    if( $estimate_time < 1 )
	    {
	        return 'less than 1 second ago';
	    }

	    $condition = array( 
	                12 * 30 * 24 * 60 * 60  =>  'year',
	                30 * 24 * 60 * 60       =>  'month',
	                24 * 60 * 60            =>  'day',
	                60 * 60                 =>  'hour',
	                60                      =>  'minute',
	                1                       =>  'second'
	    );

	    foreach( $condition as $secs => $str )
	    {
	        $d = $estimate_time / $secs;

	        if( $d >= 1 )
	        {
	            $r = round( $d );
	            return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
	        }
	    }
	}
        
        public function getDateFromDateTime($dateTime){
            $date = strtotime($dateTime);
            return date('d F Y', $date);
        }
        
        
            public function sendSMS($mobile,$content)
            {
                // Your Account SID and Auth Token from twilio.com/console
                $sid = 'ACe070fb81c5459bb5c4496523a4199ea7';
                $token = '7409e2e7aca8c8250d48eafd860e3426';
                $client = new Client($sid, $token);

                // Use the client to do fun stuff like send text messages!
                $client->messages->create(
                    // the number you'd like to send the message to
                    $mobile,
                    array(
                        // A Twilio phone number you purchased at twilio.com/console
                        'from' => '+601117223303',
                        // the body of the text message you'd like to send
                        'body' => $content
                    )
                );

                $result="Sent";
                return $result;
            }

            public function sendAuthSMS($mobile) {
                $validationResult=$this->checkPhoneValidation($mobile);

                if($validationResult['status'] == 1){
                    $url = 'https://api.authy.com/protected/json/phones/verification/start?api_key=wsLOBZxPXePqoUrmnCkCwisoPoDwByee';

                    $fields = array(
                        'via' => 'sms',
                        'phone_number' => $validationResult['phoneObject']['nationalNumber'],
                        'country_code' => $validationResult['phoneObject']['countryCode'],
                        'locale' => 'en'
                    );
                    
                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    $result = curl_exec($ch);
                    curl_close($ch);
                }

                return $result;
            }

            public function callAuthNumber($mobile) {
                $validationResult=$this->checkPhoneValidation($mobile);

                if($validationResult['status'] == 1){
                    $url = 'https://api.authy.com/protected/json/phones/verification/start?api_key=wsLOBZxPXePqoUrmnCkCwisoPoDwByee';

                    $fields = array(
                        'via' => 'call',
                        'phone_number' => $validationResult['phoneObject']['nationalNumber'],
                        'country_code' => $validationResult['phoneObject']['countryCode']
                    );
                    
                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    $result = curl_exec($ch);
                    curl_close($ch);
                }

                return $result;
            }
            
            public function checkPhoneValidation($mobile){


                $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
                try {
                    $phoneNumberObject = $phoneUtil->parse($mobile, "MY");

                    if(!$phoneUtil->isValidNumber($phoneNumberObject)){
                        return array('status'=>0,'msg'=>'Invalid phone number');
                    }

                    $countryCode=$phoneNumberObject->getCountryCode();
                    $nationalNumber=$phoneNumberObject->getNationalNumber();

                    return array(
                        'status'=>1,
                        'phoneObject'=>array(
                            'countryCode'=>$countryCode,
                            'nationalNumber'=> $nationalNumber
                            )
                        );

                } catch (\libphonenumber\NumberParseException $e) {
                    return array('status'=>0,'msg'=>$e->getMessage());
                }
            }   
         
            function generateRandomString($length = 10) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
            }

            public function couponToJson($coupon){

               if($coupon){
                   return array(
                      'id'=>(int)$coupon->id,
                      'trans_id'=>$coupon->trans_code,
                      'coupon_code'=>$coupon->coupon_code,
                      'description'=>'Free one order submission.',
                      'created_date'=>date('Y-m-d', strtotime($coupon->created_on)),
                      'status'=>$coupon->status
                      );
               }

               return new ArrayObject();
           }
           
           public function _generatePassword($length) {
                $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
                $rand = '';
                while($length > 0) {
                    $rand .= $charset[rand(0, strlen($charset) - 1)];
                    $length--;
                }
                return $rand;
            }
}
