<?php


use Swagger\Annotations as SWG;
// use Log;


/**
* @SWG\Resource(
* 	apiVersion="1.0",
*	resourcePath="/users",
*	description="users operations",
*	produces="['application/json']"
* )
*/

class UserController extends BaseController {

    protected $layout = 'layout.admin';
    
    public function getUsers() {
        $col = Input::get('col', 'created_on');
        $sort = Input::get('sort', 'asc');
        $keyword = Input::get('keyword', '');
        $role = Input::get('role', 'all');

        $daterange = Input::get('daterange', date('01/01/Y') . ' - '.date('d/m/Y'));
        $start = date('01/01/Y');
        $end = date('d/m/Y');
        if (!empty($daterange)) {
            list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }

        if($col == 'role') {
            $col = 'role_id';
        } elseif($col == 'nameNRIC') {
            $col = 'first_name';
        }

        $users = User::getByKeywordAndRole($keyword, $role)
        ->where('user.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
        ->where('user.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
        ->orderBy($col, $sort);

        if($col == 'isPhoneVerify') {
            $users = $users->orderBy('facebook_id', $sort);
        }
        $users = $users->paginate(20);

        $this->layout->body = View::make('user.index')
        ->with('users', $users);
    }

    public function getUser($id = 0) {
        $this->appendScript('/assets/js/moment.js', 'inline');
        $this->appendScript('/assets/js/combodate.js', 'inline');
        $this->appendScript('/assets/js/user.js', 'inline');

        if (empty($id)) {
            $user = NULL;
        } else {
            $user = User::getById($id)
            ->first();
            if (!isset($user->id)) {
                return Redirect::route('user.list')
                ->with('alert.danger', 'User not found.');
            }

            if (Auth::user()->role->name_en == 'Moderator' && $user->role->name_en == 'Administrator') {
                return Redirect::route('user.list')
                ->with('alert.danger', 'You are unauthorized to change administrator info.');
            }
        }     

        $options = array('' => 'Select One');

        $userroles = UserRole::lists('name_en', 'id');

        $this->layout->body = View::make('user.form')
        ->with('user', $user)
        ->with('roles', $options + $userroles);
    }

    public function postUser($id = 0) {

        //validation
        $rules = array(
            'name' => 'required',
            'ic_number' => 'required|unique:user,ic_number,'.$id.',id,status,0,status,1',
            'email' => 'required|email|unique:user,email,'.$id.',id,status,0,status,1',
            'mobile' => 'required|unique:user,mobile,'.$id.',id,status,0,status,1',
            );
        if(empty(Input::get('role_id'))){
            $rules['role']='required';
        }

        $user = User::getById($id)->first();
        $user = empty($user->id) ? new User : $user;
        $password = Input::get('password', '');
        $mobile=Input::get('mobile');

        if(empty($user->id) || strlen($password) > 0) {
            $rules['password'] = 'required|min:6';
            $rules['confirm_password'] = 'required_with:password|same:password';
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            if (!empty($user->id)) {
                return Redirect::route('user.edit', array( 'id' => $id ))
                ->withErrors($validator)
                ->withInput(Input::except(array('password', 'confirm_password')));
            } else {
                return Redirect::route('user.add')
                ->withErrors($validator)
                ->withInput(Input::except(array('password', 'confirm_password')));
            }
        }

        $user->name = Input::get('name');
        $user->email = Input::get('email');
        $user->ic_number = Input::get('ic_number');
        $user->mobile = Input::get('mobile');
        $user->role_id = Input::get('role_id');
        $user->status = Input::get('status');

        if (strlen($password) > 0) 
            $user->password = Hash::make($password);

        $action = empty($id) ? 'added.' : 'updated.';

        if($user->save()) {
            if(empty($id) && $user->role_id == 3){
                $this->sendAuthSMS($mobile);
            }           
    
            return Redirect::route('user.edit', array( 'id' => $user->id ))
            ->with('alert.success', 'User has been successfully ' . $action);
        }

        if(empty($user->id)){
            //save failed
            return Redirect::route('user.new')
            ->with('alert.danger', 'Add new user had failed. Please try again.');
        } else {
            return Redirect::route('user.edit', array( 'id' => $id ))
            ->with('alert.danger', 'Update user failed. Please try again.');
        }

    }
    
    public function deleteUser($id = 0) {
        $user = User::find($id);

        if (empty($user->id)) {
            return Redirect::route('user.list')
            ->with('alert.danger', 'User does not exist.');
        }

        $user->status = -1;
        $user->save();

        return Redirect::route('user.list')
        ->with('alert.success', 'User has been deleted.');
    }
    
    public function getVerify($id) {
        $verify = User::where('id', $id)
        ->first();
        $verify->isPhoneVerify = 'true';
        $verify->save();
        return Redirect::route('user.list')
        ->with('alert.success', $verify->first_name.' '. $verify->last_name.' verified.');
    }

    public function getCSV() {
        $keyword = Input::get('skeyword', '');
        $records = Input::get('srecords', 0);
        $startid = Input::get('sstartid', 1);

        $daterange = Input::get('sdate', '01/01/2015 - '.date('d/m/Y'));
        $start = '01/01/2015';
        $end = date('d/m/Y');
        if (!empty($daterange)) {
            list($start, $end) = array_map('trim', explode('-', $daterange));
        } else {
            $daterange = $start . ' - ' . $end;
        }

        $role = Input::get('srole', 'all');

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="users.csv"',
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
            );

        $output = array('role','name','agency','mobile','email','status','created on', 'phone verify');

        $temp = User::getByKeywordAndRole($keyword,$role)->select('user.id')
        ->where('user.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
        ->where('user.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
        ->where('user.id', '>=', $startid)
        ->where('user.status', '<>', -1);
        if($records > 0) {
            $temp = $temp->limit($records);
        }

        $temp = $temp->get();

        $user_count = count($temp);
        $limitrate = 1000;

        return Response::stream(function() use ($user_count,$records, $limitrate, $output, $keyword, $start, $end, $startid, $role) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $output);

            $counter = 0;
            $remainder = $user_count;
            if($user_count > $limitrate) {
                while($counter < $user_count){
                    $users = User::getByKeywordAndRole($keyword,$role)
                    ->where('user.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                    ->where('user.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                    ->where('user.id', '>=', $startid)
                    ->skip($counter)
                    ->take(min($remainder, $limitrate))
                    ->get();

                    foreach($users as $row) {
                        if ($row->status == 1) {
                            $status = 'Active';
                        } else {
                            $status = 'Inactive';
                        }

                        $user_role = UserRole::find($row->role_id);
                        $u_role = '';
                        if(count($user_role) > 0) {
                            $u_role = $user_role->name_en;
                        }

                        $agency_name = '';
                        if (!empty($row->agency_id)) {
                            $user_agency = Agency::find($row->agency_id);
                            if(count($user_agency) > 0) {
                                $agency_name = $user_agency->name;
                            }
                        }
                        if ($row->isPhoneVerify == 'true') {
                            $phone = 'Verified';
                        } else {
                            if ($row->facebook_id != '' || !empty($row->facebook_id)) {
                                $phone = 'Facebook Login';
                            } else {
                                $phone = '-';
                            }
                        }

                        fputcsv($file, array($u_role,$row->first_name .' ' . $row->last_name,$agency_name,$row->mobile,$row->email,$status,$row->created_on,$phone));
                    }

                    $counter = $counter + $limitrate;
                    $remainder = $remainder - $limitrate;
                }
            } else {
                $users = User::getByKeywordAndRole($keyword,$role)
                ->where('user.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
                ->where('user.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
                ->where('user.id', '>=', $startid)
                ->take($user_count)
                ->get();

                foreach($users as $row) {
                    if ($row->status == 1) {
                        $status = 'Active';
                    } else {
                        $status = 'Inactive';
                    }

                    $user_role = UserRole::find($row->role_id);
                    $u_role = '';
                    if (count($user_role) > 0) {
                        $u_role = $user_role->name_en;
                    }

                    $agency_name = '';
                    if (!empty($row->agency_id)) {
                        $user_agency = Agency::find($row->agency_id);
                        if(count($user_agency) > 0) {
                            $agency_name = $user_agency->name;
                        }
                    }
                    if ($row->isPhoneVerify == 'true') {
                        $phone = 'Verified';
                    } else {
                        if ($row->facebook_id != '' || !empty($row->facebook_id)) {
                            $phone = 'Facebook Login';
                        } else {
                            $phone = '-';
                        }
                    }
                    fputcsv($file, array($u_role,$row->first_name .' ' . $row->last_name,$agency_name,$row->mobile,$row->email,$status,$row->created_on,$phone));
                }
            }

            fclose($file);
        }, 200, $headers);
}

public function getTopCsv() {
    $keyword = Input::get('skeyword', '');
    $type =  Input::get('stype', '');
    $country =  Input::get('scountry', '');
    $state =  Input::get('sstate', '');
    $area =  Input::get('sarea', '');

    $daterange = Input::get('sdate', '01/01/2015 - '.date('d/m/Y'));
    $start = '01/01/2015';
    $end = date('d/m/Y');
    if (!empty($daterange)) {
        list($start, $end) = array_map('trim', explode('-', $daterange));
    } else {
        $daterange = $start . ' - ' . $end;
    }

    $toplisting = Property::getagent($keyword, $type, $country, $state, $area)
    ->where('property.created_on', '>=', date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start))))
    ->where('property.created_on', '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end))))
    ->get();

    $output = "name,agency,mobile,email,total,\n";
    foreach ($toplisting as $row) {

        $output .=  '"' . $row->first_name .' ' . $row->last_name .
        '","' . $row->name .
        '","' . $row->mobile . 
        '","' . $row->email .
        '","' . $row->total_p .
        "\"\n";
    }
    $headers = array(
        'Content-Type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename="top.csv"',
        );

    return Response::make($output, 200, $headers);     
}















/**
*
* @SWG\Api(
*   path="/users/login",
*   @SWG\Operations(
*     @SWG\Operation(
*       method="POST",
*       summary="User login",
*       notes="Returns a json",
*       nickname="login",
*       @SWG\Parameters(
*         @SWG\Parameter(
*           name="email",
*           description="user's email",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="password",
*           description="user's password",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="currentVersionCode",
*           description="user's current version",
*           paramType="form",
*           required=false,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="platform",
*           description="device's platform",
*           paramType="form",
*           required=true,
*           enum={"android", "ios"},
*           type="string",
*          
*          ),
*       ),
*          @SWG\ResponseMessage(
*            code=401,
*            message="Email and password does not match"
*          )
*       )
*     )
*   )
* )
*/ 
public function apiLogin() {
    $jsonResponse = new JsonResponse();
    $response = '';

    $currentVersionCode= Input::get('currentVersionCode',"");
    $platform=Input::get('platform',"");

    $rules = array(
        'email' => 'required',
        'password' => 'required'
        );

    $validator = Validator::make(Input::all(), $rules);

    if($validator->fails()) {
        $jsonResponse->setCode(400);
        $jsonResponse->setBody($validator->messages()->first());

    } else {
        $user = User::where('email', Input::get('email'))
        ->whereIn('status', array(0,1))
        ->first();

        if (isset($user->id)) {
            $attempt = Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')));
            if ($attempt) {
                $this->insertCurrentVersion($currentVersionCode,$platform);
                $response =$this->userToJson($user, "");
            } else {
                $jsonResponse->setCode(401);
                $jsonResponse->setSubject('Error');
                $jsonResponse->setBody('Email and password does not match');
            }
        } else {
            $jsonResponse->setCode(401);
            $jsonResponse->setSubject('Error');
            $jsonResponse->setBody('Email and password does not match');
        }
    }
    $jsonResponse->setResponse($response);
    return $jsonResponse->get();
}

/**
*
* @SWG\Api(
*   path="/users/register",
*   @SWG\Operations(
*     @SWG\Operation(
*       method="POST",
*       summary="User registration",
*       notes="Returns a json",
*       nickname="Register",
*       @SWG\Parameters(
*         @SWG\Parameter(
*           name="name",
*           description="user's name",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="ic_number",
*           description="user's I/C No.",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="email",
*           description="user's email",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="mobile",
*           description="user's mobile number",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="password",
*           description="user's password",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*       ),
*          @SWG\ResponseMessage(
*            code=400,
*            message="Validation Error"
*          ),
*          @SWG\ResponseMessage(
*            code=404,
*            message="User not found."
*          )
*       )
*     )
*   )
* )
*/ 

public function apiRegister() {
    $jsonResponse = new JsonResponse();

    $rules = array(
        'name' => 'required',
        'ic_number' => 'required',
        'email' => 'required|email|unique:user,email,-2,status',
        'mobile' => 'required|unique:user,mobile',
        'password' => 'required'
        );     

    $messages = array(
        'name.required' => 'The name field is required.',
        'ic_number.required' => 'The IC number field is required.',
        'email.required' => 'The email field is required.',
        'mobile.required' => 'The mobile number field is required.',
        'password.required' => 'The password field is required.'
    );

    $validator = Validator::make(Input::all(), $rules, $messages);

    if ($validator->fails()) {
        $jsonResponse->setCode(400);
        $jsonResponse->setSubject('Error');
        $jsonResponse->setBody($validator->messages()->first());

        return $jsonResponse->get();
    }


    $name=Input::get('name','');
    $ic_number=Input::get('ic_number');
    $email=Input::get('email');
    $mobile=Input::get('mobile');
    $password = Input::get('password');

    $user = User::where('email', $email)
    ->where('status',-2)
    ->first();

    if (!$user) {
        $user = new User;
    }

    $user->name = $name;
    $user->ic_number = $ic_number;
    $user->email = $email;
    $user->mobile = $mobile;

    // Some default value
    $user->role_id = 3;
    $user->status = 0;
    $user->isPhoneVerify = 'false';
    $user->password = Hash::make($password);

    if ($user->save()) {

        $user=User::find($user->id);
        $attempt = Auth::loginUsingId($user->id);

        $response = $this->userToJson($user, '');

        $jsonResponse->setSubject('Congratulations');
        $jsonResponse->setBody('Thank you for registering with us.');

        $SMSStatus = $this->sendAuthSMS($mobile);
        $response['SMSStatus'] = json_decode($SMSStatus, true);

        $subject="Thank you for registering with NanCy";

        Mail::send('emails.user_register', array(), function($message) use ($user, $subject) {
            $message->to($user->email, $user->name)->subject($subject);
        });
    }

    $jsonResponse->setResponse($response);

    return $jsonResponse->get();
}

/**
*
* @SWG\Api(
*   path="/users/reset",
*   @SWG\Operations(
*     @SWG\Operation(
*       method="POST",
*       summary="Reset user password",
*       notes="Returns a json",
*       nickname="ResetPassword",
*       @SWG\Parameters(
*         @SWG\Parameter(
*           name="email",
*           description="user's email",
*           paramType="form",
*           required=true,
*           type="string"
*          )
*       ),
*          @SWG\ResponseMessage(
*            code=404,
*            message="Email address not found"
*          )
*       )
*     )
*   )
* )
*/ 
public function apiReset() {
    $jsonResponse = new JsonResponse();
    $response = '';

    $rules = array(
        'email' => 'required',
        );

    $validator = Validator::make(Input::all(), $rules);

    if($validator->fails()) {
        $jsonResponse->setCode(400);
        $jsonResponse->setBody($validator->messages()->first());
    } else {
        $user = User::getByEmail(Input::get('email'))
        ->first();
        if (isset($user->id)) {
            $password = $this->_generatePassword(8);
            $user->password = Hash::make($password);
            $user->modified_on = date('Y-m-d H:i:s');
            $user->modified_by = $user->id;
            $user->save();

            try {
                Mail::send('emails.auth.reset', array('password' => $password), function($message) use ($user) {
                    $message->to($user->email, $user->name)->subject('Reset password');
                });
                $jsonResponse->setBody('New password has been sent to your mailbox.');
            } catch (Exception $e) {
                $jsonResponse->setCode(400);
                $jsonResponse->setBody($e->getMessage());
            }                


        } else {
            $jsonResponse->setCode(404);
            $jsonResponse->setBody('Email address not found');
        }

    }

    $jsonResponse->setResponse($response);

    return $jsonResponse->get();

}

/**
*
* @SWG\Api(
*   path="/users/requestPhoneVerificationById",
*   @SWG\Operations(
*     @SWG\Operation(
*       method="POST",
*       summary="Request phone verification by user ID",
*       notes="Returns a json",
*       nickname="requestPhoneVerificationById",
*       @SWG\Parameters(
*         @SWG\Parameter(
*           name="userID",
*           description="user's ID",
*           paramType="form",
*           required=true,
*           type="string"
*          )
*       ),
*          @SWG\ResponseMessage(
*            code=400,
*            message="Account is Verified"
*          ),
*          @SWG\ResponseMessage(
*            code=404,
*            message="User not found."
*          )
*       )
*     )
*   )
* )
*/ 
public function apiRequestPhoneVerificationById() {

    $jsonResponse = new JsonResponse();
    $userID=Input::get('userID');
    $currentUser=User::find($userID);

    if (!$currentUser) {
        $jsonResponse->setCode(404);
        $jsonResponse->setBody('User not found.');
        return $jsonResponse->get();
    }

    $mobile=$currentUser->mobile;
    $isPhoneVerify=$currentUser->isPhoneVerify;

    if ($isPhoneVerify == 'true') {            
        $isPhoneVerify = true;
    } else {
        $isPhoneVerify = false;
    }

    if ($isPhoneVerify) {
        $jsonResponse->setCode(400);
        $jsonResponse->setSubject('Error');
        $jsonResponse->setBody('Your account is verified');
        return $jsonResponse->get();  
    }

    $SMSStatus = $this->sendAuthSMS($mobile);

    $jsonResponse->setBody('Your mobile verification code has been sent.');

    $response = json_decode($SMSStatus, true);

    $jsonResponse->setResponse($response);

    return $jsonResponse->get();
}

/**
*
* @SWG\Api(
*   path="/users/requestPhoneVerificationCallById",
*   @SWG\Operations(
*     @SWG\Operation(
*       method="POST",
*       summary="Request a phone call for verification by user ID",
*       notes="Returns a json",
*       nickname="requestPhoneVerificationById",
*       @SWG\Parameters(
*         @SWG\Parameter(
*           name="userID",
*           description="user's ID",
*           paramType="form",
*           required=true,
*           type="string"
*          )
*       ),
*          @SWG\ResponseMessage(
*            code=400,
*            message="Account is Verified"
*          ),
*          @SWG\ResponseMessage(
*            code=404,
*            message="User not found."
*          )
*       )
*     )
*   )
* )
*/ 
public function apiRequestPhoneVerificationCallById() {

    $jsonResponse = new JsonResponse();
    $userID=Input::get('userID');
    $currentUser=User::find($userID);

    if (!$currentUser) {
        $jsonResponse->setCode(404);
        $jsonResponse->setBody('User not found.');
        return $jsonResponse->get();
    }

    $mobile=$currentUser->mobile;
    $isPhoneVerify=$currentUser->isPhoneVerify;

    if ($isPhoneVerify == 'true') {            
        $isPhoneVerify = true;
    } else {
        $isPhoneVerify = false;
    }

    if ($isPhoneVerify) {
        $jsonResponse->setCode(400);
        $jsonResponse->setSubject('Error');
        $jsonResponse->setBody('Your account is verified');
        return $jsonResponse->get();  
    }

    $CallStatus = $this->callAuthNumber($mobile);

    $jsonResponse->setBody('Your mobile verification code has been sent.');

    $response = json_decode($CallStatus, true);

    $jsonResponse->setResponse($response);

    return $jsonResponse->get();
}

/**
*
* @SWG\Api(
*   path="/users/submitPhoneVerificationById",
*   @SWG\Operations(
*     @SWG\Operation(
*       method="POST",
*       summary="Submit phone verification code by user id",
*       notes="Returns a json",
*       nickname="submitPhoneVerificationById",
*       @SWG\Parameters(
*         @SWG\Parameter(
*           name="verificationCode",
*           description="user's verification code",
*           paramType="form",
*           required=true,
*           type="string"
*          ),
*         @SWG\Parameter(
*           name="userID",
*           description="user's ID",
*           paramType="form",
*           required=true,
*           type="string"
*          )
*       )
*       )
*     )
*   )
* )
*/ 
public function apiSubmitPhoneVerificationById() {
    $jsonResponse = new JsonResponse();
    $verificationCode= Input::get('verificationCode');
    $userID=Input::get('userID');
    $currentUser=User::find($userID);

    if (!$currentUser) {
        $jsonResponse->setCode(404);
        $jsonResponse->setBody('User not found.');
        return $jsonResponse->get();
    }

    $isPhoneVerify=$currentUser->isPhoneVerify;

    if ($isPhoneVerify == 'true') {            
        $isPhoneVerify=true;
    } else {
        $isPhoneVerify=false;
    }


    if ($isPhoneVerify) {
        $jsonResponse->setCode(400);
        $jsonResponse->setSubject('Error');
        $jsonResponse->setBody('Your account is verified');
        return $jsonResponse->get();  
    }

    $mobile=$currentUser->mobile;
    $validationResult=$this->checkPhoneValidation($mobile);

    $phoneNumber = $validationResult['phoneObject']['nationalNumber'];
    $countryCode = $validationResult['phoneObject']['countryCode'];
    $url = 'https://api.authy.com/protected/json/phones/verification/check?api_key=wsLOBZxPXePqoUrmnCkCwisoPoDwByee&phone_number=' . $phoneNumber . '&country_code=' . $countryCode . '&verification_code=' . $verificationCode;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $response = json_decode($result, true);

    if ($response['success']) {
        $currentUser->isPhoneVerify='true';
        $currentUser->status=1;
        $currentUser->save();

        $jsonResponse->setBody('Your mobile number has been verified.');
        $isPhoneVerify=true;

        $attempt = Auth::loginUsingId($currentUser->id);
    } else {
        $jsonResponse->setCode(400);
        $jsonResponse->setBody('Invalid verification code. Please try again later');
    }

    $jsonResponse->setResponse($response);

    return $jsonResponse->get();
}

public function insertCurrentVersion($currentVersionCode,$platform){

    if(Auth::user() != null && $currentVersionCode !=""){
        $user=User::find(Auth::user()->id);

        if($platform == 'android')
            $user->AndroidVersionCode = $currentVersionCode;
        else if($platform == 'ios')
            $user->IosVersionCode = $currentVersionCode;

        $user->save();
    }
}

function gen_uuid($len=5,$userID) {

    $hex = md5("wonderlist88" . uniqid($userID, true));

    $pack = pack('H*', $hex);
    $tmp =  base64_encode($pack);

    $uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

    $len = max(4, min(128, $len));

    while (strlen($uid) < $len)
        $uid .= gen_uuid(22);

    return strtoupper(substr($uid, 0, $len));
}


function gen_uuid_integer($len=6,$userID) {

    $timestamp=time();
    $userID_length=strlen($userID);

    if ($len>$userID_length)
        $len_remaining=$len-$userID_length;

    $newstring = substr($timestamp, -$len_remaining);

    return "$newstring$userID";
}
}
