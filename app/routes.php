<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

/* Route::get('/', function(){
  return File::get(public_path() . '/index.html');
  }); */

Route::get('/', array(
    'before' => 'force.ssl',
    'as' => 'home',
    'uses' => 'HomeController@getIndex'
));

Route::post('/', array(
    'as' => 'contact-us',
    'uses' => 'HomeController@postContactUs'
));

Route::get('{id}/testpdf', array(
    'as' => 'pdf.view',
    'uses' => 'OrderController@docxToPdf'
))->where('id', '[0-9]+');

Route::get('{id}/testsign', array(
    'as' => 'pdf.view',
    'uses' => 'OrderController@generateOrderSignPDF'
))->where('id', '[0-9]+');

Route::get('/mailtest', array(
    'as' => 'mailtest',
    'uses' => 'HomeController@getMail'
));

Route::get('{id}/scheduleB', array(
    'as' => 'pdf.scheduleB',
    'uses' => 'OrderController@getOrderScheduleB'
))->where('id', '[0-9]+');

// Order
Route::group(['prefix' => 'order'], function() {
    Route::get('{id}/email', ['as' => 'order.email', 'uses' => 'OrderController@sendReceivedEmail'])
            ->where('id', '[0-9]+');
});
//E-sign
Route::group(array('prefix' => 'esign', 'before' => 'guest'), function() {
    Route::get('/', array(
        'as' => 'user.esign',
        'uses' => 'OrderController@getUserEsign'
    ));
    Route::post('/', array(
        'uses' => 'OrderController@postUserEsign'
    ));
    Route::get('view', array(
        'as' => 'user.esign_view',
        'uses' => 'OrderController@getEsignOrder'
    ));
    Route::post('view', array(
        'as' => 'user.esign_save',
        'uses' => 'OrderController@postEsignOrder'
    ));
});

// User
Route::group(array('prefix' => 'users'), function() {

    Route::get('register', array(
        'as' => 'user.register',
        'before' => 'guest',
        'uses' => 'AccountController@getUserRegister'
    ));
    Route::post('register', array(
        'before' => 'guest',
        'uses' => 'AccountController@postUserRegister'
    ));
    Route::get('verification', array(
        'as' => 'user.verifyCode',
        'before' => 'guest',
        'uses' => 'AccountController@getUserVerification'
    ));
    Route::post('verification', array(
        'before' => 'guest',
        'uses' => 'AccountController@postUserVerification'
    ));
    Route::get('verified', array(
        'as' => 'user.verified',
        'uses' => 'AccountController@getUserVerified'
    ));

    Route::get('login', array(
        'as' => 'user.login',
        'before' => 'guest',
        'uses' => 'AccountController@getUserLogin'
    ));
    Route::post('login', array(
        'before' => 'guest',
        'uses' => 'AccountController@postUserLogin'
    ));
    Route::get('forgetpassword', array(
        'as' => 'user.forget',
        'before' => 'guest',
        'uses' => 'AccountController@getResetPW'
    ));
    Route::post('forgetpassword', array(
        'before' => 'guest',
        'uses' => 'AccountController@postResetPW'
    ));
    Route::post('resend', array(
        'before' => 'guest',
        'as' => 'resend.code',
        'uses' => 'UserController@apiRequestPhoneVerificationById'
    ));

    Route::get('logout', array(
        'as' => 'user.logout',
        'uses' => 'AccountController@getUserLogout'
    ));

    Route::get('activate/{hash}', array(
        'as' => 'user.activate',
        'uses' => 'AccountController@getUserActivate'
    ));
});

//auth.user
Route::group(array('before' => 'auth.user'), function() {
    Route::group(array('prefix' => 'users'), function() {
        Route::get('me', array(
            'as' => 'user.me',
            'uses' => 'AccountController@getUserProfile'
        ));
        Route::post('me', 'AccountController@postUserProfile');

        Route::post('password', array(
            'as' => 'user.password',
            'uses' => 'AccountController@postUserPassword'
        ));

        Route::post('{id}/review', array(
            'as' => 'user.rate',
            'uses' => 'UserController@postReview'
        ));
        Route::get('statistic', array(
            'as' => 'user.statistic',
            'uses' => 'AnalyticController@getUserStatistics'
        ));

        // Favourie property list

        Route::get('favourite', array(
            'as' => 'property.favourite',
            'uses' => 'PropertyController@getFavourite'
        ));

        Route::post('totalfav', array(
            'as' => 'property.totalfav',
            'uses' => 'PropertyController@getTotalFavourite'
        ));
        // End Favourite property list

        Route::get('{id}/reviews', array(
            'as' => 'user.reviews',
            'uses' => 'UserController@getUserReviews'
        ));
        Route::get('{id}/followerlists', array(
            'as' => 'user.followerlists',
            'uses' => 'UserController@getUserFollowerlist'
        ));

        /* User verify page */
        Route::get('verify', array(
            'as' => 'user.codeverify',
            'uses' => 'AccountController@getUserVerify'
        ));
        Route::post('sendvcode', array(
            'as' => 'user.sendvcode',
            'uses' => 'UserController@apiRequestPhoneVerificationById'
        ));
        Route::post('submitvcode', array(
            'as' => 'user.submitvcode',
            'uses' => 'UserController@apiSubmitPhoneVerificationById'
        ));
        /* end of user verify page */
    });

    Route::group(array('prefix' => 'order'), function() {
        Route::get('/', array(
            'as' => 'order.lists',
            'uses' => 'OrderController@getUserOrder'
        ));

        Route::get('add', array(
            'as' => 'order.add',
            'uses' => 'OrderController@getOrderForm'
        ));
        Route::post('add', 'OrderController@postOrderForm');

        Route::get('{id}/edit', array(
            'as' => 'order_user.edit',
            'uses' => 'OrderController@getOrderForm'
        ))->where('id', '[0-9]+');

        Route::post('{id}/edit', 'OrderController@postOrderForm');

        Route::get('{id}/detail', array(
            'as' => 'order.detail',
            'uses' => 'OrderController@getOrderDetail'
        ))->where('id', '[0-9]+');

        Route::get('{id}/preview', array(
            'as' => 'order.preview',
            'uses' => 'OrderController@getOrderPDF'
        ))->where('id', '[0-9]+');

        Route::post('/sendOTP', array(
            'as' => 'order.sendOTP',
            'uses' => 'OrderController@apiSendSignInvitation'
        ));

        Route::post('/resend', array(
            'as' => 'order.resend',
            'uses' => 'OrderController@apiResendAgreement'
        ));

        Route::get('explanation', array(
            'as' => 'order.explanation',
            'uses' => 'OrderController@getOrderExplanation'
        ));

        Route::post('{id}/manual', array(
            'as' => 'order.manualSign',
            'uses' => 'OrderController@postOrderManualSign'
        ))->where('id', '[0-9]+');
        
        Route::get('{id}/renew', array(
            'as' => 'order.renew',
            'uses' => 'OrderController@getOrderForm'
        ))->where('id', '[0-9]+');

        Route::post('{id}/renew', 'OrderController@postOrderForm');
    });
});
//END auth.user

Route::group(array('prefix' => 'payment'), function() {
    Route::get('redirect', array(
        'as' => 'payment.redirect',
        'uses' => 'PaymentController@getRedirect'
    ));
    Route::post('ipay88/response', array(
        'as' => 'payment.ipay88_response',
        'uses' => 'PaymentController@postIPay88Response'
    ));
    Route::get('{id}/success', array(
        'as' => 'payment.success',
        'uses' => 'PaymentController@getSuccess'
    ));
    Route::get('failed', array(
        'as' => 'payment.failed',
        'uses' => 'PaymentController@getFailed'
    ));
});
// the ipay backend doesn't keep the session
Route::post('payment/ipay88/backend', array(
    'as' => 'payment.ipay88_backend',
    'uses' => 'PaymentController@postIPay88Backend'
));

Route::post('api/payment/ipay88/sdkBackend', array(
    'as' => 'payment.ipay88_sdk_backend',
    'uses' => 'PaymentController@postSdkIPay88Backend'
));


Route::post('api/payment/ipay88/sdkBackendEstamping', array(
    'as' => 'payment.ipay88_sdk_backend_estamping',
    'uses' => 'PaymentController@postSdkIPay88Estamping'
));

// Admin
Route::group(array('prefix' => 'system', 'before' => 'force.ssl'), function() {

    Route::any('', function() {

        return Redirect::route('admin.login');
    });

    // Login
    Route::get('login', array(
        'as' => 'admin.login',
        'uses' => 'AccountController@getAdminLogin'
    ));
    Route::post('login', 'AccountController@postAdminLogin');

    // Logout
    Route::get('logout', array(
        'as' => 'admin.logout',
        'uses' => 'AccountController@getAdminLogout'
    ));

    // Auth
    Route::group(array('before' => 'auth.admin'), function() {

        // Settings
        Route::get('settings', array(
            'as' => 'admin.settings',
            'uses' => 'AccountController@getAdminSettings'
        ));
        Route::post('settings', 'AccountController@postAdminSettings');

        // Dashboard
        Route::group(array('before' => 'auth.adminOnly'), function() {
            Route::group(array('prefix' => 'dashboard'), function() {
                Route::get('', array(
                    'as' => 'dashboard.list',
                    'uses' => 'DashboardController@getPropAnalytic'
                ));
            });
        });
        // User 
        Route::group(array('prefix' => 'users'), function() {
            Route::get('', array(
                'as' => 'user.list',
                'uses' => 'UserController@getUsers'
            ));

            Route::get('csv', array(
                'as' => 'user.csv',
                'uses' => 'UserController@getCSV'
            ));
            Route::group(array('before' => 'auth.adminOnly'), function() {
                Route::get('add', array(
                    'as' => 'user.add',
                    'uses' => 'UserController@getUser'
                ));
                Route::post('add', 'UserController@postUser');
            });
            Route::get('{id}/edit', array(
                'as' => 'user.edit',
                'uses' => 'UserController@getUser'
            ))->where('id', '[0-9]+');

            Route::post('{id}/edit', 'UserController@postUser');

            Route::post('{id}/delete', array(
                'as' => 'user.delete',
                'uses' => 'UserController@deleteUser'
            ))->where('id', '[0-9]+');
            
            Route::get('{id}/verify', array(
                'as' => 'user.verify',
                'uses' => 'UserController@getVerify'
            ));
            Route::get('top', array(
                'as' => 'top.csv',
                'uses' => 'UserController@getTopCsv'
            ));
        });
        //tenancy
        Route::group(['prefix' => 'tenancy_agreement'], function() {
            Route::get('', ['as' => 'order.list', 'uses' => 'OrderController@getAdminOrder']);
            Route::get('{id}', ['as' => 'order.edit', 'uses' => 'OrderController@getAdminOrderForm'])
                    ->where('id', '[0-9]+');
            Route::post('{id}', ['as' => 'order.post', 'uses' => 'OrderController@postAdminOrderForm'])
                    ->where('id', '[0-9]+');
            Route::get('csv', ['as' => 'order.csv', 'uses' => 'OrderController@getCSV']);
        });
        //e-sign
        Route::group(['prefix' => 'e_sign'], function() {
            Route::get('', ['as' => 'e_sign.list', 'uses' => 'OrderController@getAdminOrder']);
        });
        //e-stamping
        Route::group(['prefix' => 'e_stamping'], function() {
            Route::get('', ['as' => 'e_stamping.list', 'uses' => 'OrderController@getAdminOrder']);
        });
        //e-storage
        Route::group(['prefix' => 'e_storage'], function() {
            Route::get('', ['as' => 'e_storage.list', 'uses' => 'OrderController@getAdminOrder']);
        });
        
        //explanation
        Route::group(['prefix' => 'explanation'], function() {
            Route::get('', ['as' => 'explanation.list', 'uses' => 'OrderController@getExplanationList']);
            Route::get('add', ['as' => 'explanation.add','uses' => 'OrderController@getExplanationForm']);
            Route::post('add', 'OrderController@postExplanationForm');
                       
            Route::get('{id}/edit', ['as' => 'explanation.edit','uses' => 'OrderController@getExplanationForm'])
                    ->where('id', '[0-9]+');
            Route::post('{id}/edit', 'OrderController@postExplanationForm');
            
            Route::post('{id}/delete', ['as' => 'explanation.delete','uses' => 'OrderController@deleteExplanation'])
                    ->where('id', '[0-9]+');
        });
        
        Route::group(['prefix' => 'payment'], function() {
            Route::get('', ['as' => 'order.payment', 'uses' => 'OrderController@getOrderPayment']);
        });

        //E-Stamping
//		Route::group(['prefix' => 'estamping'], function(){
//				Route::get('', ['as' => 'estamping.list', 'uses' => 'OrderController@getEStamping']);
//				Route::get('{id}', ['as' => 'estamping.edit', 'uses' => 'OrderController@getEStampingForm'])
//						->where('id', '[0-9]+');
//				Route::post('{id}', ['as' => 'estamping.post', 'uses' => 'OrderController@postEStampingForm'])
//						->where('id', '[0-9]+');
//				Route::get('csv', ['as' => 'estamping.csv','uses' => 'OrderController@getEStampingCSV']);
//		});
    }); // Auth
}); // Admin


// API
Route::group(array('prefix' => 'api'), function() {

    //Users
    Route::group(array('prefix' => 'users'), function() {
        Route::post('login', 'UserController@apiLogin');
        Route::post('register', 'UserController@apiRegister');
        Route::post('reset', 'UserController@apiReset');
        Route::post('requestPhoneVerificationCallById', 'UserController@apiRequestPhoneVerificationCallById');
        Route::post('requestPhoneVerificationById', 'UserController@apiRequestPhoneVerificationById');
        Route::post('submitPhoneVerificationById', 'UserController@apiSubmitPhoneVerificationById');
    });

    Route::group(array('prefix' => 'order'), function() {
        Route::group(array('before' => 'auth.api'), function() {
            Route::post('new', array('uses' => 'OrderController@apiNewOrder'));
            Route::post('edit', array('uses' => 'OrderController@apiEditOrder'));
            Route::post('newWithPDF', array('uses' => 'OrderController@apiNewOrderWithPDF'));
            Route::post('list', array('uses' => 'OrderController@apiListOrder'));
            Route::post('sendSignInvitation', array('uses' => 'OrderController@apiSendSignInvitation'));
            Route::post('getEstampingPrice', array('uses' => 'OrderController@apiGetEstampingPrice'));
            Route::post('getExplanationAudio', array('uses' => 'OrderController@apiGetExplanationAudio'));
            Route::post('resendAgreement', array('uses' => 'OrderController@apiResendAgreement'));
            Route::post('approve', array('uses' => 'OrderController@apiApproveOrder'));
            Route::post('reject', array('uses' => 'OrderController@apiRejectOrder'));
            Route::post('updateSpecialRequest', array('uses' => 'OrderController@apiUpdateOrderSpecialRequest'));
            Route::post('apiSubmitManualSignature', array('uses' => 'OrderController@apiSubmitManualSignature'));
            Route::post('fullDetail', array('uses' => 'OrderController@apiOrderFullDetail'));
        });
        Route::post('loginViaPasscode', array('uses' => 'OrderController@apiLoginViaPasscode'));
        Route::post('apiSubmitSignature', array('uses' => 'OrderController@apiSubmitSignature'));
        Route::post('detail', array('uses' => 'OrderController@apiOrderDetail'));


        Route::get('/appStore', ['as' => 'app.redirect', function() {
                return View::make('order.redirect');
            }]);

        Route::get('/signaturePDF', ['as' => 'app.signpdf', function() {
                return View::make('order.pdf.sign');
            }]);

        Route::get('/signaturePDF/{id}', array(
            'uses' => 'OrderController@showSignPage'
        ))->where('id', '[0-9]+');
    });

    Route::get('getAppVersion', 'UserController@apiGetAppVersionAndInsert');
});

