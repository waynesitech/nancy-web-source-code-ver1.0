<?php 

require('rotation.php');

class PDFWatermark extends PDF_Rotate {

    protected $_outerText1;// dynamic text
    protected $_outerText2;

    function setWaterText($txt1="", $txt2=""){
        $this->_outerText1 = $txt1;
        $this->_outerText2 = $txt2;
    }
    
    function Header() {
        //global $fullPathToFile;
        
        //Put the watermark
        $this->Image(public_path().'/assets/images/watermark.png', 30, 100, 150, 0, 'PNG');
//        $this->SetFont('Arial', 'B', 50);
//        $this->SetTextColor(255, 192, 203);
//        $this->RotatedText(35,190, $this->_outerText1, 45);
//        $this->RotatedText(75,190, $this->_outerText2, 45);
    }

    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

}


?>
