<style>
    .td-restrict-100{max-width: 100px;overflow: hidden;text-overflow: ellipsis;}
    .td-restrict-150{max-width: 150px;overflow: hidden;text-overflow: ellipsis;}
</style>
<div class="page-heading clearfix">
    <h1 class="heading-title pull-left">E-Stamping
    </h1>
    <div class="pull-right" style="text-align:right;margin-bottom:10px;">
        <form class="form-inline" style="margin-right:0;margin-bottom:10px;">
            <div class="form-group">
                <div class="input-group">
                    {{ Form::select('status', ['' => 'All Status', 'Received' => 'Received', 'Processing' => 'Processing', 'Ready' => 'Ready', 'Rejected' => 'Rejected'], empty(Request::get('status'))? '' : Request::get('status'), array('id' => 'status','class' => 'btn btn-default', 'style' => 'margin-top:0;')) }} 
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        {{ Form::text('daterange', $daterange, array('class' => 'form-control', 'id' => 'daterange', 'style' => 'width: 200px')) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">     
                    {{ Form::text('keyword', $keyword, array('class' => 'form-control')) }}
                    <span class="input-group-btn">
                    {{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-default')) }}
                    </span>
                </div>
            </div>
        </form>
        {{ Form::open(array('id' => 'sendcsv', 'class' => 'form-inline','method' => 'get', 'style' => 'margin-right:0;' , 'enctype' => 'multipart/form-data', 'route' => 'estamping.csv')) }}
            <input type='hidden' name='skeyword' id='skeyword' value=''>
            <input type='hidden' name='sdate' id='sdate' value=''>
            <input type='hidden' name='sstatus' id='sstatus' value=''>
            <div class="form-group">
                <button id="export_csv" class="btn btn-primary pull-right" type="button">Export CSV</button>
            </div>
        {{Form::close()}}
            
    </div>
</div>
@if(Session::has('alert.success'))
<div class="alert alert-dismissable alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('alert.success') }}
</div>
@endif         
@if(Session::has('alert.danger'))
<div class="alert alert-dismissable alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('alert.danger') }}
</div>
@endif
<div class="page-body">
    <table class="table table-hover table-sort">
        <thead>
            <tr>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'order_num', 'keyword' => Input::get('keyword'))) }}">Order</a>
                    @if($col == 'order_num')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'tenant_name', 'keyword' => Input::get('keyword'))) }}">Tenant</a>
                    @if($col == 'tenant_name')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'land_lord_name', 'keyword' => Input::get('keyword'))) }}">Land Lord</a>
                    @if($col == 'land_lord_name')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'user_name', 'keyword' => Input::get('keyword'))) }}">Created By</a>
                    @if($col == 'user_name')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'agency', 'keyword' => Input::get('keyword'))) }}">Agency</a>
                    @if($col == 'agency')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'created_on', 'keyword' => Input::get('keyword'))) }}">Created On</a>
                    @if($col == 'created_on')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'estamping_duplicate', 'keyword' => Input::get('keyword'))) }}">Duplicate</a>
                    @if($col == 'estamping_duplicate')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'sign_status', 'keyword' => Input::get('keyword'))) }}">Digital Sign</a>
                    @if($col == 'sign_status')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
                <th class="sorting">
                    <a href="{{ route('estamping.list', array('daterange' => Input::get('daterange'), 'sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'status', 'keyword' => Input::get('keyword'))) }}">Status</a>
                    @if($col == 'status')
                        @if($sort == 'asc')
                        <i class="fa fa-sort-up"></i>
                        @else
                        <i class="fa fa-sort-down"></i>
                        @endif
                        @else
                        <i class="fa fa-sort"></i>
                    @endif
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td><a href='{{ route('estamping.edit',['id' => $order->id]) }}'>{{ isset($order->order_num) && $order->order_num !='' ? $order->order_num : '-' }}</a></td>
                <td class="td-restrict-150">{{ $order->tenant_name }}</td>
                <td class="td-restrict-150">{{ $order->land_lord_name }}</td>
                <td>{{ isset($order->user_name) ? $order->user_name : '-' }}</td>
                <td>{{ $order->agency }}</td>
                <td>{{ date('d/m/Y H:i:s', strtotime($order->created_on)) }}</td>
                <td><?php echo isset($order->estamping_duplicate) ? (($order->estamping_duplicate == 0) ? 'No' : 'Yes') : ''?></td>
                <td style="color:<?php echo isset($order->sign_status) ? ($order->sign_status == 'ALL_SIGNED' ? '#5cb85c' : '#feb836' ) : '#feb836' ?>;border-radius:10px;font-weight:600;"><?php echo isset($order->sign_status) ? ($order->sign_status == 'ALL_SIGNED' ? 'Completed' : 'Pending') : 'Pending' ?></td>
                <td style="color:<?php echo isset($order->estamping_status) ? ($order->estamping_status == 'Processing' ? '#5c76b8' : ($order->estamping_status == 'Ready' ? '#5cb85c' : ($order->estamping_status == 'Rejected' ? '#d9534f' : '#feb836'))) : '#feb836'; ?>;border-radius:10px;font-weight:600;">{{ $order->estamping_status }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <nav class="pull-right">
        <?php echo $orders->appends(array('daterange' => $daterange, 'sort' => $sort, 'col' => $col, 'keyword' => $keyword))->links(); ?>
    </nav>
</div>
{{ HTML::script('/assets/js/moment.js') }}
{{ HTML::script('/assets/js/bootstrap-daterangepicker/daterangepicker.js') }}
{{ HTML::style('/assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css') }}

<script>
    $('#daterange').daterangepicker({
        format: 'DD/MM/YYYY',
        opens: 'left'
    }, function(start, end, label) {

    });
    
    $('#skeyword').val('<?php echo $keyword; ?>');
    $('#sdate').val($('#daterange').val());
    $('#sstatus').val($('#status').val());
    
    $('button#export_csv').on('click', function(){
            $('#sendcsv').submit();
    });
</script>