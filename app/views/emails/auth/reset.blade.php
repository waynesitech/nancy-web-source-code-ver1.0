<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>

		<div>
            Your new password is <strong>{{ $password }}</strong>
		</div>
	</body>
</html>
