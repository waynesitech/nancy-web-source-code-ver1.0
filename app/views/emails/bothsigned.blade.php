<p>Dear {{ $name }},</p>
<br>

<p>Your Tenancy Agreement {{$order_num}} has been signed successfully by both parties.</p>
<p>Please refer to the email attachment. Looking forward to serving you again.</p>
<br>

<p>Cheers from Lesys Tenancy!</p>