<p>Dear {{ $name }},</p>
<br>
<p>Your Stamping File is ready. Please refer to the attachment.</p>
<p>Looking forward to serving you again.</p>
<br>
<p>Cheers from Lesys Tenancy !</p>