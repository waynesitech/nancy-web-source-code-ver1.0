<p>Dear {{ $name }},</p>
<br>
<p>Your E-Stamping is processing.</p> 
<p>You will receive a sms notification or e-mail when the tenancy is ready.</p>
<br>
<p>Cheers from Lesys Tenancy !</p>