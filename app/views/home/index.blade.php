<style>
    .send {
        background-color: #eb0440;
        border: none;
        border-radius: 10px;
        color: white;
        padding: 4px 20px;
        text-align: center;
        text-decoration: none;
        position: relative;
        margin-right: 10px;
        cursor: pointer;
    }
</style>
<!--header section-->
<header>
    <div class="jumbotron jumbotron-fluid" id="banner">       
        <div class="parallax">
            <div class="container top_banner">
                <div class="parallax-pattern-overlay row">
                    <div class="col-sm-8">
                        <div class="banner_header row">
                            <div class="col-sm-3 mycontent-left">
                                <img src="/assets/images/nancylogowhite.png" class="img-fluid">
                            </div>
                            <div class="col-sm-9 mycontent-right">
                                <h1>NANCY</h1>
                                <p>YOUR TENANCY ROBOT</p>
                            </div>
                                
                                
                            <div class="container login_form col-sm-10">
                                {{ Form::open(array('method' => 'post', 'route' => array('user.login'), 'enctype' => 'multipart/form-data')) }}
                                <h4>Sign In Here</h4><br>
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1" aria-hidden="true">
                                        <i class="fa fa-envelope-o fa-2x" aria-hidden="true">
                                        </i>
                                    </label>
                                    <div class="col-sm-10">
                                        {{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder'=>'Email', 'required' => '') ) }}
                                            
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label" for="exampleInputEmail1" aria-hidden="true">
                                        <i class="fa fa-unlock-alt fa-2x" aria-hidden="true"></i>
                                    </label>
                                    <div class="col-sm-10">
                                        {{ Form::password('password', array('class' => 'form-control', 'placeholder'=>'Password', 'required' => '') ) }}
                                    </div>
                                </div>
                                <div>
                                    
                                </div>
                                <div class="col-sm-12 col-sm-offset-6" style="text-align: center;">
                                    {{ Form::button('Sign In', array( 'type' => 'submit', 'class' => 'form_button btn btn-danger')) }}
                                </div>                                    
                                {{ Form::close() }}
                                    
                                <div class="col-sm-12 col-sm-offset-6" style="text-align: center;">
                                    <span>
                                        Doesnt have an account?
                                        <a href="{{ URL::route('user.register') }}" ><strong>Sign Up now</strong></a>
                                    </span><br>
                                    <span>
                                        <a href="{{ URL::route('user.forget') }}"><strong>Forgot Password?</strong></a>
                                    </span>
                                </div>
                                    
                            </div>
                                
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <img src="/assets/images/nancyphone.png"  class="img-fluid" style="min-width: 250px" alt="nancyphone.png">
                    </div>
                        
                </div>
            </div>
                
        </div>
    </div>
</header>
    
<!--about us section-->
<section class="about" id="about">
    <div class="container">
        <div class="row">
            <h1 class="maintitle">ABOUT NANCY</h1>
        </div>
        <div class="row">
            <div class="col-sm-6" style="margin-top: 40px;">
                <img src="/assets/images/nancypic1.png" class="img-fluid">
            </div>
            <div class="col-sm-6">
                <p>
                    Introducing NanCy, your last tenancy assistant that you will ever need! Powered by Artificial Intelligence, she is able to speak and understand multiple languages including English, Chinese, and … Not only that, she is filled with tenancy related legal knowledge and she is capable of translating legal gibberish into natural and simple English sentences.
                </p>
                <p>
                    Whenever you need a personalized experience while customizing your own tenancy agreement, NanCy is the most patient tenancy assistant you can ever find in this planet. NanCy will be the best candidate whenever you are lazy to meet up with your tenant to sign the tenancy agreement. With the e-signing feature, who will need to meet up with the grumpy landlord for agreement signing? Let Nancy take care of him…
                </p>
                <p>
                    Besides e-signing, NanCy will take care of the whole tedious stamping process. After the tenancy agreement has been stamped, do not worry about missing files! Let NanCy, your all rounded virtual assistant to store your files and manage it for you! You can always order NanCy to remind you of expiration of any tenancy agreements, always plan ahead!
                </p>
            </div>
                
        </div>
    </div>
</section>
    
<!--features section-->
<section class="features" id="features">
    <div class="container">
        <div class="row">
            <h1 class="maintitle">FEATURES</h1>
        </div>
            
        <div class="row margin_top">
            <div class="col-sm-2">
                <img src="/assets/images/general_icn.png" class="img-fluid">
                <div class="container icn_textbox">
                    <h5>Generate Tenancy Agreement</h5>
                    <span class="feature_txt">
                        Answer a few questions and create your own tenancy agreement using our artificial intelligence robot
                    </span>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="/assets/images/explaination_icn.png" class="img-fluid">
                <div class="container icn_textbox">
                    <h5>Explaination of Clauses</h5>
                    <span class="feature_txt">
                        Listen to NanCy’s explaination clauses to make your understand the Tenancy Agreement better
                    </span>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="/assets/images/esign_icn.png" class="img-fluid">
                <div class="container icn_textbox">
                    <h5>E-Sign</h5>
                    <span class="feature_txt">
                        E-sign your tenancy agreement anytime, anywhere without having to meet the landlord/tenant physically.
                    </span>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="/assets/images/estamp_icn.png" class="img-fluid">
                <div class="container icn_textbox">
                    <h5>E-Stamp</h5>
                    <span class="feature_txt">
                        E-Stamping for your convenience.
                    </span>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="/assets/images/list_icn.png" class="img-fluid">
                <div class="container icn_textbox">
                    <h5>E-Storage</h5>
                    <span class="feature_txt">
                        Store your tenancy agreement safely with us.
                    </span>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="/assets/images/clock_icn.png" class="img-fluid">
                <div class="container icn_textbox">
                    <h5>Auto Reminder</h5>
                    <span class="feature_txt">
                        Auto-reminder prevents you for missing important dates.
                    </span>
                </div>
            </div>
                
        </div>
    </div> <!-- container -->
</section>
    
<!--features section-->
<section class="download " id="download">
    <div class="container">
        <div class="row ">
            <div class="col-sm-7" style="margin-top: 100px;">
                <h3>Start using your personal Tenancy Robot today!</h3>
                <br/>
                <div class="row" >
                    <div class="col-sm-4">
                        <a href="https://play.google.com/store" target="_blank"><img src="/assets/images/playstore.png" class="img-fluid"></a>
                    </div>
                    <div class="col-sm-4">
                        <a href="https://www.apple.com/mac/" target="_blank"><img src="/assets/images/appstore.png" class="img-fluid"></a>
                    </div>
                    <div class=" col-sm-4 ">
                        <a class="page-scroll" href="#page-top"><img src="/assets/images/Group.png" class="img-fluid"></a>
                    </div>
                        
                </div>
            </div>
            <div class="col-sm-5">
                <img src="/assets/images/nancypic2.png" class="img-fluid" >
            </div>
                
        </div>
    </div>
</section>
    
<!--contact us section-->
<section class="contact" id="contact">
    <div class="container">
        <div class="row">
            <h1 class="maintitlewhite">CONTACT</h1>
        </div>
            
        <div class="row">
            <div class="col-sm-6">
                <div class="container col-sm-6 col-sm-offset-12">
                    <img src="/assets/images/profile.png">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-1">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="col-sm-11">
                            F-5-17, IOI Boulevard, Jalan Kenari 5, Bandar Puchong Jaya, <br/> 47170 Puchong, Selangor Darul Ehsan, Malaysia.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1">
                            <i class="fa fa-envelope-o " aria-hidden="true"></i>
                        </div>
                        <div class="col-sm-11">
                            info@10nancy.com							
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="container contact_form">
                    {{ Form::open(array('route' => array('contact-us'), 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                    <div class="form-group row">
                        <label class="col-sm-1 col-form-label" for="exampleInputEmail1" aria-hidden="true">
                        </label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-1 col-form-label" for="exampleInputEmail1" aria-hidden="true">
                        </label>
                        <div class="col-sm-12">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-1 col-form-label" for="exampleInputEmail1" aria-hidden="true">
                        </label>
                        <div class="col-sm-12">
                            <textarea class="form-control" id="message" name="message" rows="6"  placeholder="Your Message"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-3 send form-group">
                        <input type="submit" value="SEND" class="send" />
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
            
    </div>
</div>
</section>
    
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Register</h4>
            </div>
            {{ Form::open(array('method' => 'post', 'class' => 'form-horizontal','route' => array('user.register'), 'enctype' => 'multipart/form-data')) }}
            <div class="modal-body">
                <div style='border-bottom:0;'>
                    <div class="col-sm-12">
                        {{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder' => 'Name')) }}
                    </div>
                </div>	
                <div style='border-bottom:0;'>
                    <div class="col-sm-12">
                        {{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
                    </div>
                </div>
                <div style='border-bottom:0;'>
                    <div class="col-sm-12">
                        {{ Form::text('mobile', Input::old('mobile'), array('class' => 'form-control', 'placeholder' => 'Mobile Number')) }}
                    </div>
                </div>
                <div>
                    <div class="col-sm-12">
                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                    </div>
                </div>
                <div>
                    <div class="col-sm-12">
                        {{ Form::password('confirm_password', array('class' => 'form-control', 'placeholder' => 'Re-enter Password')) }}
                    </div>
                </div>
                <div>
                    <div class="col-sm-12">
                        {{ Form::checkbox('agree', 'Agree', false,array('id'=>'agree')) }} I have read, understand and agree to the NanCy's <a href="" style='color: #0275d8;'>Terms and Service</a>.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Register</button>
            </div>
            {{ Form::close() }}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>