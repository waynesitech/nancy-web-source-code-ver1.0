<div class="page-heading clearfix">
	<h1 class="heading-title">Broadcast
		<a class="btn btn-primary pull-right" href="{{ route('broadcast.add') }}">Add New</a>
        <form class="form-inline pull-right" role="form">
		    <div class="form-group">
		      <div class="input-group">
		        {{ Form::text('keyword', $keyword, array('class' => 'form-control')) }}
		        <span class="input-group-btn">
		          {{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-default')) }}
		        </span>      
		      </div>
		    </div>
	  	</form>
	</h1>
</div>

@if(Session::has('alert.success'))
	<div class="alert alert-dismissable alert-success">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {{ Session::get('alert.success') }}
	</div>
@endif
@if(Session::has('alert.danger'))
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert" aaria-hidden="true">&times;</button>
	  {{ Session::get('alert.danger') }}
	</div>
@endif

<div class="page-body">
	<table class="table table-hover table-sort">
		<thead>
			<tr>
				<th class="sorting">
					<a href="{{ route('broadcast.list', array('sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'title')) }}">Title</a>
					@if($col == 'title')
						@if($sort == 'asc')
							<i class="fa fa-sort-up"></i>
						@else
							<i class="fa fa-sort-up"></i>
						@endif
					@else
						<i class="fa fa-sort"></i>
					@endif
				</th>
                <th class="sorting">
					<a href="{{ route('broadcast.list', array('sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'created_on', 'keyword' => Input::get('keyword'))) }}">Date Created</a>
					@if($col == 'created_on')
						@if($sort == 'asc')
							<i class="fa fa-sort-up"></i>
						@else
							<i class="fa fa-sort-up"></i>
						@endif
					@else
						<i class="fa fa-sort"></i>
					@endif
				</th>
                <th class="sorting">
					<a href="{{ route('broadcast.list', array('sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'last_sent_datetime', 'keyword' => Input::get('keyword'))) }}">Last Sent DateTime</a>
					@if($col == 'last_sent_datetime')
						@if($sort == 'asc')
							<i class="fa fa-sort-up"></i>
						@else
							<i class="fa fa-sort-up"></i>
						@endif
					@else
						<i class="fa fa-sort"></i>
					@endif
				</th>
                <th class="sorting">
					<a href="{{ route('broadcast.list', array('sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'pageView_count', 'keyword' => Input::get('keyword'))) }}">View Count</a>
					@if($col == 'pageView_count')
						@if($sort == 'asc')
							<i class="fa fa-sort-up"></i>
						@else
							<i class="fa fa-sort-up"></i>
						@endif
					@else
						<i class="fa fa-sort"></i>
					@endif
				</th>
                <th>
                	Action
                </th>
			</tr>
		</thead>
		<tbody>
			@foreach($broadcasts as $broadcast)
			<tr>
				<td><a href="{{ route('broadcast.edit', array('id' => $broadcast->id)) }}">{{ $broadcast->title }}</a></td>
				<td>{{ date('d/m/Y - H:i:s', strtotime($broadcast->created_on)) }}</td>
                <td>{{ ($broadcast->last_sent_datetime <> null) ? date('d/m/Y - H:i:s', strtotime($broadcast->last_sent_datetime)) : '-' }}</td>
                <td>{{ $broadcast->pageView_count }}</td>
                <td>
                	<a class="btn btn-primary" href="{{ route('broadcast.view', array('id' => $broadcast->id)) }}">View</a>
                	<a class="btn btn-primary" href="{{ route('broadcast.post', array('id' => $broadcast->id)) }}">Send</a>
                    <a class="btn btn-primary" href="{{ route('broadcast.delete', array('id' => $broadcast->id)) }}">Delete</a>
                </td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>
