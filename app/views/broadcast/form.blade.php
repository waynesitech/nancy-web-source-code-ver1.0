<div class="page-heading clearfix">
	<h1 class="heading-title">Broadcast</h1>
</div>

<div class="row">
	<div class="col-sm-8 col-sm-offset-1">
		@if(Session::has('alert.success'))
		<div class="alert alert-dismissable alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('alert.success') }}
		</div>
		@endif          
		@if(Session::has('alert.danger'))
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('alert.danger') }}
		</div>
		@endif          
		@if($errors->count() > 0)
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<ul>
				@foreach($errors->getMessages() as $msg)
				<li>{{ $msg[0] }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>


{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}

<div class="form-group">
	{{ HTML::decode(Form::label('title', 'Title <em>*</em>', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
		{{ Form::text('title', isset($broadcast->title) ? $broadcast->title : '', array('class' => 'form-control')) }}
	</div>
</div>

<div class="form-group">
    {{ HTML::decode(Form::label('content', 'Content <em>*</em>', array('class' => 'col-sm-3 control-label'))) }}
    <div class="col-sm-9">
		{{ Form::textarea('content', isset($broadcast->content) ? $broadcast->content : '', array('class' => 'form-control')) }}
	</div>
</div>

<hr>

<div class="form-group">
	<div class="col-sm-offset-3">
		<button class="btn btn-primary" type="submit">Save</button>
	</div>
</div>

{{ Form::close() }}
</div>

