@section('title', 'Tenancy List')
<div class="col-md-10">
    <div class="row">
        <div class="col-md-11">
            <div class="content-box-large">
                <div class="panel-heading">
                    <h2>Tenancy List</h2>
                </div>
                <div class="panel-body">

                    <table class="responstable">

                        <tr>
                            <th>NO.</th>
                            <th class="tablepic"></th>
                            <th class="tableref">REF. NO.</th>
                            <th class="tableproject">PROJECT</th>
                            <th class="tableadd">TENANCY<br/> AGREEMENT</th>
                            <th>ESIGN</th>
                            <th>ESTAMP</th>
                            <th>DETAIL</th>
                            <th>RENEW</th>
                        </tr>


                        @foreach($orders as $key=>$order)
                        <tr>
                            <td>{{sprintf("%02d", $key + 1)}}</td>
                            <td>
                                 @if(isset($_GET['type']))
                                 <a href="{{ route('order.detail', array( 'id' => $order->id, 'type' => $_GET['type'] )) }}">{{ HTML::image(URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/' . $order->propertyPicture), null, array('style'=>'max-width:100px')) }}</a>
                                 @else
                                 <a href="{{ route('order.detail', array( 'id' => $order->id )) }}">{{ HTML::image(URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/' . $order->propertyPicture), null, array('style'=>'max-width:100px')) }}</a>
                                 @endif
                            </td>
                            <td>
                                @if(isset($_GET['type']))
                                <a href="{{ route('order.detail', array( 'id' => $order->id, 'type' => $_GET['type'])) }}">{{$order->order_num}}</a>
                                @else
                                <a href="{{ route('order.detail', array( 'id' => $order->id)) }}">{{$order->order_num}}</a>
                                @endif
                            </td>
                            <td>
                                {{$order->propertyStreet}}<br/>
                                {{$order->propertyTown}}<br/>
                                {{$order->propertyPostcode}}<br/>
                                {{$order->propertyState}}
                            </td>
                            <td>
                                @if ($order->tenancyAgreementStatus == 'Received')
                                <span style="background:#ffef63;border-radius: 20px;padding: 10px;">{{$order->tenancyAgreementStatus}}</span>
                                @elseif ($order->tenancyAgreementStatus == 'Ready' || $order->tenancyAgreementStatus == 'Done')
                                <span style="background:#3c763d;border-radius: 20px;padding: 10px;color:#fff">{{$order->tenancyAgreementStatus}}</span>
                                @elseif ($order->tenancyAgreementStatus == 'Expired' || $order->tenancyAgreementStatus == 'Rejected')
                                <span style="background:#ff0000;border-radius: 20px;padding: 10px;">{{$order->tenancyAgreementStatus}}</span>
                                @endif
                            </td>
                            <td>{{($order->eSignStatus == 'PARTIAL_SIGNED'? 'Processing': ($order->eSignStatus == '-'? '-': 'Signed'))}}</td>
                            <td>{{$order->eStampStatus}}</td>
                            <td>
                                @if($order->tenancyAgreementStatus == 'Done' || $order->tenancyAgreementStatus == 'Expired')
                                <a href="{{URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/pdf/' . $order->actualDocument)}}" download><i class="glyphicon glyphicon-download classbutton" ></i></a>
                                @endif
                            </td>
                            <td>
                                @if($order->tenancyAgreementStatus == 'Done' || $order->tenancyAgreementStatus == 'Expired')
                                <a href="{{ route('order.renew', array( 'id' => $order->id )) }}"><i class="glyphicon glyphicon-refresh classbutton" ></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>



<script>
    function resend(id) {
        $('#btnResend').css('display', 'none');
        $.ajax({
            type: "POST",
            url: "{{ route('order.resend') }}",
            data: {orderID: id},
            dataType: 'json',
            success: function (data) {
                if (data.meta.stat == 200) {
                    $('#modalMsg').text(data.meta.msg.body);
                    $('#Resend').modal('show');
                } else {
                    alert('Error. Please Try again.');
                }
            },
            error: function (data) {
                alert(data.responseJSON.meta.msg.body);
            }
        })
                .done(function () {
                    $('#btnResend').css('display', 'block');
                })
                ;
    }
</script>