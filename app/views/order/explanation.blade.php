@section('title', 'Explanation List')
<style>
    #player {
        position:relative;
        top:10px;
        width:200px;
        overflow: hidden;
        direction: ltl;
        border-top-right-radius: 0.5em 0.5em;
        border-bottom-right-radius: 1em 0.7em;
    }
</style>
<div class="col-md-10">
    <div class="row">
        <div class="col-md-11">
            <div class="content-box-large">
                <div class="panel-heading">
                    <h2>Explanation List</h2>
                </div>
                <div class="panel-body">

                    <table class="responstable">

                        <tr>
                            <th>TITLE</th>
                            <th>CONTENT</th>
                            <th></th>
                       </tr>


                        @foreach($audios as $audio)
                        <tr>
                            <td style="width:50px">{{$audio->explanationNumber}}</td>
                            <td>{{$audio->explanationText}}</td>
                            <td style="width:250px">
                                <div id="player">
                                    <audio src="{{URL::asset(EXPLANATION_AUDIO_URL . DIRECTORY_SEPARATOR . $lang . DIRECTORY_SEPARATOR . $audio->explanationURL)}}" controls="controls" controlsList="nodownload">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                                </td>
                        </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
</script>