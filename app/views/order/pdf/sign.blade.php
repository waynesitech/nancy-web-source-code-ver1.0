<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>
    <style>
        hr {
            border:none;
            border-top:1px dotted #000;
            color:#fff;
            background-color:#fff;
            height:1px;
            width:100%;
            margin-left: 20px;
        }

        .page-break {
            page-break-after: always;
        }

        .footer {
            width: 100%;
            text-align: center;
            position: fixed;
        }

        .footer {
            bottom: 0px;
        }

    </style>
    <body style="font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-weight: 300;">
        <div style="width: 660px; margin: auto;">


            <div style="margin-top: 30px;">
                <b>IN WITHNESS WHEREOF</b> the parties hereto have hereunto set their hand and/or common seal the day and year first above written.
            </div>
            <div style="margin-top: 40px;">
                <u><b>LANDLORD</b></u>
            </div>

            <table width="100%"  cellspacing="0" style="margin-top: 20px;">
                @foreach($data['orderSignature_landlords'] as $orderSignature_landlord)
                <tr>
                    <td> SIGNED by</td>
                    <td>  )  </td>
                    <td rowspan="6"><center><img src="<?php echo (!empty($orderSignature_landlord['document_url'])) ? ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $data['order']['id'] . DIRECTORY_SEPARATOR . 'sign' . DIRECTORY_SEPARATOR . $orderSignature_landlord['document_url'] : '' ?>" style="width:150px"/></center></td>
                </tr>
                <tr>
                    <td>the abovenamed Landlord/</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>as authorized person</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>for and on behalf of the</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>abovenamed Landlord</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>as authorized person</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>trading as</td>
                    <td>  )  </td>
                </tr>  
                <tr>
                    <td>in the presence of: -</td>
                    <td>  )  </td>
                </tr>
                <tr style="margin-bottom:20px">
                    <td></td>
                    <td></td>
                    <td>  <hr/> 
                        <table style="margin-left: 20px" width="100%">
                            <tr><td>Signature of authorized person</td></tr>
                            <tr><td>Name : </td><td>{{$orderSignature_landlord['name']}}</td></tr>
                            <tr><td>NRIC No. : </td><td>{{$orderSignature_landlord['icNum']}}</td></tr>
                        </table>
                    </td>
                </tr>
                @endforeach
            </table>

            <!--TENANT-->
            <div style="margin-top: 40px;">
                <u><b>TENANT</b></u>
            </div>

            <table width="100%"  cellspacing="0" style="margin-top: 20px;margin-bottom: 40px;">
                @foreach($data['orderSignature_tenures'] as $orderSignature_tenure)


                <tr>
                    <td> SIGNED by</td>
                    <td>  )  </td>
                    <td rowspan="6"><center><img src="<?php echo (!empty($orderSignature_tenure['document_url'])) ? ORDER_IMAGE_URL . DIRECTORY_SEPARATOR . $data['order']['id'] . DIRECTORY_SEPARATOR . 'sign' . DIRECTORY_SEPARATOR . $orderSignature_tenure['document_url'] : '' ?>" style="width:150px"/></center></td>
                </tr>
                <tr>
                    <td>the abovenamed Tenant/</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>as authorized person</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>for and on behalf of the</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>abovenamed Tenant/</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>as authorized person</td>
                    <td>  )  </td>
                </tr>
                <tr>
                    <td>trading as</td>
                    <td>  )  </td>
                </tr>  
                <tr>
                    <td>in the presence of: -</td>
                    <td>  )  </td>
                </tr>
                <tr style="margin-bottom:20px">
                    <td></td>
                    <td></td>
                    <td>  <hr/> 
                        <table style="margin-left: 20px" width="100%">
                            <tr><td>Signature of authorized person</td></tr>
                            <tr><td>Name : </td><td>{{$orderSignature_tenure['name']}} </td></tr>
                            <tr><td>NRIC No. : </td><td>{{$orderSignature_tenure['icNum']}}</td></tr>
                        </table>
                    </td>
                </tr>
                @endforeach
            </table>

        </div>
    </body>
</html>
