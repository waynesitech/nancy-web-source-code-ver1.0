<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link media="all" type="text/css" rel="stylesheet" href="http://nancy.com.my/assets/css/font-awesome.css">
    </head>
    <style>
        
        td {
          padding-top: 2px;
          padding-bottom: 2px;
        }
        .l-box{
            width:250px; 
            height:25px; 
            border:1px solid;
            border-radius: 5px;
            float:right;
            padding:5px;
            margin-left: 20px
        }
        .s-box{
            width:40px; 
            height:27px; 
            border:1px solid;
            border-radius: 5px;
            float:right;
            padding: 0 5px 5px;
            text-align: center;
            margin-left: 230px
        }
        .s-box-black{
            background-color: #000
        }

    </style>
    <body style="font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-weight: 300;">
        <div style="width: 650px; margin: auto;">


            <div style="margin-top: 30px;text-align: center;">
                <p><b>SCHEDULE B</b></p>
                <p><b>ADDITIONAL CONDITIONS (only if applicable)</b></p>
            </div>
            
            <table width="100%"  cellspacing="0" cellpadding="2" >
                <tr>
                    <td style="width:50px"><b>A. </b></td>
                    <td colspan="3"><b><u>PAYMENT</u></b> (Please (<img src="http://nancy.com.my/assets/images/tick.png" width="20" />) the appropriate)</td>
                </tr>
                <tr>
                    <td style="width:50px">1.</td>
                    <td colspan="3">Direct bank in</td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:50px">(i)</td>
                    <td style="width:250px">Name of account holder:</td>
                    <td>
                        <div class="l-box {{($data['paymentAccountName'] == '')? 's-box-black' : ''}}">
                            {{$data['paymentAccountName']}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:50px">(ii)</td>
                    <td style="width:250px">Name of bank:</td>
                    <td>
                        <div class="l-box {{($data['bank'] == '')? 's-box-black' : ''}}">
                            {{$data['bank']}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:50px">(iii)</td>
                    <td style="width:250px">Bank account number:</td>
                    <td>
                        <div class="l-box {{($data['bankAccountNum'] == '')? 's-box-black' : ''}}">
                            {{$data['bankAccountNum']}}
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:50px">(iv)</td>
                    <td colspan="2">Method of notification upon payment made:</td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:270px" colspan="2">Email</td>
                    <td>
                        <div class="s-box {{($data['notificationMethod'] == 'isEmail')? '' : 's-box-black'}}">
                            @if($data['notificationMethod'] == 'isEmail')
                            <img src="http://nancy.com.my/assets/images/tick.png" width="35" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:270px" colspan="2">Text Message</td>
                    <td>
                        <div class="s-box {{($data['notificationMethod'] == 'isSMS')? '' : 's-box-black'}}">
                            @if($data['notificationMethod'] == 'isSMS')
                            <img src="http://nancy.com.my/assets/images/tick.png" width="35" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:270px" colspan="2">Whatsapp Text</td>
                    <td>
                        <div class="s-box {{($data['notificationMethod'] == 'isWhatsapp')? '' : 's-box-black'}}">
                            @if($data['notificationMethod'] == 'isWhatsapp')
                            <img src="http://nancy.com.my/assets/images/tick.png" width="35" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px">2.</td>
                    <td colspan="3">Post-dated cheque</td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:50px">(i)</td>
                    <td style="width:250px">6 months</td>
                    <td>
                        <div class="s-box {{($data['postDatedMethod'] == 'is6Months')? '' : 's-box-black'}}">
                            @if($data['postDatedMethod'] == 'is6Months')
                            <img src="http://nancy.com.my/assets/images/tick.png" width="35" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:50px">(ii)</td>
                    <td style="width:250px">12 months</td>
                    <td>
                        <div class="s-box {{($data['postDatedMethod'] == 'is12Months')? '' : 's-box-black'}}">
                            @if($data['postDatedMethod'] == 'is12Months')
                            <img src="http://nancy.com.my/assets/images/tick.png" width="35" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:50px">(iii)</td>
                    <td style="width:250px">24 months</td>
                    <td>
                        <div class="s-box {{($data['postDatedMethod'] == 'is24Months')? '' : 's-box-black'}}">
                            @if($data['postDatedMethod'] == 'is24Months')
                            <img src="http://nancy.com.my/assets/images/tick.png" width="35" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px">3.</td>
                    <td colspan="3">Collection by Landlord</td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td style="width:270px" colspan="2"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Yes</td>
                    <td>
                        <div class="s-box {{($data['paymentMethod'] == 'isCollectionByLandlord')? '' : 's-box-black'}}">
                            @if($data['paymentMethod'] == 'isCollectionByLandlord')
                            <img src="http://nancy.com.my/assets/images/tick.png" width="35" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width:50px"><b>B. </b></td>
                    <td colspan="3"><b><u>OTHER ADDITIONAL CONDITIONS</u></b></td>
                </tr>
                <tr>
                    <td style="width:50px"></td>
                    <td colspan="3">
                        <div style="height:200px; width:600px; border:1px solid;border-radius: 15px; padding: 10px">
                            @if(isset($data['specialRequest']))
                            <?php $arr = json_decode($data['specialRequest'], true); ?>
                            @if(is_array($arr))
                            <?php $i = 1; ?>
                                @foreach($arr as $inner_arr)
                                    @if(is_array($inner_arr))   
                                        @if ($inner_arr['edited'] != "")
                                            {{$i.'.'}} {{ $inner_arr['edited'] }}<br/>
                                        @else
                                            {{$i.'.'}} {{ $inner_arr['original'] }}<br/>
                                        @endif
                                          
                                        <?php $i++; ?>
                                    @else
                                        {{ $inner_arr }}
                                    @endif  
                                @endforeach
                            @endif 
                            @endif 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <p style="font-size: 10px;margin-top: 20px;">
                            * in the event of any conflict or inconsistency between the clauses in this Agreement and the terms in the Schedules hereof, the terms in the Schedules will prevail, that is the terms and conditions contained in the Schedule will take precedence and supersede the clauses in this Agreement.
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
