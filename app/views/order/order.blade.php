@section('title', 'Tenancy Agreement')

<div class="container">
    {{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'orderForm')) }}
    <input type="hidden" id="hidRoute" value="{{Route::currentRouteName()}}" name="hidRoute">
    <div class="col-md-10">
        <input type="hidden" id="hidtype" value="{{ isset($_GET['type']) ? $_GET['type'] : '' }}" name="hidtype">
        @if(isset($_GET['type']))
                @if($_GET['type'] == 'esign' || $_GET['type'] == 'estamp' || $_GET['type'] == 'autoreminder')
                <div class="col-md-13" >
                    <div class="content-box-header panel-heading">
                        <div class="panel-title "><h4>Tenancy Agreement</h4></div>
                    </div>
                    <div class="content-box-large box-with-header">
                        <div align="center">
                            {{ Form::file('taPDF', array('accept' => '.pdf')) }}
                        </div>

                    </div>
                </div>
                @endif
        @endif

        <div class="content-box-header panel-heading">
            <div class="panel-title "><h4>Property Details</h4></div>	
        </div>

        <div class="content-box-large box-with-header">
            <div>
                <div class="row">
                    <!-- upload photo -->
                    <div class="col-sm-12">
                        <div class="uploadphoto col-sm-3 col-xs3">
                            @if(isset($order->propertyPicture))
                            <img src="{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->propertyPicture }}" style="width: 100%;"/>
                            @endif
                            {{ Form::file('propertyPicture', array('accept' => 'image/*', 'id' => 'propertyPicture')) }}
                            <img id='img-upload' style="width:100%;margin-top: 10px;"/>
                        </div>
                        <!-- type of property -->
                        <div class="col-sm-3 col-xs-8">
                            <h5 class="question_title1">Type of Property**</h5>
                            <div class="radio">
                                <label>{{ Form::radio('isResidential', '1', isset($order->isResidential) ? $order->isResidential : 1, ['checked'] ) }} Residential</label>
                            </div>
                            <div class="radio">
                                <label>{{ Form::radio('isResidential', '0', isset($order->isResidential) ? !$order->isResidential : 0, ['disabled' => 'disabled'] ) }} Commercial </label>
                            </div>
                        </div>
                        <!-- address -->
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <h5>Address**</h5>  
                                <div class="inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                        {{ Form::text('propertyStreet', isset($order->propertyStreet) ? $order->propertyStreet : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Address')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                        {{ Form::text('propertyTown', isset($order->propertyTown) ? $order->propertyTown : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Town')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="selectContainer">
                                    <div class="input-group col-sm-12 col-xs-12">
                                        {{ Form::select('propertyState', $states, isset($order->propertyState) ? $order->propertyState : '', array('class' => 'form-control selectpicker', 'required' => '')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="codeContainer">
                                    <div class="col-xs-6" style="padding-left: 0px">
                                        {{ Form::text('propertyPostcode', isset($order->propertyPostcode) ? $order->propertyPostcode : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Zip Code')) }}
                                    </div>
                                    <div>
                                        <label style="padding-top: 6px">Malaysia</label>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>		
                </div>
            </div>
        </div>

        <!-- Property Details end -->

        <!-- I am a starts -->

        <div class="col-md-13" >
            <div class="content-box-header panel-heading">
                <div class="panel-title "><h4>I am a</h4></div>
            </div>
            <div class="content-box-large box-with-header">
                <div align="center">
                    <label class=" radio-inline landlorad-radio">
                        {{ Form::radio('isLandlord', '1', isset($order->isLandlord) ? $order->isLandlord : 1 ) }} Landlord 
                    </label>
                    <label class=" radio-inline tenant-radio">
                        {{ Form::radio('isLandlord', '0', isset($order->isLandlord) ? !$order->isLandlord : 0 ) }} Tenant 
                    </label>
                </div>
            </div>
        </div>

        <!-- I am a end -->

        <!-- Landlord Details start -->
        <div id="divLandlord">
            @if(isset($order->landlord))
            @if(count($order->landlord) > 0)
            @foreach($order->landlord as $key=>$value)
            <div class="col-md-13" >
                <div class="content-box-header panel-heading">
                    <div class="panel-title">
                        <h4>Landlord Details</h4>
                    </div>
                    <label class=" radio-inline ">
                        {{ Form::radio('isLCompany'.($key + 1), '0', isset($value->isCompany) ? !$value->isCompany : 0, ['onchange'=>'landlordCompany(this)', 'class'=>'landlordCompany'] ) }} Individual
                    </label>
                    <label class=" radio-inline ">
                        {{ Form::radio('isLCompany'.($key + 1), '1', isset($value->isCompany) ? $value->isCompany : 1, ['onchange'=>'landlordCompany(this)', 'class'=>'landlordCompany'] ) }} Company
                    </label>
                </div>
            </div>

            <div class="content-box-large box-with-header form-horizontal" >
                <fieldset>
                    <div id="divLCompany1">
                        <!-- Company Name -->							
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Name**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('L_companyName'.($key + 1), isset($value->companyName) ? $value->companyName : '', array('class' => 'form-control', 'placeholder' => 'Company Name')) }}
                                </div>
                            </div>
                        </div>
                        <!-- Company Reg Code -->	
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Reg. No.**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('L_companyRegNum'.($key + 1), isset($value->companyRegNum) ? $value->companyRegNum : '', array('class' => 'form-control', 'placeholder' => 'Company Reg. No.')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Signatpry Person -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">Full Name**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('L_name'.($key + 1), isset($value->name) ? $value->name : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Full Name as per I/C No. & Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- IC NO -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">I.C No./Passport No.**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('L_icNum'.($key + 1), isset($value->icNum) ? $value->icNum : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'I.C No./Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- address -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Correspondence Address**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                {{ Form::text('L_address'.($key + 1), isset($value->address) ? $value->address : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Correspondence Address')) }}
                            </div>
                        </div>
                    </div>	
                    <!-- Phone -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                {{ Form::text('L_contactNum'.($key + 1), isset($value->contactNum) ? $value->contactNum : '', array('class' => 'form-control', 'placeholder' => 'Phone Number')) }}
                            </div>
                        </div>
                    </div>							
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Email Address</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-envelope"></i></span>
                                {{ Form::text('L_email'.($key + 1), isset($value->email) ? $value->email : '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
                            </div>
                        </div>
                    </div>		

                    <div class="addbutton1" align="center" >
                        <a class="btn btn-primary addbutton" onclick="addLandlord()">Add Landlord</a>
                    </div>
                </fieldset>
            </div>
            @endforeach
            @endif
            
            @else
            <div class="col-md-13" >
                <div class="content-box-header panel-heading">
                    <div class="panel-title">
                        <h4>Landlord Details</h4>
                    </div>
                    <label class=" radio-inline ">
                        {{ Form::radio('isLCompany1', '0', 0, ['onchange'=>'landlordCompany(this)', 'class'=>'landlordCompany'] ) }} Individual
                    </label>
                    <label class=" radio-inline ">
                        {{ Form::radio('isLCompany1', '1', 1, ['onchange'=>'landlordCompany(this)', 'class'=>'landlordCompany'] ) }} Company
                    </label>
                </div>
            </div>

            <div class="content-box-large box-with-header form-horizontal" >
                <fieldset>
                    <div id="divLCompany1">
                        <!-- Company Name -->							
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Name**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('L_companyName1', '', array('class' => 'form-control', 'placeholder' => 'Company Name')) }}
                                </div>
                            </div>
                        </div>
                        <!-- Company Reg Code -->	
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Reg. No.**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('L_companyRegNum1',  '', array('class' => 'form-control', 'placeholder' => 'Company Reg. No.')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Signatpry Person -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">Full Name**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('L_name1', Auth::user()->name, array('class' => 'form-control', 'required' => '', 'placeholder' => 'Full Name as per I/C No. & Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- IC NO -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">I.C No./Passport No.**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('L_icNum1', Auth::user()->ic_number, array('class' => 'form-control', 'required' => '', 'placeholder' => 'I.C No./Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- address -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Correspondence Address**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                {{ Form::text('L_address1', '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Correspondence Address')) }}
                            </div>
                        </div>
                    </div>	
                    <!-- Phone -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                {{ Form::text('L_contactNum1', Auth::user()->mobile, array('class' => 'form-control', 'placeholder' => 'Phone Number')) }}
                            </div>
                        </div>
                    </div>							
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Email Address</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-envelope"></i></span>
                                {{ Form::text('L_email1',Auth::user()->email, array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
                            </div>
                        </div>
                    </div>		

                    <div class="addbutton1" align="center" >
                        <a class="btn btn-primary addbutton" onclick="addLandlord()">Add Landlord</a>
                    </div>
                </fieldset>
            </div>
            @endif
                
            <input type="hidden" id="hidLandlordCount" value="1" name="hidLandlordCount">
        </div>
        <!-- Landlord Details end -->

        <!-- Tenant Detail starts -->
        <div id="divTenant">   
            @if(isset($order->tenant))
            @if(count($order->tenant) > 0)
            @foreach($order->tenant as $key=>$value)
            <div class="col-md-13" >
                <div class="content-box-header panel-heading">
                    <div class="panel-title">
                        <h4>Tenant Details</h4>
                    </div>
                    <label class=" radio-inline ">
                        {{ Form::radio('isTCompany'.($key + 1), '0', isset($value->isCompany) ? !$value->isCompany : 0, ['onchange'=>'tenantCompany(this)', 'class'=>'tenantCompany'] ) }} Individual
                    </label>
                    <label class=" radio-inline ">
                        {{ Form::radio('isTCompany'.($key + 1), '1', isset($value->isCompany) ? $value->isCompany : 1, ['onchange'=>'tenantCompany(this)', 'class'=>'tenantCompany'] ) }} Company
                    </label>
                </div>
            </div>

            <div class="content-box-large box-with-header form-horizontal" >
                <fieldset>
                    <div id="divTCompany1">
                        <!-- Company Name -->							
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Name**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('T_companyName'.($key + 1), isset($value->companyName) ? $value->companyName : '', array('class' => 'form-control', 'placeholder' => 'Company Name')) }}
                                </div>
                            </div>
                        </div>
                        <!-- Company Reg Code -->	
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Reg. No.**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('T_companyRegNum'.($key + 1), isset($value->companyRegNum) ? $value->companyRegNum : '', array('class' => 'form-control', 'placeholder' => 'Company Reg. No.')) }}
                                </div>
                            </div>
                        </div>
                    </div
                    <!-- Signatpry Person -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">Full Name**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('T_name'.($key + 1), isset($value->name) ? $value->name : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Full Name as per I/C No. & Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- IC NO -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">I.C No./Passport No.**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('T_icNum'.($key + 1), isset($value->icNum) ? $value->icNum : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'I.C No./Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- address -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Correspondence Address**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                {{ Form::text('T_address'.($key + 1), isset($value->address) ? $value->address : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Correspondence Address')) }}
                            </div>
                        </div>
                    </div>	
                    <!-- Phone -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                {{ Form::text('T_contactNum'.($key + 1), isset($value->contactNum) ? $value->contactNum : '', array('class' => 'form-control', 'placeholder' => 'Phone Number')) }}
                            </div>
                        </div>
                    </div>							
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Email Address</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-envelope"></i></span>
                                {{ Form::text('T_email'.($key + 1), isset($value->email) ? $value->email : '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
                            </div>
                        </div>
                    </div>			

                    <div class="addbutton1" align="center" >
                        <a class="btn btn-primary addbutton" onclick="addTenant()">Add Tenant</a>
                    </div>
                </fieldset>
            </div>
            @endforeach
            @endif
            
            @else
            <div class="col-md-13" >
                <div class="content-box-header panel-heading">
                    <div class="panel-title">
                        <h4>Tenant Details</h4>
                    </div>
                    <label class=" radio-inline ">
                        {{ Form::radio('isTCompany1', '0', 0, ['onchange'=>'tenantCompany(this)', 'class'=>'tenantCompany'] ) }} Individual
                    </label>
                    <label class=" radio-inline ">
                        {{ Form::radio('isTCompany1', '1', 1, ['onchange'=>'tenantCompany(this)', 'class'=>'tenantCompany'] ) }} Company
                    </label>
                </div>
            </div>

            <div class="content-box-large box-with-header form-horizontal" >
                <fieldset>
                    <div id="divTCompany1">
                        <!-- Company Name -->							
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Name**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('T_companyName1', '', array('class' => 'form-control', 'placeholder' => 'Company Name')) }}
                                </div>
                            </div>
                        </div>
                        <!-- Company Reg Code -->	
                        <div class="form-group">
                            <label class="col-md-4 control-label">Company Reg. No.**</label>  
                            <div class="col-md-5 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    {{ Form::text('T_companyRegNum1',  '', array('class' => 'form-control', 'placeholder' => 'Company Reg. No.')) }}
                                </div>
                            </div>
                        </div>
                    </div
                    <!-- Signatpry Person -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">Full Name**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('T_name1', '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Full Name as per I/C No. & Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- IC NO -->	
                    <div class="form-group">
                        <label class="col-md-4 control-label">I.C No./Passport No.**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                {{ Form::text('T_icNum1', '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'I.C No./Passport No.')) }}
                            </div>
                        </div>
                    </div>
                    <!-- address -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Correspondence Address**</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                {{ Form::text('T_address1', '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Correspondence Address')) }}
                            </div>
                        </div>
                    </div>	
                    <!-- Phone -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                {{ Form::text('T_contactNum1', '', array('class' => 'form-control', 'placeholder' => 'Phone Number')) }}
                            </div>
                        </div>
                    </div>							
                    <!-- Email -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Email Address</label>  
                        <div class="col-md-5 inputGroupContainer">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-envelope"></i></span>
                                {{ Form::text('T_email1', '', array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
                            </div>
                        </div>
                    </div>			

                    <div class="addbutton1" align="center" >
                        <a class="btn btn-primary addbutton" onclick="addTenant()">Add Tenant</a>
                    </div>
                </fieldset>
            </div>
            @endif
            <input type="hidden" id="hidTenantCount" value="1" name="hidTenantCount">
        </div>    
        <!-- Tenant Detail end -->

        <!-- Rental Detail start -->

        <div class="col-md-13" >
            <div class="content-box-header panel-heading">
                <div class="panel-title "><h4>Rental Details</h4></div>
            </div>
        </div>

        <div class="content-box-large box-with-header form-horizontal" >
            <fieldset>
                <!-- Commencement Date -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Commencement Date**</label>  
                    <div class="col-md-5 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            {{ Form::text('commencementDate', isset($order->commencementDate) && $order->commencementDate!='' ? ($order->commencementDate == '0000-00-00' ? date('d/m/Y') : date('d/m/Y', strtotime($order->commencementDate))) : '', array('class' => 'form-control', 'required' => '', 'id' => 'commencementDate', 'placeholder' => 'DD/MM/YY')) }}
                        </div>
                    </div>
                </div>
                <!-- Term of Tenancy -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Term of Tenancy**</label>  
                    <div class="col-md-5 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            {{ Form::select('termsOfTenancyMonths', $month, isset($order->termsOfTenancyMonths) ? $order->termsOfTenancyMonths : '', array('class' => 'form-control', 'required' => '')) }} 

                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            {{ Form::text('termsOfTenancyYears', isset($order->termsOfTenancyYears) ? $order->termsOfTenancyYears : '', array('class' => 'form-control', 'required' => '', 'placeholder' => 'Year')) }}		
                        </div>
                    </div>
                </div>
                <!-- Option to renew -->

                <div class="form-group">
                    <label class="col-md-4 control-label">
                        {{ Form::checkbox('isOptionToRenew', 1, isset($order->isOptionToRenew) ? ($order->isOptionToRenew == 1 ? true : false) : false) }}
                        <label class="optiontorenew">Option to Renew</label>
                    </label>  
                    <div class="col-md-5 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            {{ Form::select('optionToRenewMonths', $month, isset($order->optionToRenewMonths) ? $order->optionToRenewMonths : '', array('class' => 'form-control')) }}

                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            {{ Form::text('optionToRenewYears', isset($order->optionToRenewYears) ? $order->optionToRenewYears : '', array('class' => 'form-control', 'placeholder' => 'Year')) }}		
                        </div>
                    </div>
                </div>

                <!-- Term of renewal -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Term of Renewal</label>  
                    <div class="col-md-5 inputGroupContainer termofrenewal">
                        {{ Form::select('optionToRenewCondition', 
                                array('' => 'Please select',
                        'isMarketPrevaillingRate' => 'market prevailing rate',
                        'isIncreaseOf10percent' => 'increase of 10% from the last rental rate',
                        'isIncreaseOf15percent' => 'increase of 15% from the last rental rate',
                        'isMutuallyAgreedRate' => 'at a mutually agreed rate'
                        )
                        , isset($order->optionToRenewCondition) ? $order->optionToRenewCondition : '', array('class' => 'form-control')) }}
                        <br>
                        {{ Form::text('otherTermsofRenewal', isset($order->otherTermsofRenewal) ? $order->otherTermsofRenewal : '', array('class' => 'form-control', 'placeholder' => 'Other')) }}   
                    </div>
                </div>	
                <!-- Rental -->
                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Rental (RM)**</label>
                    <div class="col-sm-5">
                        {{ Form::text('rental', isset($order->rental) ? $order->rental : '', array('class' => 'form-control', 'required' => '', 'placeholder'=>'Ringgit Malaysia')) }}
                    </div>
                </div>
                <!-- Advance Rental -->
                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Advance Rental (RM)</label>
                    <div class="col-sm-5">
                        {{ Form::text('advanceRental', isset($order->advanceRental) ? $order->advanceRental : '', array('class' => 'form-control', 'placeholder'=>'Ringgit Malaysia')) }}
                    </div>
                </div>
                <!-- Security Deposit -->
                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Security Deposit</label>
                </div>			

                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Rent (RM)</label>
                    <div class="col-sm-5">
                        {{ Form::text('securityDepositRent', isset($order->securityDepositRent) ? $order->securityDepositRent : '', array('class' => 'form-control', 'placeholder'=>'Ringgit Malaysia')) }}
                    </div>
                </div>	

                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Utilities (RM)</label>
                    <div class="col-sm-5">
                        {{ Form::text('securityDepositUtilities', isset($order->securityDepositUtilities) ? $order->securityDepositUtilities : '', array('class' => 'form-control', 'placeholder'=>'Ringgit Malaysia')) }}
                    </div>
                </div>		

                <!-- Rental Free Period -->
                <div class="form-group">
                    <label class="col-md-4 control-label">Rental Free Period</label>  
                    <div class="col-md-5 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon">Week</span>
                            {{ Form::number('rentalFreePeriodWeeks', isset($order->rentalFreePeriodWeeks) ? $order->rentalFreePeriodWeeks : '', array('class' => 'form-control', 'min'=> '0')) }}
                            <span class="input-group-addon">Month</span>
                            {{ Form::select('rentalFreePeriodMonths', $month, isset($order->rentalFreePeriodMonths) ? $order->rentalFreePeriodMonths : '', array('class' => 'form-control', 'style' => 'width:70px')) }}
                            <span class="input-group-addon">Year</span>
                            {{ Form::text('rentalFreePeriodYears', isset($order->rentalFreePeriodYears) ? $order->rentalFreePeriodYears : '', array('class' => 'form-control', 'style' => 'width:40px')) }}			 
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-4 control-label">Rental Free Period (Date)</label>  
                    <div class="col-md-5">
                        <div class='col-md-6' style="padding-left:0">
                           <div class='input-group date' id='rentalFreePeriodStart' >
                               {{ Form::text('rentalFreePeriodStart', isset($order->rentalFreePeriodStart) && $order->rentalFreePeriodStart!='' ? ($order->rentalFreePeriodStart == '0000-00-00' ? date('d/m/Y') : date('d/m/Y', strtotime($order->rentalFreePeriodStart))) : '', array('class' => 'form-control', 'id' => 'rentalFreePeriodStart', 'placeholder' => 'From')) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='input-group date' id='rentalFreePeriodEnd'>
                                {{ Form::text('rentalFreePeriodEnd', isset($order->rentalFreePeriodEnd) && $order->rentalFreePeriodEnd!='' ? ($order->rentalFreePeriodEnd == '0000-00-00' ? date('d/m/Y') : date('d/m/Y', strtotime($order->rentalFreePeriodEnd))) : '', array('class' => 'form-control', 'id' => 'rentalFreePeriodEnd', 'placeholder' => 'To')) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Car park -->
                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Car Park</label>
                </div>		

                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">No. of Car Park</label>
                    <div class="col-md-5">
                        <select class="form-control input-sm" onchange="addcarParkLots(this)">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>		

                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Lot Number</label>
                    <div class="col-sm-5">
                        <div id="divcarParkLots">

                            @if(isset($order))
                                <?php $arr = json_decode($order->carParkLots, true); ?>
                                @foreach($arr as $item)
                                {{ Form::text('carpark[]', isset($item) ? $item : '', array('class' => 'form-control', 'placeholder'=>'Lot Number',  'onkeyup'=>'capitalizeCarpark(this)')) }}	
                                {{ "<br/>" }}
                                @endforeach
                            @else
                            {{ Form::text('carpark[]', '', array('class' => 'form-control', 'placeholder'=>'Lot Number',  'onkeyup'=>'capitalizeCarpark(this)')) }}
                            <br>
                            @endif
                        </div>
                    </div>
                </div>	

            </fieldset> 						
        </div>

        <!-- Rental Detail end -->

        <!-- Payment Detail start -->

        <div class="col-md-13" >
            <div class="content-box-header panel-heading">
                <div class="panel-title "><h4>Payment Details</h4>
                </div>
            </div>
        </div>

        <div class="content-box-large box-with-header form-horizontal" >
            <fieldset>
                <!-- direct Bank in -->
                <div class="form-check form-group">
                    <label class="form-check-label  col-md-4 col-xs-9">
                        {{ Form::radio('paymentMethod', 'isDirectBankIn', isset($order->paymentMethod) && $order->paymentMethod == 'isDirectBankIn' ? 1 : 1 ) }}
                        Direct Bank-in
                    </label>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Name of Account Holder</label>
                    <div class="col-sm-5">
                        {{ Form::text('paymentAccountName', isset($order->paymentAccountName) ? $order->paymentAccountName : '', array('class' => 'form-control', 'placeholder' => 'Full Name')) }}     
                    </div>
                </div>		

                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Bank</label>
                    <div class="col-sm-5">
                        {{ Form::text('bank', isset($order->bank) ? $order->bank : '', array('class' => 'form-control', 'placeholder' => 'Bank Name')) }} 
                    </div>		
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4" for="appendprepend">Bank Account Number</label>
                    <div class="col-sm-5">
                        {{ Form::text('bankAccountNum', isset($order->bankAccountNum) ? $order->bankAccountNum : '', array('class' => 'form-control', 'placeholder' => 'Account Number')) }}
                    </div>
                </div>		

                <div class="form-group">
                    <label class="control-label col-md-4 col-xs-9" for="appendprepend">Method of notification upon payment</label>
                    <div class="col-sm-5 Methodnoti" >
                        {{ Form::select('notificationMethod', 
                            array('' => 'Please select',
                                    'isEmail' => 'Email',
                                    'isSMS' => 'SMS',
                                    'isWhatsapp' => 'Whatsapp')
                            , isset($order->notificationMethod) ? $order->notificationMethod : '', array('class' => 'form-control')) }}
                    </div>
                </div><br/><br/>	

                <!-- Post-Dated Cheque -->

                <div class="form-group">
                    <label class="col-md-4 control-label">
                        {{ Form::radio('paymentMethod', 'isPostDatedCheque', isset($order->paymentMethod) && $order->paymentMethod == 'isPostDatedCheque' ? 1 : 0 ) }}
                        Post-Dated Cheque
                    </label>  
                    <div class="col-md-5" >
                        {{ Form::select('postDatedMethod', 
                            array('' => 'Please select',
                            'is6Months' => '6 Months',
                            'is12Months' => '12 Months',
                            'is24Months' => '24 Months')
                            , isset($order->postDatedMethod) ? $order->postDatedMethod : '', array('class' => 'form-control')) }}
                    </div>
                </div><br/><br/>


                <div class="form-check form-group collectivebylandlord">
                    <label class="form-check-label  col-md-4 col-xs-9">
                        {{ Form::radio('paymentMethod', 'isCollectionByLandlord', isset($order->paymentMethod) && $order->paymentMethod == 'isCollectionByLandlord' ? 1 : 0 ) }} Collective by Landlord
                    </label>
                </div>

            </fieldset>
        </div>

        <!-- Payment Details End -->

        <!-- Special condition starts -->

        <div class="col-md-13" >
            <div class="content-box-header panel-heading">
                <div class="panel-title "><h4>Special Condition(s)</h4></div>
            </div>
        </div>

        <div class="content-box-large box-with-header form-horizontal" >
            <fieldset>

                <p align="center">
                    If you have any special conditions to include in your Tenancy Agreement, kindly indicate here. <br/>You may skip this step if you do not have any.
                </p>

                @if(isset($order))
                <?php $arr = json_decode($order->specialRequest, true);?>
                @if (is_array($arr)) 
                    <?php $i = 1;
                    $content = '';?>
                    @foreach ($arr as $inner_arr) 
                        @if (is_array($inner_arr)) 
                            @if ($inner_arr['edited'] != "") 
                                <?php $content = $inner_arr['edited'];?>
                            @else 
                                <?php $content = $inner_arr['original'];?>
                            @endif
                        @else 
                            <?php $content = $inner_arr;?>
                        @endif
                        
                        <div class="form-group form-inline">
                            <div>
                                <label class="control-label col-md-3 col-xs-1" for="appendprepend">
                                    {{ $i }} 
                                </label>
                                <div class="col-md-5 col-xs-9">
                                    {{ Form::text('specialRequest[]', $content, array('class' => 'form-control col-md-6 col-xs-4', 'placeholder'=>'Special Condition')) }}   
                                </div>
                                <a href="javascript:addspecialRequest()"><i class="glyphicon glyphicon-plus plusicon"></i></a>
                            </div>
                        </div>

                        <?php $i++; ?>
                    @endforeach
                @endif
                @else
                <div class="form-group form-inline">
                    <div>
                        <label class="control-label col-md-3 col-xs-1" for="appendprepend">1.</label>
                        <div class="col-md-5 col-xs-9">
                            {{ Form::text('specialRequest[]', '', array('class' => 'form-control col-md-6 col-xs-4', 'placeholder'=>'Special Condition')) }}  
                        </div>
                        <a href="javascript:addspecialRequest()"><i class="glyphicon glyphicon-plus plusicon"></i></a>
                    </div>
                </div>
                @endif

                <div id="divspecialRequest"></div>

            </fieldset>
        </div>

        <!-- special condition end -->

        <!-- Fixture & Fitting starts -->
        <div class="col-md-13" >
            <div class="content-box-header panel-heading">
                <div class="panel-title "><h4>Fixture & Fitting(s)</h4></div>
            </div>
        </div>

        <div class="content-box-large box-with-header form-horizontal" >
            <fieldset>

                <p align="center">
                    Fixtures & Fittings, Furnitures, Electrical Appliances (If any).<br/> You may skip this step if do not have any.	
                </p>

                @if(isset($order))
                <?php $arr = json_decode($order->fixturesFitting, true); ?>
                @foreach($arr as $key=>$item)
                <div class="form-group form-inline">
                    <div>
                        <label class="control-label col-md-3 col-xs-1" for="appendprepend">
                            {{ $key + 1 }} 
                        </label>
                        <div class="col-md-5 col-xs-9">
                            {{ Form::text('fixturesFitting[]', $item, array('onkeyup'=>'capitalizeFixtures(this)', 'class' => 'form-control col-md-6 col-xs-4', 'placeholder'=>'Fixture & Fitting')) }}   
                        </div>
                        <a href="javascript:addfixturesFitting()"><i class="glyphicon glyphicon-plus plusicon"></i></a>
                    </div>
                </div>
                @endforeach
                @else
                <div class="form-group form-inline">
                    <div>
                        <label class="control-label col-md-3 col-xs-1" for="appendprepend">1.</label>
                        <div class="col-md-5 col-xs-9">
                            {{ Form::text('fixturesFitting[]', '', array('onkeyup'=>'capitalizeFixtures(this)', 'class' => 'form-control col-md-6 col-xs-4', 'placeholder'=>'Fixture & Fitting')) }}  
                        </div>
                        <a href="javascript:addfixturesFitting()"><i class="glyphicon glyphicon-plus plusicon"></i></a>
                    </div>
                </div>
                @endif
                <div id="divfixturesFitting"></div>

            </fieldset>
        </div>            
        <!-- Fixture & Fitting end-->

        <!-- Agree starts -->

        <div class="col-md-13" >

            <div class="content-box-large ">
                <div align="center">
                    <p>Render your Tenancy Agreement?</p>

                    <div class="btn-group">
                        <button type="submit" class="btn btn-default btn_best btn_spaceright" aria-expanded="false">
                            NEXT
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Agree end -->


    </div>
    {{ Form::close() }}
</div>



<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#propertyPicture").change(function(){
            readURL(this);
        }); 
                
        $('#commencementDate').datetimepicker({
            format: "DD/MM/YYYY"
        });
        $('#rentalFreePeriodStart').datetimepicker({
            format: "DD/MM/YYYY"
        });
        $('#rentalFreePeriodEnd').datetimepicker({
            format: "DD/MM/YYYY",
            useCurrent: false 
        });
        $("#rentalFreePeriodStart").on("dp.change", function (e) {
            $('#rentalFreePeriodEnd').data("DateTimePicker").minDate(e.date);
        });
        $("#rentalFreePeriodEnd").on("dp.change", function (e) {
            $('#rentalFreePeriodStart').data("DateTimePicker").maxDate(e.date);
        });
        
        $('.landlordCompany').each(function (index, value) { 
            landlordCompany(this);
        });
        
        $('.tenantCompany').each(function (index, value) { 
            tenantCompany(this);
        });
        
    });
    
    function landlordCompany(e) {
        var val = $('input[name*=' + e.name + ']:checked').val();
        var element = e.name.replace('isLCompany', '');

        if (val == 0) {
            $('#divLCompany' + element).hide();
        } else {
            $('#divLCompany' + element).show();
        }
    }

    function tenantCompany(e) {
        var val = $('input[name*=' + e.name + ']:checked').val();
        var element = e.name.replace('isTCompany', '');

        if (val == 0) {
            $('#divTCompany' + element).hide();
        } else {
            $('#divTCompany' + element).show();
        }
    }

    function addLandlord() {
        var eleCount = $("#divLandlord input[name*='name']").length;
        var str = '';
        var newIndex = eleCount + 1;

        str += '<div class="col-md-13" >';
        str += '<div class="content-box-header panel-heading">';
        str += '<div class="panel-title">';
        str += '<h4>Landlord ' + newIndex + ' Details</h4>';
        str += '</div>';
        str += '<label class=" radio-inline ">';
        str += '<input type="radio" name="isLCompany' + newIndex + '" value="0" class="landlordCompany" onchange="landlordCompany(this)">Individual';
        str += '</label>';
        str += '<label class=" radio-inline ">';
        str += '<input type="radio" name="isLCompany' + newIndex + '" value="1" class="landlordCompany" checked onchange="landlordCompany(this)">Company';
        str += '</label>';
        str += '</div>';
        str += '</div>';

        str += '<div class="content-box-large box-with-header form-horizontal" >';
        str += '<fieldset>';
        str += '<div id="divLCompany' + newIndex + '">';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Company Name**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>';
        str += '<input  name="L_companyName' + newIndex + '" placeholder="Company Name" class="form-control" type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Company Reg. No.**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>';
        str += '<input name="L_companyRegNum' + newIndex + '" placeholder="Company Reg. No." class="form-control" type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Full Name**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';
        str += '<input  name="L_name' + newIndex + '" placeholder="Full Name as per I/C No. & Passport No." class="form-control" required type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">I.C No./Passport No.**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';
        str += '<input  name="L_icNum' + newIndex + '" placeholder="I.C No./Passport No." class="form-control" required type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Correspondence Address**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>';
        str += '<input name="L_address' + newIndex + '" placeholder="Correspondence Address" class="form-control" required type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Phone</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>';
        str += '<input  name="L_contactNum' + newIndex + '" placeholder="Phone Number" class="form-control"  type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Email Address</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-envelope"></i></span>';
        str += '<input name="L_email' + newIndex + '" placeholder="Email Address" class="form-control" type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="addbutton1" align="center" >';
        str += '<a type="button" class="btn btn-primary addbutton" onclick="addLandlord()">Add Landlord</a>';
        str += '</div>';
        str += '</fieldset>';
        str += '</div>';
        $('#divLandlord').append(str);
        $('#hidLandlordCount').val(newIndex);
    }

    function addTenant() {
        var eleCount = $("#divTenant input[name*='name']").length;
        var str = '';
        var newIndex = eleCount + 1;

        str += '<div class="col-md-13" >';
        str += '<div class="content-box-header panel-heading">';
        str += '<div class="panel-title">';
        str += '<h4>Tenant ' + newIndex + ' Details</h4>';
        str += '</div>';
        str += '<label class=" radio-inline ">';
        str += '<input type="radio" name="isTCompany' + newIndex + '" value="0" class="tenantCompany" onchange="tenantCompany(this)">Individual';
        str += '</label>';
        str += '<label class=" radio-inline ">';
        str += '<input type="radio" name="isTCompany' + newIndex + '" value="1" checked class="tenantCompany" onchange="tenantCompany(this)">Company';
        str += '</label>';
        str += '</div>';
        str += '</div>';

        str += '<div class="content-box-large box-with-header form-horizontal" >';
        str += '<fieldset>';
        str += '<div id="divTCompany' + newIndex + '">';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Company Name**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>';
        str += '<input  name="T_companyName' + newIndex + '" placeholder="Company Name" class="form-control" type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Company Reg. No.**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>';
        str += '<input name="T_companyRegNum' + newIndex + '" placeholder="Company Reg. No." class="form-control" type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Full Name**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';
        str += '<input  name="T_name' + newIndex + '" placeholder="Full Name as per I/C No. & Passport No." class="form-control" required type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">I.C No./Passport No.**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>';
        str += '<input  name="T_icNum' + newIndex + '" placeholder="I.C No./Passport No." class="form-control" required type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Correspondence Address**</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>';
        str += '<input name="T_address' + newIndex + '" placeholder="Correspondence Address" class="form-control" required type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Phone</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>';
        str += '<input  name="T_contactNum' + newIndex + '" placeholder="Phone Number" class="form-control"  type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="form-group">';
        str += '<label class="col-md-4 control-label">Email Address</label>';
        str += '<div class="col-md-5 inputGroupContainer">';
        str += '<div class="input-group">';
        str += '<span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-envelope"></i></span>';
        str += '<input name="T_email' + newIndex + '" placeholder="Email Address" class="form-control" type="text">';
        str += '</div>';
        str += '</div>';
        str += '</div>';

        str += '<div class="addbutton1" align="center" >';
        str += '<a type="button" class="btn btn-primary addbutton" onclick="addTenant()">Add Tenant</a>';
        str += '</div>';
        str += '</fieldset>';
        str += '</div>';
        $('#divTenant').append(str);
        $('#hidTenantCount').val(newIndex);
    }

    function addcarParkLots(e) {
        var selected = e.value;
        var eleCount = $("input[name*='carpark[]']").length;
        var newIndex = 0;
        var str = '';

        if (selected > eleCount) {
            newIndex = selected - eleCount;

            for (i = 0; i < newIndex; i++) {
                str += '<input type="text" name="carpark[]" class="form-control" placeholder="Lot Number" onkeyup="capitalizeCarpark(this)">';
                str += '<br/>';
            }
            $('#divcarParkLots').append(str);
        } else {
            for (i = 0; i < selected; i++) {
                str += '<input type="text" name="carpark[]" class="form-control" placeholder="Lot Number" onkeyup="capitalizeCarpark(this)">';
                str += '<br/>';
            }
            $('#divcarParkLots').html(str);
        }

    }

    function addspecialRequest() {
        var eleCount = $("input[name*='specialRequest']").length;
        var newIndex = eleCount + 1;
        var str = '<div class="form-group form-inline" id="specialRequest'+newIndex+'"><div><label class="control-label col-md-3 col-xs-1" for="appendprepend">' + newIndex + '.</label>';
        str += '<div class="col-md-5 col-xs-9">';
        str += '<input type="text" name="specialRequest[]" class="form-control col-md-6 col-xs-4" placeholder="Special Condition"></div>';
        str += '<a href="javascript:addspecialRequest(' + newIndex + ')"><i class="glyphicon glyphicon-plus plusicon"></i></a>';
        str += '<a href="javascript:removespecialRequest('+ newIndex +')"><i class="glyphicon glyphicon-minus plusicon"></i></a>';
        str +='</div></div>';
        $('#divspecialRequest').append(str);
    }
    
    function removespecialRequest(index){
        $("#specialRequest" + index).remove();
        
        $("input[name*='specialRequest']").each(function (index, value) { 
            $(this).closest('.form-group.form-inline').find('label').text((index + 1) + '.');
        });
    }

    function addfixturesFitting() {
        var eleCount = $("input[name*='fixturesFitting']").length;
        var newIndex = eleCount + 1;
        var str = '<div class="form-group form-inline" id="fixturesFitting'+newIndex+'"><div><label class="control-label col-md-3 col-xs-1" for="appendprepend">' + newIndex + '.</label>';
        str += '<div class="col-md-5 col-xs-9">';
        str += '<input type="text" id="fixturesFitting" name="fixturesFitting[]" onkeyup="capitalizeFixtures(this)" class="form-control col-md-6 col-xs-4" placeholder="Fixture & Fitting"></div>';
        str += '<a href="javascript:addfixturesFitting(' + newIndex + ')"><i class="glyphicon glyphicon-plus plusicon"></i></a>';
        str += '<a href="javascript:removefixturesFitting(' + newIndex + ')"><i class="glyphicon glyphicon-minus plusicon"></i></a>';
        str += '</div></div>';
        $('#divfixturesFitting').append(str);
    }
    
    function removefixturesFitting(index){
        $("#fixturesFitting" + index).remove();
        
        $("input[name*='fixturesFitting']").each(function (index, value) { 
            $(this).closest('.form-group.form-inline').find('label').text((index + 1) + '.');
        });
    }
    
    function capitalizeCarpark(ele){
        capitalize(ele);
    }
    
    function capitalizeFixtures(ele){
        capitalize(ele);
    }
    
    function capitalize(ele){
        if (ele.value[0] != ele.value[0].toUpperCase()) {
                // store current positions in variables
                var start = ele.selectionStart;
                var end = ele.selectionEnd; 
                ele.value = ele.value[0].toUpperCase() + ele.value.substring(1);
                // restore from variables...
                ele.setSelectionRange(start, end);
            }
    }
</script>