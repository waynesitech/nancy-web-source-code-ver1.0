
<script>

function open() {
   
    // If it's not an universal app, use IS_IPAD or IS_IPHONE
    if (getMobileOperatingSystem()=='iOS') {
         if (!document.webkitHidden) {
    
                // Replace the Apple ID following '/id'
                window.location = 'https://itunes.apple.com/my/app/lesys-tenancy/id1102710514?mt=8';
            }
    
    }else if(getMobileOperatingSystem()=='Android'){
        
         if (!document.webkitHidden) {
    
                // Replace the Apple ID following '/id'
                window.location = 'https://play.google.com/store/apps/details?id=com.lesys.leten&hl=en';
            }
    }else{
                 window.location = 'http://lesystenancy.com/';

    }
}


function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
  {
    return 'iOS';

  }
  else if( userAgent.match( /Android/i ) )
  {

    return 'Android';
  }
  else
  {
    return 'unknown';
  }
}

window.onload = open;
</script>