<div class="container">
    <div class="row">	
        <div class="col-md-10">
            <div class="content-box-large " >

                <div class="table table_project table-responsive">
                    <div class="row">
                        <div class=" container project_image col-md-4" >
                            {{ HTML::image(URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/' . $order->propertyPicture), null, array('class'=>'img-responsive project_image')) }}
                        </div>

                        <div class="container col-md-8 table-responsive project_table">

                            <table class="table table-borderless project_info">
                                <tr class="project_info_row1">
                                    <td class="project_info_column1">
                                        <h6 class="project_title"><strong>Reference No.</strong></h6>
                                    </td>
                                    <td>
                                        <p>{{$order->order_num}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="project_title"><strong>Property</strong></h6>
                                    </td>
                                    <td>
                                        <p>
                                            {{$order->propertyStreet}} <br/>
                                            {{$order->propertyTown}} <br/>
                                            {{$order->propertyPostcode}} {{$order->propertyState}}
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="project_title"><strong>Landlord</strong></h6>
                                    </td>
                                    <td>
                                        <p>
                                            @if(count($order->landlord) > 0)
                                            @foreach($order->landlord as $key=>$value)
                                            {{ucfirst($value->name)}}
                                            {{isset($order->landlord[$key+1]) ? "<br/>": ""}}
                                            @endforeach
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="project_title"><strong>Tenant</strong></h6>
                                    </td>
                                    <td>
                                        <p>
                                            @if(count($order->tenant) > 0)
                                            @foreach($order->tenant as $key=>$value)
                                            {{ucfirst($value->name)}}
                                            {{isset($order->tenant[$key+1]) ? "<br/>": ""}}
                                            @endforeach
                                            @endif
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            <div class="all-btn">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn_best btn_spaceright" aria-expanded="false" onclick="window.location.href ='{{ route('order.preview', array('id'=>$order->id)) }}'">
                                        VIEW
                                    </button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn_best btn_spaceright" aria-expanded="false" {{($order->tenancyAgreementStatus == 'Done' || $order->tenancyAgreementStatus == 'Expired') ? '' : 'disabled'}}>
                                        <a href="{{URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/pdf/' . $order->actualDocument)}}" download>DOWNLOAD</a>
                                    </button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn_best btn_spaceright" aria-expanded="false" data-toggle="modal" data-target="#Explanation">
                                        EXPLANATION
                                    </button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" id='btnResend' class="btn btn-default btn_best" aria-expanded="false" {{($order->tenancyAgreementStatus == 'Done' || $order->tenancyAgreementStatus == 'Expired') ? '' : 'disabled'}}>
                                        RESEND
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row project_info">
                <div class="col-xs-12 col-md-3 ">
                    <div class="thumbnail thumb_clr">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th><h5>Tenancy Agreement</h5></th>
                                    <th>
                                        @if ($order->tenancyAgreementStatus == 'Received')
                                        <p style="color:#ffef63">{{strtoupper('Rendering')}}</p>
                                        @elseif ($order->tenancyAgreementStatus == 'Ready' || $order->tenancyAgreementStatus == 'Done')
                                        <p style="color:#3c763d">{{strtoupper($order->tenancyAgreementStatus)}}</p>
                                        @elseif ($order->tenancyAgreementStatus == 'Expired' || $order->tenancyAgreementStatus == 'Rejected')
                                        <p style="color:#ff0000">{{strtoupper($order->tenancyAgreementStatus)}}</p>
                                        @endif
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 ">
                    <div class="thumbnail thumb_clr">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th><h5>E Sign</h5></th>
                                    <th><p></p></th>
                                    <th class="" >
                                        <div class="btn-group">
                                            <button type="button" id="btnSend" class="btn btn-default btn_best btn_spaceright" aria-expanded="false" data-toggle="modal" data-target="#SendOTP" {{($order->tenancyAgreementStatus == 'Ready' && $order->eSignStatus == 'PARTIAL_SIGNED') ? '' : 'disabled'}}>
                                                SEND OTP
                                            </button>

                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($order->landlord) > 0)
                                @foreach($order->landlord as $key=>$value)
                                <tr class="project_info_row1">
                                    <td><p>Landlord</p></td>
                                    <td><p>{{ucfirst($value->name)}}</p></td>
                                    <td><p>{{!is_null($value->document_url) ? 'Signed' : ''}}</p></td>
                                </tr>
                                @endforeach
                                @endif
                                @if(count($order->tenant) > 0)
                                @foreach($order->tenant as $key=>$value)
                                <tr class="project_info_row1">
                                    <td><p>Tenant</p></td>
                                    <td><p>{{ucfirst($value->name)}}</p></td>
                                    <td><p>{{!is_null($value->document_url) ? 'Signed' : ''}}</p></td>
                                </tr>
                                @endforeach
                                @endif
                                {{ Form::open(array('method' => 'post', 'url' => route("order.manualSign", array("id" => $order->id)), 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'manualForm')) }}
                                <tr class="project_info_row1">
                                    <td><p>Manual Sign</p></td>
                                    <td><p>{{ Form::file('manualSign', array('accept' => '.pdf', 'id' => 'manualSign')) }}</p>
                                    
                                    <p class="help-block" style="color:red">Please only upload sign page.<p/>
                                    </td>
                                    <td
                                        <div class="btn-group">
                                            <button type="submit" id="btnManual" class="btn btn-default btn_best btn_spaceright"  {{($order->tenancyAgreementStatus == 'Ready' && $order->eSignStatus == 'PARTIAL_SIGNED') ? '' : 'disabled'}}>
                                                UPLOAD SIGN
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                {{ Form::close() }}
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-xs-12 col-md-3">
                    <div class="thumbnail thumb_clr">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th><h5>E Stamp</h5></th>
                                    <th data-toggle="modal" data-target="#NewEstamp">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn_best btn_spaceright" aria-expanded="false">SUBMIT
                                            </button>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>	
                </div>
            </div>
        </div>	
    </div>
</div>

<!-- Send OTP- modal -->
<div class="modal fade" id="SendOTP" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">E Sign</h4>
            </div>
            <div class="modal-body">
                <p>OTP has been sent to <br/>
                    @if(count($order->landlord) > 0)
                    @foreach($order->landlord as $key=>$value)
                        Landlord ({{ucfirst($value->name)}}) at {{ucfirst($value->contactNum)}}
                        {{isset($order->landlord[$key+1]) ? "<br/>": ""}}
                    @endforeach
                    @endif
                    <br/>
                    @if(count($order->tenant) > 0)
                    @foreach($order->tenant as $key=>$value)
                        Tenant ({{ucfirst($value->name)}}) at {{ucfirst($value->contactNum)}}
                        {{isset($order->tenant[$key+1]) ? "<br/>": ""}}
                    @endforeach
                    @endif
                    
                </p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        OK, Thanks!
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- EStamp- modal -->
<div class="modal fade" id="NewEstamp" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">E-Stamp</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <ul id="buttons">
                        <li class="text-left">If the stamping fee is RM800 and below = RM40</li>
                        <li class="text-left">If the stamping fee is above RM800 = 5% of the total stamping fee</li>
                    </ul>								
                </div>

                <div>
                    <table class="table table-estamp-calculation">
                        <tr class="">
                            <th class="text-left"><h6>ITEMS</h6></th>
                            <th class="text-right"><h6>PRICE (RM)</th>
                        </tr>
                        <tr class="">
                            <td class="text-left">Stamping Fee</td>
                            <td class="text-right"> {{number_format($estamp['stampingFee'],2)}}</td>
                        </tr>
                        <tr class=""> 
                            <td class="text-left">Penalty Fee</td>
                            <td class="text-right"> {{number_format($estamp['penaltyFee'],2)}}</td>
                        </tr>
                        <tr class="">
                            <td class="text-left">Duplicates <br/><input type="number" name="txtDuplicate" id="txtDuplicate" min="0" class="form-control-new" value="0" onchange="calculate()" onkeyup="calculate()"> copy (ies)</td>
                            <td class="text-right"><label id="lblDuplicateAmt" name="lblDuplicateAmt">0.00</label></td>
                        </tr>
                        <tr class="table-calculation-amount">
                            <td ></td>
                            <td class="text-right " ><label id="lblTotal" name="lblTotal">0.00</label></td>
                        </tr>
                        <tr class="">
                            <td class="text-left">Service Charges</td>
                            <td class="text-right" >{{number_format($estamp['serviceFee'],2)}}</td>
                        </tr>
                        <tr class="table-calculation-amount">
                            <td class="text-left"><h4>TOTAL</h4></td>
                            <td class="text-right "><h4><label id="lblGrandTotal" name="lblGrandTotal">0.00</label></h4></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="modal-footer">
                {{ Form::open(array('method' => 'get', 'class' => 'form-horizontal', 'route' => 'payment.redirect')) }}
                {{ Form::hidden('order', $order->id) }}	
                <button type="submit" class="btn btn-default">I Agree</button>
                {{ Form::close() }}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Resend- modal -->
<div class="modal fade" id="Resend" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Resend</h4>
            </div>
            <div class="modal-body">
                <p><div id='modalMsg'></div></p>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        OK, Thanks!
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--- Explanation modal -->
<div class="modal fade" id="Explanation" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Preferred Language</h4>
            </div>
            <div class="modal-body">
                <p>Do  you need me to explain the clauses to you?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">
                    <a href="{{ route('order.explanation', array('lang'=>'en')) }}">English</a>
                </button>
                <button type="button" class="btn btn-default">
                    <a href="{{ route('order.explanation', array('lang'=>'cn')) }}">Mandarin</a>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    var stampingFee = '<?php echo $estamp['stampingFee'] ?>';
    var penaltyFee = '<?php echo $estamp['penaltyFee'] ?>';
    var stampingDuplicateFee = '<?php echo $estamp['stampingDuplicateFee'] ?>';
    var serviceFee = '<?php echo $estamp['serviceFee'] ?>';
    var amt = 0;
    var total_amt = 0;
    var grand_amt = 0;

    function calculate() {
        amt = parseFloat(stampingDuplicateFee) * parseFloat($('#txtDuplicate').val());
        total_amt = parseFloat(stampingFee) + parseFloat(penaltyFee) + parseFloat(amt);
        grand_amt = parseFloat(total_amt) + parseFloat(serviceFee);
        $("#lblDuplicateAmt").text(amt.toFixed(2));
        $("#lblTotal").text(total_amt.toFixed(2));
        $("#lblGrandTotal").text(grand_amt.toFixed(2));
    }

    $("#btnSend").click(function () {
        $.ajax({
            type: "POST",
            url: "{{ route('order.sendOTP') }}",
            data: {orderID: '<?php echo $order->id ?>'},
            dataType: 'json',
            success: function (data) {
                if (data.meta.stat == 200) {
                    $('#SendOTP').modal('show');
                } else {
                    alert('Error. Please Try again.');
                }
            },
            error: function (data) {
                alert(data.responseJSON.meta.msg.body);
            }
        });
    });
    
    $("#btnResend").click(function () {
        $("#btnResend").attr('disabled', 'disabled');        
        $.ajax({
            type: "POST",
            url: "{{ route('order.resend') }}",
            data: {orderID: '<?php echo $order->id ?>'},
            dataType: 'json',
            success: function (data) {
                if (data.meta.stat == 200) {
                    $('#modalMsg').text(data.meta.msg.body);
                    $('#Resend').modal('show');
                } else {
                    alert('Error. Please Try again.');
                }
            },
            error: function (data) {
                alert(data.responseJSON.meta.msg.body);
            }
        }).done(function () {
                    $("#btnResend").removeAttr('disabled');
                })
                ;
    });

</script>