<style>
    #signature {
        background-color:lightgrey;
        width:400px;
        height: 200px;
    }
</style>

<div class="col-md-10">
    <div class="row">
        <div class="col-md-11">
            <div class="content-box-large " >

                <div class="table table_project table-responsive">
                    <div class="row">
                        <div class=" container project_image col-md-4" >
                            {{ HTML::image(URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/' . $order->propertyPicture), null, array('class'=>'img-responsive project_image')) }}
                        </div>

                        <div class="container col-md-8 table-responsive project_table">

                            <table class="table table-borderless project_info">
                                <tr class="project_info_row1">
                                    <td class="project_info_column1">
                                        <h6 class="project_title"><strong>Reference No.</strong></h6>
                                    </td>
                                    <td>
                                        <p>{{$order->order_num}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="project_title"><strong>Property</strong></h6>
                                    </td>
                                    <td>
                                        <p>
                                            {{$order->propertyStreet}} <br/>
                                            {{$order->propertyTown}} <br/>
                                            {{$order->propertyPostcode}} {{$order->propertyState}}
                                        </p>
                                    </td>
                                </tr>
                           </table>
                            <div class="all-btn">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn_best btn_spaceright" aria-expanded="false" onclick="window.location.href ='{{ route('order.preview', array('id'=>$order->id)) }}'">
                                        VIEW
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="content-box-large">
                <h4>Sign here</h4>
                <div class="panel-body">
                    <?php echo Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'route' => array('user.esign_save'), 'enctype' => 'multipart/form-data')); ?>
                    <div id="signature"></div>
                    <input type="hidden" id="orderUserID" value="{{ $orderUserID }}"/>
                    <button type="button" id="btnSave">Save</button>
                    <?php echo Form::close(); ?>  
                </div>
            </div>
        </div>
    </div>
</div>

  

    

{{ HTML::script('/assets/js/jquery.js') }}
{{ HTML::script('/assets/js/jSignature.min.noconflict.js') }}
<script>
    (function($){      
        var topics = {};
        $.publish = function(topic, args) {
            if (topics[topic]) {
                var currentTopic = topics[topic],
                args = args || {};
                
                for (var i = 0, j = currentTopic.length; i < j; i++) {
                    currentTopic[i].call($, args);
                }
            }
        };
        
        $(document).ready(function() {            
            // This is the part where jSignature is initialized.
            var $sigdiv = $("#signature").jSignature({'UndoButton':true, width: 400, height: 200})    
            
            
            $( "#btnSave" ).click(function() {
                var data = $sigdiv.jSignature('getData', 'default');
                var result = '';
                var msg = '';
                $.publish('formatchanged');
                if (typeof data === 'string'){
                    result = data;
                } else if($.isArray(data) && data.length === 2){
                    result = data.join(',');
                    $.publish(data[0], data);
                } else {
                    try {
                        result = JSON.stringify(data);
                    } catch (ex) {
                        msg = 'Not sure how to stringify this, likely binary, format.';
                        alert(msg);
                        return;
                    }
                }
                
                if (confirm('Are you confirm to save this? Once submit cannot be changed.')) {
                    var orderUserID = $('#orderUserID').val();
                    $.ajax({
                        type: "POST",
                        url : '{{ route("user.esign_save") }}',
                        data: {orderUserID: orderUserID, sign: result},
                        success: function (data) {
                            alert(data.meta.msg.body);
                            $('#btnSave').prop('disabled', true);
                        },error: function(textStatus, errorThrown){
                            alert('Error.');
                        }
                    });
                }
            });
            
            
        });        
    })(jQuery)
</script>