<div class="container">
    <div class="row">	
        <div class="col-md-10">
            <div class="content-box-large " >

                <div class="table table_project table-responsive">
                    <div class="row">
                        <div>
                            <object data="{{URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/pdf/' . $order->previewDocument)}}" type="application/pdf" width="100%" height='600' style='margin-right: -15px; margin-left: -15px;'>
                                alt : <a href="{{URL::asset(ORDER_IMAGE_URL . '/' . $order->id . '/pdf/' . $order->previewDocument)}}">preview.pdf</a>
                            </object>
                        </div>
                    </div>
                </div>
                <div class="all-btn">
                    <div class="btn-group">
                        {{ Form::open(array('method' => 'get', 'class' => 'form-horizontal', 'route' => 'payment.redirect')) }}
                        {{ Form::hidden('order', $order->id) }}	
                        <p class="help-block" style="color:red">Please take note that once you make the payment, no changes will be accepted. Kindly ensure your number are correct if you wish to proceed E-sign.<p/>
                        <button type="submit" class="btn btn-default btn_best" aria-expanded="false">
                            CONFIRM
                        </button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

        </div>	
    </div>
</div>