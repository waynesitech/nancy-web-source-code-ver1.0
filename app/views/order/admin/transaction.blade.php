<div class="page-title">
    <div class="title_left">
      <h3>Tenancy Agreement Transaction</h3>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Listing</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Order No.</th>
                <th>Reference</th>
                <th>Transaction ID</th>
                <th>Amount</th>
                <th>Email</th>
                <th>User Name</th>
                <th>Status</th>
                <th>Date created</th>              
              </tr>
            </thead>


            <tbody>
                @foreach($payments as $payment)
                <tr>
                    <td><a href='{{ route('order.edit',['id' => $payment->order_id]) }}'>{{ isset($payment->order_num) && $payment->order_num !='' ? $payment->order_num : '-' }}</a></td>
                    <td class="td-restrict-150">{{ $payment->ref }}</td>
                    <td class="td-restrict-150">{{ $payment->ipay88_trans_id }}</td>
                    <td class="td-restrict-150">{{number_format($payment->amount,2)}}</td>
                    <td class="td-restrict-150">{{ $payment->user_email }}</td>
                    <td class="td-restrict-150">{{ $payment->user_name }}</td>
                    <td style="color:<?php echo isset($payment->status) ? ($payment->status == '1' ? '#5cb85c' : '#d9534f') : '#feb836'; ?>;">
                        <?php echo isset($payment->status) ? ($payment->status == '1' ? 'Success' : 'Failed') : '-'; ?>
                    </td>
                    <td>{{ date('d/m/Y H:i:s', strtotime($payment->created_on)) }}</td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<!-- Datatables -->
<script>
  $(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#datatable-buttons").length) {
        $("#datatable-buttons").DataTable({
            "order": [[ 4, "desc" ]],
          dom: "Bfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();



    TableManageButtons.init();
  });
</script>
<!-- /Datatables -->
