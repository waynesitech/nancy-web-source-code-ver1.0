<div class="page-title">
    <div class="title_left">
        <h3>Tenancy Details</h3>
    </div>
</div>


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                @include('msg')
                
                {{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'orderForm')) }}
                
                @if(isset($order->propertyPicture))
                <div style="max-width: 500px; margin:0 auto 15px;text-align:center;">
                    <a itemprop="photo" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->propertyPicture }}' data-lightbox="single">
                        <img src="{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->propertyPicture }}" alt="" style='max-width: 500px;'/>
                    </a>
                </div>
                
                <hr>
                @endif
                
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('order_num', 'Order Number', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block">{{ isset($order->order_num) ? $order->order_num : '' }}</p>
                    </div>
                </div>
                
                <hr>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('request_by', 'Requested By', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block">{{ isset($order->users->name) ? $order->users->name : '' }}</p>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('requester_email', 'Email', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block">{{  isset($order->users->email) ? $order->users->email : '' }}</p>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('requester_mobile', 'Contact', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block">{{  isset($order->users->mobile) ? $order->users->mobile : '' }}</p>
                    </div>
                </div>
                
                <hr>
                
                <h2>Tenant Details</h2>
                
                @if(count($order->tenant) > 0)
                @foreach($order->tenant as $key=>$value)
                <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="heading{{$key + 1}}" data-toggle="collapse" data-parent="#accordion" href="#tenant{{$key + 1}}" aria-expanded="false" aria-controls="tenant{{$key + 1}}">
                            <h4 class="panel-title">Tenant {{$key + 1}}</h4>
                        </a>
                        <div id="tenant{{$key + 1}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$key + 1}}">
                            <div class="panel-body">
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_type', 'Type', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ Form::radio('isTCompany', '0', isset($value->isCompany) ? !$value->isCompany : 0 ) }} Individual
                                        
                                        {{ Form::radio('isTCompany', '1', isset($value->isCompany) ? $value->isCompany : 1 ) }} Company
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_companyName', 'Company Name', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->companyName) ? $value->companyName : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_companyRegNum', 'Company Reg. No.', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->companyRegNum) ? $value->companyRegNum : '' }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_name', 'Tenant', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->name) ? $value->name : '' }}</p>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_icNum', 'I.C No./Passport No.', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->icNum) ? $value->icNum : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_address', 'Correspondence Address', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->address) ? $value->address : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_contactNum', 'Phone', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->contactNum) ? $value->contactNum : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('tenant_email', 'Email Address', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->email) ? $value->email : '' }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                
                <hr>
                
                <h2>Landlord Details</h2>
                
                @if(count($order->landlord) > 0)
                @foreach($order->landlord as $key=>$value)
                <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="heading{{$key + 1}}" data-toggle="collapse" data-parent="#accordion" href="#landlord{{$key + 1}}" aria-expanded="false" aria-controls="landlord{{$key + 1}}">
                            <h4 class="panel-title">Landlord {{$key + 1}}</h4>
                        </a>
                        <div id="landlord{{$key + 1}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$key + 1}}">
                            <div class="panel-body">
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_type', 'Type', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{ Form::radio('isLCompany'.$key + 1, '0', isset($value->isCompany) ? !$value->isCompany : 0 ) }} Individual
                                        
                                        {{ Form::radio('isLCompany'.$key + 1, '1', isset($value->isCompany) ? $value->isCompany : 1 ) }} Company
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_companyName', 'Company Name', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->companyName) ? $value->companyName : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_companyRegNum', 'Company Reg. No.', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->companyRegNum) ? $value->companyRegNum : '' }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_name', 'Landlord', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->name) ? $value->name : '' }}</p>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_icNum', 'I.C No./Passport No.', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->icNum) ? $value->icNum : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_address', 'Correspondence Address', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->address) ? $value->address : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_contactNum', 'Phone', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->contactNum) ? $value->contactNum : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{ HTML::decode(Form::label('landlord_email', 'Email Address', array('class' => 'col-sm-3 control-label'))) }}
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="help-block">{{ isset($value->email) ? $value->email : '' }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                
                <hr>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('propertyStreet', 'Property Address', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('propertyStreet', isset($order->propertyStreet) ? $order->propertyStreet : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ HTML::decode(Form::label('propertyTown', 'Property Town', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('propertyTown', isset($order->propertyTown) ? $order->propertyTown : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ HTML::decode(Form::label('propertyPostcode', 'Property Postcode', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('propertyPostcode', isset($order->propertyPostcode) ? $order->propertyPostcode : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ HTML::decode(Form::label('propertyState', 'Property State', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::select('propertyState', $states, isset($order->propertyState) ? $order->propertyState : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('property_type', 'Property Type', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block">{{ isset($order->isResidential) && $order->isResidential == '0' ? 'Residential':'Commercial' }}</p>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('commencementDate', 'Commencement Date', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('commencementDate', isset($order->commencementDate) && $order->commencementDate!='' ? ($order->commencementDate == '0000-00-00' ? date('d/m/Y') : date('d/m/Y', strtotime($order->commencementDate))) : '', array('class' => 'form-control col-md-7 col-xs-12', 'id' => 'commencementDate')) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('termsOfTenancy', 'Term of Tenancy', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-sm-2">
                            {{ Form::select('termsOfTenancyMonths', $month, isset($order->termsOfTenancyMonths) ? $order->termsOfTenancyMonths : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }} 
                    </div>
                    <div class="col-sm-1">Months</div>
                    <div class="col-sm-2">
                        {{ Form::text('termsOfTenancyYears', isset($order->termsOfTenancyYears) ? $order->termsOfTenancyYears : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                    </div>
                    <div class="col-sm-1">Years</div>
                </div>
                               
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('isOptionToRenew', 'Option to Renew', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block">{{ isset($order->isOptionToRenew) && $order->isOptionToRenew == '0' ? 'No': 'Yes ('.$order->optionToRenewMonths.' Months '. $order->optionToRenewYears .' Years)'}}</p>
                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('optionToRenewCondition', 'Term of Renewal', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::select('optionToRenewCondition', 
                                    array('isMarketPrevaillingRate' => 'market prevailing rate',
                            'isIncreaseOf10percent' => 'increase of 10% from the last rental rate',
                            'isIncreaseOf15percent' => 'increase of 15% from the last rental rate',
                            'isMutuallyAgreedRate' => 'at a mutually agreed rate',
                            '' => 'none')
                                    , isset($order->optionToRenewCondition) ? $order->optionToRenewCondition : '', array('class' => 'form-control')) }}
                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('otherTermsofRenewal', 'Other Term of Renewal', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('otherTermsofRenewal', isset($order->otherTermsofRenewal) ? $order->otherTermsofRenewal : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('rental', 'Rental (RM)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12"> 
                        {{ Form::text('rental', isset($order->rental) ? $order->rental : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('advanceRental', 'Advance Rental (RM)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('advanceRental', isset($order->advanceRental) ? $order->advanceRental : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('securityDepositRent', 'Security Deposit Rent (RM)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('securityDepositRent', isset($order->securityDepositRent) ? $order->securityDepositRent : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('securityDepositUtilities', 'Security Deposit Utilities (RM)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('securityDepositUtilities', isset($order->securityDepositUtilities) ? $order->securityDepositUtilities : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}                                </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('otherSecurityDeposit', 'Security Deposit (Others)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php $arr = json_decode($order->otherSecurityDeposit, true); ?>
                        @if(is_array($arr))
                            @foreach($arr as $inner_arr)
                                @if(is_array($inner_arr))
                                    @foreach($inner_arr as $key=>$item)
                                        @if($key == 'amount')
                                        {{ 'RM '.$item }}
                                        @elseif($key == 'name')
                                        {{ '('.ucwords($item) . ')<br/>' }}
                                        @endif  
                                    @endforeach
                                @else
                                    {{ $inner_arr }}
                                @endif  
                            @endforeach
                        @endif      
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('rentalFreePeriod', 'Rental Free Period', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon">Week</span>
                            {{ Form::number('rentalFreePeriodWeeks', isset($order->rentalFreePeriodWeeks) ? $order->rentalFreePeriodWeeks : '', array('class' => 'form-control', 'min'=> '0', 'required' => '')) }}
                            <span class="input-group-addon">Month</span>
                            {{ Form::select('rentalFreePeriodMonths', $month, isset($order->rentalFreePeriodMonths) ? $order->rentalFreePeriodMonths : '', array('class' => 'form-control', 'style' => 'width:70px', 'required' => '')) }}
                            <span class="input-group-addon">Year</span>
                            {{ Form::text('rentalFreePeriodYears', isset($order->rentalFreePeriodYears) ? $order->rentalFreePeriodYears : '', array('class' => 'form-control', 'required' => '')) }}			 
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('rentalFreePeriod_date', 'Rental Free Period (Date)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class='col-md-6' style="padding-left:0">
                           <div class='input-group date' id='rentalFreePeriodStart' >
                               {{ Form::text('rentalFreePeriodStart', isset($order->rentalFreePeriodStart) && $order->rentalFreePeriodStart!='' ? ($order->rentalFreePeriodStart == '0000-00-00' ? date('d/m/Y') : date('d/m/Y', strtotime($order->rentalFreePeriodStart))) : '', array('class' => 'form-control col-md-7 col-xs-12', 'id' => 'rentalFreePeriodStart')) }}
                               <!--<input type='text' class="form-control" placeholder="From" name="rentalFreePeriodStart"/>-->
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='input-group date' id='rentalFreePeriodEnd'>
                                {{ Form::text('rentalFreePeriodEnd', isset($order->rentalFreePeriodEnd) && $order->rentalFreePeriodEnd!='' ? ($order->rentalFreePeriodEnd == '0000-00-00' ? date('d/m/Y') : date('d/m/Y', strtotime($order->rentalFreePeriodEnd))) : '', array('class' => 'form-control col-md-7 col-xs-12', 'id' => 'rentalFreePeriodEnd')) }}
                                <!--<input type='text' class="form-control"  placeholder="To" name="rentalFreePeriodEnd"/>-->
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('carParkLots', 'Car Park Lot', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        
                        <?php $arr = json_decode($order->carParkLots, true); ?>
                        @if(is_array($arr))
                            @foreach($arr as $item)
                                {{ $item . "<br/>" }}
                            @endforeach
                        @endif
                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('paymentMethod', 'Payment Method', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::select('paymentMethod', 
                                    array('isDirectBankIn' => 'Direct Bank-in',
                            'isPostDatedCheque' => 'Post-Dated Cheque',
                            'isCollectionByLandlord' => 'Collective by Landlord',
                            '' => 'none')
                                    , isset($order->paymentMethod) ? $order->paymentMethod : '', array('class' => 'form-control')) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('paymentAccountName', 'Name of Account Holder', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('paymentAccountName', isset($order->paymentAccountName) ? $order->paymentAccountName : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('bank', 'Bank', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('bank', isset($order->bank) ? $order->bank : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}                       
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('bankAccountNum', 'Bank Account Number', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('bankAccountNum', isset($order->bankAccountNum) ? $order->bankAccountNum : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => '')) }}
                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('notificationMethod', 'Method of notification upon payment', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::select('notificationMethod', 
                                    array('isEmail' => 'Email',
                            'isSMS' => 'SMS',
                            'isWhatsapp' => 'Whatsapp',
                            '' => 'none')
                                    , isset($order->notificationMethod) ? $order->notificationMethod : '', array('class' => 'form-control')) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('postDatedMethod', 'Post-Dated Cheque', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::select('postDatedMethod', 
                                    array('is6Months' => '6 Months',
                            'is12Months' => '12 Months',
                            'is24Months' => '24 Months',
                            '' => 'none')
                                    , isset($order->postDatedMethod) ? $order->postDatedMethod : '', array('class' => 'form-control')) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('specialRequest', 'Special Condition(s)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        @if(isset($order->specialRequest))
                        <?php $arr = json_decode($order->specialRequest, true); ?>
                        @if(is_array($arr))
                        <?php $i = 1; ?>
                            @foreach($arr as $inner_arr)
                                @if(is_array($inner_arr))   
                                    <p class="help-block">{{$i.'.'}} {{ $inner_arr['original'] }}</p>
                                    <input type="hidden" id="hidspecialRequest" name="hidspecialRequest[]" value="{{ $inner_arr['original'] }}" />
                                    <div class="input-group">
                                        <div class="input-group-addon">{{$i.'.'}}</div>
                                        {{ Form::textarea ('specialRequest[]', $inner_arr['edited'], array('class' => 'form-control col-md-6 col-xs-4', 'placeholder'=>'Special Condition', 'size' => '30x3')) }}
                                    </div>           
                                    <?php $i++; ?>
                                @else
                                    <p class="help-block">{{ $inner_arr }}</p>
                                @endif  
                            @endforeach
                        @endif 
                        @endif 
<!--                        <div id="divspecialRequest"></div>-->
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('fixturesFitting', 'Fixture & Fitting(s)', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        
                        <?php $arr = json_decode($order->fixturesFitting, true); ?>
                        @if(is_array($arr))
                            @foreach($arr as $item)
                                {{ $item . "<br/>" }}
                            @endforeach
                        @endif
                        
                    </div>
                </div>
                
                <hr>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('tenancyAgreementStatus', 'Status', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block">{{ $order->tenancyAgreementStatus }}</p>
                        
                    </div>
                </div>
                
                <div class="form-group">
                    {{ HTML::decode(Form::label('tenancyAgreement', 'Preview', array('class' => 'col-sm-3 control-label'))) }}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <p class="help-block"><a target="_blank" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR . 'pdf' . DIRECTORY_SEPARATOR .$order->previewDocument }}'>{{ $order->previewDocument }}</a></p>
                        
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-3">
                        @if($order->uploadType == 'E_SIGN')
                            <a class="btn btn-primary" href="{{ route('e_sign.list') }}" type="button">Back</a>
                        @elseif($order->uploadType == 'E_STAMP')
                            <a class="btn btn-primary" href="{{ route('e_stamping.list') }}" type="button">Back</a>
                        @else
                            <a class="btn btn-primary" href="{{ route('order.list') }}" type="button">Back</a>
                        @endif
                        
                        @if($order->tenancyAgreementStatus != 'Rejected')
                            @if(Auth::user()->role->name_en == 'Administrator')
                            <button data-toggle="modal" data-target="#rejectModal" class="btn btn-danger" type="button">Reject</button>
                            @endif
                        @endif
                        @if($order->tenancyAgreementStatus == 'Received')
                            <button id="btn_ready" class="btn btn-success btn-submit" type="button">Approve</button>
                        @endif
                        <button id="btn_save" class="btn btn-success btn-submit" type="button">Save</button>
                    </div>
                </div>
                <input id="orderStatus" type="hidden" name="orderStatus" value="">
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to reject?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btn_reject" type="button" class="btn btn-danger btn-submit">Reject</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/assets/js/fileinput.min.js"></script>
{{ HTML::script('/assets/js/moment.js') }}
{{ HTML::script('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}
{{ HTML::style('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}
{{ HTML::script('/assets/js/lightbox/lightbox.js') }}
{{ HTML::style('/assets/js/lightbox/lightbox.css') }}
<script>
    $(document).ready(function () {
        $('.btn-submit').on('click', function(){
            var id = $(this).attr('id');
        
            if(id == 'btn_save')
            {
                $('#orderStatus').val('Update');
            }
            else if(id=='btn_ready')
            {
                $('#orderStatus').val('Ready');
            }
            else if(id == 'btn_reject')
            {
                $('#orderStatus').val('Rejected');
            }
            $('#orderForm').submit();
        });
        
        $('#commencementDate').datetimepicker({
            format: "DD/MM/YYYY"
        });
        $('#rentalFreePeriodStart').datetimepicker({
            format: "DD/MM/YYYY"
        });
        $('#rentalFreePeriodEnd').datetimepicker({
            format: "DD/MM/YYYY",
            useCurrent: false 
        });
        $("#rentalFreePeriodStart").on("dp.change", function (e) {
            $('#rentalFreePeriodEnd').data("DateTimePicker").minDate(e.date);
        });
        $("#rentalFreePeriodEnd").on("dp.change", function (e) {
            $('#rentalFreePeriodStart').data("DateTimePicker").maxDate(e.date);
        });
    
    });
    
    function addspecialRequest() {
        var eleCount = $("input[name*='specialRequest']").length;
        var newIndex = eleCount + 1;
        var str = '<div class="form-group form-inline"><div><label class="control-label col-md-1 col-xs-1" for="appendprepend">' + newIndex + '.</label>';
        str += '<div class="col-md-5 col-xs-9">';
        str += '<input type="text" name="specialRequest[]" class="form-control col-md-6 col-xs-4" placeholder="Special Condition"></div>';
        str += '<a href="javascript:addspecialRequest(' + newIndex + ')"><i class="glyphicon glyphicon-plus plusicon"></i></a></div></div>';
        $('#divspecialRequest').append(str);
    }

</script>
