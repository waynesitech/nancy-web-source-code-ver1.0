<div class="page-title">
    <div class="title_left">
      <h3>
          @if(Route::currentRouteName() == 'order.list')
            Tenancy Agreement
          @elseif(Route::currentRouteName() == 'e_sign.list')
            E-Sign
          @elseif(Route::currentRouteName() == 'e_stamping.list')
            E-Stamping
          @elseif(Route::currentRouteName() == 'e_storage.list')
            E-Storage
          @endif
      </h3>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Listing</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Order No.</th>
                <th>Tenant</th>
                <th>Land Lord</th>
                <th>Status</th>
                <th>Date created</th>
                <th>Uploaded Date</th>                
              </tr>
            </thead>


            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td><a href='{{ route('order.edit',['id' => $order->id]) }}'>{{ isset($order->order_num) && $order->order_num !='' ? $order->order_num : '-' }}</a></td>
                    <td class="td-restrict-150">{{ $order->tenant }}</td>
                    <td class="td-restrict-150">{{ $order->landlord }}</td>
                    <td style="color:<?php echo isset($order->tenancyAgreementStatus) ? ($order->tenancyAgreementStatus == 'Ready' ? '#5c76b8' : ($order->tenancyAgreementStatus == 'Done' ? '#5cb85c' : ($order->tenancyAgreementStatus == 'Expired' ? '#d9534f' : '#feb836'))) : '#feb836'; ?>;">{{ $order->tenancyAgreementStatus }}</td>
                    <td>{{ date('d/m/Y H:i:s', strtotime($order->created_on)) }}</td>
                    <td>{{ (strtotime($order->upload_date) != 0) ? date('d/m/Y H:i:s', strtotime($order->upload_date)) : '' }}</td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<!-- Datatables -->
<script>
  $(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#datatable-buttons").length) {
        $("#datatable-buttons").DataTable({
            "order": [[ 4, "desc" ]],
          dom: "Bfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();



    TableManageButtons.init();
  });
</script>
<!-- /Datatables -->
