<div class="page-heading clearfix">
	<h1 class="heading-title">Banners
		<a class="btn btn-default pull-right" href="{{ route('banner.csv') }}">Export CSV</a>
		<a class="btn btn-primary pull-right" href="{{ route('banner.add') }}">Add New</a>
		<form class="form-inline pull-right" role="form">
		    <div class="form-group">
		      <div class="input-group">
		        {{ Form::text('keyword', $keyword, array('class' => 'form-control')) }}
		        <span class="input-group-btn">
		          {{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-default')) }}
		        </span>      
		      </div>
		    </div>
	  	</form>
	</h1>
</div>

@if(Session::has('alert.success'))
	<div class="alert alert-dismissable alert-success">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {{ Session::get('alert.success') }}
	</div>
@endif
@if(Session::has('alert.danger'))
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert" aaria-hidden="true">&times;</button>
	  {{ Session::get('alert.danger') }}
	</div>
@endif

<div class="page-body">
	<table class="table table-hover table-sort">
		<thead>
			<tr>
				<th class="sorting">
					<a href="{{ route('banner.list', array('sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'name', 'keyword' => Input::get('keyword'))) }}">Name</a>
					@if($col == 'name')
						@if($sort == 'asc')
							<i class="fa fa-sort-up"></i>
						@else
							<i class="fa fa-sort-up"></i>
						@endif
					@else
						<i class="fa fa-sort"></i>
					@endif
				</th>
				<th class="sorting" width="250">
					<a href="{{ route('banner.list', array('sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'type', 'keyword' => Input::get('keyword'))) }}">Type</a>
					@if($col == 'type')
						@if($sort == 'asc')
							<i class="fa fa-sort-up"></i>
						@else
							<i class="fa fa-sort-up"></i>
						@endif
					@else
						<i class="fa fa-sort"></i>
					@endif
				</th>
				<th class="sorting" width="140">
					<a href="{{ route('banner.list', array('sort' => ($sort == 'asc'?'desc':'asc'), 'col' => 'created_on', 'keyword' => Input::get('keyword'))) }}">Date Created</a>
					@if($col == 'created_on')
						@if($sort == 'asc')
							<i class="fa fa-sort-up"></i>
						@else
							<i class="fa fa-sort-up"></i>
						@endif
					@else
						<i class="fa fa-sort"></i>
					@endif
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach($banners as $banner)
			<tr>
				<td><a href="{{ route('banner.edit', array('id' => $banner->id)) }}">{{ $banner->name }}</a></td>
					<td>{{ $banner->type }}</td>
				<td>{{ date('d/m/Y', strtotime($banner->created_on)) }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>
