<div class="page-heading clearfix">
	<h1 class="heading-title">{{ Route::currentRouteName() == 'banner.add' ? 'New' : 'Edit' }} Banner</h1>
</div>

<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		@if(Session::has('alert.success'))
			<div class="alert alert-dismissable alert-success">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  {{ Session::get('alert.success') }}
			</div>
		@endif          
		@if(Session::has('alert.danger'))
			<div class="alert alert-dismissable alert-danger">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  {{ Session::get('alert.danger') }}
			</div>
		@endif          
		@if($errors->count() > 0)
			<div class="alert alert-dismissable alert-danger">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <ul>
			    @foreach($errors->getMessages() as $msg)
			    	<li>{{ $msg[0] }}</li>
			    @endforeach
			  </ul>
			</div>
		@endif
	</div>
</div>


@if(empty($banner->id))
{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
@else
{{ Form::open(array('method' => 'post', 'url' => '#', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
@endif
	<div class="form-group">
		{{ HTML::decode(Form::label('name', 'Name <em>*</em>', array('class' => 'col-sm-3 control-label'))) }}
	    <div class="col-sm-6">
	     	{{ Form::text('name', isset($banner->name) ? $banner->name : '', array('class' => 'form-control')) }}
	    </div>
	</div>

	<div class="form-group">
		{{ HTML::decode(Form::label('image', 'Image', array('class' => 'col-sm-3 control-label'))) }}
	    <div class="col-sm-6 form-upload">
	    	<div class="img-thumbnail" {{ empty($banner->image) ? 'style="display: none;"' : 'style="background-image: url(' .$banner->image. ');"' }}></div>
	    	<a href="#" class="btn btn-default">
	    		Select
	     		{{ Form::file('image', array('class' => 'form-control', 'accept' => 'image/*')) }}
	     	</a>
	     	<p class="help-block">640x480 JPG or PNG.</p>
	    </div>
	</div>

	<div class="form-group">
		{{ HTML::decode(Form::label('url', 'URL <em>*</em>', array('class' => 'col-sm-3 control-label'))) }}
	    <div class="col-sm-6">
	     	{{ Form::text('url', isset($banner->url) ? $banner->url : '', array('class' => 'form-control')) }}
	    </div>
	</div>

	<div class="form-group">
		{{ HTML::decode(Form::label('type', 'Type <em>*</em>', array('class' => 'col-sm-3 control-label'))) }}
		<div class="col-sm-3">
			
			{{ Form::select('type', $types, isset($banner->type) ? $banner->type : '', array('class' => 'form-control')) }}
			
		</div>
	</div>

	<div class="form-group">
	{{ Form::label('status', 'Status', array('class' => 'col-sm-3 control-label')) }}
		<div class="col-sm-6">
			<div class="radio">
				<label>
					{{ Form::radio('status', '1', isset($banner->status) ? $banner->status : 1 ) }}
					Active
				</label>
			</div>
			<div class="radio">
				<label>
					{{ Form::radio('status', '0', isset($banner->status) ? !$banner->status : 0 ) }}
					Inactive
				</label>
			</div>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
		@if(!empty($banner->id))
		<button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" type="button">Delete</button>
		@else
		<button class="btn btn-primary" type="submit">Save</button>
		@endif			
	</div>
</div>
{{ Form::close() }}

@if(!empty($banner->id))
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete?</p>
			</div>
			<div class="modal-footer">
				{{ Form::open(array('method' => 'post', 'route' => array('banner.delete', $banner->id))) }}
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-danger">Delete</button>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endif




















