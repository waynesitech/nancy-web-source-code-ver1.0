<!DOCTYPE html>
<html>
    <head>
        <title>Payment...</title>
    </head>
    <body>
        <h1>Processing Payment...</h1>
        {{ Form::open(array('id' => 'frm-submit', 'method' => 'post', 'url' => $ipay88_url)) }}
            <p>If this page does not redirect to payment gateway in seconds, please click RETRY button below</p>
            {{ Form::button('RETRY', array(
                'type' => 'Submit',
            )) }}
            @foreach ($data as $key => $param)
                {{ Form::hidden($key, $param) }}
            @endforeach
        {{ Form::close() }}
        <script type="text/javascript">
            setTimeout(function() {
                document.getElementById('frm-submit').submit();
            }, 100);
        </script>
    </body>
</html>
