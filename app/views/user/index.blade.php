<div class="page-title">
    <div class="title_left">
      <h3>Users</h3>
    </div>
    
    <div class="title_right">
        <div class="col-md-offset-5 col-sm-offset-5 form-group pull-right top_search">
          <div class="input-group">
            <a class="btn btn-primary" href="{{ route('user.add') }}">Add New</a>
          </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Listing</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Role</th>
                <th>Name in NRIC</th>
                <th>E-mail</th>
                <th>Status</th>
                <th>Date created</th>
                <th>Phone Verification</th>
              </tr>
            </thead>


            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ !isset($user->role)?$user->role->name_en: '' }}</td>
                    <td><a href="{{ route('user.edit', array( 'id' => $user->id )) }}">{{ $user->name }}</a></td>
                    <td>{{($user->email)}}</td>
                    <td>{{($user->status == 1 ? 'Active' : 'Inactive')}}</td>
                    <td>{{ date('d/m/Y', strtotime($user->created_on)) }}</td>
                    <td>
                        @if($user->isPhoneVerify == 'true')
                        <i class="fa fa-check text-primary" aria-hidden="true"></i>
                        @else
                            @if($user->facebook_id != '' || !empty($user->facebook_id))
                                    Facebook Login
                            @else
                                    <i class="fa fa-times text-danger" aria-hidden="true"></i>
                            @endif
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<!-- Datatables -->
<script>
  $(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#datatable-buttons").length) {
        $("#datatable-buttons").DataTable({
          dom: "Bfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();



    TableManageButtons.init();
  });
</script>
<!-- /Datatables -->
