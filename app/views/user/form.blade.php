<div class="page-title">
    <div class="title_left">
      <h3>{{ Route::currentRouteName() == 'user.add' ? 'New' : 'Edit' }}  User</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content">
            @include('msg')
          <br />
          {{ Form::open(array('method' => 'post', 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data', 'data-parsley-validate')) }}
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="NRIC_Name">Name in NRIC <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('name', isset($user->name) ? $user->name : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="NRIC">NRIC <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('ic_number', isset($user->ic_number) ? $user->ic_number : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('email', isset($user->email) ? $user->email : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile">Mobile <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::text('mobile', isset($user->mobile) ? $user->mobile : '', array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Role <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                @if(Auth::user() && Auth::user()->role->name_en == 'Administrator')
                    {{ Form::select('role', $roles, isset($user->role_id) ? $user->role_id : '', array('class' => 'form-control', 'required' => 'required', 'id' => 'role')) }}
                @else
                    {{ Form::select('role', $roles, isset($user->role_id) ? $user->role_id : '', array('class' => 'form-control', 'disabled')) }}
                @endif
                {{ Form::hidden('role_id', isset($user->role_id) ? $user->role_id : '',  array('id' => 'role_id')) }}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="radio">
			<label>
				{{ Form::radio('status', '1', isset($user->status) ? $user->status : 1 ) }}
				Active
			</label>
		</div>
		<div class="radio">
			<label>
				{{ Form::radio('status', '0', isset($user->status) ? !$user->status : 0 ) }}
				Inactive
			</label>
		</div>
              </div>
            </div>
            <div class="ln_solid"></div>
            
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password {{ !isset($user->password) ? '<span class="required">*</span>' : '' }}
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::password('password', array('class' => 'form-control col-md-7 col-xs-12')) }}
                <p class="help-block">Password must contains at least 6 values</p>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confirm_password">Confirm Password {{ !isset($user->password) ? '<span class="required">*</span>' : '' }}
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                {{ Form::password('confirm_password', array('class' => 'form-control col-md-7 col-xs-12')) }}
              </div>
            </div>
            
          
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-success" type="submit">Save</button>
		@if(!empty($user->id))
		<button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" type="button">Delete</button>
		@endif
              </div>
            </div>

          {{ Form::close() }}
        </div>
      </div>
    </div>
</div>


@if(!empty($user->id))
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete?</p>
			</div>
			<div class="modal-footer">
				{{ Form::open(array('method' => 'post', 'route' => array('user.delete', $user->id))) }}
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-danger">Delete</button>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	@endif
</div>

<script>
$(document).ready(function(){
    $('#role').on('change', function() {
            $('#role_id').val(this.value);
    });
});
</script>