

<div class="page-heading clearfix">
	<h1 class="heading-title">Dashboard</h1>
</div>

@if(Session::has('alert.success'))
<div class="alert alert-dismissable alert-success">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	{{ Session::get('alert.success') }}
</div>
@endif          
@if(Session::has('alert.danger'))
<div class="alert alert-dismissable alert-danger">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	{{ Session::get('alert.danger') }}
</div>
@endif

<div class="page-body">
    <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $total_user }}</div>
                            <div>Total Active User</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="panel panel-purple">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $total_new_user }}</div>
                            <div>New User for {{ date('F') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    	<div class="col-lg-3 col-md-6">
          <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                        	<div class="huge">{{ $total_order }}</div>
                            <div>Total Order</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                        	<div class="huge">{{ $total_pending_order }}</div>
                            <div>Total New Order</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="height:55px;">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Order Analytic Chart
                    
                    <form class="form-inline pull-right" role="form">
	        <div class="form-group">
	            <div class="input-group">
	                <div class="input-prepend input-group">
	                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                    {{ Form::text('daterange', $daterange, array('class' => 'form-control', 'id' => 'daterange', 'style' => 'width: 200px')) }}
                            <span class="input-group-btn">
						{{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-default')) }}
					</span>
                        </div>
	            </div>
	        </div>		
		</form>
                </div>
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="morrisbar"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="height:55px;">
                    <i class="fa fa-bar-chart-o fa-fw"></i> User Analytic Chart
                    
                    <form class="form-inline pull-right" role="form">
	        <div class="form-group">
	            <div class="input-group">
	                <div class="input-prepend input-group">
	                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                    {{ Form::text('daterange_user', $daterange_user, array('class' => 'form-control', 'id' => 'daterange_user', 'style' => 'width: 200px')) }}
                            <span class="input-group-btn">
                                    {{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-default')) }}
                            </span>
                        </div>
	            </div>
	        </div>		
		</form>
                </div>
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="morrisbarUser"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    
</div>

{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
{{ HTML::script('https://www.google.com/jsapi') }}
<style>
div[dir=ltr] {
    margin:auto;
}
</style>
<script>
jQuery(document).ready(function() {
	Morris.Area({
        element: 'morrisbar',
        data: <?php echo json_encode($data); ?>,
        xkey: 'day',
        ykeys: ['total'],
        labels: ['Total'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
    
        Morris.Area({
        element: 'morrisbarUser',
        data: <?php echo json_encode($data_user); ?>,
        xkey: 'day',
        ykeys: ['total'],
        labels: ['Total'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
});

$(function() {
$('#daterange').daterangepicker({
    format: 'DD/MM/YYYY',
    opens: "left"
});
$('#daterange_user').daterangepicker({
    format: 'DD/MM/YYYY',
    opens: "left"
});
});
</script>
 <!-- Morris Charts JavaScript -->
{{ HTML::script('/assets/bower_components/raphael/raphael-min.js') }}
{{ HTML::script('/assets/bower_components/morrisjs/morris.min.js') }}
{{ HTML::script('/assets/js/morris-data.js') }}