<section class="form1">
    
    <div class="colorlogo">
        <img src="<?php echo URL::asset('assets/images/nancylogocolor.png'); ?>" class="colorlogo"/>
    </div>
        
    <div class="form container">
        <h3 class="small-title">Sign In</h3>
            
        {{ Form::open(array('method' => 'post', 'enctype' => 'multipart/form-data')) }}        
        <div class="row">
            <label>
                <img src="<?php echo URL::asset('assets/images/USERICON.png'); ?>" class="img-fluid iconsize" />
            </label>
            {{ Form::text('email', Input::old('email'), array('class' => 'inputrow col', 'placeholder' => 'Email')) }}
        </div>
            
            
        <div class="row">
            <label>
                <img src="<?php echo URL::asset('assets/images/pwicon.png'); ?>" class="img-fluid iconsize" />  	   </label>
           {{ Form::password('password', array('class' => 'inputrow col', 'placeholder' => 'Password')) }}
        </div>
            
        <br/>
            
        <div class="send">
            <button type='submit'>Sign In</button>
        </div>
        <div class="send">
            <button type='button' onclick="location.href='{{ URL::route('user.esign') }}'">E-Sign Via One-Time-Password</button>
        </div>
            
        <p class="message">Don’t have an account? <a href="">Sign Up</a> now! <br/>
            <a href="{{ URL::route('user.forget') }}">Forgot Password?</a> 
        </p>
        {{ Form::close() }}	
    </div>
        
</section>