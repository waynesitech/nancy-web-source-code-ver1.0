<p>Hi {{ $name }} ,</p>

<p>Thank you for signing up for Lesys Tenancy.</p>
<p>To complete and confirm your registration for Lesys Tenancy, you’ll need to verify your Phone number. To do so, please insert code <b>{{$verificationCode}}</b> into your mobile App</p>
<p>Regards,<br>Lesys Tenancy Team<p>
