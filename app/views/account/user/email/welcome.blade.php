<h3>EMAIL VERIFICATION</h3>

<p>Thank you for signing up for Nancy.</p>
<p>To complete and confirm your registration for NanCy, you’ll need to verify your email address. To do so, please click the link below:</p>

<p>{{ $link }}</p>

<p>Regards,<br>NanCy Team<p>