				<div class="panel panel-default panel-sidebar">
					<div class="panel-heading panel-avatar">
						<div id="img-avatar" class="img-responsive" style="background-image: url('{{ empty($user->image) ? URL::asset('assets/img/featured_placeholder.png') : URL::asset('files/users/'.$user->id.'/'.$user->image) }}');"></div>
					</div>
					<div class="panel-body">
						<h3 class="user-name">{{ $user->first_name }} {{ $user->last_name }}</h3>
						<div id="account-ratings" class="user-ratings" data-score="{{ $user->getAverageRating() }}"></div>
						<ul>
							<li><a href="{{ URL::route('user.me') }}">Edit Profile</a></li>
							<li><a href="{{ URL::route('user.logout') }}">Log out</a></li>
						</ul>
					</div>
				</div>
