<section class="form1">
    
    <div class="colorlogo">
        <img src="<?php echo URL::asset('assets/images/nancylogocolor.png'); ?>" class="colorlogo"/>
    </div>
        
    <div class="form container">
        <h3 class="small-title">Congratulation</h3>
        <p class="message">You're now a NanCy's user.</p>
        <div class="send">
            <button><a href='{{URL::route('home')}}'>Done!</a></button>
        </div>
    </div>
</div>
    
</section>
