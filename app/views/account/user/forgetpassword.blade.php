<section class="form1">
    
    <div class="colorlogo">
        <img src="<?php echo URL::asset('assets/images/nancylogocolor.png'); ?>" class="colorlogo"/>
    </div>
        
    <div class="form container">
        <h3 class="small-title">Forgot Password?</h3>
            
        {{ Form::open(array('method' => 'post', 'enctype' => 'multipart/form-data')) }}
            <p class="forgot-txt"> No worries.<br/>Just enter your email to reset your password. </p>
                
                
            <div class="row">
                <label>
                    <img src="<?php echo URL::asset('assets/images/emailicon.png'); ?>" class="img-fluid iconsize" />
                </label>
                {{ Form::text('email', Input::old('email'), array('class' => 'inputrow col', 'placeholder' => 'Email Address')) }}
            </div>
            <br/>
                
            <div class="send">
                <button type="submit" >Send</button>
            </div>
        {{ Form::close() }}	
    </div>
</div>
    
</section>