@section('title', 'Profile')

<div class="col-md-10">
    <div class="row">
        <div class="col-md-11">

            <div class="content-box-large">
                <div class="panel-heading">
                    <h2>Profile</h2>
                </div>

                <div class="panel-body">
                    {{ Form::open(array('method' => 'post', 'class' => '', 'enctype' => 'multipart/form-data')) }}
                        <fieldset >
                            <!-- userName -->							
                            <div class="form-group col-md-10">
                                <label class="col-md-2 col-xs-12 control-label"><h6>Name</h6></label>  
                                <div class=" col-md-6 col-xs-12">
                                    <div class="">
                                        {{ Form::text('name', $user->name, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <!-- email -->							
                            <div class="form-group col-md-10">
                                <label class="col-md-2 col-xs-12 control-label"><h6>Email</h6></label>  
                                <div class=" col-md-6 col-xs-12">
                                    <div class="">
                                        {{ Form::email('email', $user->email, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>		
                            <!-- Contact -->							
                            <div class="form-group col-md-10">
                                <label class="col-md-2 col-xs-12 control-label"><h6>Contact</h6></label>  
                                <div class=" col-md-6 col-xs-12">
                                    <div class="">
                                        {{ Form::text('mobile', $user->mobile, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <!-- Password1 -->							
                            <div class="form-group col-md-10">
                                <label class="col-md-2 col-xs-12 control-label"><h6>Password</h6></label>  
                                <div class=" col-md-6 col-xs-12">
                                    <div class="">
                                        {{ Form::password('new_password', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                            <!-- Password2 -->							
                            <div class="form-group col-md-10">
                                <label class="col-md-2 col-xs-12 control-label"><h6>Re-enter Password</h6></label>  
                                <div class=" col-md-6 col-xs-12">
                                    <div class="">
                                        {{ Form::password('retype_new_password', array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                    <div class="form-group col-md-6 ">
                        <div class="btn-group col-xs-3" >
                            <button type="submit" class="btn btn-default btn_best btn_spaceright" aria-expanded="false">
                                SAVE
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}		
                </div>

            </div>
            <div class="fake"" style="margin-bottom: 70px"></div>"
        </div>
    </div>

</div>