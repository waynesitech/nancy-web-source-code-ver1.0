<section class="form1">
    
    <div class="colorlogo">
        <img src="<?php echo URL::asset('assets/images/nancylogocolor.png'); ?>" class="colorlogo"/>
    </div>
        
    <div class="form container">
        <h3 class="small-title">Sign Up</h3>
        {{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}       
            <div class="row">
                <label>
                    <img src="<?php echo URL::asset('assets/images/USERICON.png'); ?>" class="img-fluid iconsize" />
                </label>
                {{ Form::text('name', Input::old('name'), array('class' => 'inputrow col', 'placeholder' => 'Name')) }}
            </div>
                
            <div class="row">
                <label>
                    <img src="<?php echo URL::asset('assets/images/emailicon.png'); ?>" class="img-fluid iconsize" />
                </label>
                {{ Form::text('email', Input::old('email'), array('class' => 'inputrow col', 'placeholder' => 'Email Address')) }}
            </div>
                
            <div class="row">
                <label>
                    <img src="<?php echo URL::asset('assets/images/mobileicon.png'); ?>" class="img-fluid iconsize" />  		   				
                </label>
                {{ Form::text('mobile', Input::old('mobile'), array('class' => 'inputrow col', 'placeholder' => 'Mobile Number')) }}
            </div>
                
            <div class="row">
                <label>
                    <img src="<?php echo URL::asset('assets/images/pwicon.png'); ?>" class="img-fluid iconsize" />  		   				
                </label>
                {{ Form::password('password', array('class' => 'inputrow col', 'placeholder' => 'Password')) }}
            </div>
                
            <div class="row">
                <label>
                    <img src="<?php echo URL::asset('assets/images/pwicon.png'); ?>" class="img-fluid iconsize"/>   			   		   				
                </label>
                {{ Form::password('confirm_password', array('class' => 'inputrow col', 'placeholder' => 'Re-enter Password')) }}
            </div>
                
            <br/>
                
            <div class="row col">
                {{ Form::checkbox('agree', 'Agree', false,array('id'=>'agree', 'class'=>'col')) }}
                <p class="col-11 read">I have read, agree and understand to the NanCyʼs <a href="#"><i>Terms and Services</i></a>.</p>
            </div>
                
            <br/>
                
            <div class="send">
                <button type='submit'>Sign Up</button>
            </div>
                
            <p class="message">Have an account? <a href="{{ URL::route('user.login') }}">Log In Now</a>!</p>
        {{ Form::close() }}			
    </div>
</div>
    
</section>