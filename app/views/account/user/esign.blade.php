<section class="form1">
    
    <div class="colorlogo">
        <img src="<?php echo URL::asset('assets/images/nancylogocolor.png'); ?>" class="colorlogo"/>
    </div>
        
    <div class="form container">
        <h3 class="small-title">E Sign</h3>
            
        {{ Form::open(array('method' => 'post', 'enctype' => 'multipart/form-data')) }}  
        <p class="forgot-txt"> One Time Passcode (OTP)</p>
        <div class="row">
            {{ Form::password('otp1', array('class' => 'otpbox col', 'maxlength' => '1')) }}
            {{ Form::password('otp2', array('class' => 'otpbox col', 'maxlength' => '1')) }}
            {{ Form::password('otp3', array('class' => 'otpbox col', 'maxlength' => '1')) }}
            {{ Form::password('otp4', array('class' => 'otpbox col', 'maxlength' => '1')) }}
        </div>
            
        <p class="forgot-txt"> Last 4 Digits of Your I/C or Passport No.</p>
        <div class="row">
            {{ Form::password('ic1', array('class' => 'otpbox col', 'maxlength' => '1')) }}
            {{ Form::password('ic2', array('class' => 'otpbox col', 'maxlength' => '1')) }}
            {{ Form::password('ic3', array('class' => 'otpbox col', 'maxlength' => '1')) }}
            {{ Form::password('ic4', array('class' => 'otpbox col', 'maxlength' => '1')) }}
        </div>
            
        <br/>
            
        <div class="send">
            <button type='submit'>Verify</button>
        </div>
        {{ Form::close() }}	
    </div>
        
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(function() {
        $("input").keyup(function () {
            if (this.value.length == 1) {
              $(this).next('input').focus();
            }
        });
    });
    </script>