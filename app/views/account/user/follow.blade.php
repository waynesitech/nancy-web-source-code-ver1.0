

<div class="page page-user">
	<div class="container">

		<div class="row">
			<div class="col-md-3">
				@include('account.user.profile')
			</div>
			<div class="col-md-9">

				<div class="panel panel-default">
					<div class="panel-heading"><h2 class="panel-title">Follow</h2></div>
					<div class="panel-body">
						@include('msg')
						@if(count($followers))
						@foreach($followers as $follower)
						<article class="clearfix">
							@if(Auth::user()->id == $follower->follower->id)
							{{ HTML::image(empty($follower->user->image) ? URL::asset('assets/img/featured_placeholder.png') : URL::asset($image_path . '/' . $follower->user->id . '/' . $follower->user->image)) }}
							@else
							{{ HTML::image(empty($follower->follower->image) ? URL::asset('assets/img/featured_placeholder.png') : URL::asset($image_path . '/' . $follower->follower->id . '/' . $follower->follower->image)) }}
							@endif
							<div class="pull-left">
								@if(Auth::user()->id == $follower->follower->id)
								<h4>{{ $follower->user->first_name . ' ' .$follower->user->last_name }}</h4>
								@else
								<h4>{{ $follower->follower->first_name . ' ' .$follower->follower->last_name }}</h4>
								@endif
								<p class="additional-info"><i class="fa fa-clock-o"></i> {{ date('d M Y', strtotime($follower->created_on)) }}</p>
                                @if ($follower->status == 0)
                                    <span class="label label-danger">Declined</span>
                                @elseif ($follower->status == 1)
                                    <span class="label label-success">Accepted</span>
                                @else
                                    <span class="label label-default">Pending</span>
                                @endif
							</div>
							<div class="pull-right">
								
								<div class="icons">
									@if ($follower->user_id == Auth::user()->id)

										@if ($follower->status == 0)
											{{ Form::open(array('method' => 'post', 'url' => route("follow.archive", array("id" => $follower->id)),'class' => 'form-inline')) }}
											<button class="btn-archive" type="submit"></button>									
											{{ Form::close() }}
										@elseif ($follower->status == 1)
											{{ Form::open(array('method' => 'post', 'url' => route("follow.decline", array("id" => $follower->follower->id)),'class' => 'form-inline', 'enctype' => 'multipart/form-data')) }}
											<button class="btn-decline" type="submit"></button>
											{{ Form::close() }}	
											{{ Form::open(array('method' => 'post', 'url' => route("follow.archive", array("id" => $follower->id)),'class' => 'form-inline')) }}
											<button class="btn-archive" type="submit"></button>									
											{{ Form::close() }}	
										@else
											{{ Form::open(array('method' => 'post', 'url' => route("follow.accept", array("id" => $follower->follower->id)),'class' => 'form-inline', 'enctype' => 'multipart/form-data')) }}
											<button class="btn-accept" type="submit"></button>									
											{{ Form::close() }}
											{{ Form::open(array('method' => 'post', 'url' => route("follow.decline", array("id" => $follower->follower->id)),'class' => 'form-inline', 'enctype' => 'multipart/form-data')) }}
											<button class="btn-decline" type="submit"></button>
											{{ Form::close() }}	
										@endif
									@else
										@if ($follower->status == 0)
											{{ Form::open(array('method' => 'post', 'url' => route("follow.archive", array("id" => $follower->id)),'class' => 'form-inline')) }}
											<button class="btn-archive" type="submit"></button>									
											{{ Form::close() }}
										@elseif ($follower->status == 1)
											{{ Form::open(array('method' => 'post', 'url' => route("unfollow.post", array("id" => $follower->user_id)),'class' => 'form-inline', 'enctype' => 'multipart/form-data')) }}
											<button class="btn-decline" type="submit"></button>
											{{ Form::close() }}	
											{{ Form::open(array('method' => 'post', 'url' => route("follow.archive", array("id" => $follower->id)),'class' => 'form-inline')) }}
											<button class="btn-archive" type="submit"></button>									
											{{ Form::close() }}
										@else
											{{ Form::open(array('method' => 'post', 'url' => route("unfollow.post", array("id" => $follower->user_id)),'class' => 'form-inline', 'enctype' => 'multipart/form-data')) }}
											<button class="btn-decline" type="submit"></button>
											{{ Form::close() }}
										@endif
									@endif

								</div>							
							</div>
						</article>	
						@endforeach	
						<nav class="pull-right">
							<ul class="pagination">
								<?php echo $followers->links(); ?>
							</ul>
						</nav>
						@endif													
					</div>
				</div>

			</div>
		</div>

	</div>
</div>
