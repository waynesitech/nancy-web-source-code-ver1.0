<style>
body .btn
{
    color: #333;
    border: 1px solid #adadad;
    border-radius: 4px;
    box-shadow: none;
    background-color: #e6e6e6;
    min-width: 0;
    padding: 10px 12px;
    font-size: 14px;
    font-weight: 400;
    margin-left:0;
}

body .btn:active,
body .btn:focus,
body .btn:hover {
    color: #333;
    background-color: #e6e6e6;
}

body .icon {
    opacity: 1;
    text-indent: 0;
    height: 0;
    width: 0;
}
</style>
<div class="page page-user">
        <div class="container">
            <div class="row">
                @include('msg')
                <div class="col-md-3">
                        @include('account.user.profile')
                </div>
                <div class="col-md-9 user_statistics" align='center'>
                    <div class="panel panel-default panel-gallery">
                        <div class="panel-heading" style="height:55px;">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Total Visitor

                            <form class="form-inline pull-right" role="form">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-prepend input-group">
                                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                            {{ Form::text('daterange', $daterange, array('class' => 'form-control', 'id' => 'daterange', 'style' => 'width: 200px')) }}
                                            <span class="input-group-btn">
                                                    {{ Form::button('Search', array('type' => 'submit', 'class' => 'btn btn-default')) }}
                                            </span>
                                        </div>
                                    </div>
                                </div>		
                            </form>
                        </div>
                
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="padding:0 0 20px;">
                            <div id="linegraph" style="height: 500px;"></div>
                        </div>
                        
                        <div class="panel-heading" style="height:55px;">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Total Visitor by Country

                            
                        </div>
                
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="padding:0 0 20px;">
                            <div id="piechart" style="height: 500px;"></div>
                        </div>
                    </div>
                </div>    
        </div>
    </div>
</div>
{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
{{ HTML::script('https://www.google.com/jsapi') }}

<script>
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

	var data = new google.visualization.DataTable(<?php echo json_encode($data); ?>);
	
        var options = {
	  title: 'Visitors',
          hAxis: {
          title: 'Date'
            }
	};
	
        var chart = new google.visualization.LineChart(document.getElementById('linegraph'));
	
	chart.draw(data, options);
}

google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart2);
function drawChart2() {

	var data = new google.visualization.DataTable(<?php echo json_encode($data_country); ?>);
	
        var options = {
	  title: 'Total Visitor'
	};
	
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	
	chart.draw(data, options);
}

$(window).resize(function(){
  drawChart();
  drawChart2();
});
</script>