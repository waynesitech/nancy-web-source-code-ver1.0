<style>
    .send button{
        width: 45%;
        margin-right: 5px;
    }
</style>
<section class="form1">
    
    <div class="colorlogo">
        <img src="<?php echo URL::asset('assets/images/nancylogocolor.png'); ?>" class="colorlogo"/>
    </div>
        
    <div class="form container">
        <h3 class="small-title">Verification</h3>
        {{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
            <input type="hidden" name="id" value="{{ $user_id }}" />
            <p class="forgot-txt"> NanCy had sent a Verification code to {{ $mobile }}. </p>
            <p class="forgot-txt">Please enter the verification code.</p>
            <div class="row">
                <label>
                    <img src="<?php echo URL::asset('assets/images/pwicon.png'); ?>" class="img-fluid iconsize" />
                </label>
                {{ Form::password('verificationCode', array('class' => 'inputrow col', 'placeholder' => 'Verification Code')) }}
            </div>
                
            <br/>
                
            <div class="row send">
                <button type="button" onclick="resend({{$user_id}})">Resend</button>
                <button type='submit'>Next</button>
            </div>
                            
            <p class="message">If you do not receive your Verification Code within 60 seconds, please click resend to request for a new one.</p>
            <p id="code_msg" style="display:none; color:red"></p>
        {{ Form::close() }}
    </div>
</div>
    
</section>

<script>
function resend(id)
{
    $.ajax({
        type: "POST",
        url : '{{ route("resend.code") }}',
        dataType: 'json',
        timeout: 10000,
        data: {userID: id},
        success:function(data){
            $('#code_msg').text(data.meta.msg.body);
            $('#code_msg').css('display', 'block');
        },
        error: function(xhr, stt, err){
            $('#code_msg').css('display', 'block');
            if(stt == 'timeout')
            {
                $('#code_msg').text('Timeout. Please try later.');
            }
            else
            {
                $('#code_msg').text(xhr.responseJSON.meta.msg.body);
            }
        }
    });
};
</script>