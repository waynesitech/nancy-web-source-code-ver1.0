<div class="page page-user">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default panel-gallery">
                    <div class="panel-heading" style='text-align: center;'>
                        <h2 style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;'>
                            <strong>User's Review</strong>
                        </h2>
                    </div>
                    <div class="panel-default" style='overflow: hidden;position:relative;width:100%'>
                        <div style='max-height: 800px;overflow-y:auto;border:1px solid rgba(205, 201, 201, 0.5);'>
                        @foreach($reviews as $review)
                        <div style='margin: 5px 5px; padding:5px 5px;overflow:hidden;border:1px solid #ccc;'>
                            <div class="img-profile" align='center' style='height:120px;width:120px;float: left;margin-right: 10px;'>
                                @if(empty($review->member->image))
                                    {{ HTML::image(URL::asset('assets/img/featured_placeholder.png'), null, array('style' => 'max-width:118px;max-height:118px;')) }}
                                @else
                                    @if(strpos($review->member->image, "http://")!== false || strpos($review->member->image, "https://") !== false)
                                    {{ HTML::image($review->member->image, null, array('style' => 'max-width:118px;max-height:118px;')) }}
                                    @else
                                    {{ HTML::image(URL::asset(USER_IMAGE_URL . '/' . $review->member->id . '/' . $review->member->image), null, array('style' => 'max-width:118px;max-height:118px;')) }}
                                    @endif
                                @endif
                            </div>
                            <div class='pull-left' style='font-size: 16px;'>
                                <div style='font-weight: 500; color:#416990; margin-right: 5px;'>
                                    {{ $review->member->first_name }} {{ $review->member->last_name }}
                                </div>
                                <div style='font-size: 0.75em; font-weight: 600;color: #416990;opacity: 0.9;'>
                                    {{ $review->member->role }}
                                </div>
                                <div>
                                    <div class='ratings pull-left' data-score='{{ $review->rating }}' style='margin-right:10px'>
                                    </div>
                                    <div class='pull-left' style='font-size: 0.85em;'>
                                        {{ date('d/m/Y', strtotime($review->created_on)) }}
                                    </div>
                                </div>
                                @if(isset($review->comment) && $review->comment !='')
                                <div style='clear:left;'>
                                    {{ $review->comment }}
                                </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 prop-user">
                <div class="panel panel-default panel-user">
                    <div class="panel-body text-center">
                        @if(empty($owner->image))
                        <div id="img-avatar" class="img-avatar img-responsive" style="background-image: url('{{ URL::asset('assets/img/featured_placeholder.png') }}');"></div>
                        @else
                            @if(strpos($owner->image, "http://")!== false || strpos($owner->image, "https://") !== false)
                            <div id="img-avatar" class="img-avatar img-responsive" style="background-image: url('{{ $owner->image }}');"></div>
                            @else
                            <div id="img-avatar" class="img-avatar img-responsive" style="background-image: url('{{ URL::asset(USER_IMAGE_URL . DIRECTORY_SEPARATOR . $owner->id . DIRECTORY_SEPARATOR . $owner->image) }}');"></div>
                            @endif
                        @endif
                        <h3 class="user-name">{{ $owner->first_name }} {{ $owner->last_name }}</h3>
                        @if (!empty($owner->agency_id))
                            <p class="user-agency">{{ $owner->agency->name }}</p>
                        @if (!is_null($follow) || $owner->id == $user->id)
                            <p class="user-mobile"><a href="tel://{{ $owner->mobile }}">{{ $owner->mobile }}</a></p>
                            <p class="user-email"><a href="mailto:{{ $owner->email }}">{{ $owner->email }}</a></p>
                        @endif
                        @endif
                        <div id="ratings" class="user-ratings" data-score="{{ $owner->getAverageRating() }}"></div>
                        @if($owner->role == 'agent')
                        <p>
                            <a href='{{ route('property.statistic', array('id' => $owner->id)) }}'><i class="fa fa-pie-chart"></i> Statistic</a>
                        </p>
                        @endif
                    </div>
                </div>									
            </div>
        </div>
    </div>          
</div>