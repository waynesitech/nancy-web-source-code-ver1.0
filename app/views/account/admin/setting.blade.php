<div class="page admin-setting-page">
	<div class="container">
		<div class="page-heading clearfix">
			<h1 class="heading-title">Settings</h1>
		</div>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-1">
			@if(Session::has('alert.success'))
				<div class="alert alert-dismissable alert-success">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  {{ Session::get('alert.success') }}
				</div>
			@endif          
			@if(Session::has('alert.danger'))
				<div class="alert alert-dismissable alert-danger">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  {{ Session::get('alert.danger') }}
				</div>
			@endif          
			@if($errors->count() > 0)
				<div class="alert alert-dismissable alert-danger">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				  <ul>
				    @foreach($errors->getMessages() as $msg)
				    <li>{{ $msg[0] }}</li>
				    @endforeach
				  </ul>
				</div>
			@endif
			</div>
		</div>


		{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal')) }}

		<!--	<div class="form-group">
				{{ Form::label('first_name', 'First name', array('class' => 'col-sm-3 control-label')) }}
			    <div class="col-sm-5">
			     	{{ Form::text('first_name', isset($user->first_name) ? $user->first_name : '', array('class' => 'form-control')) }}
			    </div>
			</div>

			<div class="form-group">
				{{ Form::label('last_name', 'Last name', array('class' => 'col-sm-3 control-label')) }}
			    <div class="col-sm-5">
			     	{{ Form::text('last_name', isset($user->last_name) ? $user->last_name : '', array('class' => 'form-control')) }}
			    </div>
			</div>

			<div class="form-group">
				{{ Form::label('username', 'Username', array('class' => 'col-sm-3 control-label')) }}
			    <div class="col-sm-5">
			     	{{ Form::text('username', isset($user->username) ? $user->username : '', array('class' => 'form-control')) }}
			    </div>
			</div>  -->

			<div class="form-group">
				{{ Form::label('email', 'Email address', array('class' => 'col-sm-3 control-label')) }}
			    <div class="col-sm-5">
			     	{{ Form::email('email', isset($user->email) ? $user->email : '', array('class' => 'form-control')) }}
			    </div>
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Current Password', array('class' => 'col-sm-3 control-label')) }}
			    <div class="col-sm-5">
			     	{{ Form::password('password', array('class' => 'form-control')) }}
			    </div>
			</div>

			<div class="form-group">
				{{ Form::label('new_password', 'New Password', array('class' => 'col-sm-3 control-label')) }}
			    <div class="col-sm-5">
			     	{{ Form::password('new_password', array('class' => 'form-control')) }}
			     	<p class="help-block">Password must contains at least 8 characters</p>
			    </div>
			</div>

			<div class="form-group">
				{{ Form::label('confirm_password', 'Confirm password', array('class' => 'col-sm-3 control-label')) }}
			    <div class="col-sm-5">
			     	{{ Form::password('confirm_password', array('class' => 'form-control')) }}
			    </div>
			</div>

		<!--	<div class="form-group">
				{{ Form::label('status', 'Status', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-5">
					<div class="radio">
						<label>
							{{ Form::radio('status', '1', isset($user->status) ? $user->status : 1 ) }}
							Active
						</label>
					</div>
					<div class="radio">
						<label>
							{{ Form::radio('status', '0', isset($user->status) ? !$user->status : 0 ) }}
							Inactive
						</label>
					</div>
				</div>
			</div> -->

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-5">
					<button class="btn btn-primary" type="submit">Save</button>
				</div>
			</div>

		{{ Form::close() }}
	</div>
</div>