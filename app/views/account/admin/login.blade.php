<div>
  <a class="hiddenanchor" id="signup"></a>
  <a class="hiddenanchor" id="signin"></a>

  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
        <form method="post">
          <h1>Login</h1>
          <div>
            <!--<input type="text" class="form-control" placeholder="Username" required="" />-->
            {{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder'=>'Email', 'required' => '') ) }}
          </div>
          <div>
            <!--<input type="password" class="form-control" placeholder="Password" required="" />-->
            {{ Form::password('password', array('class' => 'form-control', 'placeholder'=>'Password', 'required' => '') ) }}
          </div>
          <div>
          	{{ Form::button('Log In', array( 'type' => 'submit', 'class' => 'btn btn-default submit')) }}
          </div>

          <div class="clearfix"></div>

          <div class="separator">
            <div>
              <img src="/assets/images/nancylogocolor.png" class="img-fluid" width="40">
              <p>©2017 All Rights Reserved.</p>
            </div>
          </div>
        </form>
      </section>
    </div>
  </div>
</div>

<!-- div class="page page-login">

	@if($errors->count() > 0)
        <div class="alert alert-dismissable alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          	<ul>
          		@foreach($errors->getMessages() as $msg)
		    	<li>{{ $msg[0] }}</li>
		    	@endforeach
          	</ul>
        </div>
    @endif

    @if(Session::has('alert.success'))
		<div class="alert alert-dismissable alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('alert.success') }}
		</div>
	@endif     

     @if(Session::has('alert.danger'))
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('alert.danger') }}
		</div>
     @endif

	<section class="panel panel-default">
		<form class="panel-body" role="form" method="post">
			<div class="form-group">
				{{ Form::label('email', 'Email/Username', array('class' => 'control-label')) }}
				{{ Form::text('email', Input::old('email'), array('class' => 'form-control') ) }}
			</div>
			<div class="form-group">
				{{ Form::label('password', 'Password', array('class' => 'control-label')) }}
				{{ Form::password('password', array('class' => 'form-control') ) }}
			</div>
			<div class="form-group">
				{{ Form::button('Sign In', array( 'type' => 'submit', 'class' => 'btn btn-primary')) }}
			</div>
		</form>
	</section>

</div -->