<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'Nancy Web')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        {{ HTML::style('/assets/bootstrap/css/bootstrap.min.css') }}
        <!-- styles -->
        {{ HTML::style('/assets/css/styles.css') }}
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://code.jquery.com/jquery.js"></script>
        {{ HTML::script('/assets/js/custom.js') }}

    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <!-- Logo -->
                        <div class="logo">
                            <img src="/assets/images/nancylogowhite.png" width="45px">
                        </div>
                    </div>

                    <!-- 	           <div class="col-1">
                                                    <h5>User</h5>
                                       </div> -->

                    <div class="col-md-7">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <li><a href="{{ URL::route('user.me') }}">Profile</a></li>
                                            <li><a href="{{ URL::route('user.logout') }}">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('msg')

        <div class="page-content">
            <div class="row">
                <div class="col-md-2">
                    <div class="sidebar content-box" style="display: block;">
                        <ul class="nav">
                            <!-- Main menu -->
                            <li class="{{ (Route::currentRouteName() == 'order.lists' || Route::currentRouteName() == 'order.detail') && !isset($_GET['type']) ? 'current' : '' }}" ><a href="{{ route('order.lists') }}"><img src="/assets/images/listicon2.png" width="25px"> Tenancy List</a></li>
                            <li class="{{ Route::currentRouteName() == 'order.add' && !isset($_GET['type']) ? 'current' : '' }}"><a id="genTA" href="javascript:void(0)"><img src="/assets/images/newicon.png" width="25px"> New Tenancy Agreement</a></li>
                            <li class="{{ isset($_GET['type']) && $_GET['type'] == 'esign' ? 'current' : '' }}"><a href="javascript:checkProceedType('esign')"><img src="/assets/images/ESIGN.png" width="25px">  E Sign</a></li>
                            <li class="{{ isset($_GET['type']) && $_GET['type'] == 'estamp' ? 'current' : '' }}"><a href="javascript:checkProceedType('estamp')"><img src="/assets/images/Estamp.png" width="25px">  E Stamp</a></li>
                            <li class="{{ isset($_GET['type']) && $_GET['type'] == 'autoreminder' ? 'current' : '' }}"><a href="javascript:checkProceedType('autoreminder')"><img src="/assets/images/autoreminder.png" width="25px"> Auto Reminder</a></li>
                        </ul>
                    </div>
                </div>
                {{ $body }} 

                <!-- New TA modal -->
                <div class="modal fade" id="chargesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Reminder</h4>
                            </div>
                            <div class="modal-body">
                                <p>My charges for generating new Tenancy Agreement is RM1.00</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default genTA" >I Agree</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ESign- modal -->
                <div class="modal fade" id="NewEsign" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header" >
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">E Sign</h4>
                            </div>
                            <div class="modal-body">
                                <p>Choose one</p>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default">
                                        <a href="{{ route('order.lists', array('type'=>'esign')) }}">Existing Tenancy Agreement</a>
                                    </button>
                                    <button type="button" class="btn btn-default">
                                        <a href="{{ route('order.add', array('type'=>'esign')) }}">Upload Tenancy Agreement</a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- EStamp- modal -->
                <div class="modal fade" id="NewEstampTenancy" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header" >
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">E Stamp</h4>
                            </div>
                            <div class="modal-body">
                                <p>Choose one</p>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default">
                                        <a href="{{ route('order.lists', array('type'=>'estamp')) }}">Existing Tenancy Agreement</a>
                                    </button>
                                    <button type="button" class="btn btn-default">
                                        <a href="{{ route('order.add', array('type'=>'estamp')) }}">Upload Tenancy Agreement</a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

                <!-- NewAutoReminder- modal -->
                <div class="modal fade" id="NewAutoReminder" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header" >
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Auto Reminder</h4>
                            </div>
                            <div class="modal-body">
                                <p>I will remind you when your Tenancy Agreement is expiring in</p>
                                <p>
                                    4 months' time,<br/>
                                    3 and a half months' time,<br/>
                                    3 months' time,<br/>
                                    2 months' time,<br/>
                                    1 month's time,<br/>
                                    14 days' time &amp;<br/>
                                    on the day of expiry<br/>
                                </p>
                                <h5><i>* Whichever applies.</i></h5>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default">
                                        <a href="{{ route('order.lists', array('type'=>'autoreminder')) }}">Existing Tenancy Agreement</a>
                                    </button>
                                    <button type="button" class="btn btn-default">
                                        <a href="{{ route('order.add', array('type'=>'autoreminder')) }}">Upload Tenancy Agreement</a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <footer>
            <div class="container">

                <div class="copy text-center">
                    <span>Copyright &copy;2017 Lepro System Berhad. All Rights Reserved.</span><br/>
                    <span>
                        <a href="">Term & Conditions</a>&nbsp;&nbsp; |  &nbsp;&nbsp;
                        <a href="">Refund Policy</a>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <a href="">Private Policy</a>
                    </span>
                </div>

            </div>
        </footer>


        <!-- Include all compiled plugins (below), or include individual files as needed -->
        {{ HTML::script('/assets/bootstrap/js/bootstrap.min.js') }}
        {{ HTML::script('/assets/js/moment.js') }}
        {{ HTML::script('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}
        {{ HTML::style('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}
        <script>
$(function () {
    $("a#genTA").on("click", function (e) {
        $('#chargesModal').modal('show');
    });
    $("button.genTA").on("click", function (e) {
        window.location = "{{ route('order.add') }}";
    });

});

function checkProceedType(val) {
    var type = val;

    if (type !== '') {
        if (type === 'esign') {
            $('#NewEsign').modal('show');
        } else if (type === 'estamp') {
            $('#NewEstampTenancy').modal('show');
        } else if (type === 'autoreminder') {
            $('#NewAutoReminder').modal('show');
        }
    }
}
        </script>
    </body>
</html>