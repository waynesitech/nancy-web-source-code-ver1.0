<!-- DOCTYPE -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Nancy Web</title>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="author" content="Sam Norton">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        {{ HTML::style('/assets/css/style.css') }}
        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600,700,900' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
            
        <!-- favicon -->
        <link rel="icon" href="/assets/images/favicon.png" sizes="16x16" type="image/png"> 
                    
        @if( isset($styles['head']) )
        @foreach ($styles['head'] as $style)
        {{ HTML::style($style) }}
        @endforeach
        @endif
    </head>
        
        
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        
        <!-- Navigation -->
        <nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
            <div class="container">
                
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    
                <!--  <a class="page-scroll navbar-brand" href="#page-top">Navbar</a> -->
                    
                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mr-auto mt-2 mt-md-0">
                        <li class="nav-item active">
                            <a class="nav-link page-scroll" href="#banner">Home&nbsp; &nbsp;  <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#about">About &nbsp; &nbsp; <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#features">Features&nbsp; &nbsp;  <span class="sr-only">(current)</span></a>			      
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#download">Download&nbsp; &nbsp;  <span class="sr-only">(current)</span></a>			      
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#contact">Contact&nbsp; &nbsp;  <span class="sr-only">(current)</span></a>			      
                        </li>
                    </ul>
                        
                    <ul class="navbar-nav">
                        <li class="nav-item" style="padding-right: 1em">
                            @if (Auth::check())
                            Welcome, {{Auth::user()->name}}
                            <button type="button" class="btn btn-warning">
                                <a class="page-scroll" href="{{ URL::route('user.logout') }}">Logout </a>
                            </button>
                            @else
                            <button type="button" class="btn btn-warning">
                                <a class="page-scroll" href="{{ URL::route('user.login') }}">Login <span class="sr-only">(current)</span></a>
                            </button>
                            @endif
                        </li>
                        @if (!Auth::check())
                        <li class="nav-item">
                            <button type="button" class="btn btn-warning">
                                <a class="" href="{{ URL::route('user.register') }}">Sign Up <span class="sr-only">(current)</span></a>
                            </button>	      
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @include('msg')
            
        {{ $body }}    	
            
        <section class="footer" id="footer">
            <div class="center_align">
                <span>Copyright ©2017 Lepro System Berhad. All Rights Reserved.</span><br/>
                <span>
                    <a href="">Term & Conditions</a>&nbsp;&nbsp; |  &nbsp;&nbsp;
                    <a href="">Refund Policy</a>&nbsp;&nbsp; | &nbsp;&nbsp;
                    <a href="">Private Policy</a>
                </span>
            </div>
        </section>	
            
        <!-- JavaScripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <!-- Scrolling Nav JavaScript -->
        {{ HTML::script('/assets/js/jquery.easing.min.js') }}
        {{ HTML::script('/assets/js/scrolling-nav.js') }}
    </body>
</html>