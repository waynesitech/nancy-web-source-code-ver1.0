<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'Nancy Web')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        {{ HTML::style('/assets/bootstrap/css/bootstrap.min.css') }}
        <!-- styles -->
        {{ HTML::style('/assets/css/styles.css') }}
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://code.jquery.com/jquery.js"></script>
        {{ HTML::script('/assets/js/custom.js') }}

    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <!-- Logo -->
                        <div class="logo">
                            <img src="/assets/images/nancylogowhite.png" width="45px">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('msg')

        <div class="page-content">
            <div class="row">
                <div class="col-md-2"></div>
                {{ $body }} 

            </div>
        </div>


        <footer>
            <div class="container">

                <div class="copy text-center">
                    <span>Copyright &copy;2017 Lepro System Berhad. All Rights Reserved.</span><br/>
                    <span>
                        <a href="">Term & Conditions</a>&nbsp;&nbsp; |  &nbsp;&nbsp;
                        <a href="">Refund Policy</a>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <a href="">Private Policy</a>
                    </span>
                </div>

            </div>
        </footer>


        <!-- Include all compiled plugins (below), or include individual files as needed -->
        {{ HTML::script('/assets/bootstrap/js/bootstrap.min.js') }}
        {{ HTML::script('/assets/js/moment.js') }}
        {{ HTML::script('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}
        {{ HTML::style('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}
        
    </body>
</html>