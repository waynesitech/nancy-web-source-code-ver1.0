<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NaNcy</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/font-awesome/css/font-awesome.min.css') }}">
    <!-- NProgress -->
    <link rel="stylesheet" href="{{ asset('assets/admin/nprogress/nprogress.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('assets/admin/iCheck/skins/flat/green.css') }}">
    <!-- bootstrap-daterangepicker -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/build/css/custom.min.css') }}">
    <!-- Datatables -->
    <link href="{{ asset('assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/admin/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    
    <!-- Morris Chart -->
    {{ HTML::style('/assets/bower_components/morrisjs/morris.css') }}
    
    <!-- jQuery -->
    {{ HTML::script('/assets/admin/jquery/dist/jquery.min.js') }}
   
    @if( isset($styles['head']) )
      @foreach ($styles['head'] as $style)
        {{ HTML::style($style) }}
      @endforeach
    @endif
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><img src="/assets/images/nancylogocolor.png" class="img-fluid" width="30"> <span>NaNcy Backend System</span></a>
            </div>

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                    @if(Auth::user()->role->name_en == 'Administrator')
                    <li><a href="{{ route('dashboard.list') }}"><i class="fa fa-laptop"></i> Dashboard </a></li>
                    <li><a href="{{ route('user.list') }}"><i class="fa fa-users"></i> Users </a></li>
                    <li><a href="{{ route('order.list') }}"><i class="fa fa-file-text-o"></i> Tenancy Agreement </a></li>
                    <li><a href="{{ route('e_sign.list') }}"><i class="fa fa-file-text-o"></i> E-Sign </a></li>
                    <li><a href="{{ route('e_stamping.list') }}"><i class="fa fa-file-text-o"></i> E-Stamping </a></li>
                    <li><a href="{{ route('explanation.list') }}"><i class="fa fa-file-text-o"></i> Explanation List </a></li>
                    <li><a href="{{ route('order.payment') }}"><i class="fa fa-file-text-o"></i> Transaction </a></li>
<!--                    <li><a href="{{ route('e_storage.list') }}"><i class="fa fa-file-text-o"></i> E-Storage </a></li>-->
                    @elseif(Auth::user()->role->name_en == 'Lesys Team')
                    <li><a href="{{ route('order.list') }}"><i class="fa fa-file-text-o"></i> Tenancy Agreement </a></li>
                    @endif
                    
<!--                    <li><a href="#"><i class="fa fa-usd"></i> Payment Transaction </a></li>-->
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Welcome, {{Auth::user()->name}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            {{ $body }}
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            NaNcy ©2017 All Rights Reserved.</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    {{ HTML::script('/assets/admin/bootstrap/dist/js/bootstrap.min.js') }}
    <!-- FastClick -->
	{{ HTML::script('/assets/admin/fastclick/lib/fastclick.js') }}
    <!-- NProgress -->
	{{ HTML::script('/assets/admin/nprogress/nprogress.js') }}
    <!-- Chart.js -->
	{{ HTML::script('/assets/admin/Chart.js/dist/Chart.min.js') }}
    <!-- DateJS -->
	{{ HTML::script('/assets/admin/DateJS/build/date.js') }}
    <!-- bootstrap-daterangepicker -->
    {{ HTML::script('/assets/admin/moment/min/moment.min.js') }}
    {{ HTML::script('/assets/admin/bootstrap-daterangepicker/daterangepicker.js') }}
    <!-- Datatables -->
    {{ HTML::script('/assets/admin/datatables.net/js/jquery.dataTables.min.js') }}
    {{ HTML::script('/assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}
    {{ HTML::script('/assets/admin/datatables.net-buttons/js/dataTables.buttons.min.js') }}
    {{ HTML::script('/assets/admin/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}
    {{ HTML::script('/assets/admin/datatables.net-buttons/js/buttons.html5.min.js') }}
    {{ HTML::script('/assets/admin/datatables.net-buttons/js/buttons.print.min.js') }}    
    <!-- Custom Theme Scripts -->
    {{ HTML::script('/assets/js/custom.min.js') }}
        
    @if( isset($scripts['inline']) )
      @foreach ($scripts['inline'] as $script)
        {{ HTML::script($script) }}
      @endforeach
    @endif

    @if( isset($styles['inline']) )
      @foreach ($styles['inline'] as $style)
        {{ HTML::style($style) }}
      @endforeach
    @endif 

    <!-- bootstrap-daterangepicker -->
    <script type="text/javascript">
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->
  </body>
</html>
