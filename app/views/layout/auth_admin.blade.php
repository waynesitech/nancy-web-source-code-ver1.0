<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NaNcy Backend System</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/font-awesome/css/font-awesome.min.css') }}">
    <!-- NProgress -->
    <link rel="stylesheet" href="{{ asset('assets/admin/nprogress/nprogress.css') }}">
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('assets/admin/animate.css/animate.min.css') }}">

    <!-- Custom Theme Style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/build/css/custom.min.css') }}">
  </head>

  <body class="login">
    {{ $body }}
  </body>
</html>