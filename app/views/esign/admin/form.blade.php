<div class="page-heading clearfix">
	<h1 class="heading-title">E-Stamping - Order Details</h1>
</div>

<div class="row">
	<div class="col-sm-8 col-sm-offset-1">
		@if(Session::has('alert.success'))
		<div class="alert alert-dismissable alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('alert.success') }}
		</div>
		@endif          
		@if(Session::has('alert.danger'))
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{{ Session::get('alert.danger') }}
		</div>
		@endif          
		@if($errors->count() > 0)
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<ul>
				@foreach($errors->getMessages() as $msg)
				<li>{{ $msg[0] }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

{{ Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'id' => 'orderForm')) }}

@if(count($order->images) > 0)
    @if(count($order->images) > 1)
    	<div class="owl-carousel" style="max-width: 500px; margin:0 auto 15px;text-align:center;">
            @foreach($order->images as $image)
            <div class='items' style="overflow:hidden;background:transparent;">
            	<div style="float:right; margin: 5px; font-size:16px">
                    <a href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$image->image }}' download>
                    Download This
                    </a>
                </div>
                <a itemprop="photo" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$image->image }}' data-lightbox="slider">
                    <img src="{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$image->image }}" alt="" style='height:auto;max-width: 500px;'/>
                </a>
            </div>
            @endforeach
        </div>
    @else
        <div style="max-width: 500px; margin:0 auto 15px;text-align:center;">
        	<div style="float:right; margin: 5px; font-size:16px">
                <a href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->images[0]->image }}' download>
                Download This
                </a>
            </div>
            <a itemprop="photo" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->images[0]->image }}' data-lightbox="single">
            <img src="{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->images[0]->image }}" alt="" style='max-width: 500px;'/>
            </a>
        </div>
    @endif
@endif
<hr>

<div class="form-group">
	{{ HTML::decode(Form::label('order_num', 'Order Number', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            <p class="help-block">{{ isset($order->order_num) ? $order->order_num : '' }}</p>
	</div>
</div>

<hr>

<div class="form-group">
	{{ HTML::decode(Form::label('request_by', 'Requested By', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            <p class="help-block">{{ isset($order->users->first_name) ? $order->users->first_name : '' }}</p>
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('requester_email', 'Email', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            <p class="help-block">{{  isset($order->users->email) ? $order->users->email : '' }}</p>
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('requester_mobile', 'Contact', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            <p class="help-block">{{  isset($order->users->mobile) ? $order->users->mobile : '' }}</p>
	</div>
</div>

<hr>

<div class="form-group">
	{{ HTML::decode(Form::label('tenant_name', 'Tenant', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_name', isset($order->tenant_name) ? $order->tenant_name : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('tenant_contact', 'Tenant Contact', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_contact', isset($order->tenant_contact) ? $order->tenant_contact : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('tenant_ic', 'Tenant IC', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_ic', isset($order->tenant_ic) ? $order->tenant_ic : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('tenant_email', 'Tenant Email', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_email', isset($order->tenant_email) ? $order->tenant_email : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('tenant_as_company', 'As Company', array('class' => 'col-sm-3 control-label')) }}
	<div class="col-sm-5">
		<div class="radio-inline">
			<label>
				{{ Form::radio('tenant_as_company', '0', isset($order->tenant_comp_name) && !empty($order->tenant_comp_name) ? 0 : 1 ) }}
				No
			</label>
		</div>
		<div class="radio-inline">
			<label>
				{{ Form::radio('tenant_as_company', '1', isset($order->tenant_comp_name) && !empty($order->tenant_comp_name) ? 1 : 0 ) }}
				Yes
			</label>
		</div>
	</div>
</div>

<div id="divTenant" style={{isset($order->tenant_comp_name) && !empty($order->tenant_comp_name) ? 'display:block' : 'display:none'}}>
<p class="help-block"><i>As Company Details</i></p>
<div class="form-group">
	{{ HTML::decode(Form::label('tenant_comp_name', 'Name', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_comp_name', isset($order->tenant_comp_name) ? $order->tenant_comp_name : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('tenant_comp_contact', 'Contact', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_comp_contact', isset($order->tenant_comp_contact) ? $order->tenant_comp_contact : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('tenant_comp_ic', 'IC', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_comp_ic', isset($order->tenant_comp_ic) && ($order->tenant_comp_ic != 0) ? $order->tenant_comp_ic : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('tenant_comp_email', 'Email', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('tenant_comp_email', isset($order->tenant_comp_email) ? $order->tenant_comp_email : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>
</div>

<hr>

<div class="form-group">
	{{ Form::label('land_lord_name', 'Landlord', array('class' => 'control-label col-sm-3 control-required')) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_name', isset($order->land_lord_name) ? $order->land_lord_name : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>	

<div class="form-group">
	{{ Form::label('land_lord_contact', 'Landlord Contact', array('class' => 'control-label col-sm-3 control-required')) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_contact', isset($order->land_lord_contact) ? $order->land_lord_contact : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>	

<div class="form-group">
	{{ HTML::decode(Form::label('land_lord_ic', 'Landlord IC', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_ic', isset($order->land_lord_ic) ? $order->land_lord_ic : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('land_lord_email', 'Landlord Email', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_email', isset($order->land_lord_email) ? $order->land_lord_email : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('land_as_company', 'As Company', array('class' => 'col-sm-3 control-label')) }}
	<div class="col-sm-5">
		<div class="radio-inline">
			<label>
				{{ Form::radio('land_as_company', '0', isset($order->land_lord_comp_name) && !empty($order->land_lord_comp_name) ? 0 : 1 ) }}
				No
			</label>
		</div>
		<div class="radio-inline">
			<label>
				{{ Form::radio('land_as_company', '1', isset($order->land_lord_comp_name) && !empty($order->land_lord_comp_name) ? 1 : 0 ) }}
				Yes
			</label>
		</div>
	</div>
</div>

<div id="divLandlord" style={{isset($order->land_lord_comp_name) && !empty($order->land_lord_comp_name) ? 'display:block' : 'display:none'}}>
<p class="help-block"><i>As Company Details</i></p>
<div class="form-group">
	{{ HTML::decode(Form::label('land_lord_comp_name', 'Name', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_comp_name', isset($order->land_lord_comp_name) ? $order->land_lord_comp_name : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('land_lord_comp_contact', 'Contact', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_comp_contact', isset($order->land_lord_comp_contact) ? $order->land_lord_comp_contact : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('land_lord_comp_ic', 'IC', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_comp_ic', isset($order->land_lord_comp_ic) && $order->land_lord_comp_ic != 0  ? $order->land_lord_comp_ic : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('land_lord_comp_email', 'Email', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
            {{ Form::text('land_lord_comp_email', isset($order->land_lord_comp_email) ? $order->land_lord_comp_email : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>
</div>

<hr>

<div class="form-group">
	{{ Form::label('status', 'Status', array('class' => 'control-label col-sm-3 control-required')) }}
	<div class="col-sm-5">
            <p class="help-block">{{ isset($order->status) ? $order->status : '' }}</p>
        </div>
</div>
<div class="form-group">
	{{ HTML::decode(Form::label('said_property', 'The Said Property', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
		{{ Form::text('said_property', isset($order->said_property) ? $order->said_property : '', array('class' => 'form-control', 'required' => '')) }}
	</div>
</div>
<div class="form-group">
	{{ HTML::decode(Form::label('tenure', 'Tenure', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-2">
		{{ Form::select('tenure_year', $tenure_year, isset($order->tenure_year) ? $order->tenure_year : '', array('class' => 'form-control', 'required' => '', 'id' => 'tenure_year')) }}
	</div>
    <div class="col-sm-2">
		{{ Form::select('tenure_month', $tenure_month, isset($order->tenure_month) ? $order->tenure_month : '', array('class' => 'form-control', 'required' => '', 'id' => 'tenure_month')) }}
	</div>
</div>
<div class="form-group">
	{{ HTML::decode(Form::label('expiry_date', 'Tenancy Date', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
		{{ Form::text('expiry_date', isset($order->expiry_date) && $order->expiry_date!='' ? ($order->expiry_date == '0000-00-00' ? date('d/m/Y') : date('d/m/Y', strtotime($order->expiry_date))) : '', array('class' => 'form-control', 'id' => 'expiry_date', 'readonly')) }}
	</div>
</div>
<input type="hidden" id="hidCreatedOn" value="{{date('m/d/Y', strtotime($order->created_on))}}" />

<div class="form-group">
	{{ HTML::decode(Form::label('rental', 'Rental', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
		{{ Form::text('rental_fee', isset($order->rental_fee) ? floatval($order->rental_fee) : '', array('class' => 'form-control', 'required' => '', 'id' => 'rental_fee')) }}
	</div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('e-fee', 'E-Stamping Fee', array('class' => 'col-sm-3 control-label'))) }}
	<div class="col-sm-5">
		{{ Form::text('e-fee', $e_fee, array('class' => 'form-control', 'readonly' => 'readonly')) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('sign', 'Sign', array('class' => 'control-label col-sm-3')) }}
	<div class="col-sm-5">
        <p class="help-block">{{ isset($order->sign) ? (empty($order->sign)? '-' : $order->sign) : '' }}</p>
    </div>
</div>

<div class="form-group">
	{{ Form::label('remark', 'Remark', array('class' => 'control-label col-sm-3')) }}
    <div class="col-sm-5">
    	{{ Form::textarea('remark', isset($order->remark) ? $order->remark : Input::old('remark', ''), array('size' => '50x2', 'class' => 'form-control')) }}
    </div>
</div>

<hr>

<div class="form-group" >
	{{ HTML::decode(Form::label('document_upload', 'Upload PDF (with signatory)', array('class' => 'control-label col-sm-3 control-required'))) }}
	<div class="col-sm-6 form-upload">
		@if(isset($order->document) && $order->document!='')
        <div>
            Current File: <a target="_blank" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->document }}'>{{ $order->document_name }}</a>
        </div>
        @else
    		<p class="help-block">No file uploaded</p>
        @endif
            
    </div>
</div>


<div class="form-group" >
	{{ HTML::decode(Form::label('team_file_2', 'Upload PDF (without signatory)', array('class' => 'control-label col-sm-3'))) }}
	<div class="col-sm-6 form-upload">
		@if(isset($order->tenancy_team_file_pdf) && $order->tenancy_team_file_pdf!='')
        <div>
            Current File: <a target="_blank" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->tenancy_team_file_pdf }}' download="Tenancy Team File (pdf)">Tenancy Team File (pdf)</a>
        </div>
        @else
    		<p class="help-block">No file uploaded</p>
        @endif
    </div>
</div>

<div class="form-group">
	{{ HTML::decode(Form::label('team_file', 'Tenancy Team File', array('class' => 'control-label col-sm-3'))) }}
	<div class="col-sm-6 form-upload">
    @if(isset($order->tenancy_team_file) && $order->tenancy_team_file!='')
    <div>
        Current File: <a target="_blank" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->tenancy_team_file }}' download="Tenancy Team File">Tenancy Team File</a>
    </div>
    @else
    	<p class="help-block">No file uploaded</p>
    @endif
    </div>
</div>

<hr>

<div class="form-group">
	{{ Form::label('estamping_duplicate', 'Duplicate', array('class' => 'col-sm-3 control-label')) }}
	<div class="col-sm-5">
    	{{ Form::text('estamping_duplicate', isset($order->estamping_duplicate) ? $order->estamping_duplicate : '', array('class' => 'form-control', 'readonly' => 'readonly', 'id' => 'estamping_duplicate')) }}
	</div>
</div>

<div class="form-group" >
	{{ HTML::decode(Form::label('estamping_doc', 'Stamping File <em>*</em>', array('class' => 'control-label col-sm-3'))) }}
	<div class="col-sm-6 form-upload">
		<input id="estamping_doc" name='estamping_doc' type="file" class="file-loading" accept="application/pdf" >
        <p class="help-block"><i>Stamping file in .pdf format.</i></p>
        @if(isset($order->estamping_doc) && $order->estamping_doc!='')
        <div>
            Current File: <a target="_blank" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR.$order->estamping_doc }}' download="E-Stamping document">E-Stamping document</a>
        </div>
        @endif
        
    </div>
</div>

@if(isset($order->digital_merge_agreement))
<div class="form-group">
	{{ HTML::decode(Form::label('digital_merge_agreement', 'E-Sign & Logs', array('class' => 'control-label col-sm-3'))) }}
	<div class="col-sm-6 form-upload">
    @if(isset($order->digital_merge_agreement) && $order->digital_merge_agreement!='')
    <div style="padding-top:7px">
        <a target="_blank" href='{{ ORDER_IMAGE_URL.DIRECTORY_SEPARATOR.$order->id.DIRECTORY_SEPARATOR."sign".DIRECTORY_SEPARATOR.$order->digital_merge_agreement }}' download="E-Sign Document">Download E-Sign document</a>
    </div>
    @endif
    </div>
</div>
@endif



<hr>

<div class="form-group">
	<div class="col-sm-offset-3">
		<a class="btn btn-primary" href="{{ route('estamping.list') }}" type="button">Back</a>
        @if($order->estamping_status == 'Received' || $order->estamping_status != 'Rejected')
            @if(Auth::user()->role->name_en == 'Administrator')
            <button data-toggle="modal" data-target="#rejectModal" class="btn btn-danger" type="button">Reject</button>
            @endif
            <button id="btn_save" class="btn btn-success btn-submit" type="button">Save</button>
        @elseif($order->estamping_status == 'Rejected')
            @if(isset($order->estamping_doc) && $order->estamping_doc !='')
            <?php /*?><button id="btn_ready" class="btn btn-success btn-submit" type="button">Ready</button><?php */?>
            @else
            <button id="btn_save" class="btn btn-success btn-submit" type="button">Save</button>
            @endif
        @endif
	</div>
</div>
<input id="orderStatus" type="hidden" name="orderStatus" value="">
{{ Form::close() }}

<!-- Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to reject?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="btn_reject" type="button" class="btn btn-danger btn-submit">Reject</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="/assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/assets/js/fileinput.min.js"></script>
{{ HTML::script('/assets/js/moment.js') }}
{{ HTML::script('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}
{{ HTML::style('/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}
{{ HTML::script('/assets/js/lightbox/lightbox.js') }}
{{ HTML::style('/assets/js/lightbox/lightbox.css') }}
<script>
$(document).ready(function () {
	//make all element to readonly except e-stamping section
	$('form input').attr('disabled', 'disabled');
	$('form select').attr('disabled', 'disabled');
	$('form textarea').attr('disabled', 'disabled');
	$('input[name="estamping_duplicate"]').removeAttr('disabled');
	$('#estamping_doc').removeAttr('disabled');
	$('#orderStatus').removeAttr('disabled');
	
    $("#estamping_doc").fileinput({
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: false,
        showUpload: false,
        allowedFileExtensions: ['pdf'],
        maxFileSize: <?php echo $val; ?>
    });
    
    $('.btn-submit').on('click', function(){
        var id = $(this).attr('id');
        
        if(id == 'btn_save')
        {
            $('#orderStatus').val('Update');
        }
        else if(id == 'btn_reject')
        {
            $('#doc_upload').val('');
            $('#orderStatus').val('Rejected');
        }
        else if(id=='btn_ready')
        {
            $('#orderStatus').val('Ready');
        }
        $('#orderForm').submit();
    });
    
    <?php if (count($order->images) > 1){ ?>
        var owl = $('.owl-carousel');

        owl.owlCarousel({
            nav: true, // Show next and prev buttons
			navText: ["<img src='<?php echo URL::to('assets/img/prev.png'); ?>' width='30'>","<img src='<?php echo URL::to('assets/img/next.png'); ?>' width='30'>"],
			navClass: ["owl-prev","owl-next"],
            slideSpeed: 300,
            paginationSpeed: 400,
            items: 1,
            responsiveClass:true,
            autoplay:true,
            loop: true,
            autoHeight : true,
			autoplayHoverPause: true
        });
    <?php } ?>
    
    $('#expiry_date').datetimepicker({
        format: "DD/MM/YYYY"
    });
	
	$("#rental_fee").val(function(index, value) {
		var parts = value.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
	});

});


</script>
