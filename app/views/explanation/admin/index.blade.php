<style>
    .explanation{
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        width: 450px
    }
</style>

<div class="page-title">
    <div class="title_left">
      <h3>Explanation List</h3>
    </div>
    <div class="title_right">
        <div class="col-md-offset-5 col-sm-offset-5 form-group pull-right top_search">
          <div class="input-group">
            <a class="btn btn-primary" href="{{ route('explanation.add') }}">Add New</a>
          </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Listing</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Explanation No.</th>
                <th>Text</th>
                <th>Chinese Text</th>
              </tr>
            </thead>


            <tbody>
                @foreach($explanation as $item)
                <tr>
                    <td style="width:150px"><a href='{{ route('explanation.edit',['id' => $item->id]) }}'>{{ $item->explanationNumber }}</a></td>
                    <td class="td-restrict-150" style="width:350px"><div class="explanation">{{ $item->explanationText }}</div></td>
                    <td class="td-restrict-150" style="width:350px"><div class="explanation">{{ $item->explanationText_cn }}</div></td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<!-- Datatables -->
<script>
  $(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#datatable-buttons").length) {
        $("#datatable-buttons").DataTable({
          dom: "Bfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();



    TableManageButtons.init();
  });
</script>
