<div class="page-title">
    <div class="title_left">
        <h3>{{ Route::currentRouteName() == 'explanation.add' ? 'New' : 'Edit' }}  Explanation</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                @include('msg')
                <br />
                {{ Form::open(array('method' => 'post', 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data', 'data-parsley-validate')) }}
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ExplanationNumber">Explanation Number</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::text('explanationNumber', isset($explanation->explanationNumber) ? $explanation->explanationNumber : '', array('class' => 'form-control col-md-7 col-xs-12')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ExplanationText_en">Explanation Text (English)</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::textarea('explanationText', isset($explanation->explanationText) ? $explanation->explanationText : Input::old('explanationText', ''), array('size' => '50x10', 'class' => 'form-control col-md-7 col-xs-12')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ExistingAudio_en">Existing Audio File (English)
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        @if(!empty($explanation->explanationURL))
                        <audio src="{{URL::asset(EXPLANATION_AUDIO_URL . DIRECTORY_SEPARATOR . 'en' . DIRECTORY_SEPARATOR . $explanation->explanationURL)}}" controls="controls" controlsList="nodownload">
                            Your browser does not support the audio element.
                        </audio>
                        @else
                        -
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Audio_en">Audio File (English)
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::file('explanationURL', array('class' => 'form-control', 'accept' => 'audio/*')) }}
                    </div>
                </div>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ExplanationText_cn">Explanation Text (Chinese)</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::textarea('explanationText_cn', isset($explanation->explanationText_cn) ? $explanation->explanationText_cn : Input::old('explanationText_cn', ''), array('size' => '50x10', 'class' => 'form-control col-md-7 col-xs-12')) }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ExistingAudio_cn">Existing Audio File (Chinese)
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        @if(!empty($explanation->explanationURL_cn))
                        <audio src="{{URL::asset(EXPLANATION_AUDIO_URL . DIRECTORY_SEPARATOR . 'cn' . DIRECTORY_SEPARATOR . $explanation->explanationURL_cn)}}" controls="controls" controlsList="nodownload">
                            Your browser does not support the audio element.
                        </audio>
                        @else
                        -
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Audio_cn">Audio File (Chinese)
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{ Form::file('explanationURL_cn', array('class' => 'form-control', 'accept' => 'audio/*')) }}
                        
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button class="btn btn-success" type="submit">Save</button>
                        @if(!empty($explanation->id))
                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" type="button">Delete</button>
                        @endif
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>


@if(!empty($explanation->id))
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete?</p>
            </div>
            <div class="modal-footer">
                {{ Form::open(array('method' => 'post', 'route' => array('explanation.delete', $explanation->id))) }}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    @endif
</div>
