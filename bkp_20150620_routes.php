<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
	'as' => 'home',
	'uses' => 'HomeController@getIndex'
	));

Route::get('page', array(
	'as' => 'page',
	'uses' => 'HomeController@getPage'
	));

Route::get('terms', array(
	'as' => 'terms',
	'uses' => 'HomeController@getTerms'
	));
Route::get('privacy', array(
	'as' => 'privacy',
	'uses' => 'HomeController@getPrivacy'
	));
Route::get('contact-us', array(
	'as' => 'contact',
	'uses' => 'HomeController@getContact'
	));

Route::get('/mailtest', array(
	'as' => 'mailtest',
	'uses' => 'HomeController@getMail'
	));

// Properties
Route::group(array('prefix' => 'properties'), function() {

	Route::get('', array(
		'as' => 'property.list',
		'uses' => 'PropertyController@getProperties'
		));

	//Ajax to Get states
	Route::get('ajax/areas', array(
		'as' => 'properties.ajax.areas',
		'uses' => 'PropertyController@ajaxGetAreas'
		));	

	//Ajax to Get states
	Route::get('/ajax/search/areas', array(
		'as' => 'properties.ajax.search.areas',
		'uses' => 'PropertyController@ajaxGetSearchAreas'
		));	

});

// User
Route::group(array('prefix' => 'users'), function(){

	Route::get('register', array(
		'as' => 'user.register',
		'before' => 'guest',
		'uses' => 'AccountController@getUserRegister'
		));
	Route::post('register', array(
		'before' => 'guest',
		'uses' => 'AccountController@postUserRegister'
		));

	Route::get('login', array(
		'as' => 'user.login',
		'before' => 'guest',
		'uses' => 'AccountController@getUserLogin'
		));
	Route::post('login', array(
		'before' => 'guest',
		'uses' => 'AccountController@postUserLogin'
		));
	
	Route::get('logout', array(
		'as' => 'user.logout',
		'uses' => 'AccountController@getUserLogout'
		));

	Route::get('activate/{hash}', array(
		'as' => 'user.activate',
		'uses' => 'AccountController@getUserActivate'
		));	

});

Route::group(array('before' => 'auth.user'), function() {

	Route::group(array('prefix' => 'users'), function(){		
		Route::get('me', array(
			'as' => 'user.me',
			'uses' => 'AccountController@getUserProfile'
			));
		Route::post('me', 'AccountController@postUserProfile');

		Route::post('password', array(
			'as' => 'user.password',
			'uses' => 'AccountController@postUserPassword'
			));		

		Route::post('{id}/review', array(
			'as' => 'user.rate',
			'uses' => 'UserController@postReview'
        ));		
	});	

	Route::group(array('prefix' => 'slots'), function(){
		Route::get('{id}', array(
			'as' => 'slots.details',
			'uses' => 'PropertyController@getSlotDetails'
			))->where('id', '[0-9]+');
		
		Route::get('renew/{id}/package/{package_id}', array(
			'as' => 'slots.renew',
			'uses' => 'PropertyController@getSlotRenew'
			))
		->where('id', '[0-9]+')
		->where('package_id', '[0-9]+')
		;
		Route::get('purchase/{id}', array(
			'as' => 'slots.purchase',
			'uses' => 'PropertyController@getSlotPurchase'
			))->where('id', '[0-9]+');
	});

	Route::group(array('prefix' => 'properties'), function() {
		Route::get('me', array(
			'as' => 'property.me',
			'uses' => 'PropertyController@getMyProperties'
			));	


		Route::get('{id}', array(
			'as' => 'property.details',
			'uses' => 'PropertyController@getProperty'
			))->where('id', '[0-9]+');		

		Route::get('add', array(
			'as' => 'property.add',
			'uses' => 'PropertyController@getForm'
			));
		Route::post('add', 'PropertyController@postForm');

		Route::get('{id}/edit', array(
			'as' => 'property.edit',
			'uses' => 'PropertyController@getForm'
			))->where('id', '[0-9]+');
		Route::post('{id}/edit', 'PropertyController@postForm')
		->where('id', '[0-9]+');	

		Route::post('{id}/delete', array(
			'as' => 'property.delete',
			'uses' => 'PropertyController@deleteProperty'
			))->where('id', '[0-9]+');

		Route::get('{id}/nego', array(
			'as' => 'property.nego',
			'uses' => 'PropertyController@getNego'
			))->where('id', '[0-9]+');		

		Route::post('{id}/abuse', array(
			'as' => 'property.abuse',
			'uses' => 'AbuseController@reportAbuse'
			))->where('id', '[0-9]+');

		Route::get('{id}/toggle', array(
			'as' => 'property.toggle',
			'uses' => 'PropertyController@getToggle'			
		))->where('id', '[0-9]+');			

	});

	Route::group(array('prefix' => 'trans'), function() {
		Route::get('me', array(
			'as' => 'trans.me',
			'uses' => 'NegoController@getMyTransactions'
			));	
	});	


	Route::group(array('prefix' => 'negos'), function() {
		Route::get('me', array(
			'as' => 'nego.me',
			'uses' => 'NegoController@getMyNegos'
			));	
		Route::get('{id}', array(
			'as' => 'nego.details',
			'uses' => 'NegoController@getNego'
			))->where('id', '[0-9]+');
		Route::post('{id}', 'NegoController@postOffer')
		->where('id', '[0-9]+');
		Route::post('{id}/accept', array(
			'as' => 'nego.accept',
			'uses' => 'NegoController@postAccept'
			))->where('id', '[0-9]+');
		Route::post('{id}/decline', array(
			'as' => 'nego.decline',
			'uses' => 'NegoController@postDecline'
			))->where('id', '[0-9]+');

	});

	Route::group(array('prefix' => 'follows'), function() {
		Route::get('me', array(
			'as' => 'follow.me',
			'uses' => 'FollowerController@getMyFollowers'
			));
		Route::post('{id}', array(
			'as' => 'follow.post',
			'uses' => 'FollowerController@postFollow'
			))->where('id', '[0-9]+');
		Route::post('{id}/accept', array(
			'as' => 'follow.accept',
			'uses' => 'FollowerController@acceptFollower'
			))->where('id', '[0-9]+');
		Route::post('{id}/decline', array(
			'as' => 'follow.decline',
			'uses' => 'FollowerController@declineFollower'
			))->where('id', '[0-9]+');	
		Route::post('{id}/archive', array(
			'as' => 'follow.archive',
			'uses' => 'FollowerController@archiveFollow'
        ))->where('id', '[0-9]+');	// `follower`.`id`
	});	
	Route::group(array('prefix' => 'unfollow'), function() {
		Route::post('{id}', array(
			'as' => 'unfollow.post',
			'uses' => 'FollowerController@postUnfollow'
			))->where('id', '[0-9]+');
	});

	Route::group(array('prefix' => 'payment'), function() {
		Route::get('redirect', array(
			'as' => 'payment.redirect',
			'uses' => 'PaymentController@getRedirect'
        ));
		Route::post('ipay88/response', array(
			'as' => 'payment.ipay88_response',
			'uses' => 'PaymentController@postIPay88Response'
        ));
        Route::get('{id}/success', array(
			'as' => 'payment.success',
			'uses' => 'PaymentController@getSuccess'
        ));
        Route::get('failed', array(
			'as' => 'payment.failed',
			'uses' => 'PaymentController@getFailed'
        ));
	});

});
// the ipay backend doesn't keep the session
Route::post('payment/ipay88/backend', array(
    'as' => 'payment.ipay88_backend',
    'uses' => 'PaymentController@postIPay88Backend'
));

// Admin
Route::group(array('prefix' => 'admin'), function() {

	Route::any('', function() {

		return Redirect::route('admin.login');

	});

	// Login
	Route::get('login', array(
		'as' => 'admin.login',
		'uses' => 'AccountController@getAdminLogin'
		));
	Route::post('login', 'AccountController@postAdminLogin');

	// Logout
	Route::get('logout', array(
		'as' => 'admin.logout',
		'uses' => 'AccountController@getAdminLogout'
		));	

	// Auth
	Route::group(array('before' => 'auth.admin'), function() {

		// Settings
		Route::get('settings', array(
			'as' => 'admin.settings',
			'uses' => 'AccountController@getAdminSettings'
			));
		Route::post('settings', 'AccountController@postAdminSettings');

	    // User
		Route::group(array('prefix' => 'users'), function() {
			Route::get('', array(
				'as' => 'user.list',
				'uses' => 'UserController@getUsers'
				));
			Route::get('add', array(
				'as' => 'user.add',
				'uses' => 'UserController@getUser'
				));
			Route::post('add', 'UserController@postUser');

			Route::get('{id}/edit', array(
				'as' => 'user.edit',
				'uses' => 'UserController@getUser'
				))->where('id', '[0-9]+');

			Route::post('{id}/edit', 'UserController@postUser');

			Route::post('{id}/delete',array(
				'as' => 'user.delete',
				'uses' => 'UserController@deleteUser'
				))->where('id', '[0-9]+');
		});

		Route::group(array('prefix' => 'abuses'), function() {
			Route::get('', array(
				'as' => 'abuse.list',
				'uses' => 'AbuseController@getAbuses'
				));

			Route::get('{id}/edit', array(
				'as' => 'abuse.edit',
				'uses' => 'AbuseController@getAbuse'
				))->where('id', '[0-9]+');	

			Route::post('{id}/edit', 'AbuseController@postAbuse'
				)->where('id', '[0-9]+');							
		});

		Route::group(array('prefix' => 'subscriptions'), function() {
			Route::get('', array(
				'as' => 'subscription.list',
				'uses' => 'SubscriptionController@getSubscriptions'
				));
			Route::get('csv', array(
				'as' => 'subscription.csv',
				'uses' => 'SubscriptionController@getCSV'
				));			

			Route::get('add', array(
				'as' => 'subscription.add',
				'uses' => 'SubscriptionController@getSubscription'
				));
			Route::post('add', 'SubscriptionController@postSubscription');

			Route::get('{id}/edit', array(
				'as' => 'subscription.edit',
				'uses' => 'SubscriptionController@getSubscription'
				))->where('id', '[0-9]+');
			Route::post('{id}/edit', 'SubscriptionController@postSubscription');			
            Route::get('{id}/download', array(
                'as' => 'payment.receipt.download',
                'uses' => 'SubscriptionController@downloadReceipt'
            ));

		});		

		// Banner
		Route::group(array('prefix' => 'banners'), function() {
			Route::get('', array(
				'as' => 'banner.list',
				'uses' => 'BannerController@getBanners'
				));

			Route::get('csv', array(
				'as' => 'banner.csv',
				'uses' => 'BannerController@getCSV'
				));				

			Route::get('add', array(
				'as' => 'banner.add',
				'uses' => 'BannerController@getBanner'
				));

			Route::post('add', 'BannerController@postBanner');

			Route::get('{id}/edit', array(
				'as' => 'banner.edit',
				'uses' => 'BannerController@getBanner'
				))->where('id', '[0-9]+');

			Route::post('{id}/edit', 'BannerController@postBanner');

			Route::post('{id}/delete', array(
				'as' => 'banner.delete',
				'uses' => 'BannerController@deleteBanner'
				))->where('id', '[0-9]+');
		});		

		// Nego
		Route::group(array('prefix' => 'negos'), function() {
			Route::get('', array(
				'as' => 'nego.list',
				'uses' => 'NegoController@getNegos'
				));

			Route::get('add', array(
				'as' => 'nego.add',
				'uses' => 'NegoController@getForm'
				));

			Route::post('add', 'NegoController@postForm');

			Route::get('{id}/edit', array(
				'as' => 'nego.edit',
				'uses' => 'NegoController@getForm'
				))->where('id', '[0-9]+');

			Route::post('{id}/edit', 'NegoController@postForm');

			Route::post('{id}/delete', array(
				'as' => 'nego.delete',
				'uses' => 'NegoController@deleteNego'
				))->where('id', '[0-9]+');
		});		


	}); // Auth

}); // Admin



// API

Route::group(array('prefix' => 'api'), function() {

	//Users
	Route::group(array('prefix' => 'users'), function(){
		Route::post('login', 'UserController@apiLogin');

		Route::post('register', 'UserController@apiRegister');
		Route::post('reset', 'UserController@apiReset');

		Route::group(array('before' => 'auth.api'), function() {
			Route::get('me', 'UserController@apiGetMe');
			Route::post('me', 'UserController@apiPostMe');
			Route::post('logout', 'UserController@apiLogout');	

			Route::get('{id}/me', 'UserController@apiGetUser');

			Route::post('{id}/follow', 'FollowerController@apiPostFollow');
			Route::post('{id}/unfollow', 'FollowerController@apiPostUnfollow');

			Route::post('{id}/review', 'UserController@apiPostReview');
		});	
	});

	Route::group(array('prefix' => 'agencies'), function() {
		Route::get('/', 'AgencyController@apiGetAgencies');	
		Route::get('{id}/', 'AgencyController@apiGetAgency');			
	});

	//Followers
	Route::group(array('prefix' => 'followers'), function() {

		Route::group(array('before' => 'auth.api'), function() {
			Route::get('me', 'FollowerController@apiGetMyFollowers');	
			Route::get('{role}/me', 'FollowerController@apiGetMyFollowersByRole');
			Route::post('{id}/accept', 'FollowerController@apiAcceptFollower');
			Route::post('{id}/decline', 'FollowerController@apiDeclineFollower');
			Route::post('{id}/archive', 'FollowerController@apiPostArchive');
		});

	});

	Route::group(array('prefix' => 'abuses'), function() {
		Route::get('/', 'AbuseController@apiGetAbuses')
		;	

		Route::get('{id}/', 'AbuseController@apiGetAbuse')
		;			
	});

	Route::group(array('prefix' => 'notifications'), function() {
		Route::post('/on', 'NotificationController@apiPostOn')
		;	
		Route::any('/ios', 'NotificationController@apiPostApple')
		;		
		Route::any('/android', 'NotificationController@apiPostGoogle')
		;
		Route::any('/send', 'NotificationController@apiAnySend')
		;
		Route::get('/me', 'NotificationController@apiGetMyNotifications')
		;
		Route::get('/me/unread', 'NotificationController@apiGetMyUnreadNotifications')
		;
	});	

	Route::group(array('prefix' => 'negos'), function() {

		Route::group(array('before' => 'auth.api'), function() {
			Route::get('me', 'NegoController@apiGetMyNegos');		
            Route::get('{id}', 'NegoController@apiGetNegoDetails')
                ->where('id', '[0-9]+');
            Route::post('{id}/archive', 'NegoController@apiPostArchive');
		});

	});

	//Banners
	Route::get('banners/type/{id}', 'BannerController@apiGetBannerByType')
	->where('id', '[0-9]+')
	;

	//Properties
	Route::group(array('prefix' => 'properties'), function(){
		Route::get('', 'PropertyController@apiGetProperties');

		Route::group(array('before' => 'auth.api'), function() { 
			Route::get('{id}', 'PropertyController@apiGetProperty')
			->where('id', '[0-9]+')
			;
			Route::get('{id}/country', 'ListController@apiGetPropertiesByCountry')
			->where('id', '[0-9]+')
			;
            // the diff between 'apiGetProperty' & 'apiGetPropertyDetails' is 
            // that 'apiGetPropertyDetails' will return more info like 
            // renovation ID, etc
			Route::get('{id}/details', 'PropertyController@apiGetPropertyDetails')
			->where('id', '[0-9]+')
			;
			Route::post('add', 'PropertyController@apiAddProperty')
			->where('id', '[0-9]+')
			;	
			Route::post('{id}/update', 'PropertyController@apiEditProperty')
			->where('id', '[0-9]+')
			;	
			Route::post('{id}/delete', 'PropertyController@apiDeleteProperty');

			Route::get('{id}/nego', 'NegoController@apiGetNego');
			Route::post('{id}/offer', 'NegoController@apiPostOffer');					
			Route::post('{id}/accept', 'NegoController@apiPostAccept');								
			Route::post('{id}/decline', 'NegoController@apiPostDecline');

			Route::post('{id}/abuse', 'AbuseController@apiPostAbuse')
			->where('id', '[0-9]+')
			;	

			Route::post('{id}/toggle', 'PropertyController@apiPostToggle')
			->where('id', '[0-9]+')
			;

			Route::post('{id}/broadcast', 'PropertyController@apiSendBroadcast')
			->where('id', '[0-9]+')
			;															
		});
	});

	Route::group(array('prefix' => 'trans'), function() {
		Route::get('me', 'NegoController@apiGetMyTransactions');	
    });
    Route::group(array('prefix' => 'mobileSubscriptions'), function() {

		Route::post('/update/subscribe', 'PaymentController@apiUpdateAndroidPurchase');	
                Route::post('/update/cancel', 'PaymentController@apiCancelAndroidPurchase');	
                Route::get('/getStatus/platform/{platform}', 'PaymentController@apiCheckAndroidPurchase');
                Route::post('/getPaymentDetails', 'PaymentController@apiCheckPurchaseDetails');

                

    });
    

    Route::group(array('before' => 'auth.api'), function() {
        Route::group(array('prefix' => 'batches'), function() {
            Route::get('me', 'PropertyController@apiGetMyBatches');	
            Route::get('{id}', 'PropertyController@apiGetMyBatch');	
        });
    });	

	// //States
	// Route::group(array('prefix' => 'states'), function(){

	// 	Route::get('', 'StateController@apiGetStates')
	// 	;

	// 	Route::get('{id}/areas', 'StateController@apiGetAreasByState')
	// 	->where('id', '[0-9]+')
	// 	;
	// });

	// //Get all Areas
	// Route::get('areas', 'AreaController@apiGetAreas');

	// //Facilities
	// Route::get('facilities', 'FacilityController@apiGetFacilities')
	// ;

	Route::group(array('prefix' => 'countries'), function(){
		Route::get('', 'ListController@apiGetCountries')
		;
		
		Route::get('{id}/states', 'ListController@apiGetStatesByCountry')
		->where('id', '[0-9]+')
		;
	});
	Route::group(array('prefix' => 'states'), function(){
		Route::get('', 'ListController@apiGetStates')
		;

		Route::get('{id}/areas', 'ListController@apiGetAreasByState')
		->where('id', '[0-9]+')
		;
	});
	Route::get('areas', 'ListController@apiGetAreas')
	;	
	Route::get('tenures', 'ListController@apiGetTenures')
	;
	Route::get('categories', 'ListController@apiGetCategories')
	;	
	Route::get('types', 'ListController@apiGetTypes')
	;
	Route::get('facilities', 'ListController@apiGetFacilities')
	;	
	Route::get('amenities', 'ListController@apiGetAmenities')
	;		
	Route::get('directions', 'ListController@apiGetDirections')
	;		
	Route::get('renovations', 'ListController@apiGetRenovations')
	;	
	Route::get('packages', 'ListController@apiGetPackagesByPlatform')
	;
        Route::get('packagesByPlatform/{platform}', 'ListController@apiGetPackagesByPlatform')
	;
	Route::get('getAppVersion', 'BaseController@apiGetAppVersion')
	;
});

