(function(window, $) {
	$(function() {

		if(jQuery().combodate) {
		    $('#dob').combodate({
				value: new Date(),
				minYear: 1900,
				maxYear: moment().format('YYYY')
				//customClass: 'custom-combodate' 
	    	});  
		}

		if(jQuery().daterangepicker) {
                  $('#daterange').daterangepicker({
                  	format: 'DD/MM/YYYY'
                  }, function(start, end, label) {

                  });

		}

		$('input[type="file"]').change(function() {
	        if (this.files && this.files[0]) {
	            var reader = new FileReader();

	            reader.onload = function (e) {
            		$('#img-avatar')
            			.css('background-image', 'url(' + e.target.result + ')');
	            }
	            reader.readAsDataURL(this.files[0]);
	        }			
		});

		$('.form-upload .btn-danger').click(function(e){
			e.preventDefault();
			var $frame = $(this).parent().parent().find('.img-thumbnail');
			$frame.css('background-image', '');
			$frame.hide();
			$(this).parent().find('input[type="file"]').val('');
			$(this).hide();
		});

		$('.form-upload input[type="file"]').change(function() {
			var $frame = $(this).parent().parent().find('.img-thumbnail');
			readURL(this, $frame);
		});

		// helper
		function readURL(input, frame) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					frame.css('background-image', 'url(' + e.target.result + ')');
					frame.css('width', '100px');
					frame.css('height', '100px');
					frame.css('background-size', 'cover');
					frame.css('display', 'inline-block');
					frame.show();
					frame.parent().find('.btn-danger').show();
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

	});
} (window, jQuery));
