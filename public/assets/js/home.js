(function(window, $) {
        $(function() {
                /*
                $('#search-bar .dropdown').select2({
                        minimumResultsForSearch: -1
                });*/

                //Get areas for each state, Form filling
                $( "#keyword" ).addClass("form-control textfield");
                
                $( "#keyword" ).autocomplete({
                        source: "autocomplete",
                        minLength: 3,
                        select: function(event, ui) {
                                $('#keyword').val(ui.item.value);
                        }
		});
        
		window.addEventListener("scroll", function() {
			var img_path = $('.navbar-brand').find('img').attr('src');
			var arrImg = '';
			if(img_path != ''){
				arrImg = img_path.split('/');
			}
			var currUrl = window.location.href;
			var n = 0;
			n = currUrl.indexOf('/properties/');
			
			if (window.scrollY > 1) {
				$('.navbar').css('background-color', '#fff');
				$('.navbar-brand').find('img').attr('src', arrImg[0] + '//' + arrImg[2] + '/assets/img/header_logo_green.png');
				$('.navbar-auth').css('color', '#575a5c');
			}
			else {
				if(n <= 0){
					$('.navbar').css('background-color', 'transparent');
					$('.navbar-brand').find('img').attr('src', arrImg[0] + '//' + arrImg[2] + '/assets/img/header_logo.png');
					$('.navbar-auth').css('color', '#FFFFFF');
				}
			}
		},false);

	});
} (window, jQuery));