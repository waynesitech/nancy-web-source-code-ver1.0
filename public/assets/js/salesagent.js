(function(window, $){
	$(function(){

		$('.form-upload input[type="file"]').change(function() {
			var $frame = $(this).parent().parent().find('.img-thumbnail');
			readURL(this, $frame);
		});

		// helper
		function readURL(input, frame) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					frame.css('background-image', 'url(' + e.target.result + ')');
					frame.css('width', '100px');
					frame.css('height', '100px');
					frame.css('background-size', 'cover');
					frame.css('display', 'inline-block');
					frame.show();
					frame.parent().find('.btn-danger').show();
				}
				reader.readAsDataURL(input.files[0]);
			}
		}



	});



}(window, jQuery));
