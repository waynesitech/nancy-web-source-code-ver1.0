(function(window, $) {
    $(function() {
        $('#timeline_container').on('change', 'input[type="file"]#bannerUpload',function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-banner')
                        .attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }			
        });

        $('#timeline_container').on('change', 'input[type="file"]#t_imageUpload', function(e) {
            o = $(this).parent().parent();
            a = o.find('#img-timeline');
            btn = o.find('button[name="img_del"]');
            
            var files = e.target.files; 
            f= files[0];
            if (f==undefined) {
               a.css('background-image', 'none');
               btn.css('display', 'none');
            }
            else if (this.files && this.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    a.css('background-image', 'url(' + e.target.result + ')');
                    btn.css('display', 'inline-block');
                }
                reader.readAsDataURL(this.files[0]);
            }			
        });
        
            
        $(document).on('change', 'input[type="file"]#t_eimageUpload', function(e) {
            o = $(this).parent().parent();
            a = o.find('#img-etimeline');
            b = o.find('.sel_oimg');
            c = o.find('.ex_img');
            d = o.find('.ex_oimg');
            btn = o.find('button[name="img_del"]');
            
            var files = e.target.files; 
            f= files[0];
            if (f==undefined) {
                a.css('background-image', 'url(' + b.val() + ')');
                c.val(d.val());
                
                if(b.val() == '')
                {
                    btn.css('display', 'none');
                }
                else
                {
                    btn.css('display', 'inline-block');
                }
            }
            else if (this.files && this.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    a.css('background-image', 'url(' + e.target.result + ')');
                    btn.css('display', 'inline-block');
                    c.val('');
                }
                reader.readAsDataURL(this.files[0]);
            }			
        });
    
        $('#timeline_container').on('click', ".btn_comment", function(){
        a = $(this).parent().parent().parent().parent().parent().find(".collapse.cmmt");
                
        if(a.collapse('show'))
        {
            a.collapse('hide');
        }
        
        else
        {
            a.collapse('show')
        }
    });
        
        $(document).on('click', "button#img_del", function(){
            o = $(this).parent();
            a = o.find('div.img-responsive');
            b = o.find('input[type="file"]');
            c = o.find('input[type="hidden"].ex_img');
            d = o.find('input[type="hidden"].ex_oimg');
            e = o.find('input[type="hidden"].sel_oimg');
            
            a.css('background-image', 'none');
            b.val('');
            c.val('');
            d.val('');
            e.val('');
            
            $(this).css('display', 'none');
        });
        
        //--select all--
        $(document).on('click', 'input#btn_selall', function(){
            if($(this).prop('checked'))
            {
                $("input[type=checkbox][name='user_ids[]']").each(function(){
                    $(this).prop('checked', true);
                });
            }
            else
            {
                $("input[type=checkbox][name='user_ids[]']").each(function(){
                    $(this).prop('checked', false);
                });
            }
        });
        
        $(document).on('click', 'input#btn_sendtoall', function(){
            if($(this).prop('checked'))
            {
                $("input[type=checkbox][name='user_ids[]']").each(function(){
                    $(this).attr('disabled', true);
                });
            }
            else
            {
                $("input[type=checkbox][name='user_ids[]']").each(function(){
                    $(this).attr('disabled', false);
                });
            }
        });
        
        $(document).on('click', "input[type=checkbox][name='user_ids[]']", function(){
            $('input#btn_selall').prop('checked', false);

            $("input[type=checkbox][name='user_ids[]']").each(function(){
                if(!($(this).prop('checked')))
                {
                    $("input[type=checkbox]#btn_selall").prop('checked', false);
                    return false;
                }
                else
                {
                    $("input[type=checkbox]#btn_selall").prop('checked', true);
                }
            });
        });
        //--end of select all--
        
        //--post new timeline
        $('#timeline_container').on('click', ".btn_expand", function(){
            a = $(this).parent().find("#post_field.collapse");

            if(a.collapse('show'))
            {
                a.collapse('hide');
            }

            else
            {
                a.collapse('show')
            }
        });
    
        $('#timeline_container').on('click', "button#post_type", function(){
            a = $(this).parent().find("#post_selection.collapse");

            if(a.collapse('show'))
            {
                a.collapse('hide');
            }

            else
            {
                a.collapse('show')
            }
        });
        //--end of new timeline
        
        //--timeline menu
        $('#timeline_container').on('click', '.btn_ownermenu',function(){
            a = $(this).parent().find(".collapse_menu");

            if(a.collapse('show'))
            {
                a.collapse('hide');
            }

            else
            {
                a.collapse('show')
            }
        });
        //--end of timeline menu
        
        //getfollower
        var skip = 0;
        var take = 5; //change this for amount of records wanted
        
        getfollower();
        
        $(document).on('click', 'button#btn_morefollower', function(){
            getfollower();
        });
        function getfollower(){
            
            $.ajax({
                    url: '/timeline/ajax/followerlists',
                    type: 'get',
                    data: {'skip':skip, 'take': take + 1},
                    success: function(data) {
                            var record = data.result;
                            $('div.follower_btn').remove();
                            
                            $("input[type=checkbox][name='btn_sendtoall']").each(function(){
                                if($(this).prop('checked'))
                                {
                                    chk_all = true;
                                }
                                else{
                                    chk_all = false;
                                }
                            });
                            if(record.length > 0){
                                for(i = 0; i < record.length; i++){
                                    var user = record[i]['user'];
                                    str = '';
                                    if(i == take)
                                    {
                                        str += '<div class="follower_btn">';
                                        str += ' <button id="btn_morefollower" type="button">Show More...</button>';
                                        str += '</div>';
                                    }
                                    else{
                                        str += '<div class="follower_info">';
                                        str += '<div class="img-profile">';
                                        str += '<img src="'+ user['image'] +'" alt="">';
                                        str += '</div>';
                                        str += '<div class=pull-right>';
                                        
                                        if(chk_all){
                                            str += '<input name=user_ids[] type=checkbox value="'+ user['id'] +'" disabled>';
                                        }
                                        else{
                                            str += '<input name=user_ids[] type=checkbox value="'+ user['id'] +'">';
                                        }
                                        
                                        str += '</div>';
                                        str += '<div>';
                                        str += '<div class="pull-left">';
                                        str += user['name'];
                                        str += '</div>';
                                        str += '</div>';
                                        str += '<div class="user_role">';
                                        str += user['role'];
                                        str += '</div>';
                                        str += '</div>';
                                    }

                                    $('div.follower_list').append(str);
                                }
                            skip = skip + take;
                        }
                    }
            });
            
        }
    });
} (window, jQuery));
