(function(window, $){
	$(function(){

		if(jQuery().raty) {
			$('#ratings').raty({
				readOnly: true,
				starOff: '/assets/img/star_off.png',
				starOn: '/assets/img/star_on.png',			
				score: function() {
					return $(this).attr('data-score');
				}
			});
                        
                        $('.ratings').raty({
				readOnly: true,
				starOff: '/assets/img/star_off.png',
				starOn: '/assets/img/star_on.png',			
				score: function() {
					return $(this).attr('data-score');
				}
			});
		}

		if(jQuery().cycle) {
			var slideshows = $('.cycle-slideshow').on('cycle-next cycle-prev', function(e, opts) {
				slideshows.not(this).cycle('goto', opts.currSlide);
			});

			$('#cycle li').click(function() {
				var index = $('#cycle').data('cycle.API').getSlideIndex(this);
			    // slideshows.cycle('goto', index);

			    // var url = $('li', slideshows).eq(index).children('a').css('background-image').replace('url(','').replace(')','');
			    $('#stage').css('background-image', $('li', slideshows).eq(index+1).children('a').css('background-image'));
			});		
		}


		$('#type').change(function(){
			if ($(this).val() == 'Residential Land') {
				$('.metric').text('ac');
				// $('#build').removeClass('hide');
				// $('#landsize').addClass('hide');
			} else {	
				$('.metric').text('f');
				// $('#build').addClass('hide');
				// $('#landsize').removeClass('hide');
			}
			
		});

		$('#follow').on('click', function(e){
			var id = $(this).data('id');
                        var propId = $(this).data('prop-id');
                        
                        if (typeof propId == 'undefined') propId = 0;

			$.ajax({
				url: '/follows/' + id,
                                data: {prop_id: propId},
				type: 'post',
				success: function(response) {
					$('#follow').text('Pending');
					$('#follow').attr('disabled', true);
				}

			});
			

			return false;

		});

		$("#pending").on("click", function(event){
			event.preventDefault();
		});
                
                $('#img-thumb-placeholder').on('click', '.btn-danger', function(e) {
			e.preventDefault();
			$wrapper = $(this).parent();
			$wrapper.find('input[name="existing_imgs[]"]').val('');
                        try{
                            $wrapper.find('img').attr('src', $wrapper.find('img').data('placeholder'));
                        }
                        catch(e)
                        {
                            console.log(e);
                        }
                        //$wrapper.hide(300, function() {
                        //  $(this).remove();
                        //});
		});
                
                $('#img-thumb-placeholder.existingimg_field').on('click', 'button#dlt', function(){
                        $(this).parent().remove();
                });
                
                $('#img-thumb-placeholder.newimg_field').on('click', 'button#dlt', function(){
                        $(this).parent().remove();
                        var upImg = $('div#img-thumb-placeholder div.img-thumb.form-upload').length;
                        var actImg = $('input[type="hidden"][name="newImg[]"]').length;
                        
                        if(upImg == 0 || actImg == 0)
                        {
                            $('.form-upload input[type="file"]').val('');
                        }
                });

		$('.form-upload input[type="file"]').change(function(e) {
			var $frame = $(this).parent().parent();
                        
                        if($(this).attr('id') == 'multipicture')
                        {
                                if(!e.target.files) return;
                                
                                var files = e.target.files;
                                str = '';
                                var frame = $('div#img-thumb-placeholder.newimg_field');
                                
                                if (files.length == 0) {
                                        frame.html('');
                                        $(this).val('');
                                }
                                else
                                {       
                                        $.each(e.target.files, function(index, file) {
                                                var reader = new FileReader();
                                                
                                                reader.onload = function (e) {
                                                        frame.html('');
                                                        var text = e.target.result;
                                                        str += '<div class="img-thumb form-upload">';
                                                        str += '<img src="' + text +'">';
                                                        str += '<button id="dlt" class="btn btn-sm btn-danger" type="button">Remove</button>';
                                                        str += '<input type="hidden" id="newImg" name = "newImg[]" value="' + file.name + '">';
                                                        str += '</div>';
                                                        
                                                        frame.append(str);
                                                };
                                                
                                                reader.readAsDataURL(file);
                                         });
                                         
                                }
                                
                                
                        }
                        else
                        {
                            if(this.files && this.files[0])
                            {
                                readURL(this, $frame);
                            }
                        }
                        
		});

		// helper
		function readURL(input, frame) {
                        //for (i = 0; i < input.files.length; i++) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                                //var $wrapper = $('<div>').addClass('img-thumb');
                                //var $img = $('<img>').attr('src', e.target.result);
                                // var $btn = $('<button>').addClass('btn btn-sm btn-danger').attr('type', 'button').text('Delete');
                                //$wrapper.append($img).append($btn);
                                //$wrapper.find('input[type="file"]').css('display','none');
                                //frame.append($wrapper);
                                frame.find('img').attr('src', e.target.result);
                                frame.find('input[name="existing_imgs[]"]').val('');
                        }
                        reader.readAsDataURL(input.files[0]);
      	 // }
		}
                
                /*function chkstate(st){
                    if(st == '')
                    {
                        $('#area').attr('disabled', true);
                    }else{
                        $('#area').attr('disabled', false);
                    }
                }*/
        });
}(window, jQuery));
