(function(window, $){
	$(function(){

		if(jQuery().raty) {
			$('#rate').raty({
				readOnly: false,
				starOff: '/assets/img/star_off.png',
				starOn: '/assets/img/star_on.png',			
				score: function() {
					return $(this).data('rate');
				}
			});
		}

    $('.btn-rate').click(function() {
      $('#frm-rate').attr('action', '/users/' + $(this).data('user-id') + '/review');
      $('#modal-rate').modal('show');
      return false;
    });

    $('.btn-abuse').click(function() {
      $('#frm-abuse').attr('action', '/properties/' + $(this).data('prop-id') + '/abuse');
      $('#frm-prop').text($(this).data('prop-name'));
      $('#frm-agent').text($(this).data('agent-name'));
      $('#modal-abuse').modal('show');
      return false;
    });
  });
}(window, jQuery));
