(function(window, $) {
	$(function() {

		if(jQuery().combodate) {
		    $('#dob').combodate({
				value: new Date(),
				minYear: 1900,
				maxYear: moment().format('YYYY')
				//customClass: 'custom-combodate' 
	    	});  
		}

		if(jQuery().daterangepicker) {
                  $('#daterange').daterangepicker({
                  	format: 'DD/MM/YYYY'
                  }, function(start, end, label) {

                  });

		}
            
	});
} (window, jQuery));
