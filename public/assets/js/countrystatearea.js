(function(window, $){
	$(function(){
                
                chkstate($('#state').val());
                $('#countries').on('change', function (e) {
                        getstate($(this).val());
                });

                $('#state').on('change', function(){
                        getarea($(this).val());
                });
                
                function chkstate(st)
                {
                        if(st == '')
                        {
                                $('#area').attr('disabled', true);
                                $('#s2id_area').attr('disabled', true);
                        }else{
                                $('#area').attr('disabled', false);
                                $('#s2id_area').attr('disabled', false);
                        }
                }
                
                function getstate(id)
                {       
                        $('#area').attr('disabled', true);
                        $('#state').empty();
                        $('#state').append('<option value="" selected="selected">All States</option>');
                        $('#state').attr('disabled', true);
                        $('#area').empty();
                        $('#area').append($('<option value="">All Areas</option>'));
                                        
                        $.ajax({
                                type: "POST",
                                url : '/properties/getcountry',
                                dataType: 'json',
                                data: {id: id},
                                success:function(data){
                                        
                                        
                                        var result = data.result;
                                        str = '';
                                        for(i = 0; i < result.length; i++)
                                        {
                                                str += '<option value="';
                                                str += result[i].id;
                                                str += '">' + result[i].name + '</option>';
                                        }
                                        $('#state').append(str);
                                        $('#state').attr('disabled', false);
                                        try{
                                                $('#state').select2({minimumResultsForSearch: -1});
                                        }catch(TypeError)
                                        {
                                            $('#state').val($('#state').val());
                                        }
                                        

                                },
                                error: function(xhr, stt, err){
                                
                                }
                        });
                };

                function getarea(id)
                {       
                        $('#area').empty();
                        $('#area').append($('<option value="">All Areas</option>'));
                        $('#area').attr('disabled', true);
                        
                        $.ajax({
                                url: '/properties/ajax/areas',
                                type: 'get',
                                data: { 'state': id},
                                success: function(response) {
                                        for (var i = 0; i < response.area.length; i++) {
                                                $('#area').append($("<option>").val(response.area[i].id).text(response.area[i].name));
                                        }
                                        
                                        chkstate(id);
                                        
                                        try{
                                                $('#area').select2({minimumResultsForSearch: -1});
                                        }
                                        catch(TypeError)
                                        {
                                            $('#area').val($('#area').val());
                                        }
                                        
                                }
                        });
                };
                
        });
}(window, jQuery));

