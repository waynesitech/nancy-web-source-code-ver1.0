(function(window, $){
	$(function(){

		if(jQuery().raty) {
			$('#account-ratings').raty({
				readOnly: true,
				starOff: '/assets/img/star_off.png',
				starOn: '/assets/img/star_on.png',			
				score: function() {
					return $(this).data('score');
				}
			});
		}
  });
}(window, jQuery));
